package aleksy.abspec.prebiotic.framework.neural.teaching.common.exception;

/**
 * Kohonen's learning configurations exception
 */
public class KohonenLearningConfigurationsException extends KohonenLearningException {
   public KohonenLearningConfigurationsException() {
      super("\nKohonen's map learning configurations error.");
   }

   public KohonenLearningConfigurationsException(String message) {
      super("\nKohonen's map configurations error. " + message);
   }
}
