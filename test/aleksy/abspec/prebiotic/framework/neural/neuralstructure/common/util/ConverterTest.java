package aleksy.abspec.prebiotic.framework.neural.neuralstructure.common.util;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.Converter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConverterTest {
   private PrebioticApi prebiotic;

   @Before
   public void init() {
      prebiotic = Prebiotic.newInstance();
   }

   @Test
   public void shouldConvertPrebioticNetworkToKohonenNetwork() throws InvalidStructureException, EmptyFunctionDefinitionException {
      // given
      NeuralNetwork toConvert = prepareTestKohonenNetwork();

      // when
      KohonenNeuralNetwork converted = Converter.convertToKohonenNeuralNetwork(toConvert);

      // then
      Assert.assertEquals(toConvert.getAxonFunction().getAxonFunctionRecipe(),
         converted.getAxonFunction().getAxonFunctionRecipe());
      Assert.assertEquals(toConvert.getCoreFunction().getCoreFunctionRecipe(),
         converted.getCoreFunction().getCoreFunctionRecipe());
      Assert.assertEquals(toConvert.getWeightFunctionRecipe(), converted.getWeightFunctionRecipe());
      assertEquals(toConvert.getLayers().size(), converted.getLayers().size());
      assertEquals(toConvert.size(), converted.size());
      Assert.assertEquals(toConvert.getType(), converted.getType());
      assertEquals(toConvert.getDescription(), converted.getDescription());
      Assert.assertEquals(toConvert.getDataNormalization(), converted.getDataNormalization());
      assertEquals(toConvert.isBiasPresence(), converted.isBiasPresence());
   }

   @Test(expected = InvalidStructureException.class)
   public void shouldNotConvertToKohonen() throws InvalidStructureException, EmptyFunctionDefinitionException {
      // given
      NeuralNetwork toConvert = prepareTestPrebioticNetwork();

      // when
      Converter.convertToKohonenNeuralNetwork(toConvert);

      // then exception
   }

   private NeuralNetwork prepareTestKohonenNetwork() throws InvalidStructureException, EmptyFunctionDefinitionException {
      return prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultKohonenConfigurations()
            .create()
      ).buildKohonenMap(3, 3, 1)
         .create();
   }

   private NeuralNetwork prepareTestPrebioticNetwork() throws InvalidStructureException, EmptyFunctionDefinitionException {
      return prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .create()
      ).buildSingleLayerNetwork(3, 1)
         .create();
   }
}
