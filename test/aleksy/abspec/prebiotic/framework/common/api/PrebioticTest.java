package aleksy.abspec.prebiotic.framework.common.api;

import aleksy.abspec.prebiotic.framework.common.constants.BasicConstants;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.evolution.common.api.EvolutionApi;
import aleksy.abspec.prebiotic.framework.evolution.common.impl.Evolution;
import aleksy.abspec.prebiotic.framework.neural.common.api.NeuralApi;
import aleksy.abspec.prebiotic.framework.neural.common.impl.Neural;
import aleksy.abspec.prebiotic.framework.vision.common.api.VisionApi;
import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.api.PrebioticSystemApi;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.impl.PrebioticSystem;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class PrebioticTest {
   private PrebioticApi uut;

   @Before
   public void init() {
      uut = Prebiotic.newInstance();
   }

   @Test
   public void shouldReturnTheCorrectBasicDataAboutFramework() {
      // given
      String expectedVersion = BasicConstants.VERSION;
      String expectedAuthor = BasicConstants.AUTHOR;
      String expectedName = BasicConstants.NAME;

      // when
      String actualVersion = uut.getVersion();
      String actualAuthor = uut.getAuthor();
      String actualName = uut.getFrameworkName();

      // then
      assertEquals(expectedAuthor, actualAuthor);
      assertEquals(expectedName, actualName);
      assertEquals(expectedVersion, actualVersion);
   }

   @Test
   public void shouldEnableMessageLogging() {
      // given
      final boolean expected = true;

      // when
      uut.enableEventLogging();
      boolean actual = uut.isEventLoggingEnabled();

      // then
      assertEquals(expected, actual);
   }

   @Test
   public void shouldDisableMessageLogging() {
      // given
      final boolean expected = false;

      // when
      uut.disableEventLogging();
      boolean actual = uut.isEventLoggingEnabled();

      // then
      assertEquals(expected, actual);
   }

   @Test
   public void shouldReturnCorrectModules() {
      // when
      NeuralApi neuralApi = uut.neuralApi();
      EvolutionApi evolutionApi = uut.evolutionApi();
      VisionApi visionApi = uut.visionApi();
      PrebioticSystemApi prebioticSystemApi = uut.prebioticSystemApi();

      // then
      assertEquals(Neural.class, neuralApi.getClass());
      assertEquals(Evolution.class, evolutionApi.getClass());
      assertEquals(Vision.class, visionApi.getClass());
      assertEquals(PrebioticSystem.class, prebioticSystemApi.getClass());
   }

   @Test
   public void shouldAddNewStream() throws FileNotFoundException {
      // given
      PrintStream printStream = new PrintStream("test\\resources\\stream\\test_stream.txt");

      // when
      uut.addPrintStream(printStream);

      // then
      assertEquals(uut.getPrintStreams().size(), 2);
      assertEquals(uut.getPrintStreams().get(1), printStream);
   }

   @Test
   public void shouldSetOneStream() throws FileNotFoundException {
      // given
      PrintStream printStream = new PrintStream("test\\resources\\stream\\test_stream.txt");

      // when
      uut.setOnePrintStream(printStream);

      // then
      assertEquals(uut.getPrintStreams().size(), 1);
      assertEquals(uut.getPrintStreams().get(0), printStream);
   }

   @Test
   public void shouldDontRemoveStreamIfIsOnlyOne() throws FileNotFoundException {
      // given
      PrintStream printStream = new PrintStream("test\\resources\\stream\\test_stream.txt");

      // when
      uut.setOnePrintStream(printStream);
      uut.removePrintStream(printStream);

      // then
      assertEquals(uut.getPrintStreams().size(), 1);
      assertEquals(uut.getPrintStreams().get(0), printStream);
   }

   @Test
   public void shouldRemoveStream() throws FileNotFoundException {
      // given
      PrintStream printStream = new PrintStream("test\\resources\\stream\\test_stream.txt");

      // when
      uut.addPrintStream(printStream);
      uut.removePrintStream(printStream);

      // then
      assertEquals(uut.getPrintStreams().size(), 1);
   }
}
