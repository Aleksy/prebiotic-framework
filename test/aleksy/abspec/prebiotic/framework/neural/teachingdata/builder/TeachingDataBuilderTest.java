package aleksy.abspec.prebiotic.framework.neural.teachingdata.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TeachingDataBuilderTest {
   private TeachingDataBuilder uut;

   @Before
   public void init() {
      PrebioticApi prebiotic = Prebiotic.newInstance();
      uut = new TeachingDataBuilder(prebiotic);
   }

   @Test
   public void shouldCreateTeachingDataWithTwoPackages() {
      // given
      List<Double> firstPackageInputs = new ArrayList<>(Arrays.asList(0.1, 0.2, 0.3));
      List<Double> firstPackageOutputs = new ArrayList<>(Collections.singletonList(0.9));
      List<Double> secondPackageInputs = new ArrayList<>(Arrays.asList(0.3, 0.4, 0.5));
      List<Double> secondPackageOutputs = new ArrayList<>(Collections.singletonList(0.3));
      int amountOfPacages = 2;

      // when
      uut.addDataPackage(firstPackageInputs, firstPackageOutputs);
      uut.addDataPackage(secondPackageInputs, secondPackageOutputs);
      TeachingData actualTd = uut.create();

      //then
      assertEquals(amountOfPacages, actualTd.getDataPackages().size());
      assertEquals((Double) 0.1, actualTd.getDataPackages().get(0).ins.get(0));
      assertEquals((Double) 0.2, actualTd.getDataPackages().get(0).ins.get(1));
      assertEquals((Double) 0.3, actualTd.getDataPackages().get(0).ins.get(2));
      assertEquals((Double) 0.3, actualTd.getDataPackages().get(1).ins.get(0));
      assertEquals((Double) 0.4, actualTd.getDataPackages().get(1).ins.get(1));
      assertEquals((Double) 0.5, actualTd.getDataPackages().get(1).ins.get(2));
      assertEquals((Double) 0.9, actualTd.getDataPackages().get(0).expectedOuts.get(0));
      assertEquals((Double) 0.3, actualTd.getDataPackages().get(1).expectedOuts.get(0));
   }

   @Test
   public void shouldCreateTeachingDataWithTwoSelfLearningPackages() {
      // given
      List<Double> firstPackageInputs = new ArrayList<>(Arrays.asList(0.1, 0.2, 0.3));
      List<Double> secondPackageInputs = new ArrayList<>(Arrays.asList(0.3, 0.4, 0.5));
      int amountOfPacages = 2;

      // when
      uut.addSelfLearningPackage(firstPackageInputs);
      uut.addSelfLearningPackage(secondPackageInputs);
      TeachingData actualTd = uut.create();

      //then
      assertEquals(amountOfPacages, actualTd.getDataPackages().size());
      assertEquals((Double) 0.1, actualTd.getDataPackages().get(0).ins.get(0));
      assertEquals((Double) 0.2, actualTd.getDataPackages().get(0).ins.get(1));
      assertEquals((Double) 0.3, actualTd.getDataPackages().get(0).ins.get(2));
      assertEquals((Double) 0.3, actualTd.getDataPackages().get(1).ins.get(0));
      assertEquals((Double) 0.4, actualTd.getDataPackages().get(1).ins.get(1));
      assertEquals((Double) 0.5, actualTd.getDataPackages().get(1).ins.get(2));
   }
}
