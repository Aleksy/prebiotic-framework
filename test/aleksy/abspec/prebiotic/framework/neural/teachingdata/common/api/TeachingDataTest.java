package aleksy.abspec.prebiotic.framework.neural.teachingdata.common.api;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.builder.TeachingDataBuilder;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.impl.TeachingDataManagement;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TeachingDataTest {
   private TeachingDataManagementApi uut;

   @Before
   public void init() {
      uut = new TeachingDataManagement(Prebiotic.newInstance());
   }

   @Test
   public void shouldReturnTeachingDataBuilder() {
      // when
      TeachingDataBuilder teachingDataBuilder = uut.getTeachingDataBuilder();

      // then
      assertEquals(TeachingDataBuilder.class, teachingDataBuilder.getClass());
   }
}
