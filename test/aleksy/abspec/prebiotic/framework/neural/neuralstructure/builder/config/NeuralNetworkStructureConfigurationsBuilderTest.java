package aleksy.abspec.prebiotic.framework.neural.neuralstructure.builder.config;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.*;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NeuralNetworkStructureConfigurationsBuilderTest {
   private NeuralNetworkStructureConfigurationsBuilder uut;

   @Before
   public void init() {
      PrebioticApi facade = Prebiotic.newInstance();
      uut = facade.neuralApi().getStructureConfigurationsBuilder();
   }

   @Test
   public void shouldCreateCustomConfigurations() {
      // given
      StructureConfigurations expectedSc = createTestConfigurations();
      StructureConfigurations actualSc;

      // when
      uut.setWeightFunction(expectedSc.weightFunctionRecipe);
      uut.setDataNormalization(expectedSc.dataNormalization);
      uut.setAxonFunction(expectedSc.axonFunctionRecipe);
      uut.setNeuralNetworkType(expectedSc.neuralNetworkType);
      uut.setBiasPresence(expectedSc.biasPresence);
      uut.setCoreFunction(expectedSc.coreFunctionRecipe);
      actualSc = uut.create();

      // then
      Assert.assertEquals(expectedSc.weightFunctionRecipe, actualSc.weightFunctionRecipe);
      Assert.assertEquals(expectedSc.axonFunctionRecipe, actualSc.axonFunctionRecipe);
      Assert.assertEquals(expectedSc.coreFunctionRecipe, actualSc.coreFunctionRecipe);
      assertEquals(expectedSc.biasPresence, actualSc.biasPresence);
      Assert.assertEquals(expectedSc.dataNormalization, actualSc.dataNormalization);
      Assert.assertEquals(expectedSc.neuralNetworkType, actualSc.neuralNetworkType);
   }

   @Test
   public void shouldCreateDefaultConfigurations() {
      // given
      StructureConfigurations defaultSc = new StructureConfigurations();
      defaultSc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      defaultSc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      defaultSc.axonFunctionRecipe = AxonFunctionRecipe.SIGMOID;
      defaultSc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
      defaultSc.biasPresence = true;
      defaultSc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      StructureConfigurations actualSc;


      // when
      actualSc = uut.defaultFeedforwardConfigurations().create();

      // then
      Assert.assertEquals(defaultSc.weightFunctionRecipe, actualSc.weightFunctionRecipe);
      Assert.assertEquals(defaultSc.axonFunctionRecipe, actualSc.axonFunctionRecipe);
      Assert.assertEquals(defaultSc.coreFunctionRecipe, actualSc.coreFunctionRecipe);
      assertEquals(defaultSc.biasPresence, actualSc.biasPresence);
      Assert.assertEquals(defaultSc.dataNormalization, actualSc.dataNormalization);
      Assert.assertEquals(defaultSc.neuralNetworkType, actualSc.neuralNetworkType);
   }

   @Test
   public void shouldCreateDefaultConfigurationsForKohonen() {
      // given
      StructureConfigurations defaultSc = new StructureConfigurations();
      defaultSc.weightFunctionRecipe = WeightFunctionRecipe.SQUARE_DIFFERENCE;
      defaultSc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      defaultSc.axonFunctionRecipe = AxonFunctionRecipe.HYPERBOLIC;
      defaultSc.neuralNetworkType = NeuralNetworkType.KOHONEN;
      defaultSc.biasPresence = false;
      defaultSc.dataNormalization = DataNormalizationType.NO_NORMALIZATION;
      StructureConfigurations actualSc;


      // when
      actualSc = uut.defaultKohonenConfigurations().create();

      // then
      Assert.assertEquals(defaultSc.weightFunctionRecipe, actualSc.weightFunctionRecipe);
      Assert.assertEquals(defaultSc.axonFunctionRecipe, actualSc.axonFunctionRecipe);
      Assert.assertEquals(defaultSc.coreFunctionRecipe, actualSc.coreFunctionRecipe);
      assertEquals(defaultSc.biasPresence, actualSc.biasPresence);
      Assert.assertEquals(defaultSc.dataNormalization, actualSc.dataNormalization);
      Assert.assertEquals(defaultSc.neuralNetworkType, actualSc.neuralNetworkType);
   }

   private StructureConfigurations createTestConfigurations() {
      StructureConfigurations sc = new StructureConfigurations();
      sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
      sc.biasPresence = true;
      sc.axonFunctionRecipe = AxonFunctionRecipe.SIGMOID;
      sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      return sc;
   }
}
