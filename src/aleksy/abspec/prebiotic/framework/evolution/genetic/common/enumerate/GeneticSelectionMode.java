package aleksy.abspec.prebiotic.framework.evolution.genetic.common.enumerate;

/**
 * Evolution selection mode
 */
public enum GeneticSelectionMode {
   /**
    * Roulette mode. Each individual has a part of round. Better individuals have greater parts,
    * worse - smaller.
    */
   ROULETTE,
   /**
    * Ranking mode. Better individuals have bigger probability to be chosen by algorithm,
    * worse - smaller. The dependence is non-linear.
    */
   RANKING,
   /**
    * Tournament mode. Individuals "fight" with each other and the bests are chosen to
    * populate next generation.
    */
   TOURNAMENT,

   /**
    * Threshold reproduction mode. Individuals above the threshold are used in reproduction,
    * rest is deleted.
    */
   THRESHOLD_REPRODUCTION
}
