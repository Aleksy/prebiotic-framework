package aleksy.abspec.prebiotic.framework.evolution.genetic.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.environment.Environment;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.util.BrainUtil;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

/**
 * Abstract genetic algorithm logic
 */
public class GeneticLogic extends Loggable {

   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param prebiotic api to set
    */
   public GeneticLogic(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   /**
    * Makes one iteration of algorithm
    *
    * @param population     of individuals
    * @param configurations of genetic algorithm
    * @return population after one iteration of algorithm
    */
   public List<Geneticable> start(List<Geneticable> population, GeneticAlgorithmConfigurations configurations) {
      prebiotic.prebioticSystemApi().log("One iteration of genetic algorithm starting.", getSenderName());
      prebiotic.prebioticSystemApi().log("Checking the configurations correctness.", getSenderName());
      LocalTime startTime = LocalTime.now();
      Queue<Geneticable> queue = new PriorityQueue<>(population);
      int populationSize = population.size();
      if (configurations.geneticSelectionMode == GeneticSelectionMode.ROULETTE) {
         return roulette(queue, populationSize, configurations.mutationChance);
      }
      if (configurations.geneticSelectionMode == GeneticSelectionMode.RANKING) {
         return ranking(queue, populationSize, configurations.mutationChance);
      }
      if (configurations.geneticSelectionMode == GeneticSelectionMode.THRESHOLD_REPRODUCTION)
         return threshold(queue, configurations.mutationChance);
      prebiotic.prebioticSystemApi().log("Finishing.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      return null;
   }

   /**
    * Starts genetic algorithm for given population
    *
    * @param population     of individuals
    * @param environment    to rate each individual
    * @param configurations of genetic algorithm
    * @return population after genetic algorithm calculation
    */
   public List<Geneticable> start(List<Geneticable> population, Environment environment,
                                  GeneticAlgorithmConfigurations configurations) {
      prebiotic.prebioticSystemApi().log("Evolution algorithm starting.", getSenderName());
      prebiotic.prebioticSystemApi().log("Checking the configurations correctness.", getSenderName());
      LocalTime startTime = LocalTime.now();
      for (int g = 0; g < configurations.generations; g++) {
         environment.rate(population);
         Queue<Geneticable> queue = new PriorityQueue<>(population);
         int populationSize = population.size();
         if (configurations.geneticSelectionMode == GeneticSelectionMode.ROULETTE) {
            population = roulette(queue, populationSize, configurations.mutationChance);
         }
         if (configurations.geneticSelectionMode == GeneticSelectionMode.RANKING) {
            population = ranking(queue, populationSize, configurations.mutationChance);
         }
         if (configurations.geneticSelectionMode == GeneticSelectionMode.THRESHOLD_REPRODUCTION)
            population = threshold(queue, configurations.mutationChance);

         if (g % configurations.iterationsDisplaying == 0) {
            prebiotic.prebioticSystemApi().log("Generation " + g + "/"
               + configurations.generations + " : best fitness value: " + queue.peek().getFitness(), getSenderName());
         }
      }
      prebiotic.prebioticSystemApi().log("Rating last population.", getSenderName());
      environment.rate(population);
      prebiotic.prebioticSystemApi().log("Finishing.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      return population;
   }

   /**
    * Starts genetic algorithm for given population
    *
    * @param population     of individuals
    * @param environment    to rate each individual
    * @param configurations of genetic algorithm
    * @param templateBrain  needed to set into the geneticable object
    * @return population after genetic algorithm calculation
    */
   public List<Geneticable> start(List<Geneticable> population, Environment environment,
                                  GeneticAlgorithmConfigurations configurations, Brain templateBrain) {
      prebiotic.prebioticSystemApi().log("Evolution algorithm starting.", getSenderName());
      prebiotic.prebioticSystemApi().log("Checking the configurations correctness.", getSenderName());
      LocalTime startTime = LocalTime.now();
      for (int g = 0; g < configurations.generations; g++) {
         environment.rate(population);
         Queue<Geneticable> queue = new PriorityQueue<>(population);
         int populationSize = population.size();
         if (configurations.geneticSelectionMode == GeneticSelectionMode.ROULETTE) {
            population = roulette(queue, populationSize, configurations.mutationChance);
         }
         if (configurations.geneticSelectionMode == GeneticSelectionMode.RANKING) {
            population = ranking(queue, populationSize, configurations.mutationChance);
         }
         if (configurations.geneticSelectionMode == GeneticSelectionMode.THRESHOLD_REPRODUCTION)
            population = threshold(queue, configurations.mutationChance);

         if (g % configurations.iterationsDisplaying == 0) {
            prebiotic.prebioticSystemApi().log("Generation " + g + "/"
               + configurations.generations + " : best fitness value: " + queue.peek().getFitness(), getSenderName());
         }
         population = BrainUtil.setBrainsToPopulation(population, templateBrain);
      }
      prebiotic.prebioticSystemApi().log("Rating last population.", getSenderName());
      environment.rate(population);
      prebiotic.prebioticSystemApi().log("Finishing.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      return population;
   }

   /**
    * Makes one iteration of algorithm
    *
    * @param population     of individuals
    * @param configurations of genetic algorithm
    * @param templateBrain  needed to set into the geneticable object
    * @return population after genetic algorithm calculation
    */
   public List<Geneticable> start(List<Geneticable> population, GeneticAlgorithmConfigurations configurations,
                                  Brain templateBrain) {
      prebiotic.prebioticSystemApi().log("Evolution algorithm starting.", getSenderName());
      prebiotic.prebioticSystemApi().log("Checking the configurations correctness.", getSenderName());
      LocalTime startTime = LocalTime.now();
      Queue<Geneticable> queue = new PriorityQueue<>(population);
      int populationSize = population.size();
      if (configurations.geneticSelectionMode == GeneticSelectionMode.ROULETTE) {
         population = roulette(queue, populationSize, configurations.mutationChance);
      }
      if (configurations.geneticSelectionMode == GeneticSelectionMode.RANKING) {
         population = ranking(queue, populationSize, configurations.mutationChance);
      }
      if (configurations.geneticSelectionMode == GeneticSelectionMode.THRESHOLD_REPRODUCTION)
         population = threshold(queue, configurations.mutationChance);
      population = BrainUtil.setBrainsToPopulation(population, templateBrain);
      prebiotic.prebioticSystemApi().log("Finishing.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      return population;
   }

   private List<Geneticable> threshold(Queue<Geneticable> queue, double mutationChance) {
      Random r = new Random();
      int queueSize = queue.size();
      Queue<Geneticable> theBestOf = new PriorityQueue<>();
      double position = 1.0;
      List<Geneticable> children = new ArrayList<>();
      getTheBestOf(queue, queueSize, theBestOf, position);
      List<Geneticable> theBestOfList = new ArrayList<>(theBestOf);
      for (int i = theBestOf.size(); i < queueSize; i++) {
         int randParent1 = r.nextInt(theBestOf.size());
         int randParent2 = r.nextInt(theBestOf.size());
         while (randParent2 == randParent1) {
            randParent2 = r.nextInt(theBestOf.size());
         }
         Geneticable parent1 = theBestOfList.get(randParent1);
         Geneticable parent2 = theBestOfList.get(randParent2);
         children.add(parent1.crossover(parent2, mutationChance));
      }
      children.addAll(theBestOf);
      return children;
   }

   private void getTheBestOf(Queue<Geneticable> queue, int queueSize, Queue<Geneticable> theBestOf, double position) {
      double THE_BEST_OF_POPULATION = 0.5;
      while (queue.size() > queueSize * THE_BEST_OF_POPULATION) {
         Geneticable ind = queue.poll();
         ind.setFitness(ind.getFitness() / position);
         theBestOf.add(ind);
         position += 1.0;
      }
   }

   private List<Geneticable> ranking(Queue<Geneticable> queue, int populationSize, double mutationChange) {
      int queueSize = queue.size();
      Queue<Geneticable> theBestOf = new PriorityQueue<>();
      double position = 1.0;
      getTheBestOf(queue, queueSize, theBestOf, position);
      return roulette(theBestOf, populationSize, mutationChange);
   }

   private List<Geneticable> roulette(Queue<Geneticable> queue, int populationSize, double mutationChance) {
      double sumOfFitness = 0.0;
      Queue<Geneticable> queueCopy = new PriorityQueue<>(queue);
      for (Geneticable individual : queueCopy) {
         sumOfFitness += individual.getFitness();
      }
      List<Geneticable> seed = new ArrayList<>();
      double startOfLineSegment = 0.0;
      while (!queueCopy.isEmpty()) {
         Geneticable individual = queueCopy.poll();
         individual.setLineSegment(Range.from(startOfLineSegment, startOfLineSegment
            + individual.getFitness() / sumOfFitness));
         seed.add(individual);
         startOfLineSegment = individual.getLineSegment().getTo();
      }
      List<Geneticable> newPopulation = new ArrayList<>();
      Random r = new Random();
      for (int indNumber = 0; indNumber < populationSize; indNumber++) {
         double randOfParent1 = r.nextDouble();
         Geneticable parent1 = find(seed, randOfParent1);
         assert parent1 != null;
         double randOfParent2 = r.nextDouble()
            * ((1 - parent1.getLineSegment().getTo()) + parent1.getLineSegment().getFrom());
         if (randOfParent2 > parent1.getLineSegment().getFrom() && randOfParent2 < parent1.getLineSegment().getTo()) {
            randOfParent2 += (parent1.getLineSegment().getTo() - parent1.getLineSegment().getFrom());
         }
         Geneticable parent2 = find(seed, randOfParent2);
         newPopulation.add(parent1.crossover(parent2, mutationChance));
      }
      return newPopulation;
   }

   private Geneticable find(List<Geneticable> seed, double randOfParent) {
      for (Geneticable individual : seed) {
         if (individual.getLineSegment().getFrom() < randOfParent && individual.getLineSegment().getTo() > randOfParent) {
            return individual;
         }
      }
      return null;
   }

   @Override
   protected String getSenderName() {
      return "GEN-LOGIC";
   }
}
