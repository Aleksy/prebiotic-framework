package aleksy.abspec.prebiotic.framework.neural.teaching.common.exception;

import aleksy.abspec.prebiotic.framework.common.exception.PrebioticException;

/**
 * Hopfield learning exception
 */
public class HopfieldLearningException extends PrebioticException {
   public HopfieldLearningException(String message) {
      super("\nHopfield learning exception: " + message);
   }
}
