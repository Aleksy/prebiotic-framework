package aleksy.abspec.prebiotic.framework.evolution.genetic.common.util;

import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.ChromosomeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Gene;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

import java.util.Random;

/**
 * Crossovers functions util class
 */
public class Crossovers {
   /**
    * Default crossover function
    *
    * @param g1 first parent genotype
    * @param g2 second parent genotype
    * @return genotype of child
    */
   public static Genotype def(Genotype g1, Genotype g2) {
      Random r = new Random();
      GenotypeBuilder builder = new GenotypeBuilder();
      int amountOfChromosomes = g1.getChromosomes().size();
      for (int chr = 0; chr < amountOfChromosomes; chr++) {
         ChromosomeBuilder chrBuilder = builder.addChromosome(g1.getChromosomes().get(chr).getDescription());
         int amountOfGenes = g1.getChromosomes().get(chr).size();
         int cutPoint = r.nextInt(amountOfGenes - 1);
         boolean firstParent = r.nextBoolean();
         for (int gen = 0; gen < amountOfGenes; gen++) {
            Gene gene1;
            Gene gene2;
            if (firstParent) {
               gene1 = g1.getChromosomes().get(chr).getGenes().get(gen);
               gene2 = g2.getChromosomes().get(chr).getGenes().get(gen);
            } else {
               gene1 = g2.getChromosomes().get(chr).getGenes().get(gen);
               gene2 = g1.getChromosomes().get(chr).getGenes().get(gen);
            }
            if (gen < cutPoint) {
               chrBuilder.addGene(gene1.getValue(), gene1.getValueRange(), gene1.getDescription());
            } else {
               chrBuilder.addGene(gene2.getValue(), gene2.getValueRange(), gene2.getDescription());
            }
         }
         chrBuilder.end();
      }
      return builder.create();
   }
}
