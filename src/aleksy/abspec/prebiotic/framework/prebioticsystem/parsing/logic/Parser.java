package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.enumerate.FileFormat;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NetworkWithInputLayerBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.*;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.model.PrebioticModelObject;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.constants.PnnFileFormat;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.exception.WrongPnnFileVersionException;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Prebiotic standard parser for framework
 */
public class Parser extends Loggable {
   private PrebioticApi prebiotic;

   public Parser(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   public void save(PrebioticModelObject network, String filepath, FileFormat format) throws IOException {
      if (format == FileFormat.XML) {
         saveToXml((NeuralNetwork) network, filepath);
      }
      if (format == FileFormat.PNN) {
         saveToPnn((NeuralNetwork) network, filepath);
      }
      if (format == FileFormat.JSON) {
         saveToJson((NeuralNetwork) network, filepath);
      }
   }

   public NeuralNetwork read(String filepath) throws Exception {
      if (filepath.contains(".xml")) {
         return parseFromXml(filepath, null);
      }
      if (filepath.contains(".pnn")) {
         return parseFromPnn(filepath, null);
      }
      if (filepath.contains(".json")) {
         return parseFromJson(filepath, null);
      }
      return null;
   }

   public NeuralNetwork read(String filepath, CustomFunctionsForParser customFunctionsForParser) throws Exception {
      if (filepath.contains(".xml")) {
         return parseFromXml(filepath, customFunctionsForParser);
      }
      if (filepath.contains(".pnn")) {
         return parseFromPnn(filepath, customFunctionsForParser);
      }
      if (filepath.contains(".json")) {
         return parseFromJson(filepath, customFunctionsForParser);
      }
      return null;
   }

   private NeuralNetwork parseFromXml(String filepath, CustomFunctionsForParser customFunctionsForParser) throws Exception {
      if (!filepath.contains(".xml"))
         filepath += ".xml";
      File file = new File(filepath);

      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document document = dBuilder.parse(file);
      document.normalize();

      Node networkNode = document.getElementsByTagName("neuralNetwork").item(0);
      Element networkElement = (Element) networkNode;

      String description = networkElement.getAttribute("description");
      Boolean biasPresence = Boolean.parseBoolean(networkElement.getAttribute("biasPresence"));
      DataNormalizationType dataNormalizationType
         = DataNormalizationType.valueOf(networkElement.getAttribute("dataNormalization"));
      NeuralNetworkType type = NeuralNetworkType.valueOf(networkElement.getAttribute("type"));
      int numberOfInputs = Integer.parseInt(networkElement.getAttribute("numberOfInputs"));
      int numberOfInputNeurons = Integer.parseInt(networkElement.getAttribute("numberOfInputNeurons"));
      int xSize = Integer.parseInt(networkElement.getAttribute("xSize"));
      int ySize = Integer.parseInt(networkElement.getAttribute("ySize"));
      CoreFunctionRecipe coreFunctionRecipe = CoreFunctionRecipe
         .valueOf(networkElement.getAttribute("coreRecipe"));
      AxonFunctionRecipe axonFunctionRecipe = AxonFunctionRecipe
         .valueOf(networkElement.getAttribute("axonRecipe"));
      WeightFunctionRecipe weightFunctionRecipe = WeightFunctionRecipe
         .valueOf(networkElement.getAttribute("weightRecipe"));
      NodeList layerNodeList = networkElement.getElementsByTagName("layer");
      NeuralNetworkStructureConfigurationsBuilder neuralNetworkStructureConfigurationsBuilder
         = prebiotic.neuralApi().getStructureConfigurationsBuilder()
         .setBiasPresence(biasPresence)
         .setDataNormalization(dataNormalizationType)
         .setAxonFunction(axonFunctionRecipe)
         .setCoreFunction(coreFunctionRecipe)
         .setWeightFunction(weightFunctionRecipe)
         .setNeuralNetworkType(type);
      if (axonFunctionRecipe == AxonFunctionRecipe.AXON_CUSTOM)
         neuralNetworkStructureConfigurationsBuilder.defineAxonInstance(customFunctionsForParser.axonFunction);
      if (coreFunctionRecipe == CoreFunctionRecipe.CORE_CUSTOM)
         neuralNetworkStructureConfigurationsBuilder.defineCoreInstance(customFunctionsForParser.coreFunction);
      if (weightFunctionRecipe == WeightFunctionRecipe.WEIGHT_CUSTOM)
         neuralNetworkStructureConfigurationsBuilder.defineWeightInstance(customFunctionsForParser.weightFunction);
      StructureConfigurations config = neuralNetworkStructureConfigurationsBuilder.create();

      if (type == NeuralNetworkType.FEEDFORWARD) {
         NetworkWithInputLayerBuilder builder = prebiotic.neuralApi().getNeuralNetworkBuilder(config)
            .addDefaultInputLayer(numberOfInputNeurons, numberOfInputs);
         for (int layerNumber = 1; layerNumber < layerNodeList.getLength(); layerNumber++) {
            Node layerNode = layerNodeList.item(layerNumber);
            Element layerElement = (Element) layerNode;
            NodeList neuronNodeList = layerElement.getElementsByTagName("neuron");
            builder.addLayer(neuronNodeList.getLength());
         }
         return buildWeights(builder.endBuildingNetwork().create(description), layerNodeList);
      }
      if (type == NeuralNetworkType.HOPFIELD) {
         return buildWeights(prebiotic.neuralApi().getNeuralNetworkBuilder(config)
            .buildHopfieldNetwork(numberOfInputNeurons).create(description), layerNodeList);
      }
      if (type == NeuralNetworkType.KOHONEN) {
         return buildWeights(prebiotic.neuralApi().getNeuralNetworkBuilder(config)
            .buildKohonenMap(xSize, ySize, numberOfInputs).create(description), layerNodeList);
      }
      return null;
   }

   private NeuralNetwork buildWeights(NeuralNetwork neuralNetwork, NodeList layerNodeList) {
      for (int layerNumber = 0; layerNumber < layerNodeList.getLength(); layerNumber++) {
         Node layerNode = layerNodeList.item(layerNumber);
         Element layerElement = (Element) layerNode;
         NodeList neuronNodeList = layerElement.getElementsByTagName("neuron");
         for (int neuronNumber = 0; neuronNumber < neuronNodeList.getLength(); neuronNumber++) {
            Node neuronNode = neuronNodeList.item(neuronNumber);
            Element neuronElement = (Element) neuronNode;
            NodeList weightNodeList = neuronElement.getElementsByTagName("weight");
            for (int weightNumber = 0; weightNumber < weightNodeList.getLength(); weightNumber++) {
               Node weightNode = weightNodeList.item(weightNumber);
               Element weightElement = (Element) weightNode;
               NodeList factorNodeList = weightElement.getElementsByTagName("factor");
               for (int factorNumber = 0; factorNumber < factorNodeList.getLength(); factorNumber++) {
                  Node factorNode = factorNodeList.item(factorNumber);
                  Element factorElement = (Element) factorNode;
                  neuralNetwork.getLayers().get(layerNumber)
                     .getNeurons().get(neuronNumber)
                     .getWeights().get(weightNumber)
                     .getFactors().set(factorNumber, Double.parseDouble(factorElement.getTextContent()));
               }
            }
         }
      }
      return neuralNetwork;
   }

   private void saveToJson(NeuralNetwork network, String filepath) throws IOException {
      if (!filepath.contains(".json"))
         filepath += ".json";
      File file = new File(filepath);
      if (!file.exists()) {
         file.getParentFile().mkdirs();
         file.createNewFile();
      }
      PrintWriter writer = new PrintWriter(file);
      prebiotic.prebioticSystemApi().log("Saving neural network to JSON file", getSenderName());
      writer.println("{");
      writer.println("  \"description\": \"" + network.getDescription() + "\",");
      writer.println("  \"biasPresence\": \"" + network.isBiasPresence() + "\",");
      writer.println("  \"dataNormalization\": \"" + network.getDataNormalization() + "\",");
      writer.println("  \"type\": \"" + network.getType() + "\",");
      writer.println("  \"coreRecipe\": \"" + network.getCoreFunction().getCoreFunctionRecipe() + "\",");
      writer.println("  \"axonRecipe\": \"" + network.getAxonFunction().getAxonFunctionRecipe() + "\",");
      writer.println("  \"weightRecipe\": \"" + network.getWeightFunctionRecipe() + "\",");
      writer.println("  \"numberOfInputs\": \"" + network.sizeOfInputVector() + "\",");
      writer.println("  \"xSize\": \"" + network.getXSize() + "\",");
      writer.println("  \"ySize\": \"" + network.getYSize() + "\",");
      writer.println("  \"numberOfInputNeurons\": \"" + network.getInputLayer().size() + "\",");

      writer.println("  \"layers\": [");
      for (int layerNumber = 0; layerNumber < network.getLayers().size(); layerNumber++) {
         NeuralLayer layer = network.getLayers().get(layerNumber);
         writer.println("    {\n      \"size\": \"" + layer.size() + "\",");
         writer.println("      \"neurons\": [");
         for (int neuronNumber = 0; neuronNumber < layer.getNeurons().size(); neuronNumber++) {
            Neuron neuron = layer.getNeurons().get(neuronNumber);
            writer.println("        {");
            writer.println("          \"weights\": [");
            for (int weightNumber = 0; weightNumber < neuron.getWeights().size(); weightNumber++) {
               WeightFunction weight = neuron.cloneWeights().get(weightNumber);
               writer.println("            {");
               writer.println("              \"factors\": [");
               for (int factorNumber = 0; factorNumber < weight.getFactors().size(); factorNumber++) {
                  Double factor = weight.getFactors().get(factorNumber);
                  writer.print("                \"" + factor.toString() + "\"");
                  if (factorNumber != weight.getFactors().size() - 1) {
                     writer.println(",");
                  } else {
                     writer.println();
                  }
               }
               writer.println("              ]");
               writer.print("            }");
               if (weightNumber != neuron.getWeights().size() - 1) {
                  writer.println(",");
               } else {
                  writer.println();
               }
            }
            writer.println("          ]");
            writer.print("        }");
            if (neuronNumber != layer.getNeurons().size() - 1) {
               writer.println(",");
            } else {
               writer.println();
            }
         }
         writer.println("      ]");
         writer.print("    }");
         if (layerNumber != network.getLayers().size() - 1) {
            writer.println(",");
         } else {
            writer.println();
         }
      }
      writer.println("  ]");
      writer.println("}");
      writer.close();
   }

   private void saveToXml(NeuralNetwork network, String filepath) throws IOException {
      if (!filepath.contains(".xml"))
         filepath += ".xml";
      File file = new File(filepath);
      if (!file.exists()) {
         file.getParentFile().mkdirs();
         file.createNewFile();
      }
      PrintWriter writer = new PrintWriter(file);
      prebiotic.prebioticSystemApi().log("Saving neural network to XML file", getSenderName());
      writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>");
      writer.print("    <neuralNetwork ");
      writer.println("description=\"" + network.getDescription() + "\" ");
      writer.println("                   biasPresence=\"" + network.isBiasPresence() + "\" ");
      writer.println("                   dataNormalization=\"" + network.getDataNormalization() + "\" ");
      writer.println("                   type=\"" + network.getType() + "\" ");
      writer.println("                   coreRecipe=\"" + network.getCoreFunction().getCoreFunctionRecipe() + "\" ");
      writer.println("                   axonRecipe=\"" + network.getAxonFunction().getAxonFunctionRecipe() + "\" ");
      writer.println("                   weightRecipe=\"" + network.getWeightFunctionRecipe() + "\" ");
      writer.println("                   numberOfInputs=\"" + network.sizeOfInputVector() + "\" ");
      writer.println("                   xSize=\"" + network.getXSize() + "\" ");
      writer.println("                   ySize=\"" + network.getYSize() + "\" ");
      writer.println("                   numberOfInputNeurons=\"" + network.getInputLayer().size() + "\">");

      for (NeuralLayer layer : network.getLayers()) {
         writer.println("        <layer size=\"" + layer.size() + "\">");
         for (Neuron neuron : layer.getNeurons()) {
            writer.println("            <neuron>");
            for (WeightFunction weight : neuron.cloneWeights()) {
               writer.println("                <weight>");
               for (Double factor : weight.getFactors()) {
                  writer.println("                    <factor>" + factor.toString() + "</factor>");
               }
               writer.println("                </weight>");
            }
            writer.println("            </neuron>");
         }
         writer.println("        </layer>");
      }
      writer.println("    </neuralNetwork>");
      writer.close();
   }

   private void saveToPnn(NeuralNetwork network, String filepath) throws IOException {
      if (!filepath.contains(".pnn"))
         filepath += ".pnn";
      File file = new File(filepath);
      if (!file.exists()) {
         file.getParentFile().mkdirs();
         file.createNewFile();
      }

      prebiotic.prebioticSystemApi().log("Saving neural network to PNN file", getSenderName());

      List<Byte> bytes = new ArrayList<>();
      bytes.add((byte) PnnFileFormat.version);
      bytes.addAll(convertStringToByteList(network.getDescription()));
      bytes.add((byte) '\0');
      bytes.add(booleanToByte(network.isBiasPresence()));
      bytes.add(dataNormalizationToByte(network.getDataNormalization()));
      bytes.add(neuralNetworkTypeToByte(network.getType()));
      bytes.add(coreRecipeToByte(network.getCoreFunction().getCoreFunctionRecipe()));
      bytes.add(axonRecipeToByte(network.getAxonFunction().getAxonFunctionRecipe()));
      bytes.add(weightRecipeToByte(network.getWeightFunctionRecipe()));
      bytes.add((byte) network.sizeOfInputVector());
      if (network.getType() == NeuralNetworkType.FEEDFORWARD)
         bytes.add((byte) network.numberOfLayers());
      if (network.getType() == NeuralNetworkType.KOHONEN) {
         bytes.add((byte) (network.getXSize()));
         bytes.add((byte) (network.getYSize()));
      }

      for (NeuralLayer layer : network.getLayers()) {
         bytes.add((byte) layer.size());
         for (Neuron neuron : layer.getNeurons()) {
            for (WeightFunction weight : neuron.getWeights()) {
               for (Double factor : weight.getFactors()) {
                  byte[] bs = doubleToByteArray(factor);
                  for (byte b : bs) {
                     bytes.add(b);
                  }
               }
            }
         }
      }

      FileOutputStream fileOutputStream = new FileOutputStream(file);
      fileOutputStream.write(createArrayFromByteList(bytes));
      fileOutputStream.close();
   }

   private NeuralNetwork parseFromJson(String filepath, CustomFunctionsForParser customFunctionsForParser)
      throws FileNotFoundException, JSONException, EmptyFunctionDefinitionException, InvalidStructureException {
      StringBuilder jsonStringBuilder = new StringBuilder();
      NeuralNetwork neuralNetwork = null;
      File file = new File(filepath);
      Scanner scanner = new Scanner(file);
      while (scanner.hasNext()) {
         jsonStringBuilder.append(scanner.nextLine());
      }
      String jsonString = jsonStringBuilder.toString();
      JSONObject json = new JSONObject(jsonString);

      String description = json.getString("description");
      boolean biasPresence = json.getBoolean("biasPresence");
      DataNormalizationType dataNormalization = DataNormalizationType.valueOf(json.getString("dataNormalization"));
      NeuralNetworkType type = NeuralNetworkType.valueOf(json.getString("type"));
      CoreFunctionRecipe coreRecipe = CoreFunctionRecipe.valueOf(json.getString("coreRecipe"));
      AxonFunctionRecipe axonRecipe = AxonFunctionRecipe.valueOf(json.getString("axonRecipe"));
      WeightFunctionRecipe weightRecipe = WeightFunctionRecipe.valueOf(json.getString("weightRecipe"));
      int numberOfInputs = json.getInt("numberOfInputs");
      int xSize = json.getInt("xSize");
      int ySize = json.getInt("ySize");
      JSONArray layersJson = json.getJSONArray("layers");
      NeuralNetworkStructureConfigurationsBuilder structureConfigurationsBuilder
         = prebiotic.neuralApi().getStructureConfigurationsBuilder();
      structureConfigurationsBuilder.setWeightFunction(weightRecipe);
      structureConfigurationsBuilder.setAxonFunction(axonRecipe);
      structureConfigurationsBuilder.setCoreFunction(coreRecipe);
      structureConfigurationsBuilder.setDataNormalization(dataNormalization);
      structureConfigurationsBuilder.setBiasPresence(biasPresence);
      structureConfigurationsBuilder.setNeuralNetworkType(type);
      if (axonRecipe == AxonFunctionRecipe.AXON_CUSTOM)
         structureConfigurationsBuilder.defineAxonInstance(customFunctionsForParser.axonFunction);
      if (coreRecipe == CoreFunctionRecipe.CORE_CUSTOM)
         structureConfigurationsBuilder.defineCoreInstance(customFunctionsForParser.coreFunction);
      if (weightRecipe == WeightFunctionRecipe.WEIGHT_CUSTOM)
         structureConfigurationsBuilder.defineWeightInstance(customFunctionsForParser.weightFunction);

      NeuralNetworkBuilder neuralNetworkBuilder = prebiotic.neuralApi()
         .getNeuralNetworkBuilder(structureConfigurationsBuilder.create());
      if (type == NeuralNetworkType.FEEDFORWARD) {
         int inputLayerSize = layersJson.getJSONObject(0).getInt("size");
         NetworkWithInputLayerBuilder networkWithInputLayerBuilder
            = neuralNetworkBuilder.addDefaultInputLayer(inputLayerSize, numberOfInputs);
         for (int i = 1; i < layersJson.length(); i++) {
            int size = layersJson.getJSONObject(i).getInt("size");
            networkWithInputLayerBuilder.addLayer(size);
         }
         neuralNetwork = buildWeights(networkWithInputLayerBuilder
            .endBuildingNetwork().create(description), layersJson);
      }
      if (type == NeuralNetworkType.KOHONEN) {
         neuralNetwork = buildWeights(neuralNetworkBuilder
            .buildKohonenMap(xSize, ySize, numberOfInputs)
            .create(description), layersJson);
      }
      if (type == NeuralNetworkType.HOPFIELD) {
         neuralNetwork = buildWeights(neuralNetworkBuilder
            .buildHopfieldNetwork(numberOfInputs).create(), layersJson);
      }
      return neuralNetwork;
   }

   private NeuralNetwork parseFromPnn(String filepath, CustomFunctionsForParser customFunctionsForParser) throws IOException, EmptyFunctionDefinitionException, InvalidStructureException, WrongPnnFileVersionException {
      Path path = Paths.get(filepath);
      byte[] data = Files.readAllBytes(path);
      prebiotic.prebioticSystemApi().log("Parsing from the PNN file...", getSenderName());
      int i = 0;
      if (data[i] != PnnFileFormat.version)
         throw new WrongPnnFileVersionException("Current version is " + PnnFileFormat.version + ". The file version is "
            + data[i] + ". That may generate problems because format of the file was changed.");
      i++;
      boolean stringEnd = false;
      StringBuilder description = new StringBuilder();
      while (!stringEnd) {
         if ((char) data[i] == '\0') {
            stringEnd = true;
         } else {
            description.append((char) data[i]);
         }
         i++;
      }
      boolean isBiasPresence = data[i++] == 1;
      DataNormalizationType dataNormalizationType = byteToDataNormalization(data[i++]);
      NeuralNetworkType neuralNetworkType = byteToNeuralNetworkType(data[i++]);
      CoreFunctionRecipe coreFunctionRecipe = byteToCoreRecipe(data[i++]);
      AxonFunctionRecipe axonFunctionRecipe = byteToAxonRecipe(data[i++]);
      WeightFunctionRecipe weightFunctionRecipe = byteToWeightRecipe(data[i++]);
      int numberOfInputs = data[i++];
      return buildNetwork(data, i, description, isBiasPresence, dataNormalizationType,
         neuralNetworkType, coreFunctionRecipe, axonFunctionRecipe,
         weightFunctionRecipe, numberOfInputs, customFunctionsForParser);
   }

   private NeuralNetwork buildNetwork(byte[] data, int i,
                                      StringBuilder description,
                                      boolean isBiasPresence,
                                      DataNormalizationType dataNormalizationType,
                                      NeuralNetworkType neuralNetworkType,
                                      CoreFunctionRecipe coreFunctionRecipe,
                                      AxonFunctionRecipe axonFunctionRecipe,
                                      WeightFunctionRecipe weightFunctionRecipe,
                                      int numberOfInputs, CustomFunctionsForParser customFunctionsForParser) throws EmptyFunctionDefinitionException, InvalidStructureException {

      NeuralNetworkStructureConfigurationsBuilder neuralNetworkStructureConfigurationsBuilder
         = prebiotic.neuralApi().getStructureConfigurationsBuilder()
         .setBiasPresence(isBiasPresence)
         .setDataNormalization(dataNormalizationType)
         .setAxonFunction(axonFunctionRecipe)
         .setCoreFunction(coreFunctionRecipe)
         .setWeightFunction(weightFunctionRecipe)
         .setNeuralNetworkType(neuralNetworkType);
      if (axonFunctionRecipe == AxonFunctionRecipe.AXON_CUSTOM)
         neuralNetworkStructureConfigurationsBuilder.defineAxonInstance(customFunctionsForParser.axonFunction);
      if (coreFunctionRecipe == CoreFunctionRecipe.CORE_CUSTOM)
         neuralNetworkStructureConfigurationsBuilder.defineCoreInstance(customFunctionsForParser.coreFunction);
      if (weightFunctionRecipe == WeightFunctionRecipe.WEIGHT_CUSTOM)
         neuralNetworkStructureConfigurationsBuilder.defineWeightInstance(customFunctionsForParser.weightFunction);
      StructureConfigurations config = neuralNetworkStructureConfigurationsBuilder.create();
      if (neuralNetworkType == NeuralNetworkType.FEEDFORWARD) {
         int numberOfLayers = data[i++];
         int numberOfInputsNeuron = data[i++];
         NetworkWithInputLayerBuilder builder = prebiotic.neuralApi().getNeuralNetworkBuilder(config)
            .addDefaultInputLayer(numberOfInputsNeuron, numberOfInputs);
         int oldPosition = i;
         int step = (numberOfInputs + (isBiasPresence ? 1 : 0)) * numberOfInputsNeuron * 8;
         int prevLayerSize = numberOfInputsNeuron;
         if (numberOfLayers > 1)
            for (int j = 1; j < numberOfLayers; j++) {
               int layerSize = data[i++ + step];
               i += step;
               builder.addLayer(layerSize);
               step = (prevLayerSize + (isBiasPresence ? 1 : 0)) * layerSize * 8;
               prevLayerSize = layerSize;
            }
         return buildWeights(data, oldPosition, builder.endBuildingNetwork().create(description.toString()));
      }
      if (neuralNetworkType == NeuralNetworkType.KOHONEN) {
         int xSize = data[i++];
         int ySize = data[i++];
         return buildWeights(data, ++i, prebiotic.neuralApi().getNeuralNetworkBuilder(config)
            .buildKohonenMap(xSize, ySize, numberOfInputs)
            .create(description.toString()));
      }
      if (neuralNetworkType == NeuralNetworkType.HOPFIELD) {
         int numberOfInputsNeuron = data[i++];
         return buildWeights(data, i, prebiotic.neuralApi().getNeuralNetworkBuilder(config)
            .buildHopfieldNetwork(numberOfInputsNeuron).create(description.toString()));
      }
      return null;
   }

   private NeuralNetwork buildWeights(byte[] data, int i, NeuralNetwork neuralNetwork) {
      for (NeuralLayer layer : neuralNetwork.getLayers()) {
         for (Neuron neuron : layer.getNeurons()) {
            for (WeightFunction weight : neuron.getWeights()) {
               for (int w = 0; w < weight.getFactors().size(); w++) {
                  byte[] bytes = new byte[8];
                  for (int x = 0; x < 8; x++) {
                     bytes[x] = data[i++];
                  }
                  weight.getFactors().set(w, byteArrayToDouble(bytes));
               }
            }
         }
         i++;
      }
      return neuralNetwork;
   }

   private NeuralNetwork buildWeights(NeuralNetwork neuralNetwork, JSONArray layers) throws JSONException {
      for (int l = 0; l < neuralNetwork.getLayers().size(); l++) {
         NeuralLayer layer = neuralNetwork.getLayers().get(l);
         for (int n = 0; n < layer.getNeurons().size(); n++) {
            Neuron neuron = layer.getNeurons().get(n);
            for (int w = 0; w < neuron.getWeights().size(); w++) {
               WeightFunction weight = neuron.getWeights().get(w);
               for (int f = 0; f < weight.getFactors().size(); f++) {
                  weight.getFactors().set(f,
                     layers.getJSONObject(l)
                        .getJSONArray("neurons")
                        .getJSONObject(n)
                        .getJSONArray("weights")
                        .getJSONObject(w)
                        .getJSONArray("factors")
                        .getDouble(f));
               }
            }
         }
      }
      return neuralNetwork;
   }

   private List<Byte> convertStringToByteList(String description) {
      List<Byte> bytes = new ArrayList<>();
      for (Character c : description.toCharArray()) {
         bytes.add((byte) c.charValue());
      }
      return bytes;
   }

   private byte[] createArrayFromByteList(List<Byte> list) {
      byte[] array = new byte[list.size()];
      for (int i = 0; i < list.size(); i++) {
         array[i] = list.get(i);
      }
      return array;
   }

   private byte booleanToByte(boolean b) {
      if (b)
         return 1;
      else
         return 0;
   }

   private byte dataNormalizationToByte(DataNormalizationType type) {
      switch (type) {
         case NORMALIZATION_TO_UNIT_VECTOR: {
            return 1;
         }
         case NO_NORMALIZATION: {
            return 2;
         }
         case SCALING_TO_AVERAGE: {
            return 3;
         }
         case SCALING_TO_MAX_VALUE: {
            return 4;
         }
         case SCALING_TO_STANDARD_DEVIATION: {
            return 5;
         }
         case SCALING_TO_DEVIATION_FROM_AVERAGE: {
            return 6;
         }
         case SCAlING_TO_DEVIATION_FROM_MIN_VALUE: {
            return 7;
         }
      }
      return 0;
   }

   private DataNormalizationType byteToDataNormalization(byte b) {
      switch (b) {
         case 1: {
            return DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
         }
         case 2: {
            return DataNormalizationType.NO_NORMALIZATION;
         }
         case 3: {
            return DataNormalizationType.SCALING_TO_AVERAGE;
         }
         case 4: {
            return DataNormalizationType.SCALING_TO_MAX_VALUE;
         }
         case 5: {
            return DataNormalizationType.SCALING_TO_STANDARD_DEVIATION;
         }
         case 6: {
            return DataNormalizationType.SCALING_TO_DEVIATION_FROM_AVERAGE;
         }
         case 7: {
            return DataNormalizationType.SCAlING_TO_DEVIATION_FROM_MIN_VALUE;
         }
      }
      return null;
   }

   private byte neuralNetworkTypeToByte(NeuralNetworkType type) {
      switch (type) {
         case FEEDFORWARD: {
            return 1;
         }
         case KOHONEN: {
            return 2;
         }
         case HOPFIELD: {
            return 3;
         }
      }
      return 0;
   }

   private NeuralNetworkType byteToNeuralNetworkType(byte b) {
      switch (b) {
         case 1: {
            return NeuralNetworkType.FEEDFORWARD;
         }
         case 2: {
            return NeuralNetworkType.KOHONEN;
         }
         case 3: {
            return NeuralNetworkType.HOPFIELD;
         }
      }
      return null;
   }

   private byte coreRecipeToByte(CoreFunctionRecipe recipe) {
      switch (recipe) {
         case DEFAULT_ADDER: {
            return 1;
         }
         case CORE_CUSTOM: {
            return 2;
         }
      }
      return 0;
   }

   private CoreFunctionRecipe byteToCoreRecipe(byte b) {
      switch (b) {
         case 1: {
            return CoreFunctionRecipe.DEFAULT_ADDER;
         }
         case 2: {
            return CoreFunctionRecipe.CORE_CUSTOM;
         }
      }
      return null;
   }

   private byte axonRecipeToByte(AxonFunctionRecipe recipe) {
      switch (recipe) {
         case SIGMOID: {
            return 1;
         }
         case HYPERBOLIC: {
            return 2;
         }
         case AXON_CUSTOM: {
            return 3;
         }
         case HOPFIELD_THRESHOLD: {
            return 4;
         }
         case TANH: {
            return 5;
         }
         case LINEAR: {
            return 6;
         }
         case THRESHOLD: {
            return 7;
         }
         case HOPFIELD_TANH: {
            return 8;
         }
         case HOPFIELD_SIGMOID: {
            return 9;
         }
      }
      return 0;
   }

   private AxonFunctionRecipe byteToAxonRecipe(byte b) {
      switch (b) {
         case 1: {
            return AxonFunctionRecipe.SIGMOID;
         }
         case 2: {
            return AxonFunctionRecipe.HYPERBOLIC;
         }
         case 3: {
            return AxonFunctionRecipe.AXON_CUSTOM;
         }
         case 4: {
            return AxonFunctionRecipe.HOPFIELD_THRESHOLD;
         }
         case 5: {
            return AxonFunctionRecipe.TANH;
         }
         case 6: {
            return AxonFunctionRecipe.LINEAR;
         }
         case 7: {
            return AxonFunctionRecipe.THRESHOLD;
         }
         case 8: {
            return AxonFunctionRecipe.HOPFIELD_TANH;
         }
         case 9: {
            return AxonFunctionRecipe.HOPFIELD_SIGMOID;
         }
      }
      return null;
   }

   private byte weightRecipeToByte(WeightFunctionRecipe recipe) {
      switch (recipe) {
         case DEFAULT_LINEAR: {
            return 1;
         }
         case SQUARE_DIFFERENCE: {
            return 2;
         }
         case WEIGHT_CUSTOM: {
            return 3;
         }
      }
      return 0;
   }

   private WeightFunctionRecipe byteToWeightRecipe(byte b) {
      switch (b) {
         case 1: {
            return WeightFunctionRecipe.DEFAULT_LINEAR;
         }
         case 2: {
            return WeightFunctionRecipe.SQUARE_DIFFERENCE;
         }
         case 3: {
            return WeightFunctionRecipe.WEIGHT_CUSTOM;
         }
      }
      return null;
   }

   private byte[] doubleToByteArray(double x) {
      ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES);
      byteBuffer.putDouble(x);
      if (byteBuffer.array().length != 8)
         System.out.println("AAAAAAAAA");
      return byteBuffer.array();
   }

   private double byteArrayToDouble(byte[] bytes) {
      ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES);
      byteBuffer.put(bytes);
      byteBuffer.flip();
      return byteBuffer.getDouble();
   }

   @Override
   protected String getSenderName() {
      return "PARSER";
   }
}
