package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.CoreFunctionRecipe;

/**
 * Abstract core function
 */
public abstract class AbstractCoreFunction implements CoreFunction {
   protected CoreFunctionRecipe coreFunctionRecipe;

   @Override
   public CoreFunctionRecipe getCoreFunctionRecipe() {
      return coreFunctionRecipe;
   }

   @Override
   public void setCoreFunctionRecipe(CoreFunctionRecipe coreFunctionRecipe) {
      this.coreFunctionRecipe = coreFunctionRecipe;
   }
}
