package aleksy.abspec.prebiotic.framework.evolution.genetic.common.exception;

/**
 * Genetic algorithm configurations exception
 */
public class GeneticAlgorithmConfigurationsException extends GeneticAlgorithmException {
   public GeneticAlgorithmConfigurationsException() {
      super("\nGenetic algorithm configurations error.");
   }

   public GeneticAlgorithmConfigurationsException(String message) {
      super("\nGenetic algorithm configurations error. " + message);
   }
}
