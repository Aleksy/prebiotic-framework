package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate;

/**
 * Data normalization types
 */
public enum DataNormalizationType {
   /**
    * Scaling to average of values from input vector
    */
   SCALING_TO_AVERAGE,
   /**
    * Scaling to maximal value from input vector
    */
   SCALING_TO_MAX_VALUE,
   /**
    * Scaling to deviation from the average of values from input vector
    */
   SCALING_TO_DEVIATION_FROM_AVERAGE,
   /**
    * Scaling to deviation from the minimal value from input vector
    */
   SCAlING_TO_DEVIATION_FROM_MIN_VALUE,
   /**
    * Scaling to the standard deviation of values from input vector
    */
   SCALING_TO_STANDARD_DEVIATION,
   /**
    * Normalization the input vector to unit vector
    */
   NORMALIZATION_TO_UNIT_VECTOR,
   /**
    * No normalization, leaving the raw input vector
    */
   NO_NORMALIZATION
}
