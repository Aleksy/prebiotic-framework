package aleksy.abspec.prebiotic.framework.vision.display.panel.coordinatesystem;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractCoordinateSystemNeuralPanel;

import java.awt.*;
import java.util.ArrayList;

public class CoordinateSystemNeuralPanel extends AbstractCoordinateSystemNeuralPanel {

   public CoordinateSystemNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      if (nn.getType() != NeuralNetworkType.KOHONEN) {
         drawCoordinateSystem(g);
      } else
         drawKohonenCoordinateSystem(g);
   }

   private void drawKohonenCoordinateSystem(Graphics g) {
      int actOutIndex = 0;
      for (int x = 0; x < nn.getXSize(); x++) {
         for (int y = 0; y < nn.getYSize(); y++) {
            java.util.List<Neuron> neighborhood = new ArrayList<>();
            Neuron thisNeuron = getNeuron(x, y, nn);
            neighborhood.add(getNeuron(x - 1, y, nn));
            neighborhood.add(getNeuron(x, y - 1, nn));
            neighborhood.add(getNeuron(x + 1, y, nn));
            neighborhood.add(getNeuron(x, y + 1, nn));
            for (Neuron n : neighborhood) {
               if (n != null) {
                  Double axon = 0.0;
                  if (nn.getActualOutput() != null)
                     axon = VectorNormalizator.scaleToMaxValue(nn.getActualOutput()).get(actOutIndex);
                  g.setColor(createColor(0.26 + 0.74 * axon));
                  drawConnection(((KohonenNeuralNetwork) nn).getNeuron(x, y), n, g);
               }
            }
            actOutIndex++;
         }
      }
      actOutIndex = 0;
      for (Neuron neuron : nn.getAllNeurons()) {
         Double axon = 0.0;
         if (nn.getActualOutput() != null) {
            axon = VectorNormalizator.scaleToMaxValue(nn.getActualOutput()).get(actOutIndex++);
         }
         g.setColor(createColor(0.16 + 0.84 * axon));
         drawNeuron(neuron.getVectorOfWeights(), g);
      }
   }

   private void drawConnection(Neuron n1, Neuron n2, Graphics g) {
      double[] point1 = createCoordinatePoint(n1.getVectorOfWeights());
      double[] point2 = createCoordinatePoint(n2.getVectorOfWeights());
      g.drawLine((int) point1[0], (int) point1[1], (int) point2[0], (int) point2[1]);
   }

   private Neuron getNeuron(int x, int y, NeuralNetwork nn) {
      if (x < 0 || x >= nn.getXSize() || y < 0 || y >= nn.getYSize()) {
         return null;
      } else {
         return ((KohonenNeuralNetwork) nn).getNeuron(x, y);
      }
   }

   private void drawCoordinateSystem(Graphics g) {
      drawSynapses(g);
      Range axonRange = nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues();
      for (Neuron neuron : nn.getAllNeurons()) {
         Double axon = neuron.getActualAxonOutput();
         double normalizedAxon = 1.0;
         if (axon != null)
            normalizedAxon = VectorNormalizator.setValueBetweenZeroAndOne(axon, axonRange.getFrom(), axonRange.getTo());
         g.setColor(createColor(0.16 + 0.84 * normalizedAxon));
         drawNeuron(VectorNormalizator.normalizeToUnitVector(neuron.getVectorOfWeights()), g);
      }
   }

   private void drawSynapses(Graphics g) {
      Range axonRange = nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues();
      for (int i = 0; i < nn.numberOfLayers() - 1; i++) {
         NeuralLayer layer = nn.getLayers().get(i);
         NeuralLayer nextLayer = nn.getLayers().get(i + 1);
         for (int neuronNumber = 0; neuronNumber < layer.size(); neuronNumber++) {
            Neuron neuron = layer.getNeurons().get(neuronNumber);
            Double axon = neuron.getActualAxonOutput();
            double normalizedAxon = 1.0;
            if (axon != null)
               normalizedAxon = VectorNormalizator.setValueBetweenZeroAndOne(axon, axonRange.getFrom(), axonRange.getTo());
            double[] neuronPoint = createCoordinatePoint(
               VectorNormalizator.normalizeToUnitVector(neuron.getVectorOfWeights()));
            for (int nextNeuronNumber = 0; nextNeuronNumber < nextLayer.size(); nextNeuronNumber++) {
               Neuron nextNeuron = nextLayer.getNeurons().get(nextNeuronNumber);
               double[] nextNeuronPoint = createCoordinatePoint(
                  VectorNormalizator.normalizeToUnitVector(nextNeuron.getVectorOfWeights()));
               g.setColor(createColor(0.16 + 0.84 * normalizedAxon).darker().darker().darker());
               g.drawLine((int) neuronPoint[0], (int) neuronPoint[1],
                  (int) nextNeuronPoint[0], (int) nextNeuronPoint[1]);
            }
         }
      }
   }
}
