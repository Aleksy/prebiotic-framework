package aleksy.abspec.prebiotic.framework.neural.teaching.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingDataPackage;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;

public class HopfieldLearningLogic extends Loggable {

   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public HopfieldLearningLogic(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   public NeuralNetwork start(TeachingData teachingData, NeuralNetwork network) throws HopfieldLearningException {
      prebiotic.prebioticSystemApi().log("Hopfield's network learning starting.", getSenderName());
      LocalTime startTime = LocalTime.now();
      if (network.getType() == NeuralNetworkType.HOPFIELD)
         network = algorithmCore(teachingData, network);
      else
         throw new HopfieldLearningException("Algorithm can teach only Hopfield's network. " +
            "For another types use another algorithms.");
      prebiotic.prebioticSystemApi().log("Finishing. Returning a neural network.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      return network;
   }

   private NeuralNetwork algorithmCore(TeachingData teachingData, NeuralNetwork network) {
      double networkSize = network.size();
      List<Neuron> neurons = network.getAllNeurons();
      for (int neuronNumber = 0; neuronNumber < networkSize; neuronNumber++) {
         prebiotic.prebioticSystemApi().log("Setting weights for neuron " + neuronNumber + ".", getSenderName());
         List<WeightFunction> neuronWeights = neurons.get(neuronNumber).getWeights();
         for (int inputNumber = 0; inputNumber < networkSize; inputNumber++) {
            if (neuronNumber != inputNumber) {
               double sum = 0.0;
               for (int i = 0; i < teachingData.getDataPackages().size(); i++) {
                  TeachingDataPackage tdp = teachingData.getDataPackages().get(i);
                  sum += tdp.ins.get(neuronNumber) * tdp.ins.get(inputNumber);
               }
               neuronWeights.get(inputNumber).getFactors().set(0,
                  (1 / networkSize) * sum);
            }
         }
      }
      return network;
   }

   @Override
   protected String getSenderName() {
      return "HOP-LRN";
   }
}
