package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.AbstractNeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;

/**
 * Basic implementation of neural network layer
 */
public class NeuralLayerImpl extends AbstractNeuralLayer implements NeuralLayer {
}
