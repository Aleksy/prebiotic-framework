package aleksy.abspec.prebiotic.framework.neural.neuralstructure.common.model.layer;

import aleksy.abspec.prebiotic.framework.common.util.ListCreator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl.NeuralLayerImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.AxonFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.CoreFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.WeightFunctions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NeuralLayerTest {
   private NeuralLayer uut;

   @Before
   public void init() {
      uut = new NeuralLayerImpl();

      uut.setNeurons(prepareNeurons());
   }

   @Test
   public void shouldCalculateOutputs() throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException {
      // when
      List<Double> outs = uut.f(ListCreator.createFrom(1.0, 2.0));

      // then
      assertEquals(2, outs.size());
      assertEquals(new Double(1.5), outs.get(0));
      assertEquals(new Double(1.5), outs.get(1));
   }

   @Test
   public void shouldReturnNumberOfInputs() {
      // when
      int n = uut.numberOfInputs();

      // then
      assertEquals(4, n);
   }

   private List<Neuron> prepareNeurons() {
      List<Neuron> neurons = new ArrayList<>();
      for(int i =0; i < 2; i++) {
         List<WeightFunction> weights = new ArrayList<>();
         weights.add(new WeightFunctions.DEFAULT_LINEAR(false));
         weights.add(new WeightFunctions.DEFAULT_LINEAR(false));

         for (WeightFunction w : weights)
            w.getFactors().set(0, 0.5);
         neurons.add(new NeuronImpl(weights, new CoreFunctions.DEFAULT_ADDER(),
            new AxonFunctions.LINEAR(), false));
      }
      return neurons;
   }
}
