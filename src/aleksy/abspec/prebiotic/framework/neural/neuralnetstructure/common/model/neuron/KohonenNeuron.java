package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron;

/**
 * Kohonen's neuron interface
 */
public interface KohonenNeuron extends Neuron {
   /**
    * Setter for x coordinate
    *
    * @param x to set
    */
   void setX(int x);

   /**
    * Getter for x coordinate
    *
    * @return x
    */
   int getX();

   /**
    * Setter for y coordinate
    *
    * @param y to set
    */
   void setY(int y);

   /**
    * Getter for y coordinate
    *
    * @return y
    */
   int getY();
}
