package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr;

/**
 * Neuron exception
 */
public abstract class NeuronException extends NeuralNetworkException {
   public NeuronException(String message) {
      super("neuron -> " + message);
   }
}
