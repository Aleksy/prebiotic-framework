package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl;

import aleksy.abspec.prebiotic.framework.common.constants.TechnicalConstants;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.AbstractNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.AxonFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Hopfield's neural network implementation
 */
public class HopfieldNeuralNetworkImpl extends AbstractNeuralNetwork implements NeuralNetwork {
   private List<Double> previousStepOutputs;
   private List<Double> thisStepOutputs;
   private List<Neuron> neurons;
   private List<Double> inputs;

   @Override

   public List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException {
      inputs = new ArrayList<>(xs);
      if (dataNormalization != DataNormalizationType.NO_NORMALIZATION) normalizeInputs(inputs);
      if (biasPresence)
         inputs.add(TechnicalConstants.BIAS);
      boolean isStable = false;
      Random r = new Random();
      neurons = getAllNeurons();
      for (Neuron n : neurons)
         n.setActualAxonOutput(null);
      int nextNeuronIndex;
      boolean firstStep = true;
      thisStepOutputs = null;

      while (!isStable) {
         List<Integer> indexes = new ArrayList<>();
         for (int i = 0; i < neurons.size(); i++) {
            indexes.add(i);
         }
         while (!indexes.isEmpty()) {
            int index = r.nextInt(indexes.size());
            nextNeuronIndex = indexes.get(index);
            makeIterationForNeuron(nextNeuronIndex);
            indexes.remove(index);
            if (indexes.isEmpty()) break;
         }

         if (firstStep) {
            inputs = resetInputs(inputs);
            firstStep = false;
         }
         prebiotic.visionApi().repaintAll();
         numberOfOperation++;
         if (compareLists(previousStepOutputs, thisStepOutputs)) {
            int stablePoints = 0;
            for (int i = 0; i < neurons.size(); i++) {
               makeIterationForNeuron(i);
               if (compareLists(previousStepOutputs, thisStepOutputs)) {
                  stablePoints++;
               }
            }
            if (stablePoints == size())
               isStable = true;
         }
      }
      return thisStepOutputs;
   }

   private void makeIterationForNeuron(int nextNeuronIndex) throws IncorrectNumberOfInputsInNeuronException {
      Neuron n = neurons.get(nextNeuronIndex);
      List<Double> localInputVector = new ArrayList<>();
      for (int i = 0; i < neurons.size(); i++) {
         if (i == nextNeuronIndex) {
            localInputVector.add(inputs.get(nextNeuronIndex));
         } else {
            Double axonOutput = neurons.get(i).getActualAxonOutput();
            if (axonOutput != null) {
               localInputVector.add(axonOutput);
            } else
               localInputVector.add(0.0);
         }
      }
      previousStepOutputs = getActualOutput();
      Double out = n.f(localInputVector);
      if (n.getAxonFunction().getAxonFunctionRecipe() == AxonFunctionRecipe.HOPFIELD_THRESHOLD) {
         AxonFunctions.HOPFIELD_THRESHOLD axon = (AxonFunctions.HOPFIELD_THRESHOLD) n.getAxonFunction();
         axon.setActualOutput(out);
      }
      thisStepOutputs = getActualOutput();
   }

   private boolean compareLists(List<Double> x1, List<Double> x2) {
      if (x1.size() != x2.size())
         return false;
      for (int i = 0; i < x1.size(); i++) {
         if (x1.get(i) == null || x2.get(i) == null)
            return false;
         if (!x1.get(i).equals(x2.get(i)))
            return false;
      }
      return true;
   }

   @Override
   public List<Double> getActualOutput() {
      List<Double> outputs = new ArrayList<>();
      for (Neuron n : getAllNeurons()) {
         if (n.getActualAxonOutput() != null)
            outputs.add(n.getActualAxonOutput());
         else
            outputs.add(null);
      }
      return outputs;
   }

   @Override
   public void randomizeWeights(Double from, Double to) {
      List<Neuron> neurons = getAllNeurons();
      int size = size();
      if (size == 1) {
         neurons.get(0).randomizeWeights(from, to);
         return;
      }
      for (int neuronNumber = 0; neuronNumber < size; neuronNumber++) {
         Neuron neuron = neurons.get(neuronNumber);
         neuron.randomizeWeights(from, to);
      }
      for (int neuronNumber = 0; neuronNumber < size; neuronNumber++) {
         for (int inputNumber = 0; inputNumber < size; inputNumber++) {
            if (inputNumber < neuronNumber) {
               double factor = neurons.get(inputNumber).getWeights().get(neuronNumber).getFactors().get(0);
               neurons.get(neuronNumber).getWeights().get(inputNumber).getFactors().set(0, factor);
            }
            if (neuronNumber == inputNumber)
               neurons.get(neuronNumber).getWeights().get(inputNumber).getFactors().set(0, 1.0);
         }
      }
   }

   private List<Double> resetInputs(List<Double> xs) {
      for (int i = 0; i < xs.size(); i++) {
         xs.set(i, 0.0);
      }
      for (int i = 0; i < size(); i++) {
         xs.set(i, 0.0);
         getAllNeurons().get(i).setActualInputs(xs);
      }
      return xs;
   }
}
