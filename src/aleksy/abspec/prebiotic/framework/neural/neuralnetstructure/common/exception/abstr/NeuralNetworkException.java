package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr;

import aleksy.abspec.prebiotic.framework.common.exception.PrebioticException;

/**
 * Neural network exception
 */
public abstract class NeuralNetworkException extends PrebioticException {
   public NeuralNetworkException(String message) {
      super("neural network -> " + message);
   }
}
