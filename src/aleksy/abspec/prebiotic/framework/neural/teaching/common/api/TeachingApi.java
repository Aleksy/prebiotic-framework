package aleksy.abspec.prebiotic.framework.neural.teaching.common.api;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.KohonenLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;

/**
 * Teaching module api
 */
public interface TeachingApi {

   /**
    * Starting method for deep learning algorithm. This algorithm can teach multi- and one-layer FEEDFORWARD networks.
    * Another networks are not supported yet.
    *
    * @param teachingData               data with packages to teach the network
    * @param network                    to teach
    * @param deepLearningConfigurations configurations of deep learning parameters
    * @return neural network after teaching process
    * @throws NeuralNetworkException when some problem with internal structure of neural network exists (e. g.
    *                                when input vector size is not correct)
    * @throws DeepLearningException  when something in algorithm goes wrong
    */
   NeuralNetwork deepLearning(TeachingData teachingData, NeuralNetwork network,
                              DeepLearningConfigurations deepLearningConfigurations)
      throws NeuralNetworkException, DeepLearningException;

   /**
    * Starting method for kohonen learning algorithm. This algorithm can teach Kohonen's map networks.
    * Teaching data packages can have only inputs vectors, no expected outputs are needed.
    *
    * @param teachingData                  with packages (only input vectors) to teach the network
    * @param network                       to teach
    * @param kohonenLearningConfigurations configurations of kohonen learning parameters
    * @return neural network after teaching process
    * @throws KohonenLearningException                 when something in algorithm goes wrong
    * @throws IncorrectNumberOfInputsInNeuronException when neuron has a problem with number of inputs
    * @throws IncorrectNumberOfInputsInLayerException  when layer has a problem with number of inputs
    */
   NeuralNetwork kohonenLearning(TeachingData teachingData, NeuralNetwork network,
                                 KohonenLearningConfigurations kohonenLearningConfigurations)
      throws KohonenLearningException, IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException;

   /**
    * Getter for new instance of kohonen learning configurations parsing
    *
    * @return parsing
    */
   KohonenLearningConfigurationsBuilder getKohonenLearningConfigurationsBuilder();

   /**
    * Getter for new instance of deep learning configurations parsing
    *
    * @return parsing
    */
   DeepLearningConfigurationsBuilder getDeepLearningConfigurationsBuilder();


   /**
    * Starting method for Hopfield's network learning algorithm (Hebb's rule). Teaching data packages can
    * have only input vectors, no expected output are needed.
    *
    * @param teachingData with packages (only input vectors) to teach the network
    * @param network      to teach
    * @return neural network after teaching process
    * @throws HopfieldLearningException when type of network is wrong
    */
   NeuralNetwork hopfieldLearning(TeachingData teachingData, NeuralNetwork network) throws HopfieldLearningException;
}
