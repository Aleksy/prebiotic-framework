package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl.NeuralLayerImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder of neural network with defined input layer
 */
public class NetworkWithInputLayerBuilder extends Loggable {

   private StructureConfigurations configurations;
   private NeuralNetwork neuralNetwork;
   private CoreFunction core;
   private AxonFunction axon;
   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param configurations for neural network
    * @param neuralNetwork  to build
    * @param core           instance of core function to set in every neuron of network
    * @param axon           instance of axon function to set in every neuron of network
    * @param prebiotic      api
    */
   public NetworkWithInputLayerBuilder(StructureConfigurations configurations, NeuralNetwork neuralNetwork,
                                       CoreFunction core, AxonFunction axon, PrebioticApi prebiotic) {
      this.configurations = configurations;
      this.neuralNetwork = neuralNetwork;
      this.core = core;
      this.axon = axon;
      this.prebiotic = prebiotic;
   }

   /**
    * Adds new (hidden) layer to network
    *
    * @param amountOfNeurons in new layer
    * @return builder
    */
   public NetworkWithInputLayerBuilder addLayer(int amountOfNeurons) {
      int amountOfInputs = neuralNetwork.getLayers()
         .get(neuralNetwork.numberOfLayers() - 1).size();
      NeuralLayer layer = prepareLayer(amountOfNeurons, amountOfInputs);
      neuralNetwork.getLayers().add(layer);
      return this;
   }

   /**
    * Ends the neural network building.
    *
    * @return endpoint builder
    */
   public EndpointBuilder endBuildingNetwork() {
      return new EndpointBuilder(neuralNetwork, prebiotic);
   }

   private NeuralLayer prepareLayer(int amountOfNeurons, int amountOfInputs) {
      NeuralLayer layer = new NeuralLayerImpl();
      layer.setInputLayer(false);
      List<Neuron> neurons = new ArrayList<>();
      for (int i = 0; i < amountOfNeurons; i++) {
         List<WeightFunction> weights = new ArrayList<>();
         for (int j = 0; j < amountOfInputs; j++) {
            if (configurations.weightFunctionRecipe != WeightFunctionRecipe.WEIGHT_CUSTOM)
               weights.add(configurations.weightFunctionRecipe.newInstance(false));
            else {
               WeightFunction w = (WeightFunction) configurations.weightInstance.newInstance();
               w.setWeightFunctionRecipe(WeightFunctionRecipe.WEIGHT_CUSTOM);
               weights.add(w);
            }
         }
         Neuron neuron = new NeuronImpl(weights, core, axon, neuralNetwork.isBiasPresence());
         neurons.add(neuron);
      }
      layer.setNeurons(neurons);
      return layer;
   }

   @Override
   protected String getSenderName() {
      return "NETWORK-BLD";
   }
}
