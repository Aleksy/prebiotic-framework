package aleksy.abspec.prebiotic.framework.vision.common.impl;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.vision.common.api.VisionApi;
import aleksy.abspec.prebiotic.framework.vision.display.common.api.DisplayApi;
import aleksy.abspec.prebiotic.framework.vision.display.common.impl.Display;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractPanel;

/**
 * Implementation of Vision Api
 */
public class Vision implements VisionApi {
   private DisplayApi display;

   public Vision() {
      display = new Display();
   }

   @Override
   public void display(NeuralNetwork nn, Double hue, DisplayMode mode, Range inputRange) {
      display.display(nn, hue, mode, inputRange);
   }

   @Override
   public void display(Genotype genotype) {
      display.display(genotype);
   }

   @Override
   public void repaintAll() {
      display.repaintAll();
   }

   @Override
   public void removePanelFromActivePanels(AbstractPanel panel) {
      display.removePanelFromActivePanels(panel);
   }

   @Override
   public AbstractPanel getNewPanel(NeuralNetwork nn, Double hue, DisplayMode mode, Range inputRange) {
      return display.getNewPanel(nn, hue, mode, inputRange);
   }

   @Override
   public AbstractPanel getNewPanel(Genotype genotype) {
      return display.getNewPanel(genotype);
   }
}
