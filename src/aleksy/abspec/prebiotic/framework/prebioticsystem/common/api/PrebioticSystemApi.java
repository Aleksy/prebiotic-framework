package aleksy.abspec.prebiotic.framework.prebioticsystem.common.api;

import aleksy.abspec.prebiotic.framework.common.enumerate.FileFormat;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.model.PrebioticModelObject;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder.CustomFunctionsForParserBuilder;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;

import java.io.IOException;

/**
 * Prebiotic System module api
 */
public interface PrebioticSystemApi {

   /**
    * Logs a user message in the console
    *
    * @param message to print
    */
   void log(String message);

   /**
    * Method logs message in the console
    *
    * @param message to log
    * @param sender  of the message
    */
   void log(String message, String sender);

   /**
    * Method logs an exception in the console
    *
    * @param e exception to print
    */
   void log(Exception e);

   /**
    * Method logs a separator
    */
   void separator();

   /**
    * Method logs a neural network details in the console
    *
    * @param network to log
    * @param sender  of the message
    */
   void log(NeuralNetwork network, String sender);

   /**
    * Method logs a user's neural network details in the console
    *
    * @param network to log
    */
   void log(NeuralNetwork network);

   /**
    * Logs genotype
    *
    * @param genotype to log
    * @param sender   of a message
    */
   void log(Genotype genotype, String sender);

   /**
    * Logs a user's genotype
    *
    * @param genotype to log
    */
   void log(Genotype genotype);

   /**
    * Method logs warning message in the console
    *
    * @param message to log
    * @param sender  of the message
    */
   void logWarn(String message, String sender);

   /**
    * Method logs a logo of framework
    */
   void logo();

   /**
    * Getter for new instance of {@link CustomFunctionsForParserBuilder}
    *
    * @return builder
    */
   CustomFunctionsForParserBuilder getCustomFunctionsForParserBuilder();

   /**
    * Saves a prebiotic model object (like neural network) to the file
    *
    * @param prebioticModelObject to save
    * @param filepath             of file
    * @param fileFormat           of file
    * @throws IOException when the problems with files exist
    */
   void save(PrebioticModelObject prebioticModelObject, String filepath, FileFormat fileFormat) throws IOException;

   /**
    * Reads a prebiotic model object (like neural network) from a file
    *
    * @param filepath of file
    * @return parsed model object
    * @throws Exception when something goes wrong
    */
   PrebioticModelObject read(String filepath) throws Exception;

   /**
    * Reads a prebiotic model object (like neural network) from a file. Method can be used to
    * read a neural network with custom functions. Usage:
    * Create the CustomFunctionsForParser in a builder and set the function instances of your classes.
    * EXAMPLE:
    * If you have custom axon function - set the axon function instance in the CustomFunctionsForParser
    * and put it to params. Parser'll automatically set the instances to neural network
    *
    * @param filepath                 of file
    * @param customFunctionsForParser with custom functions definitions
    * @return instance of neural network
    * @throws Exception when something goes wrong
    */
   NeuralNetwork read(String filepath, CustomFunctionsForParser customFunctionsForParser) throws Exception;
}
