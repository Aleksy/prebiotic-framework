package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuronException;

/**
 * Incorrect number of inputs in neuron exception
 */
public class IncorrectNumberOfInputsInNeuronException extends NeuronException {
   public IncorrectNumberOfInputsInNeuronException(String message) {
      super("\nIncorrect number of inputs. MESSAGE: \"" + message + "\"");
   }
}
