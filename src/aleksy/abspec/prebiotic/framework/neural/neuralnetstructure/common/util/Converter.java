package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

/**
 * Prebiotic converter util class
 */
public class Converter {
   public static KohonenNeuralNetwork convertToKohonenNeuralNetwork(NeuralNetwork neuralNetworkInterface)
      throws InvalidStructureException {
      if (neuralNetworkInterface.getType() == NeuralNetworkType.KOHONEN) {
         return (KohonenNeuralNetwork) neuralNetworkInterface;
      } else throw new InvalidStructureException("Cannot convert to Kohonen's map. Type of network is not KOHONEN");
   }
}
