package aleksy.abspec.prebiotic.framework.common.constants;

/**
 * Basic constants values from Prebiotic Framework
 */
public class BasicConstants {
   /**
    * Prebiotic Framework Version
    */
   public static final String VERSION = "2.0.0";
   /**
    * Framework name
    */
   public static final String NAME = "Prebiotic Framework";
   /**
    * Framework short name
    */
   public static final String SHORT_NAME = "PF";
   /**
    * Framework author
    */
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
}
