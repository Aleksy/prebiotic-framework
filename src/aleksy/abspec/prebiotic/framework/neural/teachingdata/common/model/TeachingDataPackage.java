package aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model;

import java.util.List;

/**
 * Single package of neural network teaching data
 */
public class TeachingDataPackage {
   /**
    * Input vector for neural network
    */
   public List<Double> ins;
   /**
    * Expected output vector from neural network. No needed when self-learning algorithm is used
    */
   public List<Double> expectedOuts;
}
