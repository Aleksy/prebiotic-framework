package aleksy.abspec.prebiotic.framework.neural.teaching.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.constants.TechnicalConstants;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.EuclideanDistance;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningConfigurationsException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingDataPackage;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;

/**
 * Deep learning class
 */
public class DeepLearningLogic extends Loggable {

   private PrebioticApi prebiotic;
   private double localLearningRate;
   private int amountOfIterations;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public DeepLearningLogic(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   /**
    * Starting method for deep learning algorithm.
    *
    * @param teachingData               data with packages to teach network
    * @param network                    to teach
    * @param deepLearningConfigurations configurations of deep learning parameters
    * @return neural network after teaching process
    * @throws NeuralNetworkException              when some problem with internal structure of neural network exists (e. g.
    *                                             when input vector size is not correct)
    * @throws DeepLearningConfigurationsException when configurations are wrong
    */
   public NeuralNetwork start(TeachingData teachingData, NeuralNetwork network,
                              DeepLearningConfigurations deepLearningConfigurations)
      throws NeuralNetworkException, DeepLearningException {
      prebiotic.prebioticSystemApi().log("Deep learning starting.", getSenderName());
      prebiotic.prebioticSystemApi().log("Checking the configurations correctness.", getSenderName());
      configValidation(deepLearningConfigurations);
      localLearningRate = deepLearningConfigurations.learningRate;
      LocalTime startTime = LocalTime.now();
      if (network.getType() == NeuralNetworkType.FEEDFORWARD)
         network = feedforwardLearning(teachingData, network, deepLearningConfigurations);
      else throw new DeepLearningException("Algorithm can teach feedforward network. " +
         "For another types use another algorithms.");
      prebiotic.prebioticSystemApi().log("Finishing. Returning a neural network.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      prebiotic.prebioticSystemApi().log("Amount of iterations: " + amountOfIterations, getSenderName());
      return network;
   }

   private NeuralNetwork feedforwardLearning(TeachingData teachingData,
                                             NeuralNetwork network,
                                             DeepLearningConfigurations config)
      throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException {
      Random r = new Random();
      Double minDefect = Double.MAX_VALUE;
      int iteration = 0;
      boolean condition = false;
      for (; !condition; iteration++) {
         condition = config.iterations != null && config.minimalDefect != null ?
            iteration > config.iterations || minDefect < config.minimalDefect :
            config.iterations != null ?
               iteration > config.iterations :
               minDefect < config.minimalDefect;
         if (config.delay > 0) {
            try {
               Thread.sleep(config.delay);
            } catch (InterruptedException e) {
               prebiotic.prebioticSystemApi().log(e);
            }
         }
         TeachingDataPackage tp = teachingData.getDataPackages()
            .get(r.nextInt(teachingData.getDataPackages().size()));
         List<Double> expectedOuts = tp.expectedOuts;
         List<Double> outs = network.f(tp.ins);

         Double distance = EuclideanDistance.between(expectedOuts, outs);
         if (distance < minDefect)
            minDefect = distance;
         setDefectsToNeurons(expectedOuts, outs, network);
         correctWeightsOfNeurons(tp, network, config);
         prebiotic.visionApi().repaintAll();
         if (config.reducedLearningRate != null) {
            localLearningRate = reduceLearningRate(
               config.iterations != null ?
                  iteration :
                  1 / Math.abs((minDefect - config.minimalDefect) + 0.01),
               config.learningRate,
               config.iterations != null ?
                  config.iterations :
                  1 / config.minimalDefect,
               config.reducedLearningRate,
               config.speedOfReducing);
         }
         if (iteration % config.iterationsDisplaying == 0) {
            String iterationInformation;
            if (config.iterations == null)
               iterationInformation = "Itearion " + iteration + " : min defect: " + roundDefect(minDefect) +
                  " : to needed min defect: "
                  + roundDefect(Math.abs(minDefect - config.minimalDefect));
            else
               iterationInformation = "Itearion " + iteration + "/"
                  + config.iterations + " : min defect: " + minDefect;
            if (config.reducedLearningRate != null) {
               double round = localLearningRate * 100000;
               round = Math.round(round);
               round /= 100000;
               iterationInformation += ", learning rate: " + round;
            }
            prebiotic.prebioticSystemApi().log(iterationInformation, getSenderName());
         }
      }
      amountOfIterations = iteration;
      return network;
   }

   private double reduceLearningRate(double iteration, double learningRate,
                                     double iterations, double reducedLearningRate,
                                     double speedOfReducing) {
      return ((1 / (10000 * speedOfReducing)) * iterations) /
         (iteration + ((1 / (10000 * speedOfReducing)) * iterations) / (learningRate - reducedLearningRate))
         + reducedLearningRate;
   }

   private void correctWeightsOfNeurons(TeachingDataPackage tp, NeuralNetwork network,
                                        DeepLearningConfigurations deepLearningConfigurations) {
      NeuralLayer inputLayer = network.getInputLayer();
      for (int i = 0; i < inputLayer.size(); i++) {
         Neuron inputNeuron = inputLayer.getNeurons().get(i);
         for (int j = 0; j < inputNeuron.getWeights().size(); j++) {
            WeightFunction weight = inputNeuron.getWeights().get(j);
            Double internalInput = j == tp.ins.size() ? TechnicalConstants.BIAS : tp.ins.get(j);
            weight.deepLearningCorrection(localLearningRate,
               inputNeuron.getDefect(), inputNeuron.getActualOutputFromCore(), network.getAxonFunction(),
               internalInput, deepLearningConfigurations.momentum);
         }
      }

      for (NeuralLayer layer : network.getLayers()) {
         if (!layer.isInputLayer())
            for (Neuron neuron : layer.getNeurons()) {
               for (int i = 0; i < neuron.getWeights().size(); i++) {
                  WeightFunction weight = neuron.getWeights().get(i);
                  weight.deepLearningCorrection(localLearningRate,
                     neuron.getDefect(), neuron.getActualOutputFromCore(), network.getAxonFunction(),
                     neuron.getActualInputs().get(i), deepLearningConfigurations.momentum);
                  if (deepLearningConfigurations.randomValuesRange != null) {
                     List<Double> factors = weight.getFactors();
                     for (int v = 0; v < factors.size(); v++) {
                        factors.set(v, factors.get(v) + generateRandomValue(deepLearningConfigurations.randomValuesRange));
                     }
                  }
               }
            }
      }
   }

   private double generateRandomValue(Range randomValuesRange) {
      Random random = new Random();
      return randomValuesRange.getTo()
         - (Math.abs(randomValuesRange.getTo()) + Math.abs(randomValuesRange.getTo())) * random.nextDouble();
   }

   private void setDefectsToNeurons(List<Double> expectedOuts, List<Double> outs,
                                    NeuralNetwork network) {
      NeuralLayer outputLayer = network.getOutputLayer();
      for (int i = 0; i < outs.size(); i++) {
         outputLayer.getNeurons().get(i).setDefect(expectedOuts.get(i) - outs.get(i));
      }

      NeuralLayer nextLayer = outputLayer;
      for (int i = network.getLayers().size() - 2; i >= 0; i--) {
         NeuralLayer hiddenLayer = network.getLayers().get(i);
         for (int j = 0; j < hiddenLayer.getNeurons().size(); j++) {
            Neuron neuron = hiddenLayer.getNeurons().get(j);
            Double defect = 0.0;
            for (Neuron nextNeuron : nextLayer.getNeurons()) {
               if (network.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR)
                  defect += nextNeuron.getDefect() * nextNeuron.getWeights().get(j).getFactors().get(0);
            }
            neuron.setDefect(defect);
         }
         nextLayer = hiddenLayer;
      }
   }

   private void configValidation(DeepLearningConfigurations deepLearningConfigurations) throws DeepLearningConfigurationsException {
      if (deepLearningConfigurations.learningRate == null)
         throw new DeepLearningConfigurationsException("Learning rate was not set");
      if (deepLearningConfigurations.iterations == null && deepLearningConfigurations.minimalDefect == null)
         throw new DeepLearningConfigurationsException("Number of iterations and minimal defect was not set. " +
            "At least one value should be chosen.");
      if (deepLearningConfigurations.learningRate < 0 || deepLearningConfigurations.learningRate > 1)
         throw new DeepLearningConfigurationsException("Learning rate should be from interval 0.0 - 1.0");
      if (deepLearningConfigurations.iterations != null)
         if (deepLearningConfigurations.iterations < 1)
            throw new DeepLearningConfigurationsException("Amount of iterations should be bigger than 0");
      if (deepLearningConfigurations.iterationsDisplaying == null) {
         int x = 1000;
         prebiotic.prebioticSystemApi().logWarn("WARNING: Iterations displaying interval parameter not set. Setting to " + x, getSenderName());
         deepLearningConfigurations.iterationsDisplaying = x;
      }
      if (deepLearningConfigurations.iterationsDisplaying < 1) {
         int x = 10;
         prebiotic.prebioticSystemApi().logWarn("WARNING: Iterations displaying interval has wrong value. Setting to " + x, getSenderName());
         deepLearningConfigurations.iterationsDisplaying = x;
      }
      if (deepLearningConfigurations.minimalDefect != null)
         if (deepLearningConfigurations.minimalDefect.equals(0.0))
            prebiotic.prebioticSystemApi().logWarn("WARNING: Minimal defect was set to zero. Please pay attention that " +
               "learning process may take infinite time", getSenderName());
      if (deepLearningConfigurations.reducedLearningRate != null) {
         if (deepLearningConfigurations.reducedLearningRate < 0 || deepLearningConfigurations.reducedLearningRate > 1)
            throw new DeepLearningConfigurationsException("Reduced learning rate should be from interval 0.0 - 1.0");
         if (deepLearningConfigurations.learningRate < deepLearningConfigurations.reducedLearningRate)
            throw new DeepLearningConfigurationsException("Reduced learning rate should be less than base learning rate.");
         if (deepLearningConfigurations.speedOfReducing == null)
            throw new DeepLearningConfigurationsException("Reduced learning rate was set, so the speed of reducing should be set too.");
         if (deepLearningConfigurations.speedOfReducing < 0 || deepLearningConfigurations.speedOfReducing > 1)
            throw new DeepLearningConfigurationsException("Speed of reducing the learning rate should be from interval 0.0 - 1.0");
      }
   }

   private double roundDefect(double defect) {
      defect *= 100000000;
      defect = Math.round(defect);
      defect /= 100000000;
      return defect;
   }

   @Override
   protected String getSenderName() {
      return "DEEP-LRN";
   }
}
