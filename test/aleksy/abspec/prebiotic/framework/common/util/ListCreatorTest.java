package aleksy.abspec.prebiotic.framework.common.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ListCreatorTest {

   @Test
   public void shouldCreateList() {
      // given
      Double[] values = new Double[] {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
      List<Double> list;

      // when
      list = ListCreator.createFrom(values[0], values[1], values[2], values[3], values[4], values[5]);

      // then
      assertEquals(ArrayList.class, list.getClass());
      assertEquals(list.get(0), values[0]);
      assertEquals(list.get(1), values[1]);
      assertEquals(list.get(2), values[2]);
      assertEquals(list.get(3), values[3]);
      assertEquals(list.get(4), values[4]);
      assertEquals(list.get(5), values[5]);
   }
}
