package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype;

import java.util.ArrayList;
import java.util.List;

/**
 * Genotype class
 */
public class Genotype {
   /**
    * List of chromosomes
    */
   private List<Chromosome> chromosomes;

   /**
    * Constructor
    */
   public Genotype() {
      chromosomes = new ArrayList<>();
   }

   /**
    * Constructor
    *
    * @param genotype to set
    */
   public Genotype(Genotype genotype) {
      chromosomes = new ArrayList<>();
      for (Chromosome chromosome : genotype.chromosomes) {
         chromosomes.add(new Chromosome(chromosome));
      }
   }

   /**
    * Getter for chromosomes
    *
    * @return list of chromosomes
    */
   public List<Chromosome> getChromosomes() {
      return chromosomes;
   }

   /**
    * Setter for chromosomes
    *
    * @param chromosomes - list of chromosomes to set
    */
   public void setChromosomes(List<Chromosome> chromosomes) {
      this.chromosomes = chromosomes;
   }

   /**
    * Amount of chromosomes
    *
    * @return amount of chromosomes
    */
   public int size() {
      return chromosomes.size();
   }

   /**
    * Amount of genes of all chromosomes
    *
    * @return amount of genes
    */
   public int amountOfGenes() {
      int amount = 0;
      for (Chromosome chromosome : chromosomes) {
         amount += chromosome.size();
      }
      return amount;
   }

   /**
    * Creates list of all genes
    *
    * @return all genes
    */
   public List<Gene> getAllGenes() {
      List<Gene> list = new ArrayList<>();
      for (Chromosome chromosome : chromosomes) {
         list.addAll(chromosome.getGenes());
      }
      return list;
   }

   /**
    * Finds chromosome by the description
    *
    * @param description of chromosome
    * @return chromosome or null if chromosome not exists
    */
   public Chromosome findChromosome(String description) {
      for (Chromosome chromosome : chromosomes) {
         if (chromosome.getDescription().equals(description)) {
            return chromosome;
         }
      }
      return null;
   }

   /**
    * Finds all chromosomes which contains pattern
    *
    * @param pattern which chromosomes contains
    * @return chromosomes
    */
   public List<Chromosome> findChromosomes(String pattern) {
      List<Chromosome> found = new ArrayList<>();
      for (Chromosome chromosome : chromosomes) {
         if (chromosome.getDescription().contains(pattern)) {
            found.add(chromosome);
         }
      }
      return found;
   }

   /**
    * Finds gene value by the string localization. Localization should
    * contains name of chromosome and name of gene with colon.
    * Example: chromosome:gene
    *
    * @param localization of gene in genotype
    * @return found gene value
    */
   public Double findGeneValue(String localization) {
      String[] split = localization.split(":");
      return findChromosome(split[0]).findGene(split[1]).getValue();
   }


   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("genotype:\n");
      for (Chromosome chromosome : chromosomes)
         sb.append(chromosome.toString());
      return sb.toString();
   }
}
