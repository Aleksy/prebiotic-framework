# Changelog
    
**[2.0.0](https://drive.google.com/open?id=13wOJSPIXgiqeNpUvdEU2lZKHJcJ3ykI_)** *29.04.19 "Package naming update"*  
  
*Added*  

- New option to create a Range object: Range.from() (old one is deprecated now)   
  
*Changed*  
   
- Package name: aleksy.abspec.prebiotic.framework   
   
  
Code quality info:  
Tested classes: 82%  
Tested methods: 69%  
Line coverage: 58%  

**[1.1.0](https://drive.google.com/open?id=16HUwo7LA_VAHlnUUxJwjLB0KnaxbCHIP)** *21.07.18 "Advanced deep learning update"*  
  
*Added*  
  
- Deep learning advanced parameters: momentum, random values adding, reducing the learning rate   
- Default configurations method to the deep learning configurations builder   
- Deep learning and Kohonen learning can be stopped by minimal needed defect, iterations number is not needed, but can be used   
- Parsing the networks to JSON format   
- Prebiotic Api can log the informations to the more stream, like files etc.   
- New displayable state graphs to the Vision Api: ativity round, activity time, input round   
  
*Changed*  
   
- Coordinate system displays the networks in 4 dimensions (represented by width, height, size and shape)   
   
*Fixed*  
  
- Builder of custom functions for parser can be called from api   
- Square structure displaying of the single-layer network works properly now   
  
Code quality info:  
Tested classes: 82%  
Tested methods: 69%  
Line coverage: 58%  
   
**[1.0.2](https://drive.google.com/open?id=1FQrpBVA6SeEQ-Y76OHcKdY-2Zq4FZ0oQ)** *6.07.18 "Bugfix"*  
  
*Deleted*  
  
- Logo from the background of the status panels from Vision Api is deleted
  
Code quality info:  
Tested classes: 84%  
Tested methods: 69%  
Line coverage: 59%  
       
**[1.0.1](https://drive.google.com/open?id=1MZN02QMh3V5VFLGBTIk0UeJO3uQwhQOQ)** *4.07.18 "Connections display update"*  
  
*Added*  
  
- Displaying the connections between the neurons in the COORDINATE_SYSTEM display mode   
  
*Fixed*  
  
- STRUCTURE_SQUARE display mode for feedforward neural networks works properly   
- For STRUCTURE_SQUARE mode also was changed the colors normalization in the last layer   
  
Code quality info:  
Tested classes: 84%  
Tested methods: 69%  
Line coverage: 59%  
   
**[1.0.0](https://drive.google.com/open?id=18kFJRVuqAIPdiHN2aSGKnkCDLmhxuvWQ)** *29.06.18 "Hopfield's networks update"*  
  
*Added*  
  
- Hopfield's neural networks (builders, teaching, classes)  
- Evolution algorithms module (with support of genetic algorithms)   
- Module for displaying the neural networks and the genotypes in the JSwing frames and JPanels   
- List creator util class   
- Delay for the teaching process (useful for neural networks displaying)   
- Prebiotic Neural Network file format. Networks can be saved to byte files   
  
*Changed*  
- Whole frameworks was divided for modules: neural, evolution, prebiotic system and vision. More info on the Wiki   
- Default methods for each neural network type in the structure configurations builder   
- Parser was updated   
  
*Fixed*  
  
- When user set the custom function, that framework don't detect it and logs "null"   
- Hyperbolic axon function returns now Double.MAX_VALUE when arguemnt equals zero   
- Bias weight is not random when randomizeWeights function was not called   
  
Code quality info:  
Tested classes: 84%  
Tested methods: 70%  
Line coverage: 60%  
   
**[3.0.0-alpha](https://drive.google.com/drive/folders/1O4RPhibBM-XECkx94Wnrhh0DlRcUfC-n)** *21.05.18 "Kohonen's map update"*  
  
*Added*  
  
- Kohonen's neural networks (builders, teaching, classes)  
- Algorithms can now display iterations with choosen iterations interval  
- Teaching Data Builder has methods for adding packages for self-learning algorithms  
- There's now possibility to create custom functions. Parser not supports parsing networks with custom model from xml file yet.  
  
*Changed*  
- Name of framework was changed from GENEURAL (which is already in use in another, not mine project), to PREBIOTIC.  
- Facade can log warnings  
- Work with model functions of neurons  
- Way of randomizing weights  
- default config method in structure builder generate configurations for different types of networks  
- Facade generates more logs  
  
*Fixed*  
  
- Description of layers and neurons support  
- Axon SINH was changed to SIGMOID (that's better axon model, sinh is unstable :) )  
  
Code quality info:  
Tested classes: 82%  
Tested methods: 59%  
Line coverage: 50%  
  
**[2.0.1-alpha](https://drive.google.com/file/d/1reyp9D5AloXyvrcLYdHwU0_2RJlP6xQE/view?usp=sharing)** *11.05.18 "Big test update"*  
  
*Added*  
  
- lot's of tests to make code more qualitative  
  
*Changed*  
  
- Not implemented networks types are not available now  
  
*Fixed*  
  
- error in deep learning, first layer weights were updated two times  
- building single layer network by one method prevented randomizing the weights  
- cleaning stuff  
  
Code quality info:  
Tested classes: 80%  
Tested methods: 65%  
Line coverage: 52%  
  
**[2.0.0-alpha](https://drive.google.com/file/d/1Ykibx9PpIoSaSxnsYBtaSbQ5Zck1HlSQ/view?usp=sharing)** *26.04.18 "First documented update"*  
  
  *Added*  
- General documentation (java docs)  
- Data validation in deep learning algorithm  
- Data normalization / scaling types  
- Time measurement in algorithms  
  
  *Changed*  
- Now message logging permission is assigned to the GeneuralFacadeInterface instance.  
- Deep learning algorithm shows more console data  
- Setting message logging to true shows basic informations about Geneural Framework.  
  
  *Fixed*  
- Neuron shows informations about difference between input vector size and expected
input vector size when exception is thrown.  
  
**1.0.0-alpha**  
- basic api defined  
- internal documentation and wiki added  
- logo presentation in logger added  
- last bugs from prealpha fixed  
- from now I try to keep semantic versioning :)  
  
**0.2.0-prealpha**  
- global message logging configuration added  
  
**0.1.1-prealpha**  
- parser added  
- deep learning algorithm added  
  
**0.1.0-prealpha**  
- genetic algorithm added  
  
**0.0.1-prealpha**  
- initialization of basic components  
- basic buildings of networks  
