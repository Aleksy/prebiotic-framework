package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class GenotypeTest {
   private Genotype uut;

   @Before
   public void init() {

      uut = Prebiotic.newInstance().evolutionApi().getGenotypeBuilder()
         .addChromosome("chr1")
         .addGene(0.0, Range.from(0.0, 1.0), "gene1")
         .end()
         .addChromosome("chr2")
         .addGene(1.0, Range.from(0.5, 1.0), "gene2")
         .addGene(0.5, Range.from(0.1, 0.7), "gene3")
         .end()
         .create();
   }

   @Test
   public void shouldCorrectlyReturnTheSizeOfGenotype() {
      // when
      int size = uut.size();

      // then
      assertEquals(2, size);
   }

   @Test
   public void shouldCorrectlyReturnAmountOfGenesInGenotype() {
      // when
      int amountOfGenes = uut.amountOfGenes();

      // then
      assertEquals(3, amountOfGenes);
   }

   @Test
   public void shouldReturnAllGenesInOneList() {
      // given
      List<Gene> genes;

      // when
      genes = uut.getAllGenes();

      // then
      assertEquals(3, genes.size());
      assertEquals(new Double(0.0), new Double(genes.get(0).getValue()));
      assertEquals(new Double(1.0), new Double(genes.get(1).getValue()));
      assertEquals(new Double(0.5), new Double(genes.get(2).getValue()));
   }

   @Test
   public void shouldFindChromosome() {
      // when
      Chromosome chr = uut.findChromosome("chr1");

      // then
      assertEquals(1, chr.size());
      assertEquals("chr1", chr.getDescription());
   }

   @Test
   public void shouldFindChromosomes() {
      // when
      List<Chromosome> chromosomes = uut.findChromosomes("ch");

      // then
      assertEquals(2, chromosomes.size());
   }

   @Test
   public void shouldFindGeneValue() {
      // given
      String genePath = "chr2:gene2";

      // when
      Double geneValue = uut.findGeneValue(genePath);

      // then
      assertEquals(new Double(1.0), geneValue);
   }
}
