package aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractWeightMatrixNeuralPanel;

import java.awt.*;
import java.util.List;

public class HopfieldWeightMatrixNeuralPanel extends AbstractWeightMatrixNeuralPanel {
   public HopfieldWeightMatrixNeuralPanel(NeuralNetwork nn, Double hue, Range inpRange) {
      super(nn, hue, inpRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      double maxAngle = 2 * Math.PI;
      double distance = maxAngle / nn.size();
      double xStep = getSize().width * 0.33;
      double yStep = getSize().height * 0.33;
      int yOffset = (int) (getSize().height * 0.02);
      List<Neuron> neurons = nn.getAllNeurons();

      hopfieldSkeleton(neurons, distance, xStep, yStep, yOffset, g);

      for (int i = 0; i < nn.size(); i++) {
         int x = (int) (getSize().width / 2 + Math.sin(i * distance) * xStep);
         int y = (int) (getSize().height / 2 + Math.cos(i * distance) * yStep);
         if (neurons.get(i).getActualAxonOutput() != null)
            g.setColor(createColor(VectorNormalizator.setValueBetweenZeroAndOne(
               neurons.get(i).getActualAxonOutput(), inputRange.getFrom(), inputRange.getTo()
            )));
         else
            g.setColor(createColor(1.0));
         if (neurons.get(i).getActualAxonOutput() != null)
            drawWeightMatrixNeuron(neurons.get(i).getVectorOfWeights(),
               neurons.get(i).getActualAxonOutput(), x, y + yOffset, g);
         else
            drawWeightMatrixNeuron(neurons.get(i).getVectorOfWeights(),
               0.0, x, y + yOffset, g);
      }
   }
}
