package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChromosomeTest {
   private Chromosome uut;

   @Before
   public void init() {
      uut = Prebiotic.newInstance().evolutionApi().getGenotypeBuilder()
         .addChromosome("chr1")
         .addGene(0.0, Range.from(0.0, 1.0), "gene1")
         .addGene(0.5, Range.from(0.0, 1.0), "gene2")
         .addGene(1.0, Range.from(0.0, 1.0), "gene3")
         .end()
         .create().getChromosomes().get(0);
   }

   @Test
   public void shouldFindGene() {
      // when
      Gene gene = uut.findGene("gene2");

      // then
      assertEquals(new Double(0.5), new Double(gene.getValue()));
      assertEquals(new Double(0.0), new Double(gene.getValueRange().from));
      assertEquals(new Double(1.0), new Double(gene.getValueRange().to));
      assertEquals("gene2", gene.getDescription());
   }

   @Test
   public void shouldCorrectlyReturnAmountOfGenes() {
      // when
      int size = uut.size();

      // then
      assertEquals(3, size);
   }
}
