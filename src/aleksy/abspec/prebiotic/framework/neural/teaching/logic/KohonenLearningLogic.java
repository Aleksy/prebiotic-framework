package aleksy.abspec.prebiotic.framework.neural.teaching.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.common.util.EuclideanDistance;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.KohonenNeuronImpl;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningConfigurationsException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingDataPackage;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class KohonenLearningLogic extends Loggable {

   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public KohonenLearningLogic(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }


   public NeuralNetwork start(TeachingData teachingData,
                              NeuralNetwork network,
                              KohonenLearningConfigurations config)
      throws KohonenLearningException, IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException {
      prebiotic.prebioticSystemApi().log("Kohonen map learning starting.", getSenderName());
      prebiotic.prebioticSystemApi().log("Checking the configurations correctness.", getSenderName());
      configValidation(config);
      LocalTime startTime = LocalTime.now();
      if (network.getType() == NeuralNetworkType.KOHONEN)
         network = kohonenLearning(teachingData, network, config);
      else
         throw new KohonenLearningException("Algorithm can teach only Kohonen'TeachingDataManagementComposite network. " +
            "For another types use another algorithms.");
      prebiotic.prebioticSystemApi().log("Finishing. Returning a neural network.", getSenderName());
      LocalTime endTime = LocalTime.now();
      Duration duration = Duration.between(startTime, endTime);
      prebiotic.prebioticSystemApi().log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
      return network;
   }

   private NeuralNetwork kohonenLearning(TeachingData teachingData,
                                         NeuralNetwork network,
                                         KohonenLearningConfigurations config)
      throws IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException {
      Random r = new Random();
      Double minDefect = Double.MAX_VALUE;
      for (int iteration = 0; iteration < config.iterations; iteration++) {
         if (config.delay > 0) {
            try {
               Thread.sleep(config.delay);
            } catch (InterruptedException e) {
               prebiotic.prebioticSystemApi().log(e);
            }
         }
         TeachingDataPackage tp = teachingData.getDataPackages()
            .get(r.nextInt(teachingData.getDataPackages().size()));
         network.f(tp.ins);
         minDefect = correctAll(network, tp.ins, config, minDefect);

         if (iteration % config.iterationsDisplaying == 0) {
            prebiotic.prebioticSystemApi().log("Iteration " + iteration + "/"
               + config.iterations + " : min distance between neuron weights " +
               "vector and input vector: " + minDefect, getSenderName());
         }
      }
      return network;
   }


   private Double correctAll(NeuralNetwork network, List<Double> inputs,
                             KohonenLearningConfigurations kohonenLearningConfigurations, Double minDefect)
      throws IncorrectNumberOfInputsInNeuronException {
      NeuralLayer map = network.getInputLayer();
      Neuron bestNeuron = map.getNeurons().get(0);
      for (Neuron neuron : map.getNeurons()) {
         double bestAnswer = bestNeuron.f(inputs);
         double neuronAnswer = neuron.f(inputs);
         if (neuronAnswer > bestAnswer)
            bestNeuron = neuron;
      }
      KohonenNeuronImpl bestKohonen = (KohonenNeuronImpl) bestNeuron;
      List<Double> bestNeuronCoords = new ArrayList<>();
      bestNeuronCoords.add((double) bestKohonen.getX());
      bestNeuronCoords.add((double) bestKohonen.getY());
      double maxDistance = kohonenLearningConfigurations.maxDistance;

      for (Neuron neuron : map.getNeurons()) {
         KohonenNeuronImpl kohonen = (KohonenNeuronImpl) neuron;
         List<Double> neuronCoords = new ArrayList<>();
         neuronCoords.add((double) kohonen.getX());
         neuronCoords.add((double) kohonen.getY());
         double distance = EuclideanDistance.between(neuronCoords, bestNeuronCoords);
         if (distance < maxDistance && distance != 0.0) {
            minDefect = correctKohonenWeights(kohonenLearningConfigurations, inputs, distance, neuron, minDefect);
         }
      }
      return minDefect;
   }

   private double correctKohonenWeights(KohonenLearningConfigurations kohonenLearningConfigurations,
                                        List<Double> inputs,
                                        double distance,
                                        Neuron neuron, double minDefect) {
      List<Double> weightVector = new ArrayList<>();
      for (WeightFunction weight : neuron.getWeights()) {
         if (weight.getWeightFunctionRecipe() == WeightFunctionRecipe.SQUARE_DIFFERENCE) {
            weightVector.add(weight.getFactors().get(0));
         }
      }
      double defect = EuclideanDistance.between(inputs, weightVector);
      if (defect < minDefect)
         minDefect = defect;
      for (int i = 0; i < neuron.getWeights().size(); i++) {
         WeightFunction weight = neuron.getWeights().get(i);
         weight.kohonenCorrection(kohonenLearningConfigurations.learningRate, distance,
            neuron.getActualInputs().get(i));
      }
      return minDefect;
   }

   private void configValidation(KohonenLearningConfigurations kohonenLearningConfigurations)
      throws KohonenLearningConfigurationsException {
      if (kohonenLearningConfigurations.learningRate < 0 || kohonenLearningConfigurations.learningRate > 1)
         throw new KohonenLearningConfigurationsException("Learning rate should be from interval 0.0 - 1.0");
      if (kohonenLearningConfigurations.iterations != null)
         if (kohonenLearningConfigurations.iterations < 1)
            throw new KohonenLearningConfigurationsException("Amount of iterations should be bigger than 0.");
      if (kohonenLearningConfigurations.iterations == null && kohonenLearningConfigurations.minimalDefect == null)
         throw new KohonenLearningConfigurationsException("Number of iterations and minimal defect was not set. " +
            "At least one value should be chosen.");
      if (kohonenLearningConfigurations.maxDistance == null)
         throw new KohonenLearningConfigurationsException("<Maximal distance has to be given.");
      if (kohonenLearningConfigurations.maxDistance <= 0)
         throw new KohonenLearningConfigurationsException("Maximal distance should be bigger than 0.");
      if (kohonenLearningConfigurations.iterationsDisplaying == null) {
         int x = 1000;
         prebiotic.prebioticSystemApi().logWarn("WARNING: Iterations displaying interval parameter not set. Setting to " + x + ".", getSenderName());
         kohonenLearningConfigurations.iterationsDisplaying = x;
      }
      if (kohonenLearningConfigurations.iterationsDisplaying < 1) {
         int x = 1000;
         prebiotic.prebioticSystemApi().logWarn("WARNING: Iterations displaying interval has wrong value. Setting to " + x + ".", getSenderName());
         kohonenLearningConfigurations.iterationsDisplaying = x;
      }
   }

   @Override
   protected String getSenderName() {
      return "KOH-LRN";
   }
}
