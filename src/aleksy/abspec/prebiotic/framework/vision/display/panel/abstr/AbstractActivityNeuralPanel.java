package aleksy.abspec.prebiotic.framework.vision.display.panel.abstr;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractActivityNeuralPanel extends AbstractNeuralPanel {

   public AbstractActivityNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }

   private double calculateDistanceForKohonen(NeuralNetwork nn, int index) {
      return VectorNormalizator.scaleToMaxValue(nn.getActualOutput()).get(index);
   }

   private double calculateDistance(Neuron neuron) {
      return VectorNormalizator.setValueBetweenZeroAndOne(neuron.getActualAxonOutput(),
         nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues().getFrom(),
         nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues().getTo());
   }

   protected double calculateDistanceForNetwork(NeuralNetwork neuralNetwork, int index) {
      if (neuralNetwork.getType() == NeuralNetworkType.KOHONEN)
         return calculateDistanceForKohonen(neuralNetwork, index);
      else
         return calculateDistance(neuralNetwork.getAllNeurons().get(index));
   }

   protected void drawRoundActivity(Graphics g, double size, boolean antiColor, boolean inputMode) {
      java.util.List<Polygon> polygons = new ArrayList<>();
      List<Color> polygonColors = new ArrayList<>();
      int sizeOfVector = inputMode ? nn.sizeOfInputVector() : nn.getAllNeurons().size();
      double x = getWidth() / 2;
      double y = getHeight() / 2;
      for (int i = 0; i < sizeOfVector; i++) {
         Polygon polygon = new Polygon();
         double distance = inputMode ? VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(i),
            inputRange.getFrom(), inputRange.getTo()) : calculateDistanceForNetwork(nn, i);
         double angle = (i / (double) sizeOfVector) * 2 * Math.PI;
         double distance2, angle2;
         if (i != sizeOfVector - 1) {
            distance2 = inputMode ? VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(i + 1),
               inputRange.getFrom(), inputRange.getTo()) : calculateDistanceForNetwork(nn, i + 1);
            angle2 = ((i + 1) / (double) sizeOfVector) * 2 * Math.PI;
         } else {
            distance2 = inputMode ? VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(0),
               inputRange.getFrom(), inputRange.getTo()) : calculateDistanceForNetwork(nn, 0);
            angle2 = 0.0;
         }
         polygon.addPoint((int) (x + 2 + Math.sin(angle) * getWidth() * size * (0.5 - 0.67 * (-distance))),
            (int) (y + 2 + Math.cos(angle) * getHeight() * size * (0.5 - 0.67 * (-distance))));
         polygon.addPoint((int) (x + 2 + Math.sin(angle2) * getWidth() * size * (0.5 - 0.67 * (-distance2))),
            (int) (y + 2 + Math.cos(angle2) * getHeight() * size * (0.5 - 0.67 * (-distance2))));
         polygon.addPoint((int) x, (int) y);
         polygons.add(polygon);
         polygonColors.add(antiColor ? createAntiColor((distance + distance2) / 2.0).darker() : createColor((distance + distance2) / 2.0).darker());
      }
      for (int i = 0; i < polygons.size(); i++) {
         g.setColor(polygonColors.get(i));
         g.fillPolygon(polygons.get(i));
      }
      for (int i = 0; i < sizeOfVector; i++) {
         double distance = inputMode ? VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(i),
            inputRange.getFrom(), inputRange.getTo()) : calculateDistanceForNetwork(nn, i);
         double angle = (i / (double) sizeOfVector) * 2 * Math.PI;
         g.setColor(antiColor ? createAntiColor(distance * 0.87) : createColor(distance * 0.87));
         g.fillOval((int) (x + Math.sin(angle) * getWidth() * size * (0.5 - 0.67 * (-distance))),
            (int) (y + Math.cos(angle) * getHeight() * size * (0.5 - 0.67 * (-distance))), 5, 5);
         g.setColor(antiColor ? createAntiColor(distance) : createColor(distance));
         g.drawOval((int) (x + Math.sin(angle) * getWidth() * size * (0.5 - 0.67 * (-distance))),
            (int) (y + Math.cos(angle) * getHeight() * size * (0.5 - 0.67 * (-distance))), 5, 5);
         g.setColor(antiColor ? createAntiColor(distance * 0.7) : createColor(distance * 0.7));
         if (i != sizeOfVector - 1) {
            double distance2 = inputMode ? VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(i + 1),
               inputRange.getFrom(), inputRange.getTo()) : calculateDistanceForNetwork(nn, i + 1);
            double angle2 = ((i + 1) / (double) sizeOfVector) * 2 * Math.PI;
            g.drawLine((int) (x + 2 + Math.sin(angle) * getWidth() * size * (0.5 - 0.67 * (-distance))),
               (int) (y + 2 + Math.cos(angle) * getHeight() * size * (0.5 - 0.67 * (-distance))),
               (int) (x + 2 + Math.sin(angle2) * getWidth() * size * (0.5 - 0.67 * (-distance2))),
               (int) (y + 2 + Math.cos(angle2) * getHeight() * size * (0.5 - 0.67 * (-distance2))));
         } else {
            double distance2 = inputMode ? VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(0),
               inputRange.getFrom(), inputRange.getTo()) : calculateDistanceForNetwork(nn, 0);
            double angle2 = 0.0;
            g.drawLine((int) (x + 2 + Math.sin(angle) * getWidth() * size * (0.5 - 0.67 * (-distance))),
               (int) (y + 2 + Math.cos(angle) * getHeight() * size * (0.5 - 0.67 * (-distance))),
               (int) (x + 2 + Math.sin(angle2) * getWidth() * size * (0.5 - 0.67 * (-distance2))),
               (int) (y + 2 + Math.cos(angle2) * getHeight() * size * (0.5 - 0.67 * (-distance2))));
         }
         g.setColor(antiColor ? createAntiColor(distance * 0.7) : createColor(distance * 0.7));
         g.drawLine((int) (x + Math.sin(angle) * getWidth() * size * (0.5 - 0.67 * (-distance))),
            (int) (y + Math.cos(angle) * getHeight() * size * (0.5 - 0.67 * (-distance))), (int) x, (int) y);

      }
   }
}
