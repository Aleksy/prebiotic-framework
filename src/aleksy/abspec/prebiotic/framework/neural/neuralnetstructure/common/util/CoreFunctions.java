package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.CoreFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AbstractCoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.Function;

import java.util.List;

/**
 * Util class for core functions
 */
public class CoreFunctions {
   public static class DEFAULT_ADDER extends AbstractCoreFunction {
      public DEFAULT_ADDER() {
         setCoreFunctionRecipe(CoreFunctionRecipe.DEFAULT_ADDER);
      }

      @Override
      public Double f(List<Double> xs) {
         Double sum = 0.0;
         for (Double x : xs)
            sum += x;
         return sum;
      }

      @Override
      public Function newInstance() {
         return new DEFAULT_ADDER();
      }
   }
}
