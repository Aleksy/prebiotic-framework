package aleksy.abspec.prebiotic.framework.common.impl;

import aleksy.abspec.prebiotic.framework.common.constants.BasicConstants;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.evolution.common.api.EvolutionApi;
import aleksy.abspec.prebiotic.framework.evolution.common.impl.Evolution;
import aleksy.abspec.prebiotic.framework.neural.common.api.NeuralApi;
import aleksy.abspec.prebiotic.framework.neural.common.impl.Neural;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.api.PrebioticSystemApi;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.impl.PrebioticSystem;
import aleksy.abspec.prebiotic.framework.vision.common.api.VisionApi;
import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Basic implementation of Prebiotic Framework Api
 */
public class Prebiotic extends Loggable implements PrebioticApi {
   private boolean eventLogging;
   private NeuralApi neural;
   private EvolutionApi evolution;
   private VisionApi vision;
   private PrebioticSystemApi prebioticSystem;
   private List<PrintStream> printStreams;

   @Override
   public String getVersion() {
      return BasicConstants.VERSION;
   }

   @Override
   public String getFrameworkName() {
      return BasicConstants.NAME;
   }

   @Override
   public String getAuthor() {
      return BasicConstants.AUTHOR;
   }

   @Override
   public void enableEventLogging() {
      this.eventLogging = true;
      prebioticSystem.logo();
      prebioticSystem.log("Event logging: true", getSenderName());
   }

   @Override
   public void disableEventLogging() {
      this.eventLogging = false;
   }

   @Override
   public void setOnePrintStream(PrintStream printStream) {
      this.printStreams = new ArrayList<>(Collections.singletonList(printStream));
      prebioticSystem.log("One print stream has been chosen", getSenderName());
   }

   @Override
   public List<PrintStream> getPrintStreams() {
      return printStreams;
   }

   @Override
   public void addPrintStream(PrintStream printStream) {
      printStreams.add(printStream);
      prebioticSystem.log("New print stream has been added", getSenderName());
   }

   @Override
   public void removePrintStream(PrintStream printStream) {
      if (printStreams.size() != 1) {
         boolean removed = printStreams.remove(printStream);
         if (removed)
            prebioticSystem.log("Existing print stream has been removed", getSenderName());
         else
            prebioticSystem.log("No print stream to remove has been found", getSenderName());
      } else {
         prebioticSystem.log("Instance has only one stream. Cannot remove");
      }
   }

   @Override
   public boolean isEventLoggingEnabled() {
      return eventLogging;
   }

   @Override
   public NeuralApi neuralApi() {
      return neural;
   }

   @Override
   public EvolutionApi evolutionApi() {
      return evolution;
   }

   @Override
   public VisionApi visionApi() {
      return vision;
   }

   @Override
   public PrebioticSystemApi prebioticSystemApi() {
      return prebioticSystem;
   }

   /**
    * Method creates new instance of Prebiotic facade ready to work
    *
    * @return new instance of Prebiotic facade
    */
   public static PrebioticApi newInstance() {
      Prebiotic prebiotic = new Prebiotic();
      prebiotic.eventLogging = false;
      prebiotic.neural = new Neural(prebiotic);
      prebiotic.evolution = new Evolution(prebiotic);
      prebiotic.vision = new Vision();
      prebiotic.prebioticSystem = new PrebioticSystem(prebiotic);
      prebiotic.setOnePrintStream(System.out);
      return prebiotic;
   }

   @Override
   protected String getSenderName() {
      return "API";
   }

}
