package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.exception;

import aleksy.abspec.prebiotic.framework.common.exception.PrebioticException;

/**
 * Wrong PNN file version exception
 */
public class WrongPnnFileVersionException extends PrebioticException {

   public WrongPnnFileVersionException(String message) {
      super("Wrong PNN file version exception. " + message);
   }

}
