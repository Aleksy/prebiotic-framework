package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AbstractAxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;

/**
 * Util class for axon functions
 */
public class AxonFunctions {
   public static class TANH extends AbstractAxonFunction {
      public TANH() {
         setAxonFunctionRecipe(AxonFunctionRecipe.TANH);
      }

      @Override
      public Double f(Double x) {
         return Math.tanh(x);
      }

      @Override
      public Double derivative(Double x) {
         return 1.0 / (Math.cosh(x) * Math.cosh(x));
      }

      @Override
      public AxonFunction newInstance() {
         return new TANH();
      }
   }

   public static class LINEAR extends AbstractAxonFunction {
      public LINEAR() {
         setAxonFunctionRecipe(AxonFunctionRecipe.LINEAR);
      }

      @Override
      public Double f(Double x) {
         return x;
      }

      @Override
      public Double derivative(Double x) {
         return 1.0;
      }

      @Override
      public AxonFunction newInstance() {
         return new LINEAR();
      }
   }

   public static class THRESHOLD extends AbstractAxonFunction {
      public THRESHOLD() {
         setAxonFunctionRecipe(AxonFunctionRecipe.THRESHOLD);
      }

      @Override
      public Double f(Double x) {
         if (x > 0.0)
            return 1.0;
         else
            return 0.0;
      }

      @Override
      public Double derivative(Double x) {
         return 0.0;
      }

      @Override
      public AxonFunction newInstance() {
         return new THRESHOLD();
      }
   }

   public static class HOPFIELD_THRESHOLD extends AbstractAxonFunction {
      private Double actualOutput;

      public HOPFIELD_THRESHOLD() {
         setAxonFunctionRecipe(AxonFunctionRecipe.HOPFIELD_THRESHOLD);
         actualOutput = 0.0;
      }

      public void setActualOutput(Double actualOutput) {
         this.actualOutput = actualOutput;
      }

      @Override
      public Double f(Double x) {
         if (x == 0.0)
            return actualOutput;
         if (x > 0.0)
            return 1.0;
         if (x < 0.0)
            return -1.0;
         return null;
      }

      @Override
      public Double derivative(Double x) {
         return 0.0;
      }

      @Override
      public AxonFunction newInstance() {
         return new HOPFIELD_THRESHOLD();
      }
   }

   public static class HOPFIELD_SIGMOID extends AbstractAxonFunction {

      public HOPFIELD_SIGMOID() {
         setAxonFunctionRecipe(AxonFunctionRecipe.HOPFIELD_SIGMOID);
      }

      @Override
      public Double f(Double x) {
         return 1 / (1 + Math.exp(-200 * x));
      }

      @Override
      public Double derivative(Double x) {
         return null;
      }

      @Override
      public AxonFunction newInstance() {
         return new HOPFIELD_THRESHOLD();
      }
   }

   public static class HOPFIELD_TANH extends AbstractAxonFunction {
      public HOPFIELD_TANH() {
         setAxonFunctionRecipe(AxonFunctionRecipe.HOPFIELD_TANH);
      }

      @Override
      public Double f(Double x) {
         return Math.tanh(100 * x);
      }

      @Override
      public Double derivative(Double x) {
         return 1.0 / (Math.cosh(100 * x) * Math.cosh(100 * x));
      }

      @Override
      public AxonFunction newInstance() {
         return new TANH();
      }
   }

   public static class SIGMOID extends AbstractAxonFunction {
      public SIGMOID() {
         setAxonFunctionRecipe(AxonFunctionRecipe.SIGMOID);
      }

      @Override
      public Double f(Double x) {
         return 1 / (1 + Math.exp(-x));
      }

      @Override
      public Double derivative(Double x) {
         return Math.exp(-x) / Math.pow((Math.exp(-x) + 1), 2);
      }

      @Override
      public AxonFunction newInstance() {
         return new SIGMOID();
      }
   }

   public static class HYPERBOLIC extends AbstractAxonFunction {
      public HYPERBOLIC() {
         setAxonFunctionRecipe(AxonFunctionRecipe.HYPERBOLIC);
      }

      @Override
      public Double f(Double x) {
         if (x != 0)
            return 1 / x;
         else
            return Double.MAX_VALUE;
      }

      @Override
      public Double derivative(Double x) {
         return -1 / (x * x);
      }

      @Override
      public AxonFunction newInstance() {
         return new HYPERBOLIC();
      }
   }
}
