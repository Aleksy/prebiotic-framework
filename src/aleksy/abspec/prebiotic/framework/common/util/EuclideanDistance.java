package aleksy.abspec.prebiotic.framework.common.util;

import java.util.List;

/**
 * Util class for calculating an euclidean distance
 */
public class EuclideanDistance {
   /**
    * Calculates an euclidean distance between two vectors.
    * Method is prepared for vectors which sizes are equals
    *
    * @param v1 vector of doubles
    * @param v2 vector of doubles
    * @return distance between v1 and v2
    */
   public static double between(List<Double> v1, List<Double> v2) {
      Double d = 0.0;
      for (int i = 0; i < v2.size(); i++) {
         d += Math.pow(v1.get(i) - v2.get(i), 2);
      }
      return Math.sqrt(d);
   }

   /**
    * Calculates an euclidean distance of vector to point 0.
    * If vector has N-size, that the point has N zeros.
    *
    * @param v vector of doubles
    * @return distance between vector and point 0
    */
   public static double toZero(List<Double> v) {
      Double d = 0.0;
      for (Double aV : v) {
         d += Math.pow(aV, 2);
      }
      return Math.sqrt(d);
   }
}
