package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.KohonenNeuronImpl;

import java.util.List;

/**
 * Kohonen's neural network interface
 */
public interface KohonenNeuralNetwork extends NeuralNetwork {
   /**
    * Calculates output for given input vector for each neuron and finds the best
    * (with the highest output value).
    *
    * @param xs input vector
    * @return best neuron (neuron which weight vector is nearest the input vector)
    * @throws IncorrectNumberOfInputsInNeuronException when number of input vector values is incorrect
    */
   KohonenNeuronImpl fBestNeuron(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException;

   /**
    * Returns the vector of weights of neuron
    *
    * @param x coordinate
    * @param y coordinate
    * @return vector of weights of neuron on coordinates given in parameters
    */
   List<Double> getVectorOfWeightsOfNeuron(int x, int y);

   /**
    * Finds the neuron on coordinates
    *
    * @param x coordinate
    * @param y coordinate
    * @return neuron on coordinates given in parameters
    */
   KohonenNeuronImpl getNeuron(int x, int y);
}
