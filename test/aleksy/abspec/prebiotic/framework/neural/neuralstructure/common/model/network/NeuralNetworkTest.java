package aleksy.abspec.prebiotic.framework.neural.neuralstructure.common.model.network;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.common.api.NeuralApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NeuralNetworkTest {
   private NeuralNetwork uut;
   private final int neuronsInInputLayer = 2;
   private final int sizeOfInputVector = 1;
   private final int neuronsInOutputLayer = 1;


   @Before
   public void init() {
      uut = createNeuralNetwork();
   }

   @Test
   public void shouldReturnInputNeuralLayer() {
      // when
      NeuralLayer returnedLayer = uut.getInputLayer();

      // then
      assertEquals(neuronsInInputLayer, returnedLayer.size());
   }

   @Test
   public void shouldReturnOutputNeuralLayer() {
      // when
      NeuralLayer returnedLayer = uut.getOutputLayer();

      // then
      assertEquals(neuronsInOutputLayer, returnedLayer.size());
   }

   @Test
   public void shouldReturnCorrectDescribingValues() {
      // given
      int expectedAmountOfNeurons = 3;
      int expectedAmountOfLayers = 2;

      // when
      int actualAmountOfNeurons = uut.size();
      int actualAmountOfLayers = uut.numberOfLayers();

      // then
      assertEquals(expectedAmountOfLayers, actualAmountOfLayers);
      assertEquals(expectedAmountOfNeurons, actualAmountOfNeurons);
   }

   @Test
   public void shouldProduceNewInstanceOfNetworkWithTheSameParameters() {
      // given
      NeuralNetwork expectedNetwork = createNeuralNetwork();

      //when
      NeuralNetwork actualNetwork = uut.copy();

      //then
      assertEquals(expectedNetwork.getLayers().size(), actualNetwork.getLayers().size());
      assertEquals(expectedNetwork.getDescription(), actualNetwork.getDescription());
      Assert.assertEquals(expectedNetwork.getType(), actualNetwork.getType());
      assertEquals(expectedNetwork.isBiasPresence(), actualNetwork.isBiasPresence());
      Assert.assertEquals(expectedNetwork.getAxonFunction().getAxonFunctionRecipe(),
         actualNetwork.getAxonFunction().getAxonFunctionRecipe());
      Assert.assertEquals(expectedNetwork.getWeightFunctionRecipe(), actualNetwork.getWeightFunctionRecipe());
      Assert.assertEquals(expectedNetwork.getCoreFunction().getCoreFunctionRecipe(),
         actualNetwork.getCoreFunction().getCoreFunctionRecipe());
      Assert.assertEquals(expectedNetwork.getDataNormalization(), actualNetwork.getDataNormalization());
      Assert.assertEquals(expectedNetwork.getInputLayer().size(), actualNetwork.getInputLayer().size());
      Assert.assertEquals(expectedNetwork.getOutputLayer().size(), actualNetwork.getOutputLayer().size());
   }

   @Test
   public void shouldCreateBiasWeights() {
      // given
      int expectedNumberOfWeights = 7;

      // when
      uut.createBiasWeights();
      List<Neuron> neurons = new ArrayList<>();
      for (NeuralLayer layer : uut.getLayers())
         neurons.addAll(layer.getNeurons());
      int actualNumberOfWeights = 0;
      for (Neuron neuron : neurons)
         actualNumberOfWeights += neuron.size();

      // then
      assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
   }

   @Test
   public void shouldReturnCorrectValueOnOutput() throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException {
      // given
      double value = 1.2;
      List<Double> inputVector = new ArrayList<>();
      inputVector.add(value);
      double expectedValueOnOutput = 2 * value;

      // when
      List<Double> actualOutput = uut.f(inputVector);

      // then
      assertTrue(actualOutput.get(0).equals(expectedValueOnOutput));
   }

   private NeuralNetwork createNeuralNetwork() {
      NeuralApi neuralApi = Prebiotic.newInstance().neuralApi();
      try {
         return neuralApi.getNeuralNetworkBuilder(
            neuralApi.getStructureConfigurationsBuilder()
               .defaultFeedforwardConfigurations()
               .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
               .setAxonFunction(AxonFunctionRecipe.LINEAR)
               .setBiasPresence(false)
               .create()
         ).addDefaultInputLayer(neuronsInInputLayer, sizeOfInputVector)
            .addLayer(neuronsInOutputLayer)
            .endBuildingNetwork()
            .create();
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
      return null;
   }
}
