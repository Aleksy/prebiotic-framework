package aleksy.abspec.prebiotic.framework.vision.display.panel.abstr;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

import java.awt.*;
import java.util.List;

/**
 * Abstract coordinate system panel displays a coordinate system and all neurons of neural network.
 * Each neuron has a vector of weights, and first two vector coordinates are taken abspec x and y coordinate.
 */
public abstract class AbstractCoordinateSystemNeuralPanel extends AbstractNeuralPanel {

   public AbstractCoordinateSystemNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }


   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      Color lineColor = new Color(23, 23, 23);
      g.setColor(lineColor);
      for (int i = 0; i < getSize().width / 16.0; i++) {
         int step = i * 16;
         g.drawLine(step, 0, step, getSize().height);
      }
      g.setColor(Color.DARK_GRAY);
      g.drawLine(getSize().width / 32 * 16, 0, getSize().width / 32 * 16, getSize().height);
      g.setColor(lineColor);
      for (int i = 0; i < getSize().height / 16.0; i++) {
         g.drawLine(0, i * 16, getSize().width, i * 16);
      }
      g.setColor(Color.DARK_GRAY);
      g.drawLine(0, getSize().height / 32 * 16, getSize().width, getSize().height / 32 * 16);
      g.setColor(Color.GRAY);
      g.setFont(font);
      g.drawString("coordinates of neurons:", 5, getSize().height / 20);
   }

   protected void drawNeuron(List<Double> vectorOfWeights, Graphics g) {
      double[] point = createCoordinatePoint(vectorOfWeights);
      g.fillRoundRect((int) (point[0] - 2), (int) (point[1] - 2),
         (int) (8 * point[2]), (int) (8 * point[2]), (int) (8 * point[3]), (int) (8 * point[3]));
      g.setColor(createContourColor(g.getColor()));
      g.drawRoundRect((int) (point[0] - 2), (int) (point[1] - 2),
         (int) (8 * point[2]), (int) (8 * point[2]), (int) (8 * point[3]), (int) (8 * point[3]));
   }

   protected double[] createCoordinatePoint(List<Double> vectorOfWeights) {
      double x;
      double y;
      double z = VectorNormalizator.setValueBetween((inputRange.getFrom()+ inputRange.getTo()) / 2,
         0.134, 1.11, inputRange.getFrom(), inputRange.getTo());
      double w = VectorNormalizator.setValueBetween((inputRange.getFrom()+ inputRange.getTo()) / 2,
         0.0, 0.4, inputRange.getFrom(), inputRange.getTo());
      double xBias = getSize().width / 2;
      double yBias = getSize().height / 2;
      double xStep = getSize().width / 32 * 16;
      double yStep = getSize().height / 32 * 16;
      double SCALING = 0.874;
      if (vectorOfWeights.size() == 1) {
         x = (vectorOfWeights.get(0) * getSize().height);
         y = getSize().height / 2;
      } else if (vectorOfWeights.size() == 2) {
         x = (xBias + (vectorOfWeights.get(0) * SCALING) * xStep);
         y = (yBias - (vectorOfWeights.get(1) * SCALING) * yStep);
         y -= 7;
      } else if (vectorOfWeights.size() == 3) {
         x = (xBias + (vectorOfWeights.get(0) * SCALING) * xStep);
         y = (yBias - (vectorOfWeights.get(1) * SCALING) * yStep);
         z = VectorNormalizator.setValueBetween(vectorOfWeights.get(2), 0.134, 1.11,
            inputRange.getFrom(), inputRange.getTo());
      } else {
         x = (xBias + (vectorOfWeights.get(0) * SCALING) * xStep);
         y = (yBias - (vectorOfWeights.get(1) * SCALING) * yStep);
         z = VectorNormalizator.setValueBetween(vectorOfWeights.get(2), 0.134, 1.11,
            inputRange.getFrom(), inputRange.getTo());
         w = VectorNormalizator.setValueBetween(vectorOfWeights.get(3), 0.0, 0.4,
            inputRange.getFrom(), inputRange.getTo());
      }
      double[] point = new double[4];
      point[0] = x;
      point[1] = y;
      point[2] = z;
      point[3] = w;
      return point;
   }
}
