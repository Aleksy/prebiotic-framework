package aleksy.abspec.prebiotic.framework.neural.common.api;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.KohonenLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.builder.TeachingDataBuilder;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;

/**
 * Neural module api
 */
public interface NeuralApi {

   /**
    * Getter for structure configurations builder
    *
    * @return new instance of structure configurations builder, which can be used to create
    * new configurations for network builder
    */
   NeuralNetworkStructureConfigurationsBuilder getStructureConfigurationsBuilder();

   /**
    * Getter for neural network builder
    *
    * @param structureConfigurations configurations for new builder instance
    * @return builder, which can be used to building new neural network
    * @throws EmptyFunctionDefinitionException when user set any function abspec CUSTOM and don't choose the function class
    */
   NeuralNetworkBuilder getNeuralNetworkBuilder(StructureConfigurations structureConfigurations) throws EmptyFunctionDefinitionException;

   /**
    * Getter for teaching data builder
    *
    * @return builder for teaching data
    */
   TeachingDataBuilder getTeachingDataBuilder();

   /**
    * Getter for new instance of deep learning configurations builder
    *
    * @return builder
    */
   DeepLearningConfigurationsBuilder getDeepLearningConfigurationsBuilder();

   /**
    * Getter for new instance of kohonen learning configurations builder
    *
    * @return builder
    */
   KohonenLearningConfigurationsBuilder getKohonenLearningConfigurationsBuilder();

   /**
    * Starting method for deep learning algorithm. This algorithm can teach multi- and one-layer FEEDFORWARD networks.
    *
    * @param teachingData               data with packages to teach the network
    * @param network                    to teach
    * @param deepLearningConfigurations configurations of deep learning parameters
    * @return neural network after teaching process
    * @throws NeuralNetworkException when some problem with internal structure of neural network exists (e. g.
    *                                when input vector size is not correct)
    * @throws DeepLearningException  when something in algorithm goes wrong
    */
   NeuralNetwork deepLearning(TeachingData teachingData, NeuralNetwork network,
                              DeepLearningConfigurations deepLearningConfigurations)
      throws NeuralNetworkException, DeepLearningException;

   /**
    * Starting method for Kohonen's map learning algorithm. This algorithm can teach Kohonen's map networks.
    * Teaching data packages can have only inputs vectors, no expected outputs are needed.
    *
    * @param teachingData                  with packages (only input vectors) to teach the network
    * @param network                       to teach
    * @param kohonenLearningConfigurations configurations of kohonen learning parameters
    * @return neural network after teaching process
    * @throws KohonenLearningException                 when something in algorithm goes wrong
    * @throws IncorrectNumberOfInputsInNeuronException when neuron has a problem with number of inputs
    * @throws IncorrectNumberOfInputsInLayerException  when layer has a problem with number of inputs
    */
   NeuralNetwork kohonenLearning(TeachingData teachingData, NeuralNetwork network,
                                 KohonenLearningConfigurations kohonenLearningConfigurations)
      throws KohonenLearningException, IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException;

   /**
    * Starting method for Hopfield's network learning algorithm (Hebb's rule). Teaching data packages can
    * have only input vectors, no expected output are needed.
    *
    * @param teachingData with packages (only input vectors) to teach the network
    * @param network      to teach
    * @return neural network after teaching process
    * @throws HopfieldLearningException when type of network is wrong
    */
   NeuralNetwork hopfieldLearning(TeachingData teachingData, NeuralNetwork network) throws HopfieldLearningException;

   /**
    * Parses {@link NeuralNetwork} to {@link KohonenNeuralNetwork}
    *
    * @param neuralNetworkInterface to parse
    * @return parsed network to Kohonen's map
    * @throws InvalidStructureException when structure of parameter is invalid
    */
   KohonenNeuralNetwork convertToKohonenNeuralNetwork(NeuralNetwork neuralNetworkInterface)
      throws InvalidStructureException;

}
