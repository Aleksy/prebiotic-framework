package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

/**
 * Geneticable interface
 */
public interface Geneticable extends Comparable<Geneticable> {

   /**
    * Setter for fitness value
    *
    * @param fitness to set
    */
   void setFitness(double fitness);

   /**
    * Getter for fitness value
    *
    * @return fitness value
    */
   double getFitness();

   /**
    * Setter for line segment to roulette and ranking method of selection
    *
    * @param lineSegment to set
    */
   void setLineSegment(Range lineSegment);

   /**
    * Getter for line segment to roulette and ranking method of selection
    *
    * @return line segment
    */
   Range getLineSegment();

   /**
    * Getter for genotype
    *
    * @return genotype
    */
   Genotype getGenotype();

   /**
    * Setter for genotype
    *
    * @param genotype to set
    */
   void setGenotype(Genotype genotype);

   /**
    * Crossover function
    *
    * @param parent         to crossover
    * @param mutationChance of geneticable instance
    * @return child geneticable instance
    */
   Geneticable crossover(Geneticable parent, double mutationChance);

   /**
    * New instance creator. It should to return new instance of object, which
    * implement this interface
    *
    * @param genotype to set in new instance
    * @return new instance
    */
   Geneticable newInstance(Genotype genotype);
}
