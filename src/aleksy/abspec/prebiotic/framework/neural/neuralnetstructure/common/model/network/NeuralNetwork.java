package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.model.PrebioticModelObject;

import java.util.List;

/**
 * Neural network interface
 */
public interface NeuralNetwork extends PrebioticModelObject {
   /**
    * Function calculates values of outputs vector from network
    *
    * @param xs inputs vector
    * @return outputs vector
    * @throws IncorrectNumberOfInputsInLayerException  when input vector has wrong size
    * @throws IncorrectNumberOfInputsInNeuronException when input vector has wrong size
    */
   List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException;

   /**
    * Setter for layers of network
    *
    * @param layers list to set
    */
   void setLayers(List<NeuralLayer> layers);

   /**
    * Getter for list of layers of network
    *
    * @return layers
    */
   List<NeuralLayer> getLayers();

   /**
    * Method returns input layer
    *
    * @return input layer of network
    */
   NeuralLayer getInputLayer();

   /**
    * Function counts all neurons
    *
    * @return number of neurons in network
    */
   int size();

   /**
    * Functions count all layers
    *
    * @return number of layers in network
    */
   int numberOfLayers();

   /**
    * Setter for string description of network
    *
    * @param description to set
    */
   void setDescription(String description);

   /**
    * Getter for string description of network
    *
    * @return description of network
    */
   String getDescription();

   /**
    * Getter for network type
    *
    * @return type
    */
   NeuralNetworkType getType();

   /**
    * Setter for network type
    *
    * @param type to set
    */
   void setType(NeuralNetworkType type);

   /**
    * Clones itself to new instance of network
    *
    * @return cloned instance
    */
   NeuralNetwork copy();

   /**
    * Randomizes weights functions
    *
    * @param from lower bound
    * @param to   upper bound
    */
   void randomizeWeights(Double from, Double to);

   /**
    * Getter for core function instance
    *
    * @return core function instance used in all neurons of network
    */
   CoreFunction getCoreFunction();

   /**
    * Getter for axon function instance
    *
    * @return axon function instance used in all neurons of network
    */
   AxonFunction getAxonFunction();

   /**
    * Getter for bias presence boolean value
    *
    * @return bias presence
    */
   boolean isBiasPresence();

   /**
    * Setter for bias presence boolean value
    *
    * @param biasPresence to set
    */
   void setBiasPresence(boolean biasPresence);

   /**
    * Method creates one additional weight for each neuron of network. This method is used during building of
    * neural network when bias presence value is true.
    */
   void createBiasWeights();

   /**
    * Getter for weight function recipe
    *
    * @return weight function recipe
    */
   WeightFunctionRecipe getWeightFunctionRecipe();

   /**
    * Getter for last layer in network
    *
    * @return output layer
    */
   NeuralLayer getOutputLayer();

   /**
    * Setter for data normalization
    *
    * @param dataNormalization to set
    */
   void setDataNormalization(DataNormalizationType dataNormalization);

   /**
    * Getter for data normalization
    *
    * @return data normalization
    */
   DataNormalizationType getDataNormalization();

   /**
    * Setter for {@link PrebioticApi}. Method is used by internal builders
    *
    * @param prebiotic to set
    */
   void setPrebiotic(PrebioticApi prebiotic);

   /**
    * Getter for {@link PrebioticApi}
    *
    * @return prebiotic api
    */
   PrebioticApi getPrebiotic();

   /**
    * Getter for x size (Kohonen's map using the coordinates of neurons)
    *
    * @return x size
    */
   int getXSize();

   /**
    * Getter for y size (Kohonen's map using the coordinates of neurons)
    *
    * @return y size
    */
   int getYSize();

   /**
    * Setter for x size (Kohonen's map using the coordinates of neurons)
    *
    * @param xSize to set
    */
   void setXSize(int xSize);

   /**
    * Setter for y size (Kohonen's map using the coordinates of neurons)
    *
    * @param ySize to set
    */
   void setYSize(int ySize);

   /**
    * Setter for actual output vector
    *
    * @param output to set
    */
   void setActualOutput(List<Double> output);

   /**
    * Getter for actual output
    *
    * @return output vector
    */
   List<Double> getActualOutput();

   /**
    * Returns list of all neurons from neural network
    *
    * @return all neurons
    */
   List<Neuron> getAllNeurons();

   /**
    * Size of input vector required by neural network
    *
    * @return size of input vector
    */
   int sizeOfInputVector();

   /**
    * Returns number of calculated operations. If neural network
    * makes a calculations, this number is incremented.
    *
    * @return number of operations
    */
   long getNumberOfOperation();

   /**
    * Returns actual input vector
    *
    * @return actual input vector
    */
   List<Double> getActualInputVector();
}
