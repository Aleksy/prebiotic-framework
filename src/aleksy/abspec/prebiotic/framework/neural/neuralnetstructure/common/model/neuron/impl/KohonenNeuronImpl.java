package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.AbstractNeuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.KohonenNeuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;

import java.util.List;

/**
 * Kohonen's neuron with coordinates for Kohonen's self-organising map implementation
 */
public class KohonenNeuronImpl extends AbstractNeuron implements KohonenNeuron {
   private int x;
   private int y;

   public KohonenNeuronImpl(List<WeightFunction> weights, CoreFunction core, AxonFunction axon, boolean biasPresence, int x, int y) {
      super(weights, core, axon, biasPresence);
      this.x = x;
      this.y = y;
   }

   @Override
   public void setX(int x) {
      this.x = x;
   }

   @Override
   public int getX() {
      return x;
   }

   @Override
   public void setY(int y) {
      this.y = y;
   }

   @Override
   public int getY() {
      return y;
   }
}
