package e2e.neural;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.util.ListCreator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test class for use case of creating and teaching the kohonen neural network.
 * Agent has four neurons and eight inputs. First neuron of map is prepared to
 * detect vector [0.9, 0.0], and the last one can detect vector [0.0, 0.9].
 * Two neurons in the middle has weights with value 0.0 and are not important. During the
 * teaching process agent should use first and fourth neuron to detect two vectors from
 * teaching data, very similar to the weight vectors. That means that the first and fourth neuron
 * after the teaching process should have the highest value from all map for given input vector.
 */
public class TeachingKohonenNeuralNetwork {

   @Test
   public void buildingTrainingAndTesting() throws NeuralNetworkException, KohonenLearningException {
      // preparing the api
      PrebioticApi prebiotic = Prebiotic.newInstance();
      prebiotic.enableEventLogging();

      // building the agent
      NeuralNetwork neuralNetwork = buildAgent(prebiotic);

      // preparing the teaching data
      TeachingData teachingData = prepareTeachingData(prebiotic);

      // training
      neuralNetwork = prebiotic.neuralApi().kohonenLearning(teachingData,
         neuralNetwork, prebiotic.neuralApi().getKohonenLearningConfigurationsBuilder()
            .setIterationsDisplaying(1000)
            .setIterations(5000)
            .setLearningRate(0.1)
            .setMaxDistance(1.1)
            .create());

      // testing
      List<Double> out0 = neuralNetwork.f(teachingData.getDataPackages().get(0).ins);
      List<Double> out1 = neuralNetwork.f(teachingData.getDataPackages().get(1).ins);

      // then
      assertEquals(0, max(out0));
      assertEquals(3, max(out1));
      assertEquals(0, ((KohonenNeuralNetwork) neuralNetwork)
         .fBestNeuron(teachingData.getDataPackages().get(0).ins).getX());
      assertEquals(0, ((KohonenNeuralNetwork) neuralNetwork)
         .fBestNeuron(teachingData.getDataPackages().get(0).ins).getY());
      assertEquals(1, ((KohonenNeuralNetwork) neuralNetwork)
         .fBestNeuron(teachingData.getDataPackages().get(1).ins).getX());
      assertEquals(1, ((KohonenNeuralNetwork) neuralNetwork)
         .fBestNeuron(teachingData.getDataPackages().get(1).ins).getY());
   }

   private int max(List<Double> out) {
      double max = out.get(0);
      int index = 0;
      for (int i = 1; i < out.size(); i++)
         if (out.get(i) > max) {
            max = out.get(i);
            index = i;
         }
      return index;
   }

   private TeachingData prepareTeachingData(PrebioticApi prebiotic) {
      return prebiotic.neuralApi().getTeachingDataBuilder()
         .addSelfLearningPackage(ListCreator.createFrom(1.0, 0.0))
         .addSelfLearningPackage(ListCreator.createFrom(0.0, 1.0))
         .create();
   }

   private NeuralNetwork buildAgent(PrebioticApi prebiotic)
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      NeuralNetwork neuralNetwork = prebiotic.neuralApi()
         .getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultKohonenConfigurations()
               .create()
         ).buildKohonenMap(2, 2, 2)
         .create();

      List<Neuron> allNeurons = neuralNetwork.getAllNeurons();
      allNeurons.get(0).getWeights().get(0).getFactors().set(0, 0.9);
      allNeurons.get(1).getWeights().get(0).getFactors().set(0, 0.0);
      allNeurons.get(2).getWeights().get(0).getFactors().set(0, 0.0);
      allNeurons.get(3).getWeights().get(0).getFactors().set(0, 0.0);
      allNeurons.get(0).getWeights().get(1).getFactors().set(0, 0.0);
      allNeurons.get(1).getWeights().get(1).getFactors().set(0, 0.0);
      allNeurons.get(2).getWeights().get(1).getFactors().set(0, 0.0);
      allNeurons.get(3).getWeights().get(1).getFactors().set(0, 0.9);
      return neuralNetwork;
   }
}
