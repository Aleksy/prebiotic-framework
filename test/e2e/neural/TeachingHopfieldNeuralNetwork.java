package e2e.neural;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.util.ListCreator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Test class for use case of creating and teaching the feedforward neural network.
 * The agent is trained to solve the XOR problem.
 */
public class TeachingHopfieldNeuralNetwork {

   @Test
   public void buildingTrainingAndTesting() throws NeuralNetworkException, HopfieldLearningException {
      // preparing the api
      PrebioticApi prebiotic = Prebiotic.newInstance();
      prebiotic.enableEventLogging();

      // building the agent
      NeuralNetwork neuralNetwork = buildAgent(prebiotic);

      // preparing the teaching data
      TeachingData teachingData = prepareTeachingData(prebiotic);

      // training
      neuralNetwork = prebiotic.neuralApi().hopfieldLearning(teachingData,
         neuralNetwork);

      // testing
      List<Double> out0 = neuralNetwork.f(teachingData.getDataPackages().get(0).ins);
      List<Double> out1 = neuralNetwork.f(teachingData.getDataPackages().get(1).ins);

      // then
      assertTrue(check(out0, teachingData.getDataPackages().get(0).ins));
      assertTrue(check(out1, teachingData.getDataPackages().get(1).ins));
   }

   private boolean check(List<Double> out, List<Double> ins) {
      boolean inverted = false;
      if (!out.get(0).equals(ins.get(0))) inverted = true;
      for (int i = 1; i < out.size(); i++) {
         if (inverted)
            if (out.get(i).equals(ins.get(i)))
               return false;
         if (!inverted)
            if (!out.get(i).equals(ins.get(i)))
               return false;
      }
      return true;
   }

   private TeachingData prepareTeachingData(PrebioticApi prebiotic) {
      return prebiotic.neuralApi().getTeachingDataBuilder()
         .addSelfLearningPackage(ListCreator.createFrom(-1.0, -1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0))
         .addSelfLearningPackage(ListCreator.createFrom(1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0))
         .create();
   }

   private NeuralNetwork buildAgent(PrebioticApi prebiotic)
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      return prebiotic.neuralApi()
         .getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultHopfieldConfigurations()
               .create()
         ).buildHopfieldNetwork(9)
         .create();
   }
}
