package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate;

/**
 * Neural network types
 */
public enum NeuralNetworkType {
   /**
    * Single- or multilayer feedforward type
    */
   FEEDFORWARD,

   /**
    * Self organising Kohonen'TeachingDataManagementComposite network type
    */
   KOHONEN,

   /**
    * Recurrent Hopfield'TeachingDataManagementComposite neural network
    */
   HOPFIELD
}
