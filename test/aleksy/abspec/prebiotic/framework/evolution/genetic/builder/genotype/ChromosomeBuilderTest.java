package aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChromosomeBuilderTest {
   private ChromosomeBuilder uut;

   @Before
   public void init() {
      GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
      uut = genotypeBuilder.addChromosome();
   }

   @Test
   public void shouldCorrectlyBuildTheChromosome() {
      // when
      uut.addGene(1.0, Range.from(0.0, 1.0), "gene1");
      uut.addGene(0.0, Range.from(-1.0, 0.5), "gene2");
      Genotype result = uut.end().create();

      // then
      assertEquals(new Double(1.0), new Double(result.getChromosomes().get(0).getGenes().get(0).getValue()));
      assertEquals(new Double(0.0), new Double(result.getChromosomes().get(0).getGenes().get(1).getValue()));
      assertEquals("gene1", result.getChromosomes().get(0).getGenes().get(0).getDescription());
      assertEquals("gene2", result.getChromosomes().get(0).getGenes().get(1).getDescription());
      assertEquals(new Double(0.0),
         new Double(result.getChromosomes().get(0).getGenes().get(0).getValueRange().from));
      assertEquals(new Double(1.0),
         new Double(result.getChromosomes().get(0).getGenes().get(0).getValueRange().to));
      assertEquals(new Double(-1.0),
         new Double(result.getChromosomes().get(0).getGenes().get(1).getValueRange().from));
      assertEquals(new Double(0.5),
         new Double(result.getChromosomes().get(0).getGenes().get(1).getValueRange().to));
   }
}
