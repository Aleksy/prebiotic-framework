package testutil;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.*;

import java.util.List;

public class TestCustomFunctions {
   public static class CUSTOM_AXON_TEST extends AbstractAxonFunction {

      @Override
      public Double f(Double x) {
         return null;
      }

      @Override
      public Double derivative(Double x) {
         return null;
      }

      @Override
      public Function newInstance() {
         return new CUSTOM_AXON_TEST();
      }
   }

   public static class CUSTOM_CORE_TEST extends AbstractCoreFunction {

      @Override
      public Double f(List<Double> xs) {
         return null;
      }

      @Override
      public Function newInstance() {
         return new CUSTOM_CORE_TEST();
      }
   }

   public static class CUSTOM_WEIGHT_TEST extends AbstractWeightFunction {

      @Override
      public Double f(Double x) {
         return null;
      }

      @Override
      public void initFactors() {

      }

      @Override
      public void deepLearningCorrection(double learningRate, double neuronDefect, double neuronActualOutputFromCore, AxonFunction axon, double internalInput, double momentum) {

      }

      @Override
      public void kohonenCorrection(double learningRate, double distance, double actualInput) {

      }

      @Override
      public Function newInstance() {
         return new CUSTOM_WEIGHT_TEST();
      }
   }
}
