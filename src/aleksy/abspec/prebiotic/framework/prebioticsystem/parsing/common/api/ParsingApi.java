package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.api;

import aleksy.abspec.prebiotic.framework.common.enumerate.FileFormat;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.model.PrebioticModelObject;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder.CustomFunctionsForParserBuilder;

import java.io.IOException;

/**
 * Parsing submodule api
 */
public interface ParsingApi {

   /**
    * Saves a prebiotic model object (like neural network) to the file
    *
    * @param prebioticModelObject to save
    * @param filepath             of file
    * @param fileFormat           of file
    * @throws IOException when the problems with files exist
    */
   void save(PrebioticModelObject prebioticModelObject, String filepath, FileFormat fileFormat) throws IOException;

   /**
    * Reads a prebiotic model object (like neural network) from a file
    *
    * @param filepath of file
    * @return parsed model object
    * @throws Exception when problem
    */
   PrebioticModelObject read(String filepath) throws Exception;

   /**
    * Reads a prebiotic model object (like neural network) from a file. Method can be used to
    * read a neural network with custom functions. Usage:
    * Create the CustomFunctionsForParser in a builder and set the function instances of your classes.
    * EXAMPLE:
    * If you have custom axon function - set the axon function instance in the CustomFunctionsForParser
    * and put it to params. Parser'll automatically set the instances to neural network
    *
    * @param filepath                 of file
    * @param customFunctionsForParser with custom functions definitions
    * @return instance of neural network
    * @throws Exception when problem
    */
   NeuralNetwork read(String filepath, CustomFunctionsForParser customFunctionsForParser) throws Exception;

   /**
    * Getter for new instance of {@link CustomFunctionsForParserBuilder}
    *
    * @return builder
    */
   CustomFunctionsForParserBuilder getCustomFunctionsForParserBuilder();
}
