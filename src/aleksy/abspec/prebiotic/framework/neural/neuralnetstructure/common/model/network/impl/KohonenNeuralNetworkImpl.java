package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.KohonenNeuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.AbstractNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.KohonenNeuronImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of Kohonen's neural network with methods useful for this type of network
 */
public class KohonenNeuralNetworkImpl extends AbstractNeuralNetwork implements KohonenNeuralNetwork {

   @Override
   public KohonenNeuronImpl fBestNeuron(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException {
      List<Neuron> neurons = getInputLayer().getNeurons();
      KohonenNeuron best = (KohonenNeuron) neurons.get(0);
      double bestAns = best.f(xs);
      for (int i = 1; i < neurons.size(); i++) {
         KohonenNeuron n = (KohonenNeuron) neurons.get(i);
         double ans = n.f(xs);
         if (ans > bestAns) {
            best = n;
            bestAns = ans;
         }
      }
      return (KohonenNeuronImpl) best;
   }

   @Override
   public List<Double> getVectorOfWeightsOfNeuron(int x, int y) {
      KohonenNeuronImpl n = getNeuron(x, y);
      List<Double> vector = new ArrayList<>();
      for (WeightFunction w : n.getWeights())
         vector.addAll(w.getFactors());
      return vector;
   }

   @Override
   public KohonenNeuronImpl getNeuron(int x, int y) {
      for (Neuron n : getInputLayer().getNeurons()) {
         KohonenNeuronImpl kn = (KohonenNeuronImpl) n;
         if (kn.getX() == x && kn.getY() == y)
            return kn;
      }
      return null;
   }
}
