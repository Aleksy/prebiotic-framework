package aleksy.abspec.prebiotic.framework.neural.teaching.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;

/**
 * Kohonen learning configurations builder
 */
public class KohonenLearningConfigurationsBuilder extends Loggable {
   private PrebioticApi prebiotic;
   private KohonenLearningConfigurations config;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public KohonenLearningConfigurationsBuilder(PrebioticApi prebiotic) {
      config = new KohonenLearningConfigurations();
      this.prebiotic = prebiotic;
   }

   /**
    * Setter for learning rate
    *
    * @param learningRate to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder setLearningRate(Double learningRate) {
      config.learningRate = learningRate;
      return this;
   }

   /**
    * Setter for maximal distance
    *
    * @param maxDistance to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder setMaxDistance(double maxDistance) {
      config.maxDistance = maxDistance;
      return this;
   }

   /**
    * Setter for iterations displaying parameter
    * Value defines iterations which will be displayed in the console.
    * Example:
    * when iterationsDisplaying = 100 then
    * algorithm display status after 100, 200, 300... iterations
    *
    * @param iterationsDisplaying to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder setIterationsDisplaying(int iterationsDisplaying) {
      config.iterationsDisplaying = iterationsDisplaying;
      return this;
   }

   /**
    * Setter for delay value. Value defines delay between each iteration of algorithm and is given in
    * milliseconds. This value can be used with {@link Vision} to
    * research the neural network behavior (WARNING: this value slows down the teaching process).
    *
    * @param delay to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder setDelay(int delay) {
      config.delay = delay;
      return this;
   }

   /**
    * Setter for iterations number
    *
    * @param iterations to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder setIterations(long iterations) {
      config.iterations = iterations;
      return this;
   }

   /**
    * Setter for minimal defect of the network. Achievement of the minimal defect
    * stops the learning process
    *
    * @param minimalDefect to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder setMinimalDefect(double minimalDefect) {
      config.minimalDefect = minimalDefect;
      return this;
   }

   /**
    * Creates a default configurations. Iterations number and maximal distance
    * are needed abspec parameters. Learning rate is set to 0.1 and iterations displaying equals 100.
    *
    * @param iterations  to set
    * @param maxDistance to set
    * @return builder
    */
   public KohonenLearningConfigurationsBuilder defaultConfigurations(long iterations, double maxDistance) {
      config.learningRate = 0.1;
      config.iterations = iterations;
      config.iterationsDisplaying = 100;
      config.maxDistance = maxDistance;
      return this;
   }

   /**
    * Creation method
    *
    * @return new instance of {@link KohonenLearningConfigurations}
    */
   public KohonenLearningConfigurations create() {
      String delay;
      if (config.delay == null) {
         delay = "not set";
         config.delay = 0;
      } else
         delay = config.delay.toString();
      prebiotic.prebioticSystemApi().log("Kohonen'TeachingDataManagementComposite learning configurations has been built.", getSenderName());
      prebiotic.prebioticSystemApi().separator();
      prebiotic.prebioticSystemApi().log(" Iterations: " + config.iterations, getSenderName());
      prebiotic.prebioticSystemApi().log(" Learning rate: " + config.learningRate, getSenderName());
      prebiotic.prebioticSystemApi().log(" Max distance: " + config.maxDistance, getSenderName());
      prebiotic.prebioticSystemApi().log(" Iterations displaying interval: " + config.iterationsDisplaying, getSenderName());
      prebiotic.prebioticSystemApi().log(" Delay: " + delay, getSenderName());
      prebiotic.prebioticSystemApi().separator();
      return config;
   }

   @Override
   protected String getSenderName() {
      return "KOH-LRN-CONF-BLD";
   }
}
