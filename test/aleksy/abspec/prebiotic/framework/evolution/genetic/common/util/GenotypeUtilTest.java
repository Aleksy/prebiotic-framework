package aleksy.abspec.prebiotic.framework.evolution.genetic.common.util;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GenotypeUtilTest {
   @Test
   public void shouldCombineToOneGenotype() {
      // given
      List<Genotype> genotypes = testGenotypes();

      // when
      Genotype genotype = GenotypeUtil.combineToOneGenotype(genotypes.get(0), genotypes.get(1));

      // then
      assertEquals(2, genotype.size());
      assertEquals(2, genotype.amountOfGenes());
      assertEquals(new Double(0.0), genotype.findGeneValue("chr1:gene1"));
      assertEquals(new Double(0.5), genotype.findGeneValue("chr2:gene2"));
   }

   @Test
   public void shouldCombineTheListToOneGenotype() {
      // given
      List<Genotype> genotypes = testGenotypes();

      // when
      Genotype genotype = GenotypeUtil.combineToOneGenotype(genotypes);

      // then
      assertEquals(2, genotype.size());
      assertEquals(2, genotype.amountOfGenes());
      assertEquals(new Double(0.0), genotype.findGeneValue("chr1:gene1"));
      assertEquals(new Double(0.5), genotype.findGeneValue("chr2:gene2"));
   }

   @Test
   public void shouldCreateGenotypeFromNeuralNetwork() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      NeuralNetwork neuralNetwork = testNeuralNetwork();
      for(Neuron n : neuralNetwork.getAllNeurons())
         for(WeightFunction w : n.getWeights())
            for(int i = 0; i < w.getFactors().size(); i++)
               w.getFactors().set(i, 0.42);

      // when
      Genotype genotype = GenotypeUtil.createGenotypeOfNetwork(neuralNetwork, "");

      // then
      assertEquals(genotype.size(), 2);
      assertEquals(4, genotype.amountOfGenes());
      assertEquals(new Double(0.42), genotype.findGeneValue("nn_layer0:w0"));
      assertEquals(new Double(0.42), genotype.findGeneValue("nn_layer0:w1"));
      assertEquals(new Double(0.42), genotype.findGeneValue("nn_layer1:w0"));
      assertEquals(new Double(0.42), genotype.findGeneValue("nn_layer1:w1"));
   }

   @Test
   public void shouldCreateGenotypeFromBrain() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      Brain brain = new Brain();
      NeuralNetwork neuralNetwork = testNeuralNetwork();
      for(Neuron n : neuralNetwork.getAllNeurons())
         for(WeightFunction w : n.getWeights())
            for(int i = 0; i < w.getFactors().size(); i++)
               w.getFactors().set(i, 0.42);
      brain.getNeuralNetworks().add(neuralNetwork);
      // when
      Genotype genotype = GenotypeUtil.createGenotypeOfBrain(brain);

      // then
      assertEquals(genotype.size(), 2);
      assertEquals(4, genotype.amountOfGenes());
      assertEquals(new Double(0.42), genotype.findGeneValue("fed_nmb0nn_layer0:w0"));
      assertEquals(new Double(0.42), genotype.findGeneValue("fed_nmb0nn_layer0:w1"));
      assertEquals(new Double(0.42), genotype.findGeneValue("fed_nmb0nn_layer1:w0"));
      assertEquals(new Double(0.42), genotype.findGeneValue("fed_nmb0nn_layer1:w1"));
   }

   private List<Genotype> testGenotypes() {
      PrebioticApi p = Prebiotic.newInstance();
      List<Genotype> genotypes = new ArrayList<>();
      genotypes.add(p.evolutionApi().getGenotypeBuilder()
         .addChromosome("chr1")
         .addGene(0.0, Range.from(0.0, 1.0), "gene1")
         .end()
         .create());
      genotypes.add(p.evolutionApi().getGenotypeBuilder()
         .addChromosome("chr2")
         .addGene(0.5, Range.from(0.0, 1.0), "gene2")
         .end()
         .create());
      return genotypes;
   }

   private NeuralNetwork testNeuralNetwork() throws EmptyFunctionDefinitionException, InvalidStructureException {
      PrebioticApi p = Prebiotic.newInstance();
      return p.neuralApi().getNeuralNetworkBuilder(
         p.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setBiasPresence(false)
            .create()
      ).addDefaultInputLayer(2, 1)
         .addLayer(1)
         .endBuildingNetwork()
         .create();
   }
}
