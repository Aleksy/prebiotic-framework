package aleksy.abspec.prebiotic.framework.vision.common.enumerate;

/**
 * Display mode
 */
public enum DisplayMode {
   /**
    * Displaying of structure
    */
   STRUCTURE,
   /**
    * Displaying of structure with last layer of network abspec square
    */
   STRUCTURE_SQUARE,
   /**
    * Displaying of structure with weight matrix of each neuron
    */
   WEIGHT_MATRIX,
   /**
    * Displaying a coordinate system with all neurons.
    * First weight factor is the x coordinate, and the second - y
    */
   COORDINATE_SYSTEM,
   /**
    * Displaying of frame with structure, weight matrix and coordinate system mode
    */
   ALL,
   /**
    * Displaying of frame with structure square, weight matrix and coordinate system mode
    */
   ALL_SQUARE,
   /**
    * Displaying description of neural network
    */
   DESCRIPTION,
   /**
    * Displaying of activity round graph of neural network
    */
   ACTIVITY_ROUND_GRAPH,
   INPUT_ROUND_GRAPH, /**
    * Displaying of activity time graph of neural network
    */
   ACTIVITY_TIME_GRAPH
}
