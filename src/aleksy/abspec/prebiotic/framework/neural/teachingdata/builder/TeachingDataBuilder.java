package aleksy.abspec.prebiotic.framework.neural.teachingdata.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingDataPackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Teaching data builder
 */
public class TeachingDataBuilder extends Loggable {
   private TeachingData teachingData;
   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public TeachingDataBuilder(PrebioticApi prebiotic) {
      teachingData = new TeachingData();
      teachingData.setDataPackages(new ArrayList<>());
      this.prebiotic = prebiotic;
   }

   /**
    * Adds new data package to teaching data object
    *
    * @param ins          input vector for neural network
    * @param expectedOuts expected output vector from neural network
    * @return parsing
    */
   public TeachingDataBuilder addDataPackage(List<Double> ins, List<Double> expectedOuts) {
      TeachingDataPackage dataPackage = new TeachingDataPackage();
      dataPackage.ins = ins;
      dataPackage.expectedOuts = expectedOuts;
      teachingData.getDataPackages().add(dataPackage);
      return this;
   }

   /**
    * Adds new data package for self-learning algorithms.
    *
    * @param ins input vector for neural network
    * @return parsing
    */
   public TeachingDataBuilder addSelfLearningPackage(List<Double> ins) {
      TeachingDataPackage dataPackage = new TeachingDataPackage();
      dataPackage.ins = ins;
      teachingData.getDataPackages().add(dataPackage);
      return this;
   }

   /**
    * Creates new instance of teaching data object
    *
    * @return new instance of teaching data object
    */
   public TeachingData create() {
      prebiotic.prebioticSystemApi().log("Teaching data with " + teachingData.getDataPackages().size()
         + " packages was created.", getSenderName());
      return teachingData;
   }

   @Override
   protected String getSenderName() {
      return "TEACHING-DATA-BLD";
   }
}
