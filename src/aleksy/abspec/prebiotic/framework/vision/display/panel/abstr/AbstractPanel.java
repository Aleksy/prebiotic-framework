package aleksy.abspec.prebiotic.framework.vision.display.panel.abstr;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractPanel extends JPanel {

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
   }
}
