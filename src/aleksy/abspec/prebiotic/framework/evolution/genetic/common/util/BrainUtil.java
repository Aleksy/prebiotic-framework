package aleksy.abspec.prebiotic.framework.evolution.genetic.common.util;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Protista;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Chromosome;

import java.util.List;

/**
 * Util methods for {@link Brain}
 */
public class BrainUtil {

   /**
    * Sets brains to populations.
    *
    * @param population    of geneticables
    * @param templateBrain it is neural network with given structure which can be copied and
    *                      used to create a new network with weights factors from genotype
    * @return population with brains
    */
   public static List<Geneticable> setBrainsToPopulation(List<Geneticable> population, Brain templateBrain) {
      for (Geneticable geneticable : population) {
         Brain brain = new Brain();
         Protista protista = (Protista) geneticable;
         Integer networkNumber = 0;
         List<Chromosome> oneNetwork;
         while (true) {
            oneNetwork = protista.getGenotype().findChromosomes("_nmb" + networkNumber.toString());
            if (oneNetwork.size() == 0)
               break;
            brain.getNeuralNetworks().add(createNetwork(oneNetwork, templateBrain.getNeuralNetworks().get(networkNumber++)));
         }
         protista.setBrain(brain);
      }
      return population;
   }

   private static NeuralNetwork createNetwork(List<Chromosome> oneNetwork, NeuralNetwork template) {
      NeuralNetwork nn = template.copy();
      int layerNumber = 0;
      for (NeuralLayer layer : nn.getLayers()) {
         int factorNumber = 0;
         for (Neuron neuron : layer.getNeurons()) {
            for (WeightFunction weight : neuron.getWeights()) {
               for (int i = 0; i < weight.getFactors().size(); i++) {
                  weight.getFactors().set(i, oneNetwork.get(layerNumber).getGenes().get(factorNumber++).getValue());
               }
            }
         }
         layerNumber++;
      }
      return nn;
   }
}
