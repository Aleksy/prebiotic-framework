package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function;


import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;

import java.util.List;

/**
 * Weight function interface
 */
public interface WeightFunction extends Function {
   /**
    * Function calculates one internal output for core function of neuron from one external input
    *
    * @param x external input
    * @return calculated internal output
    */
   Double f(Double x);


   /**
    * Setter for factors
    *
    * @param factors list of factors to set
    */
   void setFactors(List<Double> factors);

   /**
    * Getter for factors
    *
    * @return list of factors of weight function
    */
   List<Double> getFactors();

   /**
    * Setter for list of factor which exists in previous step of learning
    *
    * @param factorsInPreviousStepOfLearning to set
    */
   void setFactorsInPreviousStepOfLearning(List<Double> factorsInPreviousStepOfLearning);

   /**
    * Getter for list of factor which exists in previous step of learning
    *
    * @return list of factor which exists in previous step of learning
    */
   List<Double> getFactorsInPreviousStepOfLearning();

   /**
    * Definition of factors initialization after creating an object from constructor
    */
   void initFactors();

   /**
    * Setter for weight function recipe
    *
    * @param weightFunctionRecipe to set
    */
   void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe);

   /**
    * Getter for weight function recipe
    *
    * @return weight function recipe
    */
   WeightFunctionRecipe getWeightFunctionRecipe();

   /**
    * Corrects weight in deep learning process
    *
    * @param learningRate               learning rate
    * @param neuronDefect               defect of neuron
    * @param neuronActualOutputFromCore actual output from core of neuron
    * @param axon                       function of network
    * @param internalInput              of neuron'TeachingDataManagementComposite weight
    * @param momentum                   rate
    */
   void deepLearningCorrection(double learningRate, double neuronDefect, double neuronActualOutputFromCore,
                               AxonFunction axon, double internalInput, double momentum);

   /**
    * Correct weight in kohonen's self learning process
    *
    * @param learningRate learning rate
    * @param distance     from input vector
    * @param actualInput  of neuron'TeachingDataManagementComposite weight
    */
   void kohonenCorrection(double learningRate, double distance, double actualInput);

   /**
    * Randomizes all weight's factor
    *
    * @param from lower bound
    * @param to   upper bound
    */
   void randomizeFactors(Double from, Double to);
}
