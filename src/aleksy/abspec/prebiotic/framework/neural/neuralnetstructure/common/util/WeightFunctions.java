package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AbstractWeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.Function;

import java.util.Random;

public class WeightFunctions {
   public static class DEFAULT_LINEAR extends AbstractWeightFunction {
      public DEFAULT_LINEAR(boolean randomizeFactors) {
         super();
         setWeightFunctionRecipe(WeightFunctionRecipe.DEFAULT_LINEAR);
         weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
         if (randomizeFactors) {
            for (int i = 0; i < factors.size(); i++)
               factors.set(i, 1 - new Random().nextDouble() * 2);
         }
      }

      @Override
      public Double f(Double x) {
         return x * factors.get(0);
      }

      @Override
      public void initFactors() {
         factors.add(1.0);
         factorsInPreviousStepOfLearning.add(0.0);
      }

      @Override
      public void deepLearningCorrection(double learningRate, double neuronDefect, double neuronActualOutputFromCore,
                                         AxonFunction axon, double internalInput, double momentum) {
         double factor = factors.get(0);
         double previousFactor = factorsInPreviousStepOfLearning.get(0);
         factorsInPreviousStepOfLearning.set(0, factor);
         factor = factor + learningRate
            * internalInput
            * neuronDefect
            * axon.derivative(neuronActualOutputFromCore)
            + momentum * (factor - previousFactor);
         factors.set(0, factor);
      }

      @Override
      public void kohonenCorrection(double learningRate, double distance, double actualInput) {
         double factor = factors.get(0);
         factor = factor + learningRate
            * (1 / distance)
            * (actualInput - factor);
         factors.set(0, factor);
      }

      @Override
      public String toString() {
         return factors.get(0).toString();
      }

      @Override
      public Function newInstance() {
         return new DEFAULT_LINEAR(false);
      }
   }

   public static class SQUARE_DIFFERENCE extends AbstractWeightFunction {
      public SQUARE_DIFFERENCE(boolean randomizeFactors) {
         super();
         setWeightFunctionRecipe(WeightFunctionRecipe.SQUARE_DIFFERENCE);
         weightFunctionRecipe = WeightFunctionRecipe.SQUARE_DIFFERENCE;
         if (randomizeFactors) {
            for (int i = 0; i < factors.size(); i++)
               factors.set(i, 1 - new Random().nextDouble() * 2);
         }
      }

      @Override
      public Double f(Double x) {
         return Math.pow(factors.get(0) - x, 2);
      }

      @Override
      public void initFactors() {
         factors.add(1.0);
      }

      @Override
      public void deepLearningCorrection(double learningRate, double neuronDefect,
                                         double neuronActualOutputFromCore, AxonFunction axon,
                                         double internalInput, double momentum) {
         Double factor = factors.get(0);
         factor = factor + learningRate
            * internalInput
            * neuronDefect
            * axon.derivative(neuronActualOutputFromCore);
         factors.set(0, factor);
      }

      @Override
      public void kohonenCorrection(double learningRate, double distance, double actualInput) {
         double factor = factors.get(0);
         factor = factor + learningRate
            * (1 / distance)
            * (actualInput - factor);
         factors.set(0, factor);
      }

      @Override
      public String toString() {
         return factors.get(0).toString();
      }

      @Override
      public Function newInstance() {
         return new SQUARE_DIFFERENCE(false);
      }
   }
}
