package aleksy.abspec.prebiotic.framework.vision.display.panel.structure;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractStructureNeuralPanel;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class KohonenStructureNeuralPanel extends AbstractStructureNeuralPanel {
   private KohonenNeuralNetwork knn;

   public KohonenStructureNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
      try {
         knn = Prebiotic.newInstance().neuralApi().convertToKohonenNeuralNetwork(nn);
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(createColor(0.0));
      Neuron inputNeuron = nn.getInputLayer().getNeurons().get(0);
      List<Double> actualInputs = inputNeuron.getActualInputs();
      if (actualInputs != null) actualInputs = VectorNormalizator
         .setValuesBetweenZeroAndOne(actualInputs, inputRange.getFrom(), inputRange.getTo());
      else
         actualInputs = new ArrayList<>();
      drawNetwork(actualInputs, g);
   }

   private void drawNetwork(List<Double> inputs, Graphics g) {
      int inputsSize = inputs.size();
      int yOffset = (int) (getSize().height * 0.85) / 8;
      int xStep = (int) ((double) getSize().width / (double) (inputsSize + 1));
      for (int i = 0; i < inputsSize; i++) {
         g.setColor(createDarkerColor(inputs.get(i)));
         int actOutIndex = 0;
         drawKohonenSynapsesFromInput(knn, xStep, yOffset, i, g);
         for (int x = 0; x < knn.getXSize(); x++) {
            for (int y = 0; y < knn.getYSize(); y++) {
               int xNeuronStep = (int) ((double) getSize().width / (double) (knn.getXSize()));
               int yNeuronStep = (int) ((double) getSize().height / (double) (knn.getYSize() + 1));
               int xNeuron = (int) (xNeuronStep / 2.0) + x * xNeuronStep + 3;
               int yNeuron = (int) ((yNeuronStep + y * yNeuronStep) * 0.85) + yOffset;
               Color tmp = g.getColor();
               Double axon = VectorNormalizator.scaleToMaxValue(knn.getActualOutput()).get(actOutIndex++);
               g.setColor(createColor(axon));
               drawNeuron(xNeuron, yNeuron, g);
               g.setColor(tmp);
            }
         }
      }
      for (int i = 0; i < inputsSize; i++) {
         g.setColor(createDarkerColor(inputs.get(i)));
         drawInputNeuron(xStep + i * xStep, yOffset, inputs.get(i), g);
      }
   }
}
