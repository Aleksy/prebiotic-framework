package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype;

import java.util.ArrayList;
import java.util.List;

/**
 * Chromosome class
 */
public class Chromosome {
   private List<Gene> genes;
   private String description;

   /**
    * Constructor
    */
   public Chromosome() {
      genes = new ArrayList<>();
      description = "default_chromosome_description";
   }

   /**
    * Constructor
    *
    * @param description of chromosome
    */
   public Chromosome(String description) {
      genes = new ArrayList<>();
      this.description = description;
   }

   /**
    * Constructor
    *
    * @param chromosome to copy
    */
   public Chromosome(Chromosome chromosome) {
      genes = new ArrayList<>();
      for (Gene gene : chromosome.getGenes()) {
         genes.add(new Gene(gene));
      }
      this.description = chromosome.description;
   }

   /**
    * Setter for genes
    *
    * @param genes to set
    */
   public void setGenes(List<Gene> genes) {
      this.genes = genes;
   }

   /**
    * Getter for genes
    *
    * @return genes
    */
   public List<Gene> getGenes() {
      return genes;
   }

   /**
    * Size of chromosome
    *
    * @return number of genes
    */
   public int size() {
      return genes.size();
   }

   /**
    * Setter for description
    *
    * @param description to set
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Getter for description
    *
    * @return description
    */
   public String getDescription() {
      return description;
   }

   /**
    * Finds gene by description
    *
    * @param description of gene
    * @return found gene
    */
   public Gene findGene(String description) {
      for (Gene gene : genes) {
         if (gene.getDescription().equals(description)) {
            return gene;
         }
      }
      return null;
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(" chromosome \"").append(description).append("\":\n");
      for (Gene gene : genes)
         sb.append(gene.toString());
      return sb.toString();
   }
}
