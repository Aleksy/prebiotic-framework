package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function;

/**
 * Abstract function interface. Neural networks can take function abspec weight, core and axon function
 */
public interface Function {
   /**
    * Produce new instance of function
    *
    * @return function new instance
    */
   Function newInstance();
}
