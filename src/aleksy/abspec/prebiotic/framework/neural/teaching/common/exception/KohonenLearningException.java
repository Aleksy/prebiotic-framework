package aleksy.abspec.prebiotic.framework.neural.teaching.common.exception;

import aleksy.abspec.prebiotic.framework.common.exception.PrebioticException;

/**
 * Kohonen's learning exception
 */
public class KohonenLearningException extends PrebioticException {
   public KohonenLearningException(String message) {
      super("\nKohonen learning exception: " + message);
   }
}
