package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Abstract weight function
 */
public abstract class AbstractWeightFunction implements WeightFunction {

   protected List<Double> factors;
   protected WeightFunctionRecipe weightFunctionRecipe;
   protected List<Double> factorsInPreviousStepOfLearning;

   public AbstractWeightFunction() {
      this.factors = new ArrayList<>();
      this.factorsInPreviousStepOfLearning = new ArrayList<>();
      initFactors();
   }

   public AbstractWeightFunction(WeightFunction weightFunction) {
      this.factors = weightFunction.getFactors();
      this.factorsInPreviousStepOfLearning = weightFunction.getFactorsInPreviousStepOfLearning();
   }

   public void setFactors(List<Double> factors) {
      this.factors = factors;
   }

   public List<Double> getFactors() {
      return factors;
   }

   public void setFactorsInPreviousStepOfLearning(List<Double> factorsInPreviousStepOfLearning) {
      this.factorsInPreviousStepOfLearning = factorsInPreviousStepOfLearning;
   }

   public List<Double> getFactorsInPreviousStepOfLearning() {
      return factorsInPreviousStepOfLearning;
   }

   @Override
   public void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe) {
      this.weightFunctionRecipe = weightFunctionRecipe;
   }

   @Override
   public WeightFunctionRecipe getWeightFunctionRecipe() {
      return weightFunctionRecipe;
   }

   @Override
   public void randomizeFactors(Double from, Double to) {
      Random r = new Random();
      for (int i = 0; i < factors.size(); i++) {
         double x = from + (to - from) * r.nextDouble();
         factors.set(i, x);
      }
   }
}
