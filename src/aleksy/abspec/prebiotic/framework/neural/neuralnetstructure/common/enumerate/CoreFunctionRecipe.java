package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.CoreFunctions;

/**
 * Core functions recipes
 */
public enum CoreFunctionRecipe {
   /**
    * Default adding function
    */
   DEFAULT_ADDER,

   /**
    * Custom core function
    */
   CORE_CUSTOM;

   /**
    * Method creates the new instance of core function to set to neural network
    *
    * @return new instance of core function
    */
   public CoreFunction newInstance() {
      CoreFunction core = null;
      if (this == CoreFunctionRecipe.DEFAULT_ADDER)
         core = new CoreFunctions.DEFAULT_ADDER();
      return core;
   }
}
