package aleksy.abspec.prebiotic.framework.vision.common.constants;

import java.awt.*;

/**
 * Vision module constants
 */
public class VisionConstants {
   /**
    * Width of frame
    */
   public static final int START_FRAME_X_SIZE = 400;
   /**
    * Height of frame
    */
   public static final int START_FRAME_Y_SIZE = 400;
   /**
    * Prebiotic standard background color
    */
   public static final Color PREBIOTIC_BACKGROUND_COLOR = new Color(6, 7, 17);
}
