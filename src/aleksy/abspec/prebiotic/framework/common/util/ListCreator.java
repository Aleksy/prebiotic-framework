package aleksy.abspec.prebiotic.framework.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * List creator util class
 */
public class ListCreator {
   /**
    * Creates an {@link ArrayList} from parameters
    *
    * @param xs doubles
    * @return list
    */
   public static List<Double> createFrom(Double... xs) {
      return new ArrayList<>(Arrays.asList(xs));
   }
}
