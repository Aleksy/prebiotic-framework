package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.CoreFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl.NeuralLayerImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.AbstractNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.HopfieldNeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.KohonenNeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.NeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.KohonenNeuronImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Neural network builder - starting point in network building
 */
public class NeuralNetworkBuilder {
   private StructureConfigurations configurations;
   private AbstractNeuralNetwork rawNetwork;
   private AxonFunction axon;
   private CoreFunction core;
   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param configurations to use in building of basic neural network parameters
    * @param prebiotic      api
    * @throws EmptyFunctionDefinitionException when user choose any custom functions and don't set custom function class
    */
   public NeuralNetworkBuilder(StructureConfigurations configurations, PrebioticApi prebiotic) throws EmptyFunctionDefinitionException {
      this.configurations = configurations;
      if (configurations.neuralNetworkType == NeuralNetworkType.FEEDFORWARD)
         rawNetwork = new NeuralNetworkImpl();
      if (configurations.neuralNetworkType == NeuralNetworkType.KOHONEN)
         rawNetwork = new KohonenNeuralNetworkImpl();
      if (configurations.neuralNetworkType == NeuralNetworkType.HOPFIELD)
         rawNetwork = new HopfieldNeuralNetworkImpl();
      if (configurations.axonFunctionRecipe != AxonFunctionRecipe.AXON_CUSTOM) {
         axon = configurations.axonFunctionRecipe.newInstance();
      } else {
         if (configurations.axonInstance == null)
            throw new EmptyFunctionDefinitionException("No axon function definition detected. Use method:" +
               " defineAxonInstance() in structure configurations builder");
         axon = (AxonFunction) configurations.axonInstance.newInstance();
         axon.setAxonFunctionRecipe(AxonFunctionRecipe.AXON_CUSTOM);
      }
      if (configurations.coreFunctionRecipe != CoreFunctionRecipe.CORE_CUSTOM) {
         core = configurations.coreFunctionRecipe.newInstance();
      } else {
         if (configurations.coreInstance == null)
            throw new EmptyFunctionDefinitionException("No core function definition detected. Use method:" +
               " defineCoreInstance() in structure configurations builder");
         core = (CoreFunction) configurations.coreInstance.newInstance();
         core.setCoreFunctionRecipe(CoreFunctionRecipe.CORE_CUSTOM);
      }
      rawNetwork.setBiasPresence(configurations.biasPresence);
      rawNetwork.setDataNormalization(configurations.dataNormalization);
      rawNetwork.setType(configurations.neuralNetworkType);
      this.prebiotic = prebiotic;
   }

   /**
    * Adds default input layer. In feedforward networks whole input vector is given to all neurons of
    * this layer, so it'TeachingDataManagementComposite no matter how much neurons are placed here. When you builds this layer
    * please don't worry about number of inputs for every neurons - Prebiotic Frameworks knows what
    * to do.
    *
    * @param numberOfNeurons   to set in input layer
    * @param sizeOfInputVector number of coordinates in input vector, and number of inputs of each
    *                          vector in input layer.
    * @return builder
    * @throws InvalidStructureException        when structure of network is invalid
    * @throws EmptyFunctionDefinitionException when custom functions are not defined
    */
   public NetworkWithInputLayerBuilder addDefaultInputLayer(int numberOfNeurons, int sizeOfInputVector) throws InvalidStructureException, EmptyFunctionDefinitionException {
      NeuralLayer layer = createLayer(numberOfNeurons, sizeOfInputVector);
      layer.setInputLayer(true);
      List<NeuralLayer> layers = new ArrayList<>();
      layers.add(layer);
      rawNetwork.setLayers(layers);
      return new NetworkWithInputLayerBuilder(configurations, rawNetwork, core, axon, prebiotic);
   }

   /**
    * Builds a Kohonen'TeachingDataManagementComposite Map layer. This is a correct way to createGene Kohonen'TeachingDataManagementComposite neural network.
    *
    * @param xNeurons          amount of neurons in x axis
    * @param yNeurons          amount of neurons in y axis
    * @param sizeOfInputVector number of coordinates in input vector, and number of inputs of each
    *                          vector in this layer.
    * @return endpoint builder
    * @throws InvalidStructureException        when type of network is invalid
    * @throws EmptyFunctionDefinitionException when custom functions are not defined
    */
   public EndpointBuilder buildKohonenMap(int xNeurons, int yNeurons, int sizeOfInputVector) throws InvalidStructureException, EmptyFunctionDefinitionException {
      NeuralLayer layer = createLayer(xNeurons, yNeurons, sizeOfInputVector);
      layer.setInputLayer(true);
      List<NeuralLayer> layers = new ArrayList<>();
      layers.add(layer);
      rawNetwork.setLayers(layers);
      rawNetwork.setXSize(xNeurons);
      rawNetwork.setYSize(yNeurons);
      return new EndpointBuilder(rawNetwork, prebiotic);
   }

   /**
    * Builds the Hopfield's network.
    *
    * @param numberOfNeurons to set in network. This is also the size of input vector.
    * @return builder
    * @throws EmptyFunctionDefinitionException when definitions of custom functions are not defined
    * @throws InvalidStructureException        when structure of network is wrong
    */
   public EndpointBuilder buildHopfieldNetwork(int numberOfNeurons)
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      rawNetwork.setLayers(prepareLayerList(numberOfNeurons, numberOfNeurons));
      return new EndpointBuilder(rawNetwork, prebiotic);
   }

   /**
    * Creates one layer and returns {@link EndpointBuilder}. Each neuron gets whole input vector to
    * his inputs.
    *
    * @param numberOfNeurons   to set in input layer
    * @param sizeOfInputVector number of coordinates in input vector, and number of inputs of each
    *                          vector in input layer.
    * @return endpoint builder
    * @throws InvalidStructureException        when structure of network is invalid
    * @throws EmptyFunctionDefinitionException when custom functions are not defined
    */
   public EndpointBuilder buildSingleLayerNetwork(int numberOfNeurons,
                                                  int sizeOfInputVector) throws InvalidStructureException, EmptyFunctionDefinitionException {
      rawNetwork.setLayers(prepareLayerList(numberOfNeurons, sizeOfInputVector));
      return new EndpointBuilder(rawNetwork, prebiotic);
   }

   private NeuralLayer createLayer(int xNeurons, int yNeurons, int sizeOfInputVector) throws InvalidStructureException, EmptyFunctionDefinitionException {
      if (configurations.neuralNetworkType != NeuralNetworkType.KOHONEN)
         throw new InvalidStructureException("Only Kohonen'TeachingDataManagementComposite networks can be built by buildKohonenMap()" +
            " method. Use proper method for " + configurations.neuralNetworkType);

      NeuralLayer layer = new NeuralLayerImpl();
      List<Neuron> neurons = new ArrayList<>();
      for (int x = 0; x < xNeurons; x++) {
         for (int y = 0; y < yNeurons; y++) {
            List<WeightFunction> weights = new ArrayList<>();
            createWeights(sizeOfInputVector, weights);
            Neuron neuron = new KohonenNeuronImpl(weights, core, axon, rawNetwork.isBiasPresence(), x, y);
            neurons.add(neuron);
         }
      }
      layer.setNeurons(neurons);
      return layer;
   }

   private NeuralLayer createLayer(int numberOfNeurons, int amountOfInputsOfEachNeuron) throws InvalidStructureException, EmptyFunctionDefinitionException {
      NeuralLayer layer = new NeuralLayerImpl();
      List<Neuron> neurons = new ArrayList<>();
      for (int i = 0; i < numberOfNeurons; i++) {
         List<WeightFunction> weights = new ArrayList<>();
         createWeights(amountOfInputsOfEachNeuron, weights);
         if (rawNetwork.getType() == NeuralNetworkType.KOHONEN)
            throw new InvalidStructureException("Kohonen'TeachingDataManagementComposite neural network should be built by buildKohonenMap() method. " +
               "Use it instead of default layer creation methods.");
         Neuron neuron = new NeuronImpl(weights, core, axon, rawNetwork.isBiasPresence());
         neurons.add(neuron);
      }
      layer.setNeurons(neurons);
      return layer;
   }

   private void createWeights(int amountOfInputsOfEachNeuron, List<WeightFunction> weights) throws EmptyFunctionDefinitionException {
      for (int j = 0; j < amountOfInputsOfEachNeuron; j++) {
         if (configurations.weightFunctionRecipe != WeightFunctionRecipe.WEIGHT_CUSTOM)
            weights.add(configurations.weightFunctionRecipe.newInstance(false));
         else {
            if (configurations.weightInstance == null)
               throw new EmptyFunctionDefinitionException("No weight function definition detected. Use method:" +
                  " defineWeightInstance() in structure configurations builder");
            WeightFunction w = (WeightFunction) configurations.weightInstance.newInstance();
            w.setWeightFunctionRecipe(WeightFunctionRecipe.WEIGHT_CUSTOM);
            weights.add(w);
         }
      }
   }

   private List<NeuralLayer> prepareLayerList(int numberOfNeurons, int inputVectorSize)
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      NeuralLayer layer = createLayer(numberOfNeurons, inputVectorSize);
      layer.setInputLayer(true);
      List<NeuralLayer> layers = new ArrayList<>();
      layers.add(layer);
      return layers;
   }
}
