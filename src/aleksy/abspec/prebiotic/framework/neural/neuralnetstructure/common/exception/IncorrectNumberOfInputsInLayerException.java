package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralLayerException;

/**
 * Incorrect number of inputs in layer exception
 */
public class IncorrectNumberOfInputsInLayerException extends NeuralLayerException {
   public IncorrectNumberOfInputsInLayerException(String message) {
      super("\nIncorrect number of inputs. MESSAGE: \"" + message + "\"");
   }
}
