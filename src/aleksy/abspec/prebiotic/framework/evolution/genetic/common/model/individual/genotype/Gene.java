package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype;

import aleksy.abspec.prebiotic.framework.common.model.Range;

import java.util.Random;

/**
 * Gene class
 */
public class Gene {
   private double value;
   private Range valueRange;
   private String description;

   /**
    * Constructor
    *
    * @param gene to copy
    */
   public Gene(Gene gene) {
      this.value = gene.value;
      this.description = gene.description;
      this.valueRange = gene.valueRange;
   }

   /**
    * Constructor
    *
    * @param value         of gene
    * @param mutationRange of gene
    * @param description   of gene
    */
   public Gene(double value, Range mutationRange, String description) {
      this.value = value;
      this.valueRange = mutationRange;
      this.description = description;
   }

   /**
    * Mutation function of gene. Overriding this function can be useful in specific
    * problem
    */
   public void mutate() {
      double raw = new Random().nextDouble();
      value = valueRange.getFrom()+ raw * Math.abs(valueRange.getFrom()- valueRange.getTo());
   }

   /**
    * Getter for value
    *
    * @return value
    */
   public double getValue() {
      return value;
   }

   /**
    * Getter for description
    *
    * @return description
    */
   public String getDescription() {
      return description;
   }

   /**
    * Setter for description
    *
    * @param description to set
    */
   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Getter for value range
    *
    * @return value range
    */
   public Range getValueRange() {
      return valueRange;
   }

   @Override
   public String toString() {
      return "       gene \"" + description + "\": " + value + ", mutate from " + valueRange.getFrom()+ " to " + valueRange.getTo();
   }
}
