package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;

/**
 * Abstract axon function
 */
public abstract class AbstractAxonFunction implements AxonFunction {
   protected AxonFunctionRecipe axonFunctionRecipe;

   @Override
   public AxonFunctionRecipe getAxonFunctionRecipe() {
      return axonFunctionRecipe;
   }

   @Override
   public void setAxonFunctionRecipe(AxonFunctionRecipe axonFunctionRecipe) {
      this.axonFunctionRecipe = axonFunctionRecipe;
   }
}
