package aleksy.abspec.prebiotic.framework.vision.display.panel.activity;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.vision.common.constants.VisionConstants;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractActivityNeuralPanel;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ActivityGraphNeuralPanel extends AbstractActivityNeuralPanel {
   private List<List<Double>> activities;
   private List<List<Double>> inputs;
   private long previousNumberOfOperation;

   public ActivityGraphNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
      activities = new ArrayList<>();
      inputs = new ArrayList<>();
      previousNumberOfOperation = 0;
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(Color.GRAY);
      g.setFont(font);
      g.drawString("Activity time graph:", 5, getSize().height / 20);
      double xStep = getWidth() / (activities.size() != 0 ? activities.size() : 1.0);
      double yOffset = getHeight() / 6;
      double yBackOffset = yOffset * 1.3;
      double yStep = (getHeight() - yOffset - yBackOffset) / (nn.size() + nn.sizeOfInputVector());
      if (nn.getNumberOfOperation() != previousNumberOfOperation) {
         activities.add(new ArrayList<>());
         for (int i = 0; i < nn.size(); i++) {
            activities.get(activities.size() - 1).add(calculateDistanceForNetwork(nn, i));
         }
         inputs.add(new ArrayList<>(nn.getActualInputVector()));
         previousNumberOfOperation++;
      }
      if (activities.size() > 40) {
         activities.remove(0);
         inputs.remove(0);
      }
      for (int i = 0; i < activities.size(); i++) {
         List<Double> activity = activities.get(i);
         List<Double> input = inputs.get(i);
         for (int j = 0; j < activity.size(); j++) {
            g.setColor(createColor(activity.get(j)).darker());
            g.fillRect((int) (i * xStep), (int) (j * yStep + yOffset),
               (int) ((i + 1) * xStep), (int) ((j + 1) * yStep + yOffset));
         }
         for (int j = 0; j < input.size(); j++) {
            g.setColor(createAntiColor(
               VectorNormalizator.setValueBetweenZeroAndOne(input.get(j),
                  inputRange.getFrom(), inputRange.getTo())).darker());
            g.fillRect((int) (i * xStep), (int) ((j + activity.size()) * yStep + yOffset),
               (int) ((i + 1) * xStep), (int) ((j + 1 + activity.size()) * yStep + yOffset));
         }
      }
      g.setColor(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
      g.fillRect(0, (int) ((activities.get(0).size() + inputs.get(0).size() - 1) * yStep + yOffset),
         getWidth(), (int) yBackOffset * 4);

   }
}
