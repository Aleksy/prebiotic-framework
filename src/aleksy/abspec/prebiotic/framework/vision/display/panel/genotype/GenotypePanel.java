package aleksy.abspec.prebiotic.framework.vision.display.panel.genotype;

import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.vision.common.constants.VisionConstants;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractPanel;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Chromosome;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Gene;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

import java.awt.*;
import java.util.List;

public class GenotypePanel extends AbstractPanel {
   Genotype genotype;

   public GenotypePanel(Genotype genotype) {
      super();
      this.genotype = genotype;
      setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      List<Chromosome> chromosomes = genotype.getChromosomes();
      int genotypeHeight = 0;
      int genotypeMaxWidth = 0;
      for (Chromosome chromosome : chromosomes) {
         double square = Math.ceil(Math.sqrt(chromosome.size()));
         genotypeHeight += (int) square;
         if ((int) square > genotypeMaxWidth)
            genotypeMaxWidth = (int) square;
      }
      int yOffset = (int) (getSize().height * 0.1);
      int xOffset = (int) (getSize().width * 0.1);
      int pixelHeight = (int) ((getSize().height - yOffset * 2) / (double) (genotypeHeight));
      int pixelWidth = (int) ((getSize().width - xOffset * 2) / (genotypeMaxWidth + 1.5));

      int chrOffset = 0;
      for (Chromosome chromosome : chromosomes) {
         int actX = 0;
         int actY = 0;
         int chrFrameX = xOffset + pixelWidth * actX;
         int chrFrameY = yOffset + pixelHeight * actY + chrOffset * pixelHeight;
         int square = (int) Math.ceil(Math.sqrt(chromosome.size()));
         for (Gene gene : chromosome.getGenes()) {
            int x = xOffset + pixelWidth * actX;
            int y = yOffset + pixelHeight * actY + chrOffset * pixelHeight;
            drawGene(x, y, pixelWidth, pixelHeight, gene, g);
            if (++actX >= square) {
               actX = 0;
               actY++;
            }
         }
         chrOffset += square;
         drawChrFrame(chrFrameX, chrFrameY, pixelWidth * square, pixelHeight * square, g);
      }
   }

   private void drawChrFrame(int chrFrameX, int chrFrameY, int width, int height, Graphics g) {
      g.setColor(Color.BLACK);
      g.drawRect(chrFrameX, chrFrameY, width, height);
      g.drawRect(chrFrameX + 1, chrFrameY + 1, width, height);
   }

   private void drawGene(int x, int y, int width, int height, Gene gene, Graphics g) {
      double value = VectorNormalizator.setValueBetweenZeroAndOne(gene.getValue(),
         gene.getValueRange().getFrom(), gene.getValueRange().getTo());
      g.setColor(createColor(value));
      g.fillRect(x, y, width, height);
      g.setColor(createColor(value).brighter());
      g.drawRect(x, y, width, height);
   }

   private Color createColor(double v) {
      return new Color(Color.HSBtoRGB((float) v, 0.31f, 0.6f));
   }
}
