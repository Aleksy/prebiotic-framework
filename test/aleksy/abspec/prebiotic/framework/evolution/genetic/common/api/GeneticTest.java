package aleksy.abspec.prebiotic.framework.evolution.genetic.common.api;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.config.GeneticAlgorithmConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.impl.Genetic;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GeneticTest {
   private GeneticApi uut;

   @Before
   public void init() {
      uut = new Genetic(Prebiotic.newInstance());
   }

   @Test
   public void shouldReturnGenotypeBuilder() {
      // when
      GenotypeBuilder genotypeBuilder = uut.getGenotypeBuilder();

      // then
      assertEquals(GenotypeBuilder.class, genotypeBuilder.getClass());
   }

   @Test
   public void shouldReturnGeneticConfigurationsBuilder() {
      // when
      GeneticAlgorithmConfigurationsBuilder geneticAlgorithmConfigurationsBuilder = uut
         .getGeneticAlgorithmConfigurationsBuilder();

      // then
      Assert.assertEquals(GeneticAlgorithmConfigurationsBuilder.class,
         geneticAlgorithmConfigurationsBuilder.getClass());
   }
}
