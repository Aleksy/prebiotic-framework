package aleksy.abspec.prebiotic.framework.evolution.genetic.builder.config;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;

/**
 * Genetic algorithm configurations builder
 */
public class GeneticAlgorithmConfigurationsBuilder extends Loggable {
   private GeneticAlgorithmConfigurations config;
   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public GeneticAlgorithmConfigurationsBuilder(PrebioticApi prebiotic) {
      config = new GeneticAlgorithmConfigurations();
      this.prebiotic = prebiotic;
   }

   /**
    * Setter for mutation chance in genetic algorithm (usually mutation chance is 0.01)
    *
    * @param mutationChance to set
    * @return builder
    */
   public GeneticAlgorithmConfigurationsBuilder setMutationChance(Double mutationChance) {
      config.mutationChance = mutationChance;
      return this;
   }

   /**
    * Setter for amount of generations
    *
    * @param amountOfGenerations to set
    * @return builder
    */
   public GeneticAlgorithmConfigurationsBuilder setAmountOfGenerations(int amountOfGenerations) {
      config.generations = amountOfGenerations;
      return this;
   }

   /**
    * Setter for selection mode
    *
    * @param geneticSelectionMode to set
    * @return builder
    */
   public GeneticAlgorithmConfigurationsBuilder setSelectionMode(
      GeneticSelectionMode geneticSelectionMode) {
      config.geneticSelectionMode = geneticSelectionMode;
      return this;
   }

   /**
    * Setter for iterations displaying parameter
    * Value defines iterations which will be displayed in the console.
    * Example:
    * when iterationsDisplaying = 100 then
    * algorithm display status after 100, 200, 300... iterations
    *
    * @param iterationsDisplaying to set
    * @return builder
    */
   public GeneticAlgorithmConfigurationsBuilder setIterationsDisplaying(int iterationsDisplaying) {
      config.iterationsDisplaying = iterationsDisplaying;
      return this;
   }

   /**
    * Setter for delay value. Value defines delay between each iteration of algorithm and is given in
    * milliseconds. This value can be used with {@link Vision} to
    * research the neural network behavior (WARNING: this value slows down the teaching process).
    *
    * @param delay to set
    * @return builder
    */
   public GeneticAlgorithmConfigurationsBuilder setDelay(int delay) {
      config.delay = delay;
      return this;
   }

   /**
    * Creates a default configurations. Generations number is needed abspec parameters.
    * Mutation chance is set to 1% (0.01), genetic selection mode is ROULETTE
    * and iterations displaying equals 100.
    *
    * @param generations to set
    * @return builder
    */
   public GeneticAlgorithmConfigurationsBuilder defaultConfigurations(int generations) {
      config.geneticSelectionMode = GeneticSelectionMode.ROULETTE;
      config.mutationChance = 0.01;
      config.generations = generations;
      config.iterationsDisplaying = 100;
      return this;
   }

   /**
    * Creation method
    *
    * @return new instance of {@link GeneticAlgorithmConfigurations}
    */
   public GeneticAlgorithmConfigurations create() {
      String delay;
      if (config.delay == null) {
         delay = "not set";
         config.delay = 0;
      } else
         delay = config.delay.toString();
      prebiotic.prebioticSystemApi().log("Genetic algorithm configurations has been built.", getSenderName());
      prebiotic.prebioticSystemApi().separator();
      prebiotic.prebioticSystemApi().log("  Generations: " + config.generations, getSenderName());
      prebiotic.prebioticSystemApi().log("  Mutation chance: " + config.mutationChance, getSenderName());
      prebiotic.prebioticSystemApi().log("  Selection mode: " + config.geneticSelectionMode,
         getSenderName());
      prebiotic.prebioticSystemApi().log("  Iterations displaying interval: " + config.iterationsDisplaying, getSenderName());
      prebiotic.prebioticSystemApi().log("  Delay: " + delay, getSenderName());
      prebiotic.prebioticSystemApi().separator();
      return config;
   }

   @Override
   protected String getSenderName() {
      return "GEN-CONF-BLD";
   }
}

