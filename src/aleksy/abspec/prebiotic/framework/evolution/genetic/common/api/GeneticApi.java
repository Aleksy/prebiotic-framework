package aleksy.abspec.prebiotic.framework.evolution.genetic.common.api;

import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.config.GeneticAlgorithmConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.exception.GeneticAlgorithmException;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.environment.Environment;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;

import java.util.List;

/**
 * Genetic submodule api
 */
public interface GeneticApi {

   /**
    * Getter for genotype parsing
    *
    * @return parsing
    */
   GenotypeBuilder getGenotypeBuilder();

   /**
    * Starts the genetic algorithm
    *
    * @param population     to process
    * @param environment    to setting fitness values for population
    * @param configurations of genetic algorithm
    * @return last population
    * @throws GeneticAlgorithmException when something goes wrong
    */
   List<Geneticable> geneticAlgorithm(List<Geneticable> population, Environment environment, GeneticAlgorithmConfigurations configurations)
      throws GeneticAlgorithmException;

   /**
    * Starts the genetic algorithm (for individuals with brains)
    *
    * @param population     to process
    * @param environment    to setting fitness values for population
    * @param configurations of genetic algorithm
    * @param templateBrain  of each individual
    * @return last population
    * @throws GeneticAlgorithmException when something goes wrong
    */
   List<Geneticable> geneticAlgorithm(List<Geneticable> population, Environment environment, GeneticAlgorithmConfigurations configurations, Brain templateBrain)
      throws GeneticAlgorithmException;

   /**
    * Starts one iteration of genetic algorithm (for individuals with brains)
    *
    * @param population     to process
    * @param configurations of genetic algorithm
    * @param templateBrain  of each individual
    * @return last population
    * @throws GeneticAlgorithmException when something goes wrong
    */
   List<Geneticable> geneticAlgorithm(List<Geneticable> population, GeneticAlgorithmConfigurations configurations, Brain templateBrain)
      throws GeneticAlgorithmException;

   /**
    * Starts one iteration of genetic algorithm
    *
    * @param population     with fitness values
    * @param configurations of genetic algorithm
    * @return next population
    * @throws GeneticAlgorithmException when something goes wrong
    */
   List<Geneticable> geneticAlgorithm(List<Geneticable> population, GeneticAlgorithmConfigurations configurations)
      throws GeneticAlgorithmException;


   /**
    * Getter for new instance of genetic algorithm configurations parsing
    *
    * @return builder
    */
   GeneticAlgorithmConfigurationsBuilder getGeneticAlgorithmConfigurationsBuilder();
}
