package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;

/**
 * Invalid structure of neural network exception
 */
public class InvalidStructureException extends NeuralNetworkException {
   public InvalidStructureException(String message) {
      super("\nInvalid structure: " + message);
   }
}
