package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.WeightFunctions;

/**
 * Weight functions recipes
 */
public enum WeightFunctionRecipe {
   /**
    * Default linear function with one factor
    */
   DEFAULT_LINEAR,

   /**
    * Square difference function with one factor: (w - x)^2
    */
   SQUARE_DIFFERENCE,

   /**
    * Custom weight function
    */
   WEIGHT_CUSTOM;

   /**
    * Method creates the new instance of weight function to set to each neuron
    *
    * @param randomizeFactors new instance has randomized factors when true
    * @return new instance of weight function
    */
   public WeightFunction newInstance(boolean randomizeFactors) {
      if (this == WeightFunctionRecipe.DEFAULT_LINEAR)
         return new WeightFunctions.DEFAULT_LINEAR(randomizeFactors);
      if (this == WeightFunctionRecipe.SQUARE_DIFFERENCE)
         return new WeightFunctions.SQUARE_DIFFERENCE(randomizeFactors);
      return null;
   }
}
