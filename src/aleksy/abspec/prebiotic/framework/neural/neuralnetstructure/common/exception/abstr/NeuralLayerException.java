package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr;

/**
 * Neural layer exception
 */
public class NeuralLayerException extends NeuralNetworkException {
   public NeuralLayerException(String message) {
      super("layer -> " + message);
   }
}
