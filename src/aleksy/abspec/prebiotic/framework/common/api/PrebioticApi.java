package aleksy.abspec.prebiotic.framework.common.api;

import aleksy.abspec.prebiotic.framework.evolution.common.api.EvolutionApi;
import aleksy.abspec.prebiotic.framework.neural.common.api.NeuralApi;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.api.PrebioticSystemApi;
import aleksy.abspec.prebiotic.framework.vision.common.api.VisionApi;

import java.io.PrintStream;
import java.util.List;

/**
 * Main framework api, this is an entry point for every module of
 * Framework. Module contains also the basic information about
 * PF.
 */
public interface PrebioticApi {
   /**
    * Getter of framework version
    *
    * @return string representation of version
    */
   String getVersion();

   /**
    * Getter for framework name
    *
    * @return framework name
    */
   String getFrameworkName();

   /**
    * Getter for author name, surname and city
    *
    * @return author data
    */
   String getAuthor();

   /**
    * Allows Prebiotic Framework to logging messages to console
    */
   void enableEventLogging();

   /**
    * Disables Prebiotic Framework to logging messages to console
    */
   void disableEventLogging();

   /**
    * Setter for one print stream of Prebiotic Api
    *
    * @param printStream to set
    */
   void setOnePrintStream(PrintStream printStream);

   /**
    * Getter for print streams of Prebiotic Api
    *
    * @return print streams
    */
   List<PrintStream> getPrintStreams();

   /**
    * Adds new print stream to the api instance
    *
    * @param printStream to add
    */
   void addPrintStream(PrintStream printStream);

   /**
    * Removes existing print stream from the api instance, or does nothing if
    * the print stream doesn't exists
    *
    * @param printStream to remove
    */
   void removePrintStream(PrintStream printStream);

   /**
    * Returns true when the instance of Framework can log events in the console
    *
    * @return message logging
    */
   boolean isEventLoggingEnabled();

   /**
    * Getter for Neural module. It is the main module of Framework. It is used for
    * model, training and testing the neural networks. User can create own definition
    * of problem and train the neural networks to solve it. More information can be
    * found here:
    * https://gitlab.com/Aleksy/prebiotic-framework/wikis/prebiotic-framework
    *
    * @return neural api
    */
   NeuralApi neuralApi();

   /**
    * Getter for Evolution module. The module is used to model the problems, which can be
    * solved by evolution algorithms. In the module exists the implementation of special
    * model which uses genetic algorithms with neural networks.
    * More information can be found here:
    * https://gitlab.com/Aleksy/prebiotic-framework/wikis/protista-training
    *
    * @return evolution api
    */
   EvolutionApi evolutionApi();

   /**
    * Getter fpr Vision module. The module is used to display the status of some model objects
    * of the Framework - neural networks, genotypes. Module cans also generate graphics panels.
    * Panels can be used in the user frames, windows and gui. Displayable components
    * are implemented in JSwing library.
    *
    * @return vision api
    */
   VisionApi visionApi();

   /**
    * Getter for Prebiotic System module. The module is used to log messages in
    * the console and for operations on the files (like saving and reading neural networks)
    *
    * @return prebiotic system api
    */
   PrebioticSystemApi prebioticSystemApi();
}
