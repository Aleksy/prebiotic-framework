package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.enumerate.FileFormat;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParserTest {
   private Parser uut;

   @Before
   public void init() {
      uut = new Parser(Prebiotic.newInstance());
   }

   @Test
   public void shouldSaveFeedforwardAndReadXmlFile()
      throws Exception {
      // given
      NeuralNetwork toSave = prepareFeedforwardNeuralNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\fedNetwork.xml", FileFormat.XML);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\fedNetwork.xml");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveKohonenAndReadXmlFile()
      throws Exception {
      // given
      NeuralNetwork toSave = prepareKohonenNeuralNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\kohNetwork.xml", FileFormat.XML);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\kohNetwork.xml");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveHopfieldAndReadXmlFile()
      throws Exception {
      // given
      NeuralNetwork toSave = prepareHopfieldNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\hopNetwork.xml", FileFormat.XML);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\hopNetwork.xml");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveFeedforwardAndReadPnnFile()
      throws Exception {
      // given
      NeuralNetwork toSave = prepareFeedforwardNeuralNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\fedNetwork.pnn", FileFormat.PNN);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\fedNetwork.pnn");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveKohonenAndReadPnnFile()
      throws Exception {
      // given
      NeuralNetwork toSave = prepareKohonenNeuralNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\kohNetwork.pnn", FileFormat.PNN);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\kohNetwork.pnn");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveHopfieldAndReadPnnFile()
      throws Exception {
      // given
      NeuralNetwork toSave = prepareHopfieldNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\hopNetwork.pnn", FileFormat.PNN);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\hopNetwork.pnn");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveFeedforwardAndReadJsonFile() throws Exception {
      // given
      NeuralNetwork toSave = prepareFeedforwardNeuralNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\fedNetwork.json", FileFormat.JSON);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\fedNetwork.json");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveKohonenAndReadJsonFile() throws Exception {
      // given
      NeuralNetwork toSave = prepareKohonenNeuralNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\kohNetwork.json", FileFormat.JSON);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\kohNetwork.json");

      // then
      makeAssertions(toSave, saved);
   }

   @Test
   public void shouldSaveHopfieldAndReadJsonFile() throws Exception {
      // given
      NeuralNetwork toSave = prepareHopfieldNetwork();

      // when
      uut.save(toSave, "test\\resources\\parsertest\\hopNetwork.json", FileFormat.JSON);
      NeuralNetwork saved = uut.read("test\\resources\\parsertest\\hopNetwork.json");

      // then
      makeAssertions(toSave, saved);
   }

   private void makeAssertions(NeuralNetwork toSave, NeuralNetwork saved) {
      Assert.assertEquals(toSave.getType(), saved.getType());
      Assert.assertEquals(toSave.getAxonFunction().getAxonFunctionRecipe(), saved.getAxonFunction().getAxonFunctionRecipe());
      Assert.assertEquals(toSave.getCoreFunction().getCoreFunctionRecipe(), saved.getCoreFunction().getCoreFunctionRecipe());
      Assert.assertEquals(toSave.getWeightFunctionRecipe(), saved.getWeightFunctionRecipe());
      assertEquals(toSave.getDescription(), saved.getDescription());
      Assert.assertEquals(toSave.getDataNormalization(), saved.getDataNormalization());
      assertEquals(toSave.getXSize(), saved.getXSize());
      assertEquals(toSave.getYSize(), saved.getYSize());
      assertEquals(toSave.size(), saved.size());
      assertEquals(toSave.isBiasPresence(), saved.isBiasPresence());
      assertEquals(toSave.sizeOfInputVector(), saved.sizeOfInputVector());
      assertEquals(toSave.numberOfLayers(), saved.numberOfLayers());
      Assert.assertEquals(toSave.getAllNeurons().get(0).getWeights().get(0).getFactors().get(0),
         saved.getAllNeurons().get(0).getWeights().get(0).getFactors().get(0));
   }

   private NeuralNetwork prepareFeedforwardNeuralNetwork()
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      PrebioticApi p = Prebiotic.newInstance();
      return p.neuralApi().getNeuralNetworkBuilder(
         p.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setBiasPresence(false)
            .create()
      ).buildSingleLayerNetwork(1, 1).create();
   }

   private NeuralNetwork prepareKohonenNeuralNetwork()
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      PrebioticApi p = Prebiotic.newInstance();
      return p.neuralApi().getNeuralNetworkBuilder(
         p.neuralApi().getStructureConfigurationsBuilder()
            .defaultKohonenConfigurations()
            .create()
      ).buildKohonenMap(1, 1, 1).create();
   }

   private NeuralNetwork prepareHopfieldNetwork()
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      PrebioticApi p = Prebiotic.newInstance();
      return p.neuralApi().getNeuralNetworkBuilder(
         p.neuralApi().getStructureConfigurationsBuilder()
            .defaultHopfieldConfigurations()
            .create()
      ).buildHopfieldNetwork(1).create();
   }
}
