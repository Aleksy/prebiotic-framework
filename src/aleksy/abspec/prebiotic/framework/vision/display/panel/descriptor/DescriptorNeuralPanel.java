package aleksy.abspec.prebiotic.framework.vision.display.panel.descriptor;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractNeuralPanel;

import java.awt.*;

public class DescriptorNeuralPanel extends AbstractNeuralPanel {
   public DescriptorNeuralPanel(NeuralNetwork nn, Double hue) {
      super(nn, hue, null);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setFont(font);
      Color basic = createColor(0.8);
      Color anti = createAntiColor(1.0);
      Color neutral = Color.GRAY;
      int x = getSize().width / 40;
      int y = 20;
      int step = 1;
      drawFieldsNames(x, y, step, basic, neutral, g);
      drawValues(x, y, step, anti, g);
   }

   private void drawFieldsNames(int x, int y, int step, Color basic, Color neutral, Graphics g) {
      g.setColor(basic);
      g.drawString("Neural network data", x, y * step++);
      step++;
      g.setColor(neutral);
      g.drawString("Type:", x, y * step++);
      g.drawString("Number of layers:", x, y * step++);
      g.drawString("Number of neurons:", x, y * step++);
      g.drawString("Is bias presented:", x, y * step++);
      g.drawString("Normalization type:", x, y * step++);
      g.setColor(basic);
      g.drawString("Neuron model", x, y * step++);
      g.setColor(neutral);
      g.drawString("Weight model:", x, y * step++);
      g.drawString("Core model:", x, y * step++);
      g.drawString("Axon model:", x, y * step);
   }

   private void drawValues(int x, int y, int step, Color basic, Graphics g) {
      g.setColor(basic);
      step++;
      int bias = 137;
      String description = nn.getDescription();
      if (description.length() > 50) {
         description = description.substring(0, 47);
         description += "...";
      }
      g.setFont(italicFont);
      g.setColor(basic.darker());
      g.drawString("\"" + description + "\"", x, y * step++);
      g.setFont(font);
      g.setColor(basic);
      g.drawString(nn.getType().toString(), x + bias, y * step++);
      g.drawString(Integer.toString(nn.numberOfLayers()), x + bias, y * step++);
      g.drawString(Integer.toString(nn.size()), x + bias, y * step++);
      g.drawString(Boolean.toString(nn.isBiasPresence()), x + bias, y * step++);
      g.drawString(nn.getDataNormalization().toString(), x + bias, y * step++);
      step++;
      g.drawString(nn.getWeightFunctionRecipe().toString(), x + bias, y * step++);
      g.drawString(nn.getCoreFunction().getCoreFunctionRecipe().toString(), x + bias, y * step++);
      g.drawString(nn.getAxonFunction().getAxonFunctionRecipe().toString(), x + bias, y * step);

   }
}
