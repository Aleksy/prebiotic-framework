package aleksy.abspec.prebiotic.framework.neural.teaching.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningConfigurationsException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KohonenLearningLogicTest {
   private KohonenLearningLogic uut;
   private PrebioticApi prebiotic;

   @Before
   public void init() {
      prebiotic = Prebiotic.newInstance();
      uut = new KohonenLearningLogic(prebiotic);
   }

   @Test(expected = KohonenLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenIterationsAreLessThanZero() throws KohonenLearningException, NeuralNetworkException {
      // given
      KohonenLearningConfigurations config = prepareTestConfig();
      config.iterations = -1L;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = KohonenLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenLearningRateAreInvalid() throws KohonenLearningException, NeuralNetworkException {
      // given
      KohonenLearningConfigurations config = prepareTestConfig();
      config.learningRate = -0.2;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = KohonenLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenMaxDistanceIsIncorrect() throws KohonenLearningException, NeuralNetworkException {
      // given
      KohonenLearningConfigurations config = prepareTestConfig();
      config.maxDistance = -0.1;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = KohonenLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenNeuralNetworkStructureIsInvalid() throws KohonenLearningException, NeuralNetworkException {
      // given
      KohonenLearningConfigurations config = prepareTestConfig();

      // when
      uut.start(prepareTestTeachingData(), prepareIncorrectNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = KohonenLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenConfigsNotIncludeAnyEndingParams() throws KohonenLearningException, NeuralNetworkException {
      // given
      KohonenLearningConfigurations config = prepareTestConfig();
      config.iterations = null;
      config.minimalDefect = null;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   private TeachingData prepareTestTeachingData() {
      List<Double> inputs = new ArrayList<>(Collections.singletonList(1.0));
      List<Double> outputs = new ArrayList<>(Collections.singletonList(0.0));
      return prebiotic.neuralApi().getTeachingDataBuilder()
         .addDataPackage(inputs, outputs)
         .create();
   }

   private KohonenLearningConfigurations prepareTestConfig() {
      return prebiotic.neuralApi().getKohonenLearningConfigurationsBuilder()
         .setLearningRate(1.0)
         .setIterations(1).create();
   }

   private NeuralNetwork prepareTestNeuralNetwork() {
      NeuralNetwork testNetwork = null;
      try {
         testNetwork = prebiotic.neuralApi().getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultKohonenConfigurations()
               .create()
         ).buildKohonenMap(1, 1, 1)
            .create();
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
      return testNetwork;
   }

   private NeuralNetwork prepareIncorrectNeuralNetwork() {
      NeuralNetwork testNetwork = null;
      try {
         testNetwork = prebiotic.neuralApi().getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultFeedforwardConfigurations()
               .create()
         ).addDefaultInputLayer(1, 1)
            .endBuildingNetwork()
            .create();
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
      return testNetwork;
   }
}
