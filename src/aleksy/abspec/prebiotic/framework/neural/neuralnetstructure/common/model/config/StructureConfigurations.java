package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config;

import aleksy.abspec.prebiotic.framework.common.constants.BasicConstants;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.*;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;

/**
 * Structure configurations for {@link NeuralNetworkBuilder}
 */
public class StructureConfigurations {
   /**
    * Recipe for weight function class
    */
   public WeightFunctionRecipe weightFunctionRecipe;
   /**
    * Recipe for core function class
    */
   public CoreFunctionRecipe coreFunctionRecipe;
   /**
    * Recipe for axon function class
    */
   public AxonFunctionRecipe axonFunctionRecipe;
   /**
    * Type of neural network
    */
   public NeuralNetworkType neuralNetworkType;
   /**
    * Bias presence in neural network. If true, all neurons have additional weight always with BIAS value from
    * {@link BasicConstants}
    */
   public boolean biasPresence;
   /**
    * Input data normalization in first step of neural network work
    */
   public DataNormalizationType dataNormalization;

   /**
    * Instance of axon function needed when recipe is set to AXON_CUSTOM
    */
   public AxonFunction axonInstance;

   /**
    * Instance of core function needed when recipe is set to CORE_CUSTOM
    */
   public CoreFunction coreInstance;

   /**
    * Instance of weight function needed when recipe is set to WEIGHT_CUSTOM
    */
   public WeightFunction weightInstance;
}
