package aleksy.abspec.prebiotic.framework.vision.display.frame;

import aleksy.abspec.prebiotic.framework.common.constants.BasicConstants;
import aleksy.abspec.prebiotic.framework.vision.common.constants.VisionConstants;
import aleksy.abspec.prebiotic.framework.vision.display.panel.ResizablePanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractPanel;

import javax.swing.*;

/**
 * Frame to display the neural network with abstract component
 */
public class Frame extends JFrame {

   public Frame(AbstractPanel panel) {
      super(BasicConstants.SHORT_NAME + " " + BasicConstants.VERSION);
      setFocusable(true);
      setVisible(true);
      setSize(VisionConstants.START_FRAME_X_SIZE, VisionConstants.START_FRAME_Y_SIZE);
      setLayout(null);
      panel.setVisible(true);
      setContentPane(panel);
      setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
      setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      panel.setSize(VisionConstants.START_FRAME_X_SIZE, VisionConstants.START_FRAME_Y_SIZE);
   }

   public Frame(AbstractPanel structurePanel, AbstractPanel weightValuesPanel,
                AbstractPanel coordinateSystemPanel, AbstractPanel descriptor,
                AbstractPanel activityRound, AbstractPanel inputRound, AbstractPanel activityGraph) {
      super(BasicConstants.SHORT_NAME + " " + BasicConstants.VERSION);
      setFocusable(true);
      setVisible(true);
      setSize(VisionConstants.START_FRAME_X_SIZE * 3,
         VisionConstants.START_FRAME_Y_SIZE * 3);
      setLayout(null);
      setContentPane(new ResizablePanel());
      getContentPane().setSize(getSize());
      getContentPane().setLayout(null);
      structurePanel.setSize(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      weightValuesPanel.setSize(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      coordinateSystemPanel.setSize(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      descriptor.setSize(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      activityRound.setSize(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      inputRound.setSize(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      activityGraph.setSize(VisionConstants.START_FRAME_X_SIZE * 3,
         VisionConstants.START_FRAME_Y_SIZE);
      descriptor.setLocation(0, 0);
      weightValuesPanel.setLocation(VisionConstants.START_FRAME_X_SIZE, 0);
      structurePanel.setLocation(0, VisionConstants.START_FRAME_Y_SIZE);
      coordinateSystemPanel.setLocation(VisionConstants.START_FRAME_X_SIZE,
         VisionConstants.START_FRAME_Y_SIZE);
      activityRound.setLocation(VisionConstants.START_FRAME_X_SIZE * 2, 0);
      inputRound.setLocation(VisionConstants.START_FRAME_X_SIZE * 2,
         VisionConstants.START_FRAME_Y_SIZE);
      activityGraph.setLocation(0, VisionConstants.START_FRAME_Y_SIZE * 2);
      getContentPane().add(descriptor);
      getContentPane().add(weightValuesPanel);
      getContentPane().add(structurePanel);
      getContentPane().add(coordinateSystemPanel);
      getContentPane().add(activityRound);
      getContentPane().add(inputRound);
      getContentPane().add(activityGraph);
      setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
      setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
   }
}
