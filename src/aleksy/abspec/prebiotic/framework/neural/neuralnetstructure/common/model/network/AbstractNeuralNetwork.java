package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.constants.TechnicalConstants;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl.NeuralLayerImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.NeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract neural network
 */
public abstract class AbstractNeuralNetwork implements NeuralNetwork {
   private List<NeuralLayer> layers;
   private String description;
   private NeuralNetworkType type;
   private int xSize;
   private int ySize;
   private List<Double> actualOutput;

   protected boolean biasPresence;
   protected DataNormalizationType dataNormalization;
   protected PrebioticApi prebiotic;
   protected long numberOfOperation;

   public AbstractNeuralNetwork() {
      numberOfOperation = 0;
   }

   @Override
   public List<Double> f(List<Double> xs) throws
      IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException {
      List<Double> inputs = new ArrayList<>(xs);
      if (dataNormalization != DataNormalizationType.NO_NORMALIZATION) normalizeInputs(inputs);
      if (biasPresence)
         inputs.add(TechnicalConstants.BIAS);
      List<Double> internalOuts = new ArrayList<>(inputs);
      for (NeuralLayer layer : layers) {
         internalOuts = layer.f(internalOuts);
         if (biasPresence)
            internalOuts.add(TechnicalConstants.BIAS);
      }
      if (biasPresence)
         internalOuts.remove(internalOuts.size() - 1);
      actualOutput = internalOuts;
      numberOfOperation++;
      if (prebiotic != null) prebiotic.visionApi().repaintAll();
      return internalOuts;
   }

   @Override
   public int size() {
      int size = 0;
      for (NeuralLayer layer : layers)
         size += layer.size();
      return size;
   }

   @Override
   public int numberOfLayers() {
      return layers.size();
   }

   @Override
   public NeuralLayer getInputLayer() {
      return layers.get(0);
   }

   @Override
   public String getDescription() {
      return description;
   }

   @Override
   public void setDescription(String description) {
      this.description = description;
   }

   @Override
   public void setLayers(List<NeuralLayer> layers) {
      this.layers = layers;
   }

   @Override
   public List<NeuralLayer> getLayers() {
      return layers;
   }

   @Override
   public void setType(NeuralNetworkType type) {
      this.type = type;
   }

   @Override
   public NeuralNetworkType getType() {
      return type;
   }

   @Override
   public void randomizeWeights(Double from, Double to) {
      for (NeuralLayer layer : layers) {
         layer.randomizeWeights(from, to);
      }
   }

   @Override
   public CoreFunction getCoreFunction() {
      return getLayers().get(0).getNeurons().get(0).getCoreFunction();
   }

   @Override
   public AxonFunction getAxonFunction() {
      return getLayers().get(0).getNeurons().get(0).getAxonFunction();
   }

   @Override
   public void setBiasPresence(boolean biasPresence) {
      this.biasPresence = biasPresence;
   }

   @Override
   public boolean isBiasPresence() {
      return biasPresence;
   }

   @Override
   public void createBiasWeights() {
      List<Neuron> allNeurons = this.getAllNeurons();
      for (Neuron neuron : allNeurons) {
         neuron.addBiasWeight();
      }
   }

   @Override
   public List<Neuron> getAllNeurons() {
      List<Neuron> allNeurons = new ArrayList<>();
      for (NeuralLayer layer : layers) {
         allNeurons.addAll(layer.getNeurons());
      }
      return allNeurons;
   }

   @Override
   public WeightFunctionRecipe getWeightFunctionRecipe() {
      return getLayers().get(0).getNeurons().get(0).getWeightFunctionRecipe();
   }

   @Override
   public NeuralNetwork copy() {
      NeuralNetwork network = new NeuralNetworkImpl();
      network.setType(this.getType());
      network.setDescription(this.getDescription());
      network.setBiasPresence(this.isBiasPresence());
      network.setDataNormalization(this.getDataNormalization());
      List<NeuralLayer> layers = new ArrayList<>();
      for (NeuralLayer layer : this.getLayers()) {
         NeuralLayer newLayer = new NeuralLayerImpl();
         List<Neuron> neurons = new ArrayList<>();
         for (Neuron neuron : layer.getNeurons()) {
            neurons.add(new NeuronImpl(neuron.cloneWeights(),
               this.getCoreFunction(), this.getAxonFunction(), this.isBiasPresence()));
         }
         newLayer.setNeurons(neurons);
         newLayer.setInputLayer(false);
         layers.add(newLayer);
      }
      network.setLayers(layers);
      network.getLayers().get(0).setInputLayer(true);
      network.setPrebiotic(prebiotic);
      return network;
   }

   @Override
   public NeuralLayer getOutputLayer() {
      return layers.get(layers.size() - 1);
   }

   @Override
   public void setDataNormalization(DataNormalizationType dataNormalization) {
      this.dataNormalization = dataNormalization;
   }

   @Override
   public DataNormalizationType getDataNormalization() {
      return dataNormalization;
   }

   protected void normalizeInputs(List<Double> inputs) {
      if (dataNormalization == DataNormalizationType.SCALING_TO_AVERAGE) {
         VectorNormalizator.scaleToAverage(inputs);
         return;
      }
      if (dataNormalization == DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR) {
         VectorNormalizator.normalizeToUnitVector(inputs);
         return;
      }
      if (dataNormalization == DataNormalizationType.SCALING_TO_DEVIATION_FROM_AVERAGE) {
         VectorNormalizator.scaleToDeviationFromAverage(inputs);
         return;
      }
      if (dataNormalization == DataNormalizationType.SCAlING_TO_DEVIATION_FROM_MIN_VALUE) {
         VectorNormalizator.scaleToDeviationFromMinValue(inputs);
         return;
      }
      if (dataNormalization == DataNormalizationType.SCALING_TO_MAX_VALUE) {
         VectorNormalizator.scaleToMaxValue(inputs);
         return;
      }
      if (dataNormalization == DataNormalizationType.SCALING_TO_STANDARD_DEVIATION) {
         VectorNormalizator.scaleToStandardDeviation(inputs);
      }
   }

   @Override
   public PrebioticApi getPrebiotic() {
      return prebiotic;
   }

   @Override
   public void setPrebiotic(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   @Override
   public void setXSize(int xSize) {
      this.xSize = xSize;
   }

   @Override
   public void setYSize(int ySize) {
      this.ySize = ySize;
   }

   @Override
   public int getXSize() {
      return xSize;
   }

   @Override
   public int getYSize() {
      return ySize;
   }

   @Override
   public List<Double> getActualOutput() {
      return actualOutput;
   }

   @Override
   public void setActualOutput(List<Double> actualOutput) {
      this.actualOutput = actualOutput;
   }

   @Override
   public int sizeOfInputVector() {
      if (biasPresence)
         return getInputLayer().getNeurons().get(0).size() - 1;
      return getInputLayer().getNeurons().get(0).size();
   }

   @Override
   public long getNumberOfOperation() {
      return numberOfOperation;
   }

   @Override
   public List<Double> getActualInputVector() {
      return getAllNeurons().get(0).getActualInputs();
   }
}