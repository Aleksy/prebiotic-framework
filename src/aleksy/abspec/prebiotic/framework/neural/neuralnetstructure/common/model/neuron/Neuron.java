package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron;

import aleksy.abspec.prebiotic.framework.neural.teaching.logic.DeepLearningLogic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;

import java.util.List;

/**
 * Neuron interface
 */
public interface Neuron {
   /**
    * Function calculates output of neuron
    *
    * @param xs vector of inputs
    * @return calculated output in specific type
    * @throws IncorrectNumberOfInputsInNeuronException when number of inputs is not equals to number of weights functions
    */
   Double f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException;

   /**
    * Size of neuron
    *
    * @return number of inputs
    */
   int size();

   /**
    * Randomizes weights functions
    *
    * @param from lower bound
    * @param to   upper bound
    */
   void randomizeWeights(Double from, Double to);

   /**
    * Clones weights from neuron
    *
    * @return new list cloned weights
    */
   List<WeightFunction> cloneWeights();

   /**
    * Getter for instance of core function
    *
    * @return core function instance
    */
   CoreFunction getCoreFunction();

   /**
    * Getter for instance of axon function
    *
    * @return axon function instance
    */
   AxonFunction getAxonFunction();

   /**
    * Getter for bias presence
    *
    * @return bias presence
    */
   boolean isBiasPresence();

   /**
    * Setter for bias presence
    *
    * @param biasPresence to set
    */
   void setBiasPresence(boolean biasPresence);

   /**
    * Adds one weight to neuron when network has bias presence
    */
   void addBiasWeight();

   /**
    * Setter for weight function recipe
    *
    * @param weightFunctionRecipe to set
    */
   void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe);

   /**
    * Getter for weight function recipe
    *
    * @return weight function recipe
    */
   WeightFunctionRecipe getWeightFunctionRecipe();

   /**
    * Setter to defect of neuron (See also {@link DeepLearningLogic})
    *
    * @param defect to set
    */
   void setDefect(Double defect);

   /**
    * Getter for defect of neuron (See also {@link DeepLearningLogic})
    *
    * @return defect of neuron
    */
   Double getDefect();

   /**
    * Getter for weights list of neuron
    *
    * @return weights list
    */
   List<WeightFunction> getWeights();

   /**
    * Getter for actual output from core (See also {@link DeepLearningLogic})
    *
    * @return actual output from core
    */
   Double getActualOutputFromCore();

   /**
    * Setter for actual input vector of neuron (See also {@link DeepLearningLogic})
    *
    * @param actualInputs to set
    */
   void setActualInputs(List<Double> actualInputs);

   /**
    * Getter for actual input vector of neuron (See also {@link DeepLearningLogic})
    *
    * @return input vector
    */
   List<Double> getActualInputs();

   /**
    * Setter for actual axon output (See also {@link DeepLearningLogic})
    *
    * @param axonOutput to set
    */
   void setActualAxonOutput(Double axonOutput);

   /**
    * Getter for actual axon output (See also {@link DeepLearningLogic})
    *
    * @return actual axon output
    */
   Double getActualAxonOutput();

   /**
    * Creates a vector of all factors of all weights
    *
    * @return vector of weights
    */
   List<Double> getVectorOfWeights();
}
