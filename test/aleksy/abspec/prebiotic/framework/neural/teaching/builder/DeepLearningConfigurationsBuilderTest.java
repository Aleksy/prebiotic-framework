package aleksy.abspec.prebiotic.framework.neural.teaching.builder;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeepLearningConfigurationsBuilderTest {
   private final long ITEARTIONS = 1000L;
   private final Double LEARNING_RATE = 0.1;
   private final int ITERATIONS_DISPLAYING = 10;
   private final double MOMENTUM = 0.2;
   private final double REDUCED_LEARNING_RATE = 0.01;
   private final double SPEED_OF_REDUCING = 0.0001;
   private final double RANDOM_VALUES_MAXIMAL_RANGE = 0.01;

   private DeepLearningConfigurationsBuilder uut;

   @Before
   public void init() {
      uut = new DeepLearningConfigurationsBuilder(Prebiotic.newInstance());
   }

   @Test
   public void shouldCreateCustomConfigurations() {
      // given
      DeepLearningConfigurations expectedDlc = createTestConfig();

      // when
      uut.setIterations(expectedDlc.iterations);
      uut.setLearningRate(expectedDlc.learningRate);
      uut.setIterationsDisplaying(expectedDlc.iterationsDisplaying);
      uut.setMomentum(expectedDlc.momentum);
      uut.setRandomValuesMaximalRange(expectedDlc.randomValuesRange.to);
      uut.setSpeedOfReducing(expectedDlc.speedOfReducing);
      uut.setReducedLearningRate(expectedDlc.reducedLearningRate);
      DeepLearningConfigurations actualDlc = uut.create();

      // then
      Assert.assertEquals(new Long(ITEARTIONS), actualDlc.iterations);
      assertEquals(LEARNING_RATE, actualDlc.learningRate);
      assertEquals(ITERATIONS_DISPLAYING, (int) actualDlc.iterationsDisplaying);
      assertEquals(new Double(MOMENTUM), actualDlc.momentum);
      assertEquals(new Double(REDUCED_LEARNING_RATE), actualDlc.reducedLearningRate);
      assertEquals(new Double(SPEED_OF_REDUCING), actualDlc.speedOfReducing);
      assertEquals(new Double(RANDOM_VALUES_MAXIMAL_RANGE), new Double(actualDlc.randomValuesRange.to));
      assertEquals(new Double(-RANDOM_VALUES_MAXIMAL_RANGE), new Double(actualDlc.randomValuesRange.from));
   }

   @Test
   public void shouldCreateDefaultConfigurations() {
      // given
      int iterations = 100;

      // when
      DeepLearningConfigurations actualDlc
         = uut.defaultConfigurations(iterations).create();

      // then
      Assert.assertEquals(new Long(iterations), actualDlc.iterations);
      assertEquals(new Double(0.1), actualDlc.learningRate);
      assertEquals(100, (int) actualDlc.iterationsDisplaying);
      assertEquals(0, (int) actualDlc.delay);
   }

   private DeepLearningConfigurations createTestConfig() {
      DeepLearningConfigurations dlc = new DeepLearningConfigurations();
      dlc.iterations = ITEARTIONS;
      dlc.learningRate = LEARNING_RATE;
      dlc.iterationsDisplaying = ITERATIONS_DISPLAYING;
      dlc.reducedLearningRate = REDUCED_LEARNING_RATE;
      dlc.speedOfReducing = SPEED_OF_REDUCING;
      dlc.randomValuesRange = Range.from(-RANDOM_VALUES_MAXIMAL_RANGE, RANDOM_VALUES_MAXIMAL_RANGE);
      dlc.momentum = MOMENTUM;
      return dlc;
   }
}
