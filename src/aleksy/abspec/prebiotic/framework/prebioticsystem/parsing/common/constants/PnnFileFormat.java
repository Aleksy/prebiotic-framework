package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.constants;

/**
 * Basic data of PNN file format
 */
public class PnnFileFormat {
   /**
    * Version of PNN format
    */
   public static final int version = 1;
}
