package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;

import java.util.List;

/**
 * Neural layer interface
 */
public interface NeuralLayer {
   /**
    * Function calculates a neural layer outputs of each neuron from previous layer
    *
    * @param xs vector of inputs from previous layer (or from external input)
    * @return vector of outputs
    * @throws IncorrectNumberOfInputsInLayerException  when number of inputs is not equal to
    *                                                  number of neuron inputs in layer
    * @throws IncorrectNumberOfInputsInNeuronException when number of inputs is not equal to
    *                                                  number of neuron inputs in layer
    */
   List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException;

   /**
    * Setter for neurons list
    *
    * @param neurons list to set
    */
   void setNeurons(List<Neuron> neurons);

   /**
    * Getter for neurons list
    *
    * @return list of neurons
    */
   List<Neuron> getNeurons();

   /**
    * Getter for isInputLayer
    *
    * @return true if layer is input layer
    */
   boolean isInputLayer();

   /**
    * Setter for isInputLayer
    *
    * @param isInputLayer to set
    */
   void setInputLayer(boolean isInputLayer);

   /**
    * Function counts all neurons in layer
    *
    * @return number of neurons in layer
    */
   int size();

   /**
    * Function counts all inputs of all neurons in layer
    *
    * @return number of inputs in layer
    */
   int numberOfInputs();

   /**
    * Randomizes weights functions
    *
    * @param from lower bound
    * @param to   upper bound
    */
   void randomizeWeights(Double from, Double to);
}
