package aleksy.abspec.prebiotic.framework.neural.teaching.builder;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KohonenLearningConfigurationsBuilderTest {
   private final Long ITEARTIONS = 1000L;
   private final Double LEARNING_RATE = 0.1;
   private final Double MAX_DISTANCE = 3.0;
   private final int ITERATIONS_DISPLAYING = 10;
   private final int DELAY = 10;

   private KohonenLearningConfigurationsBuilder uut;

   @Before
   public void init() {
      uut = new KohonenLearningConfigurationsBuilder(Prebiotic.newInstance());
   }

   @Test
   public void shouldCreateCustomConfigurations() {
      // given
      KohonenLearningConfigurations expectedKlc = createTestConfig();

      // when
      uut.setIterations(expectedKlc.iterations);
      uut.setLearningRate(expectedKlc.learningRate);
      uut.setMaxDistance(expectedKlc.maxDistance);
      uut.setIterationsDisplaying(expectedKlc.iterationsDisplaying);
      uut.setDelay(expectedKlc.delay);
      KohonenLearningConfigurations actualKlc = uut.create();

      // then
      assertEquals(new Long(ITEARTIONS), actualKlc.iterations);
      assertEquals(LEARNING_RATE, actualKlc.learningRate);
      assertEquals(MAX_DISTANCE, actualKlc.maxDistance);
      assertEquals(ITERATIONS_DISPLAYING, (int) actualKlc.iterationsDisplaying);
      assertEquals(DELAY, (int) actualKlc.delay);
   }

   @Test
   public void shouldCreateDefaultConfigurations() {
      // given
      int iterations = 100;
      double maxDistance = 4.2;

      // when
      KohonenLearningConfigurations actualKlc
         = uut.defaultConfigurations(iterations, maxDistance).create();

      // then
      assertEquals(new Long(iterations), actualKlc.iterations);
      assertEquals(new Double(0.1), (Double) actualKlc.learningRate);
      assertEquals(new Double(maxDistance), (Double) actualKlc.maxDistance);
      assertEquals(100, (int) actualKlc.iterationsDisplaying);
      assertEquals(0, (int) actualKlc.delay);
   }

   private KohonenLearningConfigurations createTestConfig() {
      KohonenLearningConfigurations klc = new KohonenLearningConfigurations();
      klc.iterations = ITEARTIONS;
      klc.learningRate = LEARNING_RATE;
      klc.maxDistance = MAX_DISTANCE;
      klc.iterationsDisplaying = ITERATIONS_DISPLAYING;
      klc.delay = DELAY;
      return klc;
   }
}
