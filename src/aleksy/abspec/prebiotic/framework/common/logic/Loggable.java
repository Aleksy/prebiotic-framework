package aleksy.abspec.prebiotic.framework.common.logic;

/**
 * Interface for classes who can log events in the console
 */
public abstract class Loggable {
   /**
    * Getter for constant name of sender
    *
    * @return log sender name
    */
   protected abstract String getSenderName();
}
