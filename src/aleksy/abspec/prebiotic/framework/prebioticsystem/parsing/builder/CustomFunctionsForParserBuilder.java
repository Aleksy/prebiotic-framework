package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;

/**
 * Parser configurations builder
 */
public class CustomFunctionsForParserBuilder {
   private CustomFunctionsForParser pc;

   /**
    * Constructor
    */
   public CustomFunctionsForParserBuilder() {
      pc = new CustomFunctionsForParser();
   }

   /**
    * Setter for custom axon function
    *
    * @param axonFunction to set
    * @return builder
    */
   public CustomFunctionsForParserBuilder setAxonFunction(AxonFunction axonFunction) {
      pc.axonFunction = axonFunction;
      return this;
   }

   /**
    * Setter for custom core function
    *
    * @param coreFunction to set
    * @return builder
    */
   public CustomFunctionsForParserBuilder setCoreFunction(CoreFunction coreFunction) {
      pc.coreFunction = coreFunction;
      return this;
   }

   /**
    * Setter for custom weight function
    *
    * @param weightFunction to set
    * @return builder
    */
   public CustomFunctionsForParserBuilder setWeightFunction(WeightFunction weightFunction) {
      pc.weightFunction = weightFunction;
      return this;
   }

   /**
    * Creates new instance of parser configurations
    *
    * @return parser configurations
    */
   public CustomFunctionsForParser create() {
      return pc;
   }
}
