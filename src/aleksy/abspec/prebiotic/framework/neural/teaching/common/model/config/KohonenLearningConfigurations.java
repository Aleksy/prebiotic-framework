package aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config;

import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.abstr.AbstractAlgorithmConfigurations;

/**
 * Kohonen learning configurations
 */
public class KohonenLearningConfigurations extends AbstractAlgorithmConfigurations {
   /**
    * Learning rate to use
    */
   public Double learningRate;
   /**
    * Maximal distance of neighbour neurons to teach
    */
   public Double maxDistance;
}
