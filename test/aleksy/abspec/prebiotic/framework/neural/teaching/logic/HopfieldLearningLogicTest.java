package aleksy.abspec.prebiotic.framework.neural.teaching.logic;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.NeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HopfieldLearningLogicTest {
   private HopfieldLearningLogic uut;

   @Before
   public void init() {
      uut = new HopfieldLearningLogic(Prebiotic.newInstance());

   }
   @Test(expected = HopfieldLearningException.class)
   public void shouldThrowExceptionWhenNetworkTypeIsWrong() throws HopfieldLearningException {
      // given
      NeuralNetwork nn = new NeuralNetworkImpl();
      nn.setType(NeuralNetworkType.FEEDFORWARD);

      // when
      uut.start(null, nn);

      // then exception
   }
}
