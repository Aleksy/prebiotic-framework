package aleksy.abspec.prebiotic.framework.vision.display.panel.abstr;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.common.constants.VisionConstants;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;

import java.awt.*;
import java.util.Collections;
import java.util.List;

/**
 * Abstract panel with util methods for neural network panels.
 */
public abstract class AbstractNeuralPanel extends AbstractPanel {
   protected NeuralNetwork nn;
   protected Double hue;
   protected Range inputRange;
   protected Font font;
   protected Font italicFont;

   public AbstractNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      this.nn = nn;
      this.hue = hue;
      this.inputRange = inputRange;
      font = new Font("Consolas", Font.PLAIN, 12);
      italicFont = new Font("Consolas", Font.ITALIC, 13);
      setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
   }

   /**
    * Creates darker color from x value
    *
    * @param x value
    * @return dark color
    */
   protected Color createDarkerColor(Double x) {
      return createColor(x).darker();
   }

   /**
    * Creates a little bit darker color from x value
    *
    * @param x value
    * @return little bit darker color
    */
   protected Color createBitDarkerColor(Double x) {
      Color c = createColor(x);
      float[] hsb = new float[3];
      hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
      hsb[2] = hsb[2] * 0.8f;
      return new Color(Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]));
   }

   /**
    * Creates color from double value: 1.0 - the brightest, 0.0 - the darkest
    *
    * @param x value
    * @return color from x value
    */
   protected Color createColor(Double x) {
      if (x == null)
         x = 1.0;
      float saturation = x.floatValue() * 0.6f + 0.15f;
      float brightness = 0.95f * x.floatValue() + 0.05f;
      return new Color(Color.HSBtoRGB(hue.floatValue(), saturation, brightness));
   }

   /**
    * Creates brighter color from given color
    *
    * @param tmp color to make brighter
    * @return brighter color
    */
   protected Color createBrighterColor(Color tmp) {
      return tmp.brighter();
   }

   /**
    * Creates contour color for neurons
    *
    * @param tmp color to make contour color
    * @return contour color
    */
   protected Color createContourColor(Color tmp) {
      return tmp.brighter().brighter();
   }

   /**
    * Draws a simple synapse between two neurons
    *
    * @param xFrom x from
    * @param yFrom y from
    * @param xTo   x to
    * @param yTo   y to
    * @param g     vision instance
    */
   protected void drawSynapse(int xFrom, int yFrom, int xTo, int yTo, Graphics g) {
      g.drawLine(xFrom, yFrom, xTo, yTo);
   }

   /**
    * Draws net of synapses from input vector to Kohonen'TeachingDataManagementComposite map.
    *
    * @param knn          Kohonen neural network interface
    * @param xStep        x step
    * @param yOffset      offset from the y = 0
    * @param neuronNumber number of neurons
    * @param g            vision instance
    */
   protected void drawKohonenSynapsesFromInput(KohonenNeuralNetwork knn, int xStep, int yOffset, int neuronNumber, Graphics g) {
      for (int x = 0; x < knn.getXSize(); x++) {
         for (int y = 0; y < knn.getYSize(); y++) {
            int xNeuronStep = (int) ((double) getSize().width / (double) (knn.getXSize()));
            int yNeuronStep = (int) ((double) getSize().height / (double) (knn.getYSize() + 1));
            int xNeuron = (int) (xNeuronStep / 2.0) + x * xNeuronStep + 3;
            int yNeuron = (int) ((yNeuronStep + y * yNeuronStep) * 0.85) + yOffset;
            drawSynapse(xStep + neuronNumber * xStep, yOffset, xNeuron, yNeuron, g);
         }
      }
   }

   /**
    * Creates anti color with another hue
    *
    * @param d value of the color
    * @return anti color
    */
   protected Color createAntiColor(double d) {
      Color c = createColor(d);
      float[] hsb = new float[3];
      hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
      if (hsb[0] <= 0.5)
         hsb[0] += 0.5;
      else
         hsb[0] -= 0.5;
      hsb[1] *= 0.4;
      return new Color(Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]));
   }

   protected void feedforwardSkeleton(int inputVectorSize, List<Double> actualInputs, Range axonRange, aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode mode, Graphics g) {
      for (int i = 0; i < inputVectorSize; i++) {
         if (actualInputs != null) {
            g.setColor(createBitDarkerColor(actualInputs.get(i)));
         }
         int allNeuronsInNextLayer = nn.getInputLayer().size();
         if (!(mode == DisplayMode.STRUCTURE_SQUARE && nn.numberOfLayers() == 1))
            for (int j = 0; j < allNeuronsInNextLayer; j++) {
               prepareInputSynapse(i, nn.numberOfLayers(), inputVectorSize, j, allNeuronsInNextLayer, g);
            }
      }
      for (int i = 0; i < inputVectorSize; i++) {
         if (actualInputs != null) {
            g.setColor(createColor(actualInputs.get(i)));
            if (mode == DisplayMode.WEIGHT_MATRIX)
               drawWeightInputNeuron(i, inputVectorSize, nn.numberOfLayers(), actualInputs.get(i), g);
            else
               drawStructureInputNeuron(i, inputVectorSize, nn.numberOfLayers(), actualInputs.get(i), g);

         } else {
            if (mode == DisplayMode.WEIGHT_MATRIX)
               drawWeightInputNeuron(i, inputVectorSize, nn.numberOfLayers(), 0.0, g);
            else
               drawStructureInputNeuron(i, inputVectorSize, nn.numberOfLayers(), 0.0, g);
         }
      }
      for (int l = 0; l < nn.numberOfLayers(); l++) {
         NeuralLayer layer = nn.getLayers().get(l);
         for (int n = 0; n < layer.size(); n++) {
            Neuron neuron = layer.getNeurons().get(n);
            if (neuron.getActualAxonOutput() != null) {
               double axonNormalized = VectorNormalizator.setValueBetweenZeroAndOne(neuron.getActualAxonOutput(),
                  axonRange.getFrom(), axonRange.getTo());
               g.setColor(createBitDarkerColor(axonNormalized));
            } else
               g.setColor(createBitDarkerColor(0.0));
            if (mode != DisplayMode.STRUCTURE_SQUARE)
               if (l < nn.numberOfLayers() - 1) {
                  int allNeuronsInNextLayer = nn.getLayers().get(l + 1).size();
                  for (int i = 0; i < allNeuronsInNextLayer; i++) {
                     prepareSynapse(l, n, nn.numberOfLayers(), layer.size(), i, allNeuronsInNextLayer, g);
                  }
               }
            if (mode == DisplayMode.STRUCTURE_SQUARE)
               if (l < nn.numberOfLayers() - 2) {
                  int allNeuronsInNextLayer = nn.getLayers().get(l + 1).size();
                  for (int i = 0; i < allNeuronsInNextLayer; i++) {
                     prepareSynapse(l, n, nn.numberOfLayers(), layer.size(), i, allNeuronsInNextLayer, g);
                  }
               }
         }
      }
   }

   protected void hopfieldSkeleton(List<Neuron> neurons,
                                   double distance,
                                   double xStep,
                                   double yStep,
                                   double yOffset,
                                   Graphics g) {

      for (int i = 0; i < nn.size(); i++) {
         int xFrom = (int) (getSize().width / 2 + Math.sin(i * distance) * xStep * 1.3);
         int yFrom = (int) (getSize().height / 2 + Math.cos(i * distance) * yStep * 1.3);
         int xTo = (int) (getSize().width / 2 + Math.sin(i * distance) * xStep);
         int yTo = (int) (getSize().height / 2 + Math.cos(i * distance) * yStep);
         if (neurons.get(i).getActualInputs() != null)
            g.setColor(createDarkerColor(neurons.get(i).getActualInputs().get(i)).darker());
         else
            g.setColor(createDarkerColor(0.0).darker());
         drawSynapse(xFrom, yFrom, xTo, (int) (yTo + yOffset), g);
      }

      for (int i = 0; i < nn.size(); i++) {
         for (int j = 0; j < nn.size(); j++) {
            int xFrom = (int) (getSize().width / 2 + Math.sin(j * distance) * xStep);
            int yFrom = (int) (getSize().height / 2 + Math.cos(j * distance) * yStep);
            int xTo = (int) (getSize().width / 2 + Math.sin(i * distance) * xStep);
            int yTo = (int) (getSize().height / 2 + Math.cos(i * distance) * yStep);
            if (neurons.get(i).getActualAxonOutput() != null)
               g.setColor(createColor(VectorNormalizator.setValueBetweenZeroAndOne(
                  neurons.get(i).getActualAxonOutput(), inputRange.getFrom(), inputRange.getTo()
               )));
            else
               g.setColor(createColor(0.0));
            drawSynapse(xFrom, (int) (yFrom + yOffset), xTo, (int) (yTo + yOffset), g);
         }
      }
   }

   private void drawStructureInputNeuron(int neuronNumber, int inputVectorSize, int allLayersNumber, double actualValue, Graphics g) {
      int xStep = (int) ((this.getSize().width) / ((double) inputVectorSize + 1));
      int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
      int yOffset = generateOffset(yStep);
      int xNeuron = xStep + neuronNumber * xStep;
      Color tmp = g.getColor();
      g.fillOval(xNeuron - 6, yStep - yOffset - 6, 12, 12);
      g.setColor(createBrighterColor(tmp));
      g.drawOval(xNeuron - 6, yStep - yOffset - 6, 12, 12);
      g.setColor(createAntiColor(1.0));
      actualValue *= 1000;
      actualValue = Math.round(actualValue);
      actualValue /= 1000;
      g.setFont(font);
      g.drawString(Double.toString(actualValue), xNeuron - 12, yStep - yOffset - 10);
      g.setColor(tmp);
   }

   private void drawWeightInputNeuron(int neuronNumber, int inputVectorSize, int allLayersNumber, double actualValue, Graphics g) {
      int xStep = (int) ((this.getSize().width) / ((double) inputVectorSize + 1));
      int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
      int yOffset = generateOffset(yStep);
      int xNeuron = xStep + neuronNumber * xStep;
      drawWeightMatrixNeuron(Collections.singletonList(actualValue), actualValue, xNeuron, yStep - yOffset, g);
   }

   protected void drawWeightMatrixNeuron(List<Double> weightMatrix, double output, int x, int y, Graphics g) {
      Color tmp = g.getColor();
      int square = (int) Math.ceil(Math.sqrt(weightMatrix.size()));
      x = x - square;
      int xTmp = x;
      y = y - square;
      int yTmp = y;
      int cnt = 0;
      for (int i = 0; i < square; i++) {
         for (int j = 0; j < square; j++) {
            if (cnt >= weightMatrix.size())
               drawPixel(x, y, createColor(0.0), g);
            else
               drawPixel(x, y, createColor(weightMatrix.get(cnt)), g);
            cnt++;
            x += 5;
         }
         y += 5;
         x = xTmp;
      }
      g.setColor(createContourColor(createColor(output)));
      g.drawRect(xTmp, yTmp, square * 5, square * 5);
      g.setColor(tmp);
   }

   private void prepareInputSynapse(int neuronNumber,
                                    int allLayersNumber, int allNeuronsNumber,
                                    int neuronNumberInNextLayer, int allNeuronsInNextLayerNumber, Graphics g) {
      int xStep = (int) ((this.getSize().width) / ((double) allNeuronsNumber + 1));
      int xNextLayerStep = (int) ((this.getSize().width) / ((double) allNeuronsInNextLayerNumber + 1));
      int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
      int yOffset = generateOffset(yStep);
      int xFrom = xStep + neuronNumber * xStep;
      int xTo = xNextLayerStep + neuronNumberInNextLayer * xNextLayerStep;
      Color tmp = g.getColor();
      g.setColor(tmp.darker().darker());
      drawSynapse(xFrom, yStep - yOffset, xTo, yStep + +yOffset, g);
      g.setColor(tmp);
   }

   private void prepareSynapse(int layerNumber, int neuronNumber,
                               int allLayersNumber, int allNeuronsNumber,
                               int neuronNumberInNextLayer, int allNeuronsInNextLayerNumber, Graphics g) {
      int xStep = (int) ((this.getSize().width) / ((double) allNeuronsNumber + 1));
      int xNextLayerStep = (int) ((this.getSize().width) / ((double) allNeuronsInNextLayerNumber + 1));
      int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
      int yOffset = generateOffset(yStep);
      int xFrom = xStep + neuronNumber * xStep;
      int yFrom = yStep + layerNumber * yStep;
      int xTo = xNextLayerStep + neuronNumberInNextLayer * xNextLayerStep;
      int yTo = yStep + (layerNumber + 1) * yStep;
      Color tmp = g.getColor();
      g.setColor(tmp.darker().darker());
      drawSynapse(xFrom, yFrom + yOffset, xTo, yTo + yOffset, g);
      g.setColor(tmp);
   }

   private void drawPixel(int x, int y, Color color, Graphics g) {
      g.setColor(color);
      g.fillRect(x, y, 5, 5);
   }

   private int generateOffset(int yStep) {
      return yStep / 3;
   }
}
