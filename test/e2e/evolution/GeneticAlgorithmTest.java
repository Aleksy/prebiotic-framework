package e2e.evolution;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.environment.Environment;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Individual;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.exception.GeneticAlgorithmException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * Genetic algorithm test class tests whole flow and work with Genetic module.
 * First the test Geneticable class is implemented. Next, the Environment is created.
 * Test individuals have one feature - color, represented by three genes: red, green and blue.
 * Environment requires white individuals, and chooses the bests. Next the genetic algorithm
 * on the first generation of individuals is started. After the calculations every
 * individuals should have brightness (average of three sub-colors) higher than 0.75.
 */
public class GeneticAlgorithmTest {

   @Test
   public void ModelAndEnvironmentCreatingAndGeneticAlgorithmLogicTest() throws GeneticAlgorithmException {
      PrebioticApi prebiotic = Prebiotic.newInstance();
      prebiotic.enableEventLogging();

      final int POPULATION_SIZE = 30;
      List<Geneticable> chameleons = new ArrayList<>();
      for (int i = 0; i < POPULATION_SIZE; i++) {
         chameleons.add(new Chameleon(createRandomGenotype(prebiotic)));
      }
      chameleons = prebiotic.evolutionApi().geneticAlgorithm(chameleons, new Snowland(),
         prebiotic.evolutionApi().getGeneticAlgorithmConfigurationsBuilder()
            .setAmountOfGenerations(20)
            .setIterationsDisplaying(10)
            .setMutationChance(0.01)
            .setSelectionMode(GeneticSelectionMode.RANKING)
            .create());

      double averageBrightness = 0.0;
      for (Geneticable chameleon : chameleons)
         averageBrightness += ((Chameleon)chameleon).getBrightness();
      averageBrightness /= chameleons.size();
         assertTrue(averageBrightness > 0.75);
   }

   private Genotype createRandomGenotype(PrebioticApi prebiotic) {
      Random r = new Random();
      return prebiotic.evolutionApi().getGenotypeBuilder()
         .addChromosome("color")
         .addGene(r.nextDouble(), Range.from(0.0, 1.0), "red")
         .addGene(r.nextDouble(), Range.from(0.0, 1.0), "green")
         .addGene(r.nextDouble(), Range.from(0.0, 1.0), "blue")
         .end()
         .create();
   }

}

class Chameleon extends Individual implements Geneticable {

   Chameleon(Genotype genotype) {
      super(genotype);
   }

   @Override
   public Geneticable newInstance(Genotype genotype) {
      return new Chameleon(genotype);
   }

   public double getBrightness() {
      return ((genotype.findGeneValue("color:red") + genotype.findGeneValue("color:green")
         + genotype.findGeneValue("color:blue")) / 3);
   }
}

class Snowland extends Environment {
   @Override
   public void rate(Geneticable geneticable) {
      Genotype genotype = geneticable.getGenotype();
      double red = genotype.findGeneValue("color:red");
      double green = genotype.findGeneValue("color:green");
      double blue = genotype.findGeneValue("color:blue");
      geneticable.setFitness((red + green + blue) / 3);
   }
}
