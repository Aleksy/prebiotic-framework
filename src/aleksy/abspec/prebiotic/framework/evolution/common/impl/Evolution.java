package aleksy.abspec.prebiotic.framework.evolution.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.evolution.common.api.EvolutionApi;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.config.GeneticAlgorithmConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.api.GeneticApi;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.exception.GeneticAlgorithmException;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.impl.Genetic;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.environment.Environment;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;

import java.util.List;

/**
 * Evolution api module implementation
 */
public class Evolution implements EvolutionApi {
   private GeneticApi genetic;

   public Evolution(PrebioticApi prebiotic) {
      this.genetic = new Genetic(prebiotic);
   }

   @Override
   public GenotypeBuilder getGenotypeBuilder() {
      return genetic.getGenotypeBuilder();
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, Environment environment, GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmException {
      return genetic.geneticAlgorithm(population, environment, configurations);
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, Environment environment, GeneticAlgorithmConfigurations configurations, Brain templateBrain) throws GeneticAlgorithmException {
      return genetic.geneticAlgorithm(population, environment, configurations, templateBrain);
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmException {
      return genetic.geneticAlgorithm(population, configurations);
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, GeneticAlgorithmConfigurations configurations, Brain templateBrain) throws GeneticAlgorithmException {
      return genetic.geneticAlgorithm(population, configurations, templateBrain);
   }

   @Override
   public GeneticAlgorithmConfigurationsBuilder getGeneticAlgorithmConfigurationsBuilder() {
      return genetic.getGeneticAlgorithmConfigurationsBuilder();
   }

}
