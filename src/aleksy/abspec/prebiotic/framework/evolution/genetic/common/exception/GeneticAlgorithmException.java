package aleksy.abspec.prebiotic.framework.evolution.genetic.common.exception;

import aleksy.abspec.prebiotic.framework.common.exception.PrebioticException;

/**
 * Genetic algorithm exception
 */
public class GeneticAlgorithmException extends PrebioticException {

   public GeneticAlgorithmException(String message) {
      super(message);
   }
}
