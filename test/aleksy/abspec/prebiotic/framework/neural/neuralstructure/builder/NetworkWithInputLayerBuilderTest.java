package aleksy.abspec.prebiotic.framework.neural.neuralstructure.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NetworkWithInputLayerBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.*;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl.NeuralLayerImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.NeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NetworkWithInputLayerBuilderTest {

   private NetworkWithInputLayerBuilder uut;

   private CoreFunction core;
   private AxonFunction axon;

   @Before
   public void init() {
      PrebioticApi prebiotic = Prebiotic.newInstance();
      core = CoreFunctionRecipe.DEFAULT_ADDER.newInstance();
      axon = AxonFunctionRecipe.SIGMOID.newInstance();
      uut = new NetworkWithInputLayerBuilder(createTestConfigurations(), createSimpleNetwork(), core, axon, prebiotic);
   }

   @Test
   public void shouldAddLayerToNetwork() {
      // given
      int expectedNumberOfNeuronsInLayer = 3;
      int expectedNumberOfLayers = 2;

      // when
      uut.addLayer(expectedNumberOfNeuronsInLayer);
      NeuralNetwork actualNeuralNetwork = null;
      try {
         actualNeuralNetwork = uut.endBuildingNetwork().create();
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
      int actualNumberOfNeurons = actualNeuralNetwork.getLayers().get(1).size();
      int actualNumberOfLayers = actualNeuralNetwork.numberOfLayers();

      // then
      assertEquals(expectedNumberOfNeuronsInLayer, actualNumberOfNeurons);
      assertEquals(expectedNumberOfLayers, actualNumberOfLayers);
   }

   private NeuralNetwork createSimpleNetwork() {
      NeuralNetwork nn = new NeuralNetworkImpl();
      WeightFunction weight = WeightFunctionRecipe.DEFAULT_LINEAR.newInstance(false);
      List<WeightFunction> weights = new ArrayList<>();
      weights.add(weight);
      nn.setBiasPresence(false);
      List<NeuralLayer> layers = new ArrayList<>();
      layers.add(createSimpleInputLayer());
      nn.setLayers(layers);
      nn.setDataNormalization(DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR);
      return nn;
   }

   private StructureConfigurations createTestConfigurations() {
      StructureConfigurations sc = new StructureConfigurations();
      sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
      sc.biasPresence = true;
      sc.axonFunctionRecipe = AxonFunctionRecipe.SIGMOID;
      sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      return sc;
   }

   private NeuralLayer createSimpleInputLayer() {
      NeuralLayer layer = new NeuralLayerImpl();
      WeightFunction weight = WeightFunctionRecipe.DEFAULT_LINEAR.newInstance(false);
      List<WeightFunction> weights = new ArrayList<>();
      weights.add(weight);
      Neuron neuron = new NeuronImpl(weights, core, axon, false);
      List<Neuron> neurons = new ArrayList<>();
      neurons.add(neuron);
      layer.setNeurons(neurons);
      return layer;
   }
}
