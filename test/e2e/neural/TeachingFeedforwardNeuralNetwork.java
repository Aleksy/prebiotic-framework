package e2e.neural;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.util.ListCreator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Test class for use case of creating and teaching the feedforward neural network.
 * The agent is trained to solve the XOR problem.
 */
public class TeachingFeedforwardNeuralNetwork {

   @Test
   public void buildingTrainingAndTesting() throws NeuralNetworkException, DeepLearningException {
      // preparing the api
      PrebioticApi prebiotic = Prebiotic.newInstance();
      prebiotic.enableEventLogging();

      // building the agent
      NeuralNetwork neuralNetwork = buildAgent(prebiotic);

      // preparing the teaching data
      TeachingData teachingData = prepareTeachingData(prebiotic);

      // training
      neuralNetwork = prebiotic.neuralApi().deepLearning(teachingData,
         neuralNetwork, prebiotic.neuralApi().getDeepLearningConfigurationsBuilder()
            .setIterationsDisplaying(1000)
            .setIterations(10000)
            .setLearningRate(0.1)
            .setMomentum(0.01)
            .setReducedLearningRate(0.001)
            .setSpeedOfReducing(0.0001)
            .setRandomValuesMaximalRange(0.0000001)
            .create());

      // testing
      List<Double> out0 = neuralNetwork.f(teachingData.getDataPackages().get(0).ins);
      List<Double> out1 = neuralNetwork.f(teachingData.getDataPackages().get(1).ins);
      List<Double> out2 = neuralNetwork.f(teachingData.getDataPackages().get(2).ins);
      List<Double> out3 = neuralNetwork.f(teachingData.getDataPackages().get(3).ins);

      // then
      assertTrue(out0.get(0) < 0.0);
      assertTrue(out1.get(0) > 0.0);
      assertTrue(out2.get(0) > 0.0);
      assertTrue(out3.get(0) < 0.0);
   }

   private TeachingData prepareTeachingData(PrebioticApi prebiotic) {
      return prebiotic.neuralApi().getTeachingDataBuilder()
         .addDataPackage(ListCreator.createFrom(-1.0, -1.0), ListCreator.createFrom(-1.0))
         .addDataPackage(ListCreator.createFrom(-1.0, 1.0), ListCreator.createFrom(1.0))
         .addDataPackage(ListCreator.createFrom(1.0, -1.0), ListCreator.createFrom(1.0))
         .addDataPackage(ListCreator.createFrom(1.0, 1.0), ListCreator.createFrom(-1.0))
         .create();
   }

   private NeuralNetwork buildAgent(PrebioticApi prebiotic)
      throws EmptyFunctionDefinitionException, InvalidStructureException {
      return prebiotic.neuralApi()
         .getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultFeedforwardConfigurations()
               .setAxonFunction(AxonFunctionRecipe.TANH)
               .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
               .create()
         ).addDefaultInputLayer(3, 2)
         .addLayer(1)
         .endBuildingNetwork()
         .randomizeWeights(-1.0, 1.0)
         .create();
   }
}
