package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.AbstractNeuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;

import java.util.List;

/**
 * Basic implementation of neuron
 */
public class NeuronImpl extends AbstractNeuron implements Neuron {

   public NeuronImpl(List<WeightFunction> weights, CoreFunction core, AxonFunction axon, boolean biasPresence) {
      super(weights, core, axon, biasPresence);
   }
}
