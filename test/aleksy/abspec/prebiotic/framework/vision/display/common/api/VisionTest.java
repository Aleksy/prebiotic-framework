package aleksy.abspec.prebiotic.framework.vision.display.common.api;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.HopfieldNeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.KohonenNeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.NeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.vision.common.api.VisionApi;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;
import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.coordinatesystem.CoordinateSystemNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.descriptor.DescriptorNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.FeedforwardStructureNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.FeedforwardStructureSquareNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.HopfieldStructurePanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.KohonenStructureNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix.FeedforwardWeightMatrixNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix.HopfieldWeightMatrixNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix.KohonenWeightMatrixNeuralPanel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VisionTest {
   private VisionApi uut;

   @Before
   public void init() {
      uut = new Vision();
   }

   @Test
   public void shouldReturnCorrectClasses() {
      // given
      AbstractPanel feedforwardCoordinateSystemPanel;
      AbstractPanel kohonenCoordinateSystemPanel;
      AbstractPanel hopfieldCoordinateSystemPanel;
      AbstractPanel feedforwardStructurePanel;
      AbstractPanel kohonenStructurePanel;
      AbstractPanel hopfieldStructurePanel;
      AbstractPanel feedforwardStructureSquarePanel;
      AbstractPanel feedforwardWeightMatrixPanel;
      AbstractPanel kohonenWeightMatrixPanel;
      AbstractPanel hopfieldWeightMatrixPanel;
      AbstractPanel feedforwardDescriptionPanel;
      AbstractPanel kohonenDescriptionPanel;
      AbstractPanel hopfieldDescriptionPanel;
      NeuralNetwork feedforward = new NeuralNetworkImpl();
      feedforward.setType(NeuralNetworkType.FEEDFORWARD);
      NeuralNetwork kohonen = new KohonenNeuralNetworkImpl();
      kohonen.setType(NeuralNetworkType.KOHONEN);
      NeuralNetwork hopfield = new HopfieldNeuralNetworkImpl();
      hopfield.setType(NeuralNetworkType.HOPFIELD);

      // when
      feedforwardCoordinateSystemPanel = uut.getNewPanel(feedforward, 0.0, DisplayMode.COORDINATE_SYSTEM, Range.from(0.0, 1.0));
      kohonenCoordinateSystemPanel = uut.getNewPanel(kohonen, 0.0, DisplayMode.COORDINATE_SYSTEM, Range.from(0.0, 1.0));
      hopfieldCoordinateSystemPanel = uut.getNewPanel(hopfield, 0.0, DisplayMode.COORDINATE_SYSTEM, Range.from(0.0, 1.0));
      feedforwardStructurePanel = uut.getNewPanel(feedforward, 0.0, DisplayMode.STRUCTURE, Range.from(0.0, 1.0));
      kohonenStructurePanel = uut.getNewPanel(kohonen, 0.0, DisplayMode.STRUCTURE, Range.from(0.0, 1.0));
      hopfieldStructurePanel = uut.getNewPanel(hopfield, 0.0, DisplayMode.STRUCTURE, Range.from(0.0, 1.0));
      feedforwardStructureSquarePanel = uut.getNewPanel(feedforward, 0.0, DisplayMode.STRUCTURE_SQUARE, Range.from(0.0, 1.0));
      feedforwardWeightMatrixPanel = uut.getNewPanel(feedforward, 0.0, DisplayMode.WEIGHT_MATRIX, Range.from(0.0, 1.0));
      kohonenWeightMatrixPanel = uut.getNewPanel(kohonen, 0.0, DisplayMode.WEIGHT_MATRIX, Range.from(0.0, 1.0));
      hopfieldWeightMatrixPanel = uut.getNewPanel(hopfield, 0.0, DisplayMode.WEIGHT_MATRIX, Range.from(0.0, 1.0));
      feedforwardDescriptionPanel = uut.getNewPanel(feedforward, 0.0, DisplayMode.DESCRIPTION, Range.from(0.0, 1.0));
      kohonenDescriptionPanel = uut.getNewPanel(kohonen, 0.0, DisplayMode.DESCRIPTION, Range.from(0.0, 1.0));
      hopfieldDescriptionPanel = uut.getNewPanel(hopfield, 0.0, DisplayMode.DESCRIPTION, Range.from(0.0, 1.0));

      // then
      assertEquals(feedforwardCoordinateSystemPanel.getClass(), CoordinateSystemNeuralPanel.class);
      assertEquals(kohonenCoordinateSystemPanel.getClass(), CoordinateSystemNeuralPanel.class);
      assertEquals(hopfieldCoordinateSystemPanel.getClass(), CoordinateSystemNeuralPanel.class);
      assertEquals(feedforwardStructurePanel.getClass(), FeedforwardStructureNeuralPanel.class);
      assertEquals(kohonenStructurePanel.getClass(), KohonenStructureNeuralPanel.class);
      assertEquals(hopfieldStructurePanel.getClass(), HopfieldStructurePanel.class);
      assertEquals(feedforwardStructureSquarePanel.getClass(), FeedforwardStructureSquareNeuralPanel.class);
      assertEquals(feedforwardWeightMatrixPanel.getClass(), FeedforwardWeightMatrixNeuralPanel.class);
      assertEquals(kohonenWeightMatrixPanel.getClass(), KohonenWeightMatrixNeuralPanel.class);
      assertEquals(hopfieldWeightMatrixPanel.getClass(), HopfieldWeightMatrixNeuralPanel.class);
      assertEquals(feedforwardDescriptionPanel.getClass(), DescriptorNeuralPanel.class);
      assertEquals(kohonenDescriptionPanel.getClass(), DescriptorNeuralPanel.class);
      assertEquals(hopfieldDescriptionPanel.getClass(), DescriptorNeuralPanel.class);

   }
}
