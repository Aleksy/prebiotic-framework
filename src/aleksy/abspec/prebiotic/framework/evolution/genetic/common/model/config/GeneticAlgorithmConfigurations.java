package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config;

import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.abstr.AbstractAlgorithmConfigurations;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.enumerate.GeneticSelectionMode;

/**
 * Genetic algorithm configurations class
 */
public class GeneticAlgorithmConfigurations extends AbstractAlgorithmConfigurations {
   /**
    * Chance of mutation
    */
   public double mutationChance;
   /**
    * Amount of generations
    */
   public int generations;

   /**
    * Mode of selection
    */
   public GeneticSelectionMode geneticSelectionMode;
}
