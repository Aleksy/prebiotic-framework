package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract neural network layer
 */
public abstract class AbstractNeuralLayer implements NeuralLayer {
   protected List<Neuron> neurons;
   protected boolean isInputLayer;

   @Override
   public void setNeurons(List<Neuron> neurons) {
      this.neurons = neurons;
   }

   @Override
   public List<Neuron> getNeurons() {
      return neurons;
   }

   @Override
   public List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException {
      List<Double> out = new ArrayList<>();
      for (Neuron neuron : neurons) {
         out.add(neuron.f(xs));
      }
      return out;
   }

   @Override
   public void setInputLayer(boolean inputLayer) {
      isInputLayer = inputLayer;
   }

   @Override
   public boolean isInputLayer() {
      return isInputLayer;
   }

   @Override
   public int numberOfInputs() {
      int inputs = 0;
      for (Neuron neuron : neurons) {
         inputs += neuron.size();
      }
      return inputs;
   }

   @Override
   public int size() {
      return neurons.size();
   }

   @Override
   public String toString() {
      StringBuilder s = new StringBuilder();
      s.append("<LAYER>\n");
      for (Neuron n : neurons) {
         s.append(n.toString()).append("\n");
      }
      s.append("</LAYER>");
      return s.toString();
   }

   @Override
   public void randomizeWeights(Double from, Double to) {
      for (Neuron neuron : neurons) {
         neuron.randomizeWeights(from, to);
      }
   }
}
