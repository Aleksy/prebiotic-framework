package aleksy.abspec.prebiotic.framework.vision.display.panel.activity;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractActivityNeuralPanel;

import java.awt.*;

public class ActivityRoundInputNeuralPanel extends AbstractActivityNeuralPanel {

   public ActivityRoundInputNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(Color.GRAY);
      g.setFont(font);
      g.drawString("Activity of input round graph:", 5, getSize().height / 20);
      drawRoundActivity(g, 0.34, true, true);
   }
}
