package aleksy.abspec.prebiotic.framework.vision.display.common.api;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractPanel;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

/**
 * Display submodule api
 */
public interface DisplayApi {
   /**
    * Displays neural network in a frame
    *
    * @param nn         neural network to display
    * @param hue        of neural network
    * @param mode       of displaying
    * @param inputRange range of input values
    */
   void display(NeuralNetwork nn, Double hue, DisplayMode mode, Range inputRange);

   /**
    * Displays genotype in a frame
    *
    * @param genotype to display
    */
   void display(Genotype genotype);

   /**
    * Repaints all active panels
    */
   void repaintAll();

   /**
    * Removes panel from active panels
    *
    * @param panel to remove
    */
   void removePanelFromActivePanels(AbstractPanel panel);

   /**
    * Returns new panel with displayed neural network (which is still updated during the program process)
    *
    * @param nn         neural network to display
    * @param hue        of neural network
    * @param mode       of displaying
    * @param inputRange range of input values
    * @return new abstract panel prepared to set in displayable component
    */
   AbstractPanel getNewPanel(NeuralNetwork nn, Double hue, DisplayMode mode, Range inputRange);

   /**
    * Returns new panel with displayed genotype
    *
    * @param genotype to display
    * @return new abstract panel prepared to set in displayable component
    */
   AbstractPanel getNewPanel(Genotype genotype);
}
