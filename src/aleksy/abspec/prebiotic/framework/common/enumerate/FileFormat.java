package aleksy.abspec.prebiotic.framework.common.enumerate;

/**
 * Available parsing file formats
 */
public enum FileFormat {
   /**
    * Prebiotic Neural Network file format. This format is used by parser to
    * save the files in the light way.
    * The file format structure:
    * [PNN_format_version] - 1 byte
    * [description] - byte size is dependent for neural network description
    * [bias_presence] - 1 byte
    * [data_normalization_type] - 1 byte
    * [neural_network_type] - 1 byte
    * [core_function_recipe] - 1 byte
    * [axon_function_recipe] - 1 byte
    * [weight_function_recipe] - 1 byte
    * [size_of_input_vector] - 1 byte
    * [number_of_layers] - 1 byte (if the network type is FEEDFORWARD)
    * [x_size] and [y_size] - 2 bytes (if the network type is KOHONEN)
    * LAYER:
    * [layer_size] - 1 byte
    * [weights_of_each_neuron] - 8 bytes for each factor
    */
   PNN,
   /**
    * XML file format
    */
   XML,
   /**
    * Json file format
    */
   JSON
}
