package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.util.Crossovers;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.util.Mutations;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

/**
 * Individual class, basic abstract implementation of geneticable interface
 */
public abstract class Individual implements Geneticable {
   protected Genotype genotype;
   private double fitness;
   private Range lineSegment;

   /**
    * Constructor
    *
    * @param genotype of individual
    */
   public Individual(Genotype genotype) {
      this.genotype = genotype;
   }

   @Override
   public void setFitness(double fitness) {
      this.fitness = fitness;
   }

   @Override
   public double getFitness() {
      return fitness;
   }

   @Override
   public void setLineSegment(Range lineSegment) {
      this.lineSegment = lineSegment;
   }

   @Override
   public Range getLineSegment() {
      return lineSegment;
   }

   @Override
   public Genotype getGenotype() {
      return genotype;
   }

   @Override
   public void setGenotype(Genotype genotype) {
      this.genotype = genotype;
   }

   @Override
   public Geneticable crossover(Geneticable parent, double mutationChance) {
      return parent.newInstance(Mutations.def(Crossovers.def(genotype, parent.getGenotype()), mutationChance));
   }

   @Override
   public int compareTo(Geneticable o) {
      if (fitness > o.getFitness()) return -1;
      if (fitness < o.getFitness()) return 1;
      return 0;
   }

}
