package aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.abstr;

import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;

/**
 * Abstract algorithm configurations class
 */
public abstract class AbstractAlgorithmConfigurations {
   /**
    * Value defines iterations which will be displayed in the console.
    * Example:
    * when iterationsDisplaying = 100 then
    * algorithm display status after 100, 200, 300... iterations
    */
   public Integer iterationsDisplaying;

   /**
    * Value defines delay between each iteration of algorithm and is given in
    * milliseconds. This value can be used with {@link Vision} to
    * research the neural network behavior (WARNING: this value slows down the teaching process).
    */
   public Integer delay;

   /**
    * Amount of iterations of deep learning
    */
   public Long iterations;

   /**
    * Minimal defect to break the learning process
    */
   public Double minimalDefect;
}
