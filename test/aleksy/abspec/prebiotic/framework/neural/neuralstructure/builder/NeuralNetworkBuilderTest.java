package aleksy.abspec.prebiotic.framework.neural.neuralstructure.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.EndpointBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NetworkWithInputLayerBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.*;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.KohonenNeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import testutil.TestCustomFunctions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NeuralNetworkBuilderTest {

   private NeuralNetworkBuilder uut;
   private PrebioticApi prebiotic;

   @Before
   public void init() {
      prebiotic = Prebiotic.newInstance();
      try {
         uut = prebiotic.neuralApi().getNeuralNetworkBuilder(createTestConfigurations());
      } catch (EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
   }

   @Test
   public void shouldReturnNetworkWithInputLayerBuilder() {
      // when
      NetworkWithInputLayerBuilder actualBuilder = null;
      try {
         actualBuilder = uut.addDefaultInputLayer(1, 2);
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }

      // then
      assertEquals(NetworkWithInputLayerBuilder.class, actualBuilder.getClass());
   }

   @Test
   public void shouldReturnEndpointBuilder() {
      // when
      EndpointBuilder actualBuilder = null;
      try {
         actualBuilder = uut.buildSingleLayerNetwork(1, 2);
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }

      // then
      assertEquals(EndpointBuilder.class, actualBuilder.getClass());
   }

   @Test
   public void shouldCorrectlyCreateNeuralNetwork() {
      // given
      String description = "neural network created in builder";
      DataNormalizationType dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      NeuralNetworkType type = NeuralNetworkType.FEEDFORWARD;
      CoreFunctionRecipe coreRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      AxonFunctionRecipe axonRecipe = AxonFunctionRecipe.SIGMOID;
      WeightFunctionRecipe weightRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      int numberOfLayers = 1;
      int numberOfNeurons = 1;
      int numberOfWeightsOfNeuron = 1;
      Double weight = 1.0;

      // when
      NeuralNetwork actualNetwork = null;
      try {
         actualNetwork = prebiotic.neuralApi().getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultFeedforwardConfigurations()
               .setBiasPresence(false)
               .create()
         ).addDefaultInputLayer(1, 1)
            .endBuildingNetwork()
            .create();
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
      Neuron neuron = actualNetwork
         .getLayers().get(0)
         .getNeurons().get(0);

      // then
      assertFalse(actualNetwork.isBiasPresence());
      assertEquals(description, actualNetwork.getDescription());
      Assert.assertEquals(dataNormalization, actualNetwork.getDataNormalization());
      Assert.assertEquals(type, actualNetwork.getType());
      Assert.assertEquals(coreRecipe, actualNetwork.getCoreFunction().getCoreFunctionRecipe());
      Assert.assertEquals(axonRecipe, actualNetwork.getAxonFunction().getAxonFunctionRecipe());
      Assert.assertEquals(weightRecipe, actualNetwork.getWeightFunctionRecipe());
      assertEquals(numberOfLayers, actualNetwork.numberOfLayers());
      assertEquals(numberOfNeurons, actualNetwork.size());
      assertEquals(numberOfWeightsOfNeuron, neuron.size());
      Assert.assertEquals(weight, neuron.getWeights().get(0).getFactors().get(0));
   }

   @Test
   public void shouldSetDescriptionToTheNetwork() {
      // given
      String description = "user description";

      // when
      NeuralNetwork nn = null;
      try {
         nn = uut.addDefaultInputLayer(1, 1)
            .endBuildingNetwork()
            .create(description);
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }

      // then
      assertEquals(description, nn.getDescription());
   }

   @Test
   public void shouldCreateKohonenNeuralNetwork() {
      // given
      String description = "neural network created in builder";
      DataNormalizationType dataNormalization = DataNormalizationType.NO_NORMALIZATION;
      NeuralNetworkType type = NeuralNetworkType.KOHONEN;
      CoreFunctionRecipe coreRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      AxonFunctionRecipe axonRecipe = AxonFunctionRecipe.HYPERBOLIC;
      WeightFunctionRecipe weightRecipe = WeightFunctionRecipe.SQUARE_DIFFERENCE;
      int numberOfLayers = 1;
      int numberOfNeurons = 1;
      int numberOfWeightsOfNeuron = 1;
      Double weight = 1.0;

      // when
      NeuralNetwork actualNetwork = null;
      try {
         actualNetwork = prebiotic.neuralApi().getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultKohonenConfigurations()
               .create()
         ).buildKohonenMap(1, 1, 1)
            .create();
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
      Neuron neuron = actualNetwork
         .getInputLayer()
         .getNeurons().get(0);

      // then
      assertFalse(actualNetwork.isBiasPresence());
      assertEquals(description, actualNetwork.getDescription());
      Assert.assertEquals(dataNormalization, actualNetwork.getDataNormalization());
      Assert.assertEquals(type, actualNetwork.getType());
      Assert.assertEquals(coreRecipe, actualNetwork.getCoreFunction().getCoreFunctionRecipe());
      Assert.assertEquals(axonRecipe, actualNetwork.getAxonFunction().getAxonFunctionRecipe());
      Assert.assertEquals(weightRecipe, actualNetwork.getWeightFunctionRecipe());
      assertEquals(numberOfLayers, actualNetwork.numberOfLayers());
      assertEquals(numberOfNeurons, actualNetwork.size());
      assertEquals(numberOfWeightsOfNeuron, neuron.size());
      Assert.assertEquals(weight, neuron.getWeights().get(0).getFactors().get(0));
      assertEquals(actualNetwork.getClass(), KohonenNeuralNetworkImpl.class);
   }

   @Test(expected = InvalidStructureException.class)
   public void shouldThrowInvalidExceptionWhenKohonenNetworkIsBuilding() throws InvalidStructureException, EmptyFunctionDefinitionException {
      // given
      // when
      prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultKohonenConfigurations()
            .create()
      ).buildSingleLayerNetwork(1, 2)
         .create();
      // then exception
   }

   @Test(expected = InvalidStructureException.class)
   public void shouldThrowInvalidExceptionWhenFeedforwardNetworkIsBuilding() throws InvalidStructureException, EmptyFunctionDefinitionException {
      // given
      // when
      prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .create()
      ).buildKohonenMap(1, 1, 1)
         .create();
      // then exception
   }

   @Test
   public void shouldCreateNetworkWithCustomFunctions() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      // when
      NeuralNetwork nn = prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setAxonFunction(AxonFunctionRecipe.AXON_CUSTOM)
            .setWeightFunction(WeightFunctionRecipe.WEIGHT_CUSTOM)
            .setCoreFunction(CoreFunctionRecipe.CORE_CUSTOM)
            .defineAxonInstance(new TestCustomFunctions.CUSTOM_AXON_TEST())
            .defineCoreInstance(new TestCustomFunctions.CUSTOM_CORE_TEST())
            .defineWeightInstance(new TestCustomFunctions.CUSTOM_WEIGHT_TEST())
            .create()
      ).buildSingleLayerNetwork(1, 1)
         .create();

      // then
      Assert.assertEquals(AxonFunctionRecipe.AXON_CUSTOM, nn.getAxonFunction().getAxonFunctionRecipe());
      Assert.assertEquals(CoreFunctionRecipe.CORE_CUSTOM, nn.getCoreFunction().getCoreFunctionRecipe());
      Assert.assertEquals(WeightFunctionRecipe.WEIGHT_CUSTOM, nn.getWeightFunctionRecipe());
      Assert.assertEquals(TestCustomFunctions.CUSTOM_AXON_TEST.class, nn.getAxonFunction().getClass());
      Assert.assertEquals(TestCustomFunctions.CUSTOM_CORE_TEST.class, nn.getCoreFunction().getClass());
      Assert.assertEquals(TestCustomFunctions.CUSTOM_WEIGHT_TEST.class,
         nn.getInputLayer().getNeurons().get(0).getWeights().get(0).getClass());
   }

   @Test(expected = EmptyFunctionDefinitionException.class)
   public void shouldThrowAnExceptionWhenAxonModelIsNotDefine() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      // when
      prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setAxonFunction(AxonFunctionRecipe.AXON_CUSTOM)
            .create()
      ).buildSingleLayerNetwork(1, 1)
         .create();
      // then exception
   }

   @Test(expected = EmptyFunctionDefinitionException.class)
   public void shouldThrowAnExceptionWhenCoreModelIsNotDefine() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      // when
      prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setCoreFunction(CoreFunctionRecipe.CORE_CUSTOM)
            .create()
      ).buildSingleLayerNetwork(1, 1)
         .create();
      // then exception
   }

   @Test(expected = EmptyFunctionDefinitionException.class)
   public void shouldThrowAnExceptionWhenWeightModelIsNotDefine() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      // when
      prebiotic.neuralApi().getNeuralNetworkBuilder(
         prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setWeightFunction(WeightFunctionRecipe.WEIGHT_CUSTOM)
            .create()
      ).buildSingleLayerNetwork(1, 1)
         .create();
      // then exception
   }


   private StructureConfigurations createTestConfigurations() {
      StructureConfigurations sc = new StructureConfigurations();
      sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
      sc.biasPresence = true;
      sc.axonFunctionRecipe = AxonFunctionRecipe.SIGMOID;
      sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      return sc;
   }
}
