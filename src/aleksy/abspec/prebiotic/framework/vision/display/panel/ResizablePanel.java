package aleksy.abspec.prebiotic.framework.vision.display.panel;

import aleksy.abspec.prebiotic.framework.vision.common.constants.VisionConstants;

import javax.swing.*;
import java.awt.*;

/**
 * Panel which can resize all internal components when frame was resized
 */
public class ResizablePanel extends JPanel {
   public ResizablePanel() {
      setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      if (getComponentCount() == 7) {
         for (int i = 0; i < getComponentCount(); i++) {
            getComponent(i).setSize(getSize().width / 3,
               getSize().height / 3);
         }
         getComponent(6).setSize(getSize().width, getSize().height / 3);
         getComponent(0).setLocation(0, 0);
         getComponent(1).setLocation(getWidth() / 3, 0);
         getComponent(2).setLocation(0, getHeight() / 3);
         getComponent(3).setLocation(getWidth() / 3, getHeight() / 3);
         getComponent(4).setLocation(2 * getWidth() / 3, 0);
         getComponent(5).setLocation(2 * getWidth() / 3, getHeight() / 3);
         getComponent(6).setLocation(0, 2 * getHeight() / 3);
      }
   }
}
