package aleksy.abspec.prebiotic.framework.neural.neuralstructure.builder;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.EndpointBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.CoreFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.impl.NeuralLayerImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.KohonenNeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl.NeuralNetworkImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.KohonenNeuronImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EndpointBuilderTest {

   private EndpointBuilder uut;

   @Test
   public void shouldCreateNetworkWithDefaultDescriptionAndNotAddNewWeightsWhenBiasIsNotSet() {
      // given
      uut = new EndpointBuilder(createSimpleNetwork(false), Prebiotic.newInstance());
      String expectedDescription = "neural network created in builder";
      int expectedNumberOfWeights = 1;
      int actualNumberOfWeights;
      // when
      NeuralNetwork actualNeuralNetwork = null;
      try {
         actualNeuralNetwork = uut.create();
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
      actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
      // then
      assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
      assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
   }

   @Test
   public void shouldCreateNetworkWithDefaultDescriptionAndAddNewWeightWhenBiasIsSet() {
      // given
      uut = new EndpointBuilder(createSimpleNetwork(true), Prebiotic.newInstance());
      String expectedDescription = "neural network created in builder";
      NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
      int expectedNumberOfWeights = 2;
      int actualNumberOfWeights;
      // when
      NeuralNetwork actualNeuralNetwork = null;
      try {
         actualNeuralNetwork = uut.create();
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
      actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
      // then
      assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
      assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
      Assert.assertEquals(expectedType, actualNeuralNetwork.getType());
   }

   @Test
   public void shouldCreateNetworkWithCustomDescriptionAndNotAddNewWeightsWhenBiasIsNotSet() {
      // given
      uut = new EndpointBuilder(createSimpleNetwork(false), Prebiotic.newInstance());
      String expectedDescription = "custom description";
      NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
      int expectedNumberOfWeights = 1;
      int actualNumberOfWeights;
      // when
      NeuralNetwork actualNeuralNetwork = null;
      try {
         actualNeuralNetwork = uut.create(expectedDescription);
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
      actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
      // then
      assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
      assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
      Assert.assertEquals(expectedType, actualNeuralNetwork.getType());
   }

   @Test
   public void shouldCreateNetworkWithCustomDescriptionAndAddNewWeightWhenBiasIsSet() {
      // given
      uut = new EndpointBuilder(createSimpleNetwork(true), Prebiotic.newInstance());
      String expectedDescription = "custom description";
      NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
      int expectedNumberOfWeights = 2;
      int actualNumberOfWeights;
      // when
      NeuralNetwork actualNeuralNetwork = null;
      try {
         actualNeuralNetwork = uut.create(expectedDescription);
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
      actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
      // then
      assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
      assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
      Assert.assertEquals(expectedType, actualNeuralNetwork.getType());
   }

   @Test(expected = InvalidStructureException.class)
   public void shouldThrowInvalidStructureException() throws InvalidStructureException {
      // given
      uut = new EndpointBuilder(createInvalidNetwork(), Prebiotic.newInstance());

      // when
      uut.create();

      // then exception
   }

   private NeuralNetwork createSimpleNetwork(boolean biasPresence) {
      NeuralNetwork nn = new NeuralNetworkImpl();
      NeuralLayer layer = new NeuralLayerImpl();
      List<NeuralLayer> layers = new ArrayList<>();
      WeightFunction weight = WeightFunctionRecipe.DEFAULT_LINEAR.newInstance(false);
      List<WeightFunction> weights = new ArrayList<>();
      weights.add(weight);
      CoreFunction core = CoreFunctionRecipe.DEFAULT_ADDER.newInstance();
      AxonFunction axon = AxonFunctionRecipe.SIGMOID.newInstance();
      Neuron neuron = new NeuronImpl(weights, core, axon, biasPresence);
      List<Neuron> neurons = new ArrayList<>();
      neurons.add(neuron);
      layer.setNeurons(neurons);
      layers.add(layer);
      nn.setLayers(layers);
      nn.setBiasPresence(biasPresence);
      nn.setType(NeuralNetworkType.FEEDFORWARD);
      return nn;
   }

   private NeuralNetwork createInvalidNetwork() {
      NeuralNetwork nn = new KohonenNeuralNetworkImpl();
      NeuralLayer layer = new NeuralLayerImpl();
      List<NeuralLayer> layers = new ArrayList<>();
      WeightFunction weight = WeightFunctionRecipe.SQUARE_DIFFERENCE.newInstance(false);
      List<WeightFunction> weights = new ArrayList<>();
      weights.add(weight);
      CoreFunction core = CoreFunctionRecipe.DEFAULT_ADDER.newInstance();
      AxonFunction axon = AxonFunctionRecipe.HYPERBOLIC.newInstance();
      Neuron neuron = new KohonenNeuronImpl(weights, core, axon, false, 1, 1);
      List<Neuron> neurons = new ArrayList<>();
      neurons.add(neuron);
      layer.setNeurons(neurons);
      layers.add(layer);
      layers.add(new NeuralLayerImpl()); // additional layer in kohonen'TeachingDataManagementComposite map is invalid
      nn.setLayers(layers);
      nn.setBiasPresence(false);
      nn.setType(NeuralNetworkType.KOHONEN);
      return nn;
   }
}
