package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;

/**
 * Axon function interface
 */
public interface AxonFunction extends Function {
   /**
    * Function calculates output from core function to external neuron output
    *
    * @param x internal output from core
    * @return external neuron output in specific data type
    */
   Double f(Double x);

   /**
    * Getter for type of axon function recipe
    *
    * @return axon function recipe
    */
   AxonFunctionRecipe getAxonFunctionRecipe();

   /**
    * Setter for axon function recipe
    *
    * @param axonFunctionRecipe to set
    */
   void setAxonFunctionRecipe(AxonFunctionRecipe axonFunctionRecipe);

   /**
    * Function calculates a derivative from basic axon function. This method is needed
    * for deep learning algorithm
    *
    * @param x parameter of derivative
    * @return derivative from basic axon function
    */
   Double derivative(Double x);
}
