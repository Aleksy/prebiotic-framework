package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.AxonFunctions;

/**
 * Axon functions recipes
 */
public enum AxonFunctionRecipe {
   /**
    * Hyperbolic tangent function
    */
   TANH(-1.0, 1.0),
   /**
    * Default linear function
    */
   LINEAR(Double.MIN_VALUE, Double.MAX_VALUE),
   /**
    * Threshold function
    */
   THRESHOLD(0.0, 1.0),
   /**
    * Threshold function for Hopfield's networks
    */
   HOPFIELD_THRESHOLD(-1.0, 1.0),
   /**
    * Threshold function for Hopfield's networks
    */
   HOPFIELD_SIGMOID(0.0, 1.0),
   /**
    * Hyperbolic tangent function for Hopfield's networks - tanh(100 * x)
    */
   HOPFIELD_TANH(-1.0, 1.0),
   /**
    * Sigmoid function
    */
   SIGMOID(0.0, 1.0),
   /**
    * Hyperbolic function
    */
   HYPERBOLIC(0.0, Double.MAX_VALUE),
   /**
    * Custom axon function
    */
   AXON_CUSTOM(-1.0, 1.0);

   private double from;
   private double to;

   AxonFunctionRecipe(double from, double to) {
      this.from = from;
      this.to = to;
   }

   /**
    * Method creates the new instance of axon function to set to neural network
    *
    * @return new instance of axon function
    */
   public AxonFunction newInstance() {
      AxonFunction axon = null;
      if (this == AxonFunctionRecipe.TANH)
         axon = new AxonFunctions.TANH();
      if (this == AxonFunctionRecipe.LINEAR)
         axon = new AxonFunctions.LINEAR();
      if (this == AxonFunctionRecipe.THRESHOLD)
         axon = new AxonFunctions.THRESHOLD();
      if (this == AxonFunctionRecipe.HOPFIELD_THRESHOLD)
         axon = new AxonFunctions.HOPFIELD_THRESHOLD();
      if (this == AxonFunctionRecipe.HOPFIELD_SIGMOID)
         axon = new AxonFunctions.HOPFIELD_SIGMOID();
      if (this == AxonFunctionRecipe.HOPFIELD_TANH)
         axon = new AxonFunctions.HOPFIELD_TANH();
      if (this == AxonFunctionRecipe.SIGMOID)
         axon = new AxonFunctions.SIGMOID();
      if (this == AxonFunctionRecipe.HYPERBOLIC)
         axon = new AxonFunctions.HYPERBOLIC();
      return axon;
   }

   public Range getRangeOfPossibleValues() {
      return Range.from(from, to);
   }
}
