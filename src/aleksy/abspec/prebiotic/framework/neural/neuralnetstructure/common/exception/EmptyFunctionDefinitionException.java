package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;

/**
 * Empty function definition exception is used when user want's to create
 * neural network with custom functions but not defines it in the
 * {@link NeuralNetworkStructureConfigurationsBuilder}
 */
public class EmptyFunctionDefinitionException extends NeuralNetworkException {
   public EmptyFunctionDefinitionException() {
      super("\nEmpty function definition.");
   }

   public EmptyFunctionDefinitionException(String message) {
      super("\nEmpty function definition. " + message);
   }
}
