package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.api.NeuralStructureApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;

/**
 * Neural Structure submodule api basic implementation
 */
public class NeuralStructure implements NeuralStructureApi {
   private PrebioticApi prebiotic;

   public NeuralStructure(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   @Override
   public NeuralNetworkBuilder getNeuralNetworkBuilder(StructureConfigurations structureConfigurations)
      throws EmptyFunctionDefinitionException {
      return new NeuralNetworkBuilder(structureConfigurations, prebiotic);
   }

   @Override
   public NeuralNetworkStructureConfigurationsBuilder getStructureConfigurationsBuilder() {
      return new NeuralNetworkStructureConfigurationsBuilder(prebiotic);
   }

}
