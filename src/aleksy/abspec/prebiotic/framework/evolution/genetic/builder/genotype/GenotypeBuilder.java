package aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype;

import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

/**
 * Genotype builder
 */
public class GenotypeBuilder {
   private Genotype genotype;

   /**
    * Constructor
    */
   public GenotypeBuilder() {
      genotype = new Genotype();
   }

   /**
    * Adds new chromosome to the genotype
    *
    * @return chromosome builder
    */
   public ChromosomeBuilder addChromosome() {
      return new ChromosomeBuilder(this, genotype);
   }

   /**
    * Adds new chromosome to the genotype
    *
    * @param description to set in chromosome
    * @return chromosome builder
    */
   public ChromosomeBuilder addChromosome(String description) {
      return new ChromosomeBuilder(this, genotype, description);
   }

   /**
    * Creates new genotype
    *
    * @return genotype instance
    */
   public Genotype create() {
      return genotype;
   }
}
