![](img/prebiotic_readme.png)

**What the PF is?**

Prebiotic is a framework to create neural networks and teach them in
easy way. PF supports:  
- building multilayer feedforward neural networks  
- building Kohonen's self-organising map
- building Hopfield's reccurent networks   
- teaching neural networks standard deep learning algorithm and self-learning Kohonen's algorithm and Hebb's method     
- Saving neural network to xml and pnn file and parsing networks from file  
- Modeling the genetic algorithm problems   
- Displaying the objects in the JSwing frames and panels   

To become familiar with PF workflow and usage please see a [Wiki](https://gitlab.com/Aleksy/prebiotic-framework/wikis/home).
In the [Changelog](https://gitlab.com/Aleksy/prebiotic-framework/blob/master/CHANGELOG.md) you can find informations about history of changes, older versions.
Please also take a moment to read the [License](https://gitlab.com/Aleksy/prebiotic-framework/blob/master/LICENSE.md).  
  
**Download**  
  
Newest version:  
[Prebiotic Framework 2.0.0](https://drive.google.com/open?id=13wOJSPIXgiqeNpUvdEU2lZKHJcJ3ykI_)  

For older versions please visit [Changelog](https://gitlab.com/Aleksy/prebiotic-framework/blob/master/CHANGELOG.md).  
  
Instalation:  
If you want to use Prebiotic Framework as external library:  
    1. Read the [License](https://gitlab.com/Aleksy/prebiotic-framework/blob/master/LICENSE.md)  
    2. Click the download link above.  
    3. Include JAR file to your project as external library.
  
If you want to explore and study source code:  
    1. Read the [License](https://gitlab.com/Aleksy/prebiotic-framework/blob/master/LICENSE.md)  
    2. Clone repository:
```
git clone https://gitlab.com/Aleksy/prebiotic-framework
```
Enjoy!  
  
**Future**  
  
Version 1.0.0 was released. I want to make research about more types of neural networks and more ways of learning.