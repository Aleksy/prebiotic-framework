package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

import java.util.ArrayList;
import java.util.List;

/**
 * Brain representation. Contains list of neural networks
 */
public class Brain {
   private List<NeuralNetwork> neuralNetworks;

   /**
    * Constructor
    */
   public Brain() {
      neuralNetworks = new ArrayList<>();
   }

   /**
    * Getter for list of neural networks
    *
    * @return neural networks
    */
   public List<NeuralNetwork> getNeuralNetworks() {
      return neuralNetworks;
   }

   /**
    * Setter for neural networks
    *
    * @param neuralNetworks to set
    */
   public void setNeuralNetworks(List<NeuralNetwork> neuralNetworks) {
      this.neuralNetworks = neuralNetworks;
   }
}
