package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.api;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;

/**
 * Neural Structure submodule api
 */
public interface NeuralStructureApi {

   /**
    * Getter for structure configurations builder
    *
    * @return new instance of structure configurations builder, which can be used to createGene
    * new configurations for network builder
    */
   NeuralNetworkStructureConfigurationsBuilder getStructureConfigurationsBuilder();

   /**
    * Getter for neural network builder
    *
    * @param structureConfigurations configurations for new builder instance
    * @return builder, which can be used to building new neural network
    * @throws EmptyFunctionDefinitionException when user set any function abspec CUSTOM and don't choose the function class
    */
   NeuralNetworkBuilder getNeuralNetworkBuilder(StructureConfigurations structureConfigurations) throws EmptyFunctionDefinitionException;
}
