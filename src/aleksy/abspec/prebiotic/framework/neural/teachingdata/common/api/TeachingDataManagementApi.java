package aleksy.abspec.prebiotic.framework.neural.teachingdata.common.api;

import aleksy.abspec.prebiotic.framework.neural.teachingdata.builder.TeachingDataBuilder;

/**
 * Teaching data management submodule api
 */
public interface TeachingDataManagementApi {
   /**
    * Getter for teaching data parsing
    *
    * @return parsing for teaching data
    */
   TeachingDataBuilder getTeachingDataBuilder();
}
