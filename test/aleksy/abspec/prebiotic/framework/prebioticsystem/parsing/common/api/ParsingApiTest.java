package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.api;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder.CustomFunctionsForParserBuilder;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.impl.Parsing;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParsingApiTest {
   private ParsingApi uut;

   @Before
   public void init() {
      uut = new Parsing(Prebiotic.newInstance());
   }

   @Test
   public void shouldReturnInstanceOfCustomFunctionsBuilder() {
      // when
      CustomFunctionsForParserBuilder customFunctionsForParserBuilder
         = uut.getCustomFunctionsForParserBuilder();

      // then
      assertEquals(CustomFunctionsForParserBuilder.class, customFunctionsForParserBuilder.getClass());
   }
}
