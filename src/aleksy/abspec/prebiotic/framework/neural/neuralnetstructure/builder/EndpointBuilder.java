package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;

/**
 * Endpoint builder of neural network - last step in network building
 */
public class EndpointBuilder extends Loggable {
   private NeuralNetwork network;
   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param network   to use in builder
    * @param prebiotic api
    */
   public EndpointBuilder(NeuralNetwork network, PrebioticApi prebiotic) {
      this.network = network;
      this.prebiotic = prebiotic;
      if (network.isBiasPresence())
         network.createBiasWeights();
   }

   /**
    * Creates a new neural network with default description
    *
    * @return neural network ready to use
    * @throws InvalidStructureException when structure of network is invalid
    */
   public NeuralNetwork create() throws InvalidStructureException {
      network.setDescription("neural network created in builder");
      validate(network);
      prebiotic.prebioticSystemApi().log(network, getSenderName());
      network.setPrebiotic(prebiotic);
      return network;
   }

   /**
    * Creates a new neural network with custom description
    *
    * @param description to set in network
    * @return new neural network ready to use
    * @throws InvalidStructureException when structure of network is invalid
    */
   public NeuralNetwork create(String description) throws InvalidStructureException {
      network.setDescription(description);
      validate(network);
      prebiotic.prebioticSystemApi().log(network, getSenderName());
      network.setPrebiotic(prebiotic);
      return network;
   }

   /**
    * Randomizes the all weights in neural network
    *
    * @param from lower bound
    * @param to   upper bound
    * @return builder
    */
   public EndpointBuilder randomizeWeights(Double from, Double to) {
      network.randomizeWeights(from, to);
      prebiotic.prebioticSystemApi().log("Weights of neural network has been randomized.", getSenderName());
      return this;
   }

   private void validate(NeuralNetwork network) throws InvalidStructureException {
      if (network.getType() == NeuralNetworkType.KOHONEN) {
         if (network.numberOfLayers() != 1)
            throw new InvalidStructureException("Kohonen's networks can have only one layer. " +
               "Remove additional layers definitions.");
         if (network.isBiasPresence()) {
            throw new InvalidStructureException("Kohonen's networks can not have a bias presence. " +
               "Remove bias presence from structure configurations");
         }
      }
   }

   @Override
   protected String getSenderName() {
      return "END-BLD";
   }
}
