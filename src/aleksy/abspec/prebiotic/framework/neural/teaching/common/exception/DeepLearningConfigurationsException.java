package aleksy.abspec.prebiotic.framework.neural.teaching.common.exception;

/**
 * Deep learning configurations exception
 */
public class DeepLearningConfigurationsException extends DeepLearningException {
   public DeepLearningConfigurationsException() {
      super("\nDeep learning configurations error.");
   }

   public DeepLearningConfigurationsException(String message) {
      super("\nDeep learning configurations error. " + message);
   }
}
