package aleksy.abspec.prebiotic.framework.neural.teaching.builder;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.vision.common.impl.Vision;

/**
 * Deep learning configurations builder
 */
public class DeepLearningConfigurationsBuilder extends Loggable {
   private PrebioticApi prebiotic;
   private DeepLearningConfigurations config;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public DeepLearningConfigurationsBuilder(PrebioticApi prebiotic) {
      config = new DeepLearningConfigurations();
      this.prebiotic = prebiotic;
   }

   /**
    * Setter for learning rate
    *
    * @param learningRate to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setLearningRate(Double learningRate) {
      config.learningRate = learningRate;
      return this;
   }

   /**
    * Setter for momentum rate
    *
    * @param momentum to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setMomentum(Double momentum) {
      config.momentum = momentum;
      return this;
   }

   /**
    * Setter for random values maximal range. If exists, deep learning process adds random
    * values from range (-maximalRange, maximalRange) to all factors of the weights.
    * Range is not included to the algorithm when it is null
    *
    * @param maximalRange value
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setRandomValuesMaximalRange(double maximalRange) {
      config.randomValuesRange = Range.from(-maximalRange, maximalRange);
      return this;
   }

   /**
    * Setter for reduce learning rate. If not null, learning rate is reduced during the learning process
    * to this value
    *
    * @param reduceLearningRate to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setReducedLearningRate(double reduceLearningRate) {
      config.reducedLearningRate = reduceLearningRate;
      return this;
   }

   /**
    * Setter for speed of reducing the learning rate
    *
    * @param speedOfReducing to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setSpeedOfReducing(double speedOfReducing) {
      config.speedOfReducing = speedOfReducing;
      return this;
   }

   /**
    * Setter for iterations displaying parameter
    * Value defines iterations which will be displayed in the console.
    * Example:
    * when iterationsDisplaying = 100 then
    * algorithm display status after 100, 200, 300... iterations
    *
    * @param iterationsDisplaying to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setIterationsDisplaying(int iterationsDisplaying) {
      config.iterationsDisplaying = iterationsDisplaying;
      return this;
   }

   /**
    * Setter for delay value. Value defines delay between each iteration of algorithm and is given in
    * milliseconds. This value can be used with {@link Vision} to
    * research the neural network behavior (WARNING: this value slows down the teaching process).
    *
    * @param delay to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setDelay(int delay) {
      config.delay = delay;
      return this;
   }

   /**
    * Setter for iterations number
    *
    * @param iterations to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setIterations(long iterations) {
      config.iterations = iterations;
      return this;
   }

   /**
    * Setter for minimal defect of the network. Achievement of the minimal defect
    * stops the learning process
    *
    * @param minimalDefect to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder setMinimalDefect(double minimalDefect) {
      config.minimalDefect = minimalDefect;
      return this;
   }

   /**
    * Creates a default configurations. Iterations number is needed abspec parameter.
    * Learning rate is set to 0.1 and iterations displaying equals 100.
    * Momentum is set to 0.6.
    *
    * @param iterations to set
    * @return builder
    */
   public DeepLearningConfigurationsBuilder defaultConfigurations(long iterations) {
      config.learningRate = 0.1;
      config.iterations = iterations;
      config.iterationsDisplaying = 100;
      config.momentum = 0.6;
      return this;
   }


   /**
    * Creation method
    *
    * @return new instance of {@link DeepLearningConfigurations}
    */
   public DeepLearningConfigurations create() {
      String delay;
      if (config.delay == null) {
         delay = "not set";
         config.delay = 0;
      } else
         delay = config.delay.toString();
      prebiotic.prebioticSystemApi().log("Deep learning configurations has been built.", getSenderName());
      prebiotic.prebioticSystemApi().separator();
      prebiotic.prebioticSystemApi().log(" Iterations: " + config.iterations, getSenderName());
      prebiotic.prebioticSystemApi().log(" Learning rate: " + config.learningRate, getSenderName());
      if (config.reducedLearningRate != null) {
         prebiotic.prebioticSystemApi().log(" Reduced learning rate value: " + config.reducedLearningRate, getSenderName());
         prebiotic.prebioticSystemApi().log(" Speed of learning rate reducing: " + config.speedOfReducing, getSenderName());
      }
      prebiotic.prebioticSystemApi().log(" Momentum: " + config.momentum, getSenderName());
      if (config.randomValuesRange != null)
         prebiotic.prebioticSystemApi().log(" Range of random values to add: "
            + config.randomValuesRange.getFrom()+ " to " + config.randomValuesRange.getTo(), getSenderName());
      prebiotic.prebioticSystemApi().log(" Iterations displaying interval: " + config.iterationsDisplaying, getSenderName());
      prebiotic.prebioticSystemApi().log(" Delay: " + delay, getSenderName());
      prebiotic.prebioticSystemApi().separator();
      return config;
   }

   @Override
   protected String getSenderName() {
      return "DEEP-LRN-CONF-BLD";
   }
}
