package aleksy.abspec.prebiotic.framework.neural.teaching.common.api;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.KohonenLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.impl.Teaching;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TeachingTest {
   private TeachingApi uut;

   @Before
   public void init() {
      uut = new Teaching(Prebiotic.newInstance());
   }

   @Test
   public void shouldReturnDeepLearningConfigBuilder() {
      // when
      DeepLearningConfigurationsBuilder deepLearningConfigurationsBuilder = uut.
         getDeepLearningConfigurationsBuilder();

      // then
      Assert.assertEquals(DeepLearningConfigurationsBuilder.class,
         deepLearningConfigurationsBuilder.getClass());
   }

   @Test
   public void shouldReturnKohonenLearningConfigBuilder() {
      // when
      KohonenLearningConfigurationsBuilder deepLearningConfigurationsBuilder = uut.
         getKohonenLearningConfigurationsBuilder();

      // then
      Assert.assertEquals(KohonenLearningConfigurationsBuilder.class,
         deepLearningConfigurationsBuilder.getClass());
   }
}
