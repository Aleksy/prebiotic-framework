package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;

/**
 * Custom functions for parser class is needed when user want's to read
 * own neural network model with any recipe of function set to CUSTOM
 */
public class CustomFunctionsForParser {
   /**
    * Axon function instance
    */
   public AxonFunction axonFunction;
   /**
    * Core function instance
    */
   public CoreFunction coreFunction;
   /**
    * Weight function instance
    */
   public WeightFunction weightFunction;
}
