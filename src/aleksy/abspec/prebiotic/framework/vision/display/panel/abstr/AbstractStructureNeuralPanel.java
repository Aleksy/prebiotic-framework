package aleksy.abspec.prebiotic.framework.vision.display.panel.abstr;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

import java.awt.*;

/**
 * Abstract panel to draw structure of neural network
 */
public abstract class AbstractStructureNeuralPanel extends AbstractNeuralPanel {
   protected AbstractStructureNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(Color.GRAY);
      g.setFont(font);
      g.drawString("structure simulation:", 5, getSize().height / 20);
   }

   protected void drawNeuron(int x, int y, Graphics g) {
      Color tmp = g.getColor();
      g.fillRect(x - 6, y - 6, 12, 12);
      g.setColor(createContourColor(tmp));
      g.drawRect(x - 6, y - 6, 12, 12);
      g.setColor(tmp);
   }

   protected void drawInputNeuron(int x, int y, double value, Graphics g) {
      Color tmp = g.getColor();
      g.fillOval(x - 6, y - 6, 12, 12);
      g.setColor(createBrighterColor(tmp));
      g.drawOval(x - 6, y - 6, 12, 12);
      g.setColor(createAntiColor(1.0));
      value *= 1000;
      value = Math.round(value);
      value /= 1000;
      g.setFont(font);
      g.drawString(Double.toString(value), x - 12, y - 10);
      g.setColor(tmp);
   }

   protected void drawOutputNeuron(int x, int y, double value, Graphics g) {
      Color tmp = g.getColor();
      final int npoints = 3;
      int[] xpoints = new int[npoints];
      int[] ypoints = new int[npoints];
      xpoints[0] = x - 6;
      xpoints[1] = x + 6;
      xpoints[2] = x;
      ypoints[0] = y - 6;
      ypoints[1] = y - 6;
      ypoints[2] = y + 6;
      Polygon polygon = new Polygon(xpoints, ypoints, npoints);
      g.fillPolygon(polygon);
      g.setColor(createContourColor(tmp));
      g.drawPolygon(polygon);
      g.setColor(createAntiColor(1.0));
      value *= 1000;
      value = Math.round(value);
      value /= 1000;
      g.setFont(font);
      g.drawString(Double.toString(value), x - 12, y + 20);
      g.setColor(tmp);
   }

   protected void drawLittleNeuron(int x, int y, Double value, Graphics g) {
      g.setColor(Color.BLACK);
      g.fillRect(x - 3, y - 3, 6, 6);
      g.setColor(createColor(0.4).darker());
      g.drawRect(x - 3, y - 3, 6, 6);
      g.setColor(createColor(value));
      g.fillRect(x - 3, y - 3, 6, 6);
   }
}
