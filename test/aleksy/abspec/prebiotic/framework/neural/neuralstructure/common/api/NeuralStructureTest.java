package aleksy.abspec.prebiotic.framework.neural.neuralstructure.common.api;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.api.NeuralStructureApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.CoreFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.impl.NeuralStructure;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NeuralStructureTest {
   private NeuralStructureApi uut;

   @Before
   public void init() {
      uut = new NeuralStructure(Prebiotic.newInstance());
   }

   @Test
   public void shouldReturnStructureConfigurationsBuilder() {
      // when
      NeuralNetworkStructureConfigurationsBuilder structureConfigurationsBuilder = uut
         .getStructureConfigurationsBuilder();

      // then
      Assert.assertEquals(NeuralNetworkStructureConfigurationsBuilder.class,
         structureConfigurationsBuilder.getClass());
   }

   @Test
   public void shouldReturnNeuralNetworkBuilder() throws EmptyFunctionDefinitionException {
      // given
      StructureConfigurations configurations = new StructureConfigurations();
      configurations.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
      configurations.axonFunctionRecipe = AxonFunctionRecipe.TANH;
      configurations.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      configurations.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;

      // when
      NeuralNetworkBuilder builder = uut.getNeuralNetworkBuilder(configurations);

      // then
      assertEquals(NeuralNetworkBuilder.class,
         builder.getClass());
   }
}
