package aleksy.abspec.prebiotic.framework.vision.display.common.impl;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;
import aleksy.abspec.prebiotic.framework.vision.display.frame.Frame;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.activity.ActivityGraphNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.activity.ActivityRoundInputNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.activity.ActivityRoundNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.descriptor.DescriptorNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.genotype.GenotypePanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.FeedforwardStructureNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.FeedforwardStructureSquareNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.HopfieldStructurePanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.structure.KohonenStructureNeuralPanel;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.vision.display.common.api.DisplayApi;
import aleksy.abspec.prebiotic.framework.vision.display.panel.coordinatesystem.CoordinateSystemNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix.FeedforwardWeightMatrixNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix.HopfieldWeightMatrixNeuralPanel;
import aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix.KohonenWeightMatrixNeuralPanel;

import java.util.ArrayList;
import java.util.List;

/**
 * Display submodule api implementation
 */
public class Display implements DisplayApi {
   private List<AbstractPanel> activePanels;

   public Display() {
      activePanels = new ArrayList<>();
   }

   @Override
   public void display(NeuralNetwork nn, Double hue, DisplayMode mode, Range inputRange) {
      AbstractPanel panel;
      if (mode != DisplayMode.ALL && mode != DisplayMode.ALL_SQUARE) {
         panel = getNewPanel(nn, hue, mode, inputRange);
         new Frame(panel);
         activePanels.add(panel);
      } else {
         if (mode == DisplayMode.ALL) {
            AbstractPanel structure = getNewPanel(nn, hue, DisplayMode.STRUCTURE, inputRange);
            AbstractPanel weightMatrix = getNewPanel(nn, hue, DisplayMode.WEIGHT_MATRIX, inputRange);
            AbstractPanel coordinate = getNewPanel(nn, hue, DisplayMode.COORDINATE_SYSTEM, inputRange);
            AbstractPanel descriptor = getNewPanel(nn, hue, DisplayMode.DESCRIPTION, inputRange);
            AbstractPanel activityRound = getNewPanel(nn, hue, DisplayMode.ACTIVITY_ROUND_GRAPH, inputRange);
            AbstractPanel inputRound = getNewPanel(nn, hue, DisplayMode.INPUT_ROUND_GRAPH, inputRange);
            AbstractPanel activityGraph = getNewPanel(nn, hue, DisplayMode.ACTIVITY_TIME_GRAPH, inputRange);
            new Frame(structure, weightMatrix, coordinate, descriptor, activityRound, inputRound, activityGraph);
            activePanels.add(structure);
            activePanels.add(weightMatrix);
            activePanels.add(coordinate);
            activePanels.add(descriptor);
            activePanels.add(activityRound);
            activePanels.add(activityGraph);
         }
         if (mode == DisplayMode.ALL_SQUARE) {
            AbstractPanel structure = getNewPanel(nn, hue, DisplayMode.STRUCTURE_SQUARE, inputRange);
            AbstractPanel weightMatrix = getNewPanel(nn, hue, DisplayMode.WEIGHT_MATRIX, inputRange);
            AbstractPanel coordinate = getNewPanel(nn, hue, DisplayMode.COORDINATE_SYSTEM, inputRange);
            AbstractPanel descriptor = getNewPanel(nn, hue, DisplayMode.DESCRIPTION, inputRange);
            AbstractPanel activityRound = getNewPanel(nn, hue, DisplayMode.ACTIVITY_ROUND_GRAPH, inputRange);
            AbstractPanel inputRound = getNewPanel(nn, hue, DisplayMode.INPUT_ROUND_GRAPH, inputRange);
            AbstractPanel activityGraph = getNewPanel(nn, hue, DisplayMode.ACTIVITY_TIME_GRAPH, inputRange);
            new Frame(structure, weightMatrix, coordinate, descriptor, activityRound, inputRound, activityGraph);
            activePanels.add(structure);
            activePanels.add(weightMatrix);
            activePanels.add(coordinate);
            activePanels.add(descriptor);
            activePanels.add(activityRound);
            activePanels.add(activityGraph);
         }
      }
   }

   @Override
   public void display(Genotype genotype) {
      AbstractPanel panel = new GenotypePanel(genotype);
      new Frame(panel);
      activePanels.add(panel);
   }

   @Override
   public void repaintAll() {
      if (activePanels.size() > 0)
         for (AbstractPanel panel : activePanels)
            panel.repaint();
   }

   @Override
   public void removePanelFromActivePanels(AbstractPanel panel) {
      activePanels.remove(panel);
   }

   @Override
   public AbstractPanel getNewPanel(NeuralNetwork nn, Double hue, DisplayMode mode, Range inputRange) {
      AbstractPanel panel = null;
      if (mode == DisplayMode.STRUCTURE) {
         if (nn.getType() == NeuralNetworkType.FEEDFORWARD)
            panel = new FeedforwardStructureNeuralPanel(nn, hue, inputRange);
         if (nn.getType() == NeuralNetworkType.KOHONEN)
            panel = new KohonenStructureNeuralPanel(nn, hue, inputRange);
         if (nn.getType() == NeuralNetworkType.HOPFIELD)
            panel = new HopfieldStructurePanel(nn, hue, inputRange);
      }
      if (mode == DisplayMode.WEIGHT_MATRIX) {
         if (nn.getType() == NeuralNetworkType.FEEDFORWARD)
            panel = new FeedforwardWeightMatrixNeuralPanel(
               nn, hue, inputRange);
         if (nn.getType() == NeuralNetworkType.KOHONEN)
            panel = new KohonenWeightMatrixNeuralPanel(nn, hue, inputRange);
         if (nn.getType() == NeuralNetworkType.HOPFIELD)
            panel = new HopfieldWeightMatrixNeuralPanel(nn, hue, inputRange);
      }
      if (mode == DisplayMode.STRUCTURE_SQUARE) {
         if (nn.getType() == NeuralNetworkType.HOPFIELD)
            panel = new HopfieldStructurePanel(nn, hue, inputRange);
         if (nn.getType() == NeuralNetworkType.KOHONEN)
            panel = new KohonenStructureNeuralPanel(nn, hue, inputRange);
         if (nn.getType() == NeuralNetworkType.FEEDFORWARD)
            panel = new FeedforwardStructureSquareNeuralPanel(nn, hue, inputRange);
      }
      if (mode == DisplayMode.DESCRIPTION) {
         panel = new DescriptorNeuralPanel(nn, hue);
      }
      if (mode == DisplayMode.COORDINATE_SYSTEM) {
         panel = new CoordinateSystemNeuralPanel(nn, hue, inputRange);
      }
      if (mode == DisplayMode.ACTIVITY_ROUND_GRAPH) {
         panel = new ActivityRoundNeuralPanel(nn, hue, inputRange);
      }
      if (mode == DisplayMode.ACTIVITY_TIME_GRAPH) {
         panel = new ActivityGraphNeuralPanel(nn, hue, inputRange);
      }
      if (mode == DisplayMode.INPUT_ROUND_GRAPH) {
         panel = new ActivityRoundInputNeuralPanel(nn, hue, inputRange);
      }
      activePanels.add(panel);
      return panel;
   }

   @Override
   public AbstractPanel getNewPanel(Genotype genotype) {
      return new GenotypePanel(genotype);
   }
}
