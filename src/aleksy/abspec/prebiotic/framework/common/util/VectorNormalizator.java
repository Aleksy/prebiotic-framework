package aleksy.abspec.prebiotic.framework.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Vector normalizator util class
 */
public class VectorNormalizator {

   /**
    * Scales the vector to standard deviation.
    *
    * @param inputs list of doubles
    * @return scaled list
    */
   public static List<Double> scaleToStandardDeviation(List<Double> inputs) {
      List<Double> normalized = new ArrayList<>();
      Double avg = 0.0;
      for (Double x : inputs) {
         avg += x;
      }
      avg /= (double) inputs.size();
      List<Double> process = new ArrayList<>();
      for (Double x : inputs)
         process.add(Math.pow(x - avg, 2));
      Double sd = 0.0;
      for (Double x : process)
         sd += x;
      sd /= Math.sqrt((double) process.size());

      for (Double x : inputs)
         normalized.add((x - avg) / sd);
      return normalized;
   }

   /**
    * Scales the input vector to average of real numbers from this vector
    *
    * @param inputs list of doubles
    * @return scaled list
    */
   public static List<Double> scaleToAverage(List<Double> inputs) {
      List<Double> normalized = new ArrayList<>();
      Double avg = 0.0;
      for (Double x : inputs) {
         avg += x;
      }
      avg /= (double) inputs.size();
      for (Double x : inputs) {
         normalized.add(x / avg);
      }
      return normalized;
   }

   /**
    * Scales the input vector to deviation from the average of real numbers from this vector
    *
    * @param inputs list of doubles
    * @return scaled list
    */
   public static List<Double> scaleToDeviationFromAverage(List<Double> inputs) {
      List<Double> normalized = new ArrayList<>();
      Double avg = 0.0;
      for (Double x : inputs) {
         avg += x;
      }
      avg /= (double) inputs.size();
      for (Double x : inputs)
         normalized.add((x - avg) / avg);
      return normalized;
   }

   /**
    * Scales the input vector to deviation from the minimal value from this vector
    *
    * @param inputs list of doubles
    * @return scaled list
    */
   public static List<Double> scaleToDeviationFromMinValue(List<Double> inputs) {
      List<Double> normalized = new ArrayList<>();
      Double min = Double.MAX_VALUE;
      Double max = Double.MIN_VALUE;
      for (Double x : inputs) {
         if (x < min)
            min = x;
         if (x > max)
            max = x;
      }
      for (Double x : inputs)
         normalized.add((x - min) / (max - min));
      return normalized;
   }

   /**
    * Scales the input vector to maximal value from this vector
    *
    * @param inputs list of doubles
    * @return scaled list
    */
   public static List<Double> scaleToMaxValue(List<Double> inputs) {
      List<Double> normalized = new ArrayList<>();
      Double max = findMaxValue(inputs);
      for (Double x : inputs)
         normalized.add(x / max);
      return normalized;
   }

   /**
    * Normalizes the input vector to unit vector
    *
    * @param inputs list of doubles
    * @return scaled list
    */
   public static List<Double> normalizeToUnitVector(List<Double> inputs) {
      List<Double> normalized = new ArrayList<>();
      Double sqrSum = 0.0;
      for (Double x : inputs)
         sqrSum += x * x;
      Double length = Math.sqrt(sqrSum);

      for (Double x : inputs)
         normalized.add(x / length);
      return normalized;
   }

   /**
    * Sets values from the list between given range.
    * EXAMPLE:
    * If the given numbers are 0.0 and 1.0 and this numbers
    * are from range [0.0 - 1.0] and the new range is
    * [0.0 - 2.0] that the output values will be: 0.0 and 2.0.
    * If the actual range of 0.0 and 1.0 is [-1.0 - 1.0]
    * and the new range is [0.0 - 1.0] that the output values
    * will be 0.5 and 1.0.
    *
    * @param values            to scale
    * @param from              lower bound of range
    * @param to                upper bound of range
    * @param previousScaleFrom actual lower bound of vector
    * @param previousScaleTo   actual upper bound of vector
    * @return scaled vector
    */
   public static List<Double> setValuesBetween(List<Double> values, double from, double to, double previousScaleFrom, double previousScaleTo) {
      List<Double> normalized = new ArrayList<>();
      for (int i = 0; i < values.size(); i++) {
         normalized.add(setValueBetween(values.get(i), from, to, previousScaleFrom, previousScaleTo));
      }
      return normalized;
   }

   /**
    * Sets value between given range.
    * EXAMPLE:
    * If the given number is 1.0 and this number
    * are from range [0.0 - 1.0] and the new range is
    * [0.0 - 2.0] that the output value will be 2.0.
    * If the actual range of 1.0 is [0.0 - 2.0]
    * and the new range is [0.0 - 1.0] that the output value
    * will be 0.5.
    *
    * @param value             to scale
    * @param from              lower bound of range
    * @param to                upper bound of range
    * @param previousScaleFrom actual lower bound of value
    * @param previousScaleTo   actual upper bound of value
    * @return scaled value
    */
   public static Double setValueBetween(Double value, double from, double to, double previousScaleFrom, double previousScaleTo) {
      return (from * previousScaleTo - from * previousScaleFrom + (to - from) * (value - previousScaleFrom))
         / (previousScaleTo - previousScaleFrom);
   }

   /**
    * Sets the input vector values between zero and one.
    * EXAMPLE:
    * If the input vector is 1.5, 2.0 and it is from range
    * [1.0 - 2.0] that the output values will be 0.5, 1.0.
    * If the range is [1.5, 2.0] that the output values will be
    * 0.0, 1.0.
    *
    * @param values            to scale
    * @param previousScaleFrom actual lower bound of vector
    * @param previousScaleTo   actual upper bound of vector
    * @return scaled vector
    */
   public static List<Double> setValuesBetweenZeroAndOne(List<Double> values, double previousScaleFrom, double previousScaleTo) {
      return setValuesBetween(values, 0.0, 1.0, previousScaleFrom, previousScaleTo);
   }

   /**
    * Sets the input value between zero and one.
    * EXAMPLE:
    * If the input value is 1.0, and comes from range
    * [0.0 - 2.0] that the output value will be 0.5.
    * If this number comes from [1.0 - 2.0] that the
    * output value will be 0.0.
    *
    * @param value             to scale
    * @param previousScaleFrom actual lower bound of value
    * @param previousScaleTo   actual upper bound of value
    * @return scaled value
    */
   public static Double setValueBetweenZeroAndOne(Double value, double previousScaleFrom, double previousScaleTo) {
      return setValueBetween(value, 0.0, 1.0, previousScaleFrom, previousScaleTo);
   }

   /**
    * Finds max value in the vector
    *
    * @param vector to search
    * @return max value
    */
   public static Double findMaxValue(List<Double> vector) {
      double max = Double.MIN_VALUE;
      for (Double x : vector) {
         if (x > max)
            max = x;
      }
      return max;
   }
}
