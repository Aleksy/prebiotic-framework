package aleksy.abspec.prebiotic.framework.common.model;

/**
 * Range class defines simple one-dimension segment with two numbers
 */
public class Range {
   /**
    * Lower bound
    *
    * @deprecated since 2.0.0. This field will be not editable in the future.
    * To read the value please use the getter.
    */
   @Deprecated
   public double from;
   /**
    * Upper bound
    *
    * @deprecated since 2.0.0. This field will be not editable in the future.
    * To read the value please use the getter.
    */
   @Deprecated
   public double to;

   /**
    * Constructor
    *
    * @param from value
    * @param to   value
    * @deprecated since 2.0.0. Please use Range.from(x, y) instead.
    */
   @Deprecated
   public Range(double from, double to) {
      this.from = from;
      this.to = to;
   }

   /**
    * Getter for 'from' value
    *
    * @return lower bound
    */
   public double getFrom() {
      return from;
   }

   /**
    * Getter for 'to' value
    *
    * @return upper bound
    */
   public double getTo() {
      return to;
   }

   /**
    * Creates new instance of {@link Range}
    *
    * @param from lower bound
    * @param to   upper bound
    * @return new instance of Range
    */
   public static Range from(double from, double to) {
      return new Range(from, to);
   }
}
