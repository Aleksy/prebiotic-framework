package aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GenotypeBuilderTest {
   private GenotypeBuilder uut;

   @Before
   public void init() {
      uut = new GenotypeBuilder();
   }

   @Test
   public void shouldCorrectlyBuildTheGenotype() {
      // when
      Genotype result = uut.addChromosome("chr1")
         .addGene(0.0, Range.from(0.0, 1.0), "gene1")
         .end()
         .addChromosome("chr2")
         .addGene(1.0, Range.from(0.5, 1.0), "gene2")
         .addGene(0.5, Range.from(0.1, 0.7), "gene3")
         .end()
         .create();

      // then
      assertEquals(2, result.size());
      assertEquals(1, result.getChromosomes().get(0).size());
      assertEquals(2, result.getChromosomes().get(1).size());
      assertEquals(new Double(0.0), new Double(result.getChromosomes().get(0).getGenes().get(0).getValue()));
      assertEquals(new Double(1.0), new Double(result.getChromosomes().get(1).getGenes().get(0).getValue()));
      assertEquals(new Double(0.5), new Double(result.getChromosomes().get(1).getGenes().get(1).getValue()));
      assertEquals("gene1", result.getChromosomes().get(0).getGenes().get(0).getDescription());
      assertEquals("gene2", result.getChromosomes().get(1).getGenes().get(0).getDescription());
      assertEquals("gene3", result.getChromosomes().get(1).getGenes().get(1).getDescription());
      assertEquals(new Double(0.0),
         new Double(result.getChromosomes().get(0).getGenes().get(0).getValueRange().from));
      assertEquals(new Double(1.0),
         new Double(result.getChromosomes().get(0).getGenes().get(0).getValueRange().to));
      assertEquals(new Double(0.5),
         new Double(result.getChromosomes().get(1).getGenes().get(0).getValueRange().from));
      assertEquals(new Double(1.0),
         new Double(result.getChromosomes().get(1).getGenes().get(0).getValueRange().to));
      assertEquals(new Double(0.1),
         new Double(result.getChromosomes().get(1).getGenes().get(1).getValueRange().from));
      assertEquals(new Double(0.7),
         new Double(result.getChromosomes().get(1).getGenes().get(1).getValueRange().to));
   }
}
