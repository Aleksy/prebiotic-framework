package aleksy.abspec.prebiotic.framework.evolution.genetic.common.util;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Protista;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BrainUtilTest {

   private class TestProtista extends Protista implements Geneticable {

      TestProtista(Genotype genotype) {
         super(genotype);
      }

      @Override
      public Geneticable newInstance(Genotype genotype) {
         return new TestProtista(genotype);
      }
   }


   @Test
   public void shouldSetBrainsToPopulation() throws EmptyFunctionDefinitionException, InvalidStructureException {
      // given
      List<Geneticable> geneticables = new ArrayList<>();
      geneticables.add(new TestProtista(testGenotype()));

      // when
      geneticables = BrainUtil.setBrainsToPopulation(geneticables, testBrain());

      // then
      Assert.assertEquals(3, geneticables.get(0).getGenotype().size());
      Assert.assertEquals(4, geneticables.get(0).getGenotype().getChromosomes().get(2).size());
      assertNotNull(((Protista) geneticables.get(0)).getBrain());

   }

   private Genotype testGenotype() throws EmptyFunctionDefinitionException, InvalidStructureException {
      Genotype genotype = Prebiotic.newInstance().evolutionApi().getGenotypeBuilder()
         .addChromosome("chr1")
         .addGene(0.0, Range.from(0.0, 1.0), "gene1")
         .end()
         .addChromosome("chr2")
         .addGene(1.0, Range.from(0.5, 1.0), "gene2")
         .addGene(0.5, Range.from(0.1, 0.7), "gene3")
         .end()
         .create();
      Genotype genotypeOfBrain = GenotypeUtil.createGenotypeOfBrain(testBrain());
      return GenotypeUtil.combineToOneGenotype(genotype, genotypeOfBrain);
   }

   private Brain testBrain() throws EmptyFunctionDefinitionException, InvalidStructureException {
      List<NeuralNetwork> nets = new ArrayList<>();
      nets.add(Prebiotic.newInstance().neuralApi().getNeuralNetworkBuilder(
         Prebiotic.newInstance().neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .create()
      ).addDefaultInputLayer(2, 1).endBuildingNetwork().create());
      Brain brain = new Brain();
      brain.setNeuralNetworks(nets);
      return brain;
   }
}