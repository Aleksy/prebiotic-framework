package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.enumerate.FileFormat;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.model.PrebioticModelObject;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.logic.Parser;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder.CustomFunctionsForParserBuilder;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.api.ParsingApi;

import java.io.IOException;

/**
 * Parsing submodule api implementation
 */
public class Parsing implements ParsingApi {
   private Parser parser;

   public Parsing(PrebioticApi prebiotic) {
      parser = new Parser(prebiotic);
   }

   @Override
   public void save(PrebioticModelObject prebioticModelObject, String filepath, FileFormat fileFormat) throws IOException {
      parser.save(prebioticModelObject, filepath, fileFormat);
   }

   @Override
   public PrebioticModelObject read(String filepath) throws Exception {
      return parser.read(filepath);
   }

   @Override
   public NeuralNetwork read(String filepath, CustomFunctionsForParser customFunctionsForParser) throws Exception {
      return parser.read(filepath, customFunctionsForParser);
   }

   @Override
   public CustomFunctionsForParserBuilder getCustomFunctionsForParserBuilder() {
      return new CustomFunctionsForParserBuilder();
   }
}
