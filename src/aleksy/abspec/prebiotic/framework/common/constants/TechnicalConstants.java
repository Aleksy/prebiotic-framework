package aleksy.abspec.prebiotic.framework.common.constants;

/**
 * Technical constants values
 */
public class TechnicalConstants {
   /**
    * Bias default value for neurons used in framework
    */
   public static final Double BIAS = 1.0;
}
