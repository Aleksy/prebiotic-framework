package aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.abstr.AbstractAlgorithmConfigurations;

/**
 * Deep learning configurations class
 */
public class DeepLearningConfigurations extends AbstractAlgorithmConfigurations {
   /**
    * Learning rate defines rapidity of learning of neural network. Basically this value is 0.1
    */
   public Double learningRate;

   /**
    * Momentum value
    */
   public Double momentum;
   /**
    * Random values range
    */
   public Range randomValuesRange;
   /**
    * Reduced learning rate
    */
   public Double reducedLearningRate;
   /**
    * Speed of reducing the learning rate
    */
   public Double speedOfReducing;
}
