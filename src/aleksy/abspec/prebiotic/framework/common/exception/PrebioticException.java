package aleksy.abspec.prebiotic.framework.common.exception;

/**
 * General exception thrown from Prebiotic Framework
 */
public class PrebioticException extends Exception {
   /**
    * Constructor for exception with message
    *
    * @param message to log
    */
   public PrebioticException(String message) {
      super("\nPrebiotic Framework Exception. " + message);
   }
}
