package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual;

import aleksy.abspec.prebiotic.framework.evolution.genetic.common.util.GenotypeUtil;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

/**
 * Protista class. Abstract implementation of individual with brain
 */
public abstract class Protista extends Individual {

   protected Brain brain;

   /**
    * Constructor
    *
    * @param genotype of protista
    * @param brain    of protista
    */
   public Protista(Genotype genotype, Brain brain) {
      super(GenotypeUtil.combineToOneGenotype(genotype, GenotypeUtil.createGenotypeOfBrain(brain)));
      this.brain = brain;
   }

   /**
    * Constructor
    *
    * @param genotype of protista
    */
   public Protista(Genotype genotype) {
      super(genotype);
   }

   /**
    * Getter for brain
    *
    * @return brain
    */
   public Brain getBrain() {
      return brain;
   }

   /**
    * Setter for brain
    *
    * @param brain to set
    */
   public void setBrain(Brain brain) {
      this.brain = brain;
   }
}
