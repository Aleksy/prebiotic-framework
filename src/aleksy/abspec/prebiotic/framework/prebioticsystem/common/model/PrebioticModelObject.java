package aleksy.abspec.prebiotic.framework.prebioticsystem.common.model;

import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.logic.Parser;

/**
 * Interface of all objects which can be saved to file and parsed by
 * {@link Parser}
 */
public interface PrebioticModelObject {
}
