package aleksy.abspec.prebiotic.framework.prebioticsystem.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.constants.BasicConstants;
import aleksy.abspec.prebiotic.framework.common.enumerate.FileFormat;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Chromosome;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Gene;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.model.PrebioticModelObject;
import aleksy.abspec.prebiotic.framework.prebioticsystem.common.api.PrebioticSystemApi;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder.CustomFunctionsForParserBuilder;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.api.ParsingApi;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.impl.Parsing;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Prebiotic System module api basic implementation
 */
public class PrebioticSystem implements PrebioticSystemApi {
   private final String USER_MESSAGES_SENDER = "USER";

   private PrebioticApi prebiotic;
   private ParsingApi parsingApi;

   public PrebioticSystem(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
      parsingApi = new Parsing(prebiotic);
   }

   @Override
   public void log(String message) {
      prebiotic.getPrintStreams().forEach(stream -> stream.println(prefix(USER_MESSAGES_SENDER) + message));
   }

   @Override
   public void log(String message, String sender) {
      if (prebiotic.isEventLoggingEnabled())
         prebiotic.getPrintStreams().forEach(stream -> stream.println(prefix(sender) + message));
   }

   @Override
   public void log(Exception e) {
      if (prebiotic.isEventLoggingEnabled()) {
         prebiotic.getPrintStreams().forEach(stream -> stream.println(prefix("ERROR") + "An exception handled!"));
         e.printStackTrace();
      }
   }

   @Override
   public void separator() {
      if (prebiotic.isEventLoggingEnabled())
         prebiotic.getPrintStreams().forEach(stream -> stream.println("-----------------------------------------------" +
            "---------------------------------------------"));
   }

   @Override
   public void log(NeuralNetwork network, String sender) {
      if (prebiotic.isEventLoggingEnabled()) {
         logNeuralNetwork(network, sender);
      }
   }

   @Override
   public void log(NeuralNetwork network) {
      logNeuralNetwork(network, USER_MESSAGES_SENDER);
   }

   @Override
   public void log(Genotype genotype, String sender) {
      if (prebiotic.isEventLoggingEnabled()) {
         logGenotype(genotype, sender);
      }
   }

   @Override
   public void log(Genotype genotype) {
      logGenotype(genotype, USER_MESSAGES_SENDER);
   }

   @Override
   public void logWarn(String message, String sender) {
      if (prebiotic.isEventLoggingEnabled())
         prebiotic.getPrintStreams().forEach(stream -> stream.println(warnPrefix(sender) + message));
   }

   @Override
   public void logo() {
      prebiotic.getPrintStreams().forEach(stream -> {
         stream.printf("%c", 0x2554);
         for (int i = 0; i < 27; i++)
            stream.printf("%c", 0x2550);
         stream.printf("%c", 0x2566);
         for (int i = 0; i < 24; i++)
            stream.printf("%c", 0x2550);
         stream.printf("%c", 0x2557);
         stream.println();
         stream.printf("%c", 0x2551);

         stream.print(" " + BasicConstants.NAME + " " + BasicConstants.VERSION + " ");
         stream.printf("%c", 0x2551);
         stream.print(" " + BasicConstants.AUTHOR + " ");
         stream.printf("%c", 0x2551);
         stream.println();
         stream.printf("%c", 0x255A);
         for (int i = 0; i < 27; i++)
            stream.printf("%c", 0x2550);
         stream.printf("%c", 0x2569);
         for (int i = 0; i < 24; i++)
            stream.printf("%c", 0x2550);
         stream.printf("%c", 0x255D);
         stream.println();
      });
   }

   @Override
   public CustomFunctionsForParserBuilder getCustomFunctionsForParserBuilder() {
      return parsingApi.getCustomFunctionsForParserBuilder();
   }

   @Override
   public void save(PrebioticModelObject prebioticModelObject, String filepath, FileFormat fileFormat) throws IOException {
      parsingApi.save(prebioticModelObject, filepath, fileFormat);
   }

   @Override
   public PrebioticModelObject read(String filepath) throws Exception {
      return parsingApi.read(filepath);
   }

   @Override
   public NeuralNetwork read(String filepath, CustomFunctionsForParser customFunctionsForParser) throws Exception {
      return parsingApi.read(filepath, customFunctionsForParser);
   }


   private String prefix(String sender) {
      StringBuilder prefix = new StringBuilder();
      prefix.append(LocalDateTime.now()).append(" prebiotic-framework [").append(sender).append("]");
      for (int i = prefix.length(); i < 70; i++)
         prefix.append(" ");
      return prefix.toString();
   }

   private String warnPrefix(String sender) {
      StringBuilder prefix = new StringBuilder();
      prefix.append(LocalDateTime.now()).append(" prebiotic-framework [").append(sender).append("]");
      for (int i = prefix.length(); i < 70; i++)
         prefix.append("!");
      return prefix.toString();
   }

   private void logNeuralNetwork(NeuralNetwork network, String sender) {
      log("Neural network was created.", sender);
      separator();
      log("Description: \"" + network.getDescription() + "\"", sender);
      log("  Network type: " + network.getType(), sender);
      log("  Number of layers: " + network.numberOfLayers(), sender);
      log("  Number of neurons: " + network.size(), sender);
      log("  Bias presence: " + network.isBiasPresence(), sender);
      log("  Data normalization: " + network.getDataNormalization(), sender);
      log("Neuron model:", sender);
      log("  Weight model: " + network.getWeightFunctionRecipe(), sender);
      log("  Core model: " + network.getCoreFunction().getCoreFunctionRecipe(), sender);
      log("  Axon model: " + network.getAxonFunction().getAxonFunctionRecipe(), sender);
      separator();
   }

   private void logGenotype(Genotype genotype, String sender) {
      log("Genotype - chromosomes: " + genotype.size(), sender);
      for (Chromosome chromosome : genotype.getChromosomes()) {
         log("  Chromosome \"" + chromosome.getDescription() + "\" - genes: " + chromosome.size(), sender);
         for (Gene gene : chromosome.getGenes()) {
            log(gene.toString(), sender);
         }
      }
   }
}
