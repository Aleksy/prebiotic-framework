package aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.builder;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.AxonFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.CoreFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.WeightFunctions;
import aleksy.abspec.prebiotic.framework.prebioticsystem.parsing.common.model.CustomFunctionsForParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CustomFunctionsForParserBuilderTest {
   private CustomFunctionsForParserBuilder uut;

   @Before
   public void init() {
      uut = new CustomFunctionsForParserBuilder();
   }

   @Test
   public void shouldBuildCorrectUnit() {
      // when
      CustomFunctionsForParser customFunctionsForParser = uut.setAxonFunction(new AxonFunctions.TANH())
         .setCoreFunction(new CoreFunctions.DEFAULT_ADDER())
         .setWeightFunction(new WeightFunctions.DEFAULT_LINEAR(false))
         .create();

      // then
      Assert.assertEquals(AxonFunctions.TANH.class, customFunctionsForParser.axonFunction.getClass());
      Assert.assertEquals(CoreFunctions.DEFAULT_ADDER.class, customFunctionsForParser.coreFunction.getClass());
      Assert.assertEquals(WeightFunctions.DEFAULT_LINEAR.class, customFunctionsForParser.weightFunction.getClass());
   }
}
