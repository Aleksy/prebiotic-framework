package aleksy.abspec.prebiotic.framework.evolution.genetic.builder.config;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GeneticAlgorithmConfigurationsBuilderTest {
   private GeneticAlgorithmConfigurationsBuilder uut;

   @Before
   public void init() {
      uut = new GeneticAlgorithmConfigurationsBuilder(Prebiotic.newInstance());
   }

   @Test
   public void shouldCorrectlyBuildTheConfigurations() {
      // given
      double mutationChance = 0.1;
      int generations = 100;
      GeneticSelectionMode geneticSelectionMode = GeneticSelectionMode.ROULETTE;
      int iterationsDisplaying = 10;
      int delay = 2;

      // when
      GeneticAlgorithmConfigurations config = uut.setMutationChance(mutationChance)
         .setAmountOfGenerations(generations)
         .setSelectionMode(geneticSelectionMode)
         .setIterationsDisplaying(iterationsDisplaying)
         .setDelay(delay)
         .create();

      // then
      assertEquals(new Double(mutationChance), new Double(config.mutationChance));
      assertEquals(generations, config.generations);
      Assert.assertEquals(geneticSelectionMode, config.geneticSelectionMode);
      Assert.assertEquals(iterationsDisplaying, config.iterationsDisplaying.intValue());
      Assert.assertEquals(delay, config.delay.intValue());
   }

   @Test
   public void shouldCreateDefaultConfigurations() {
      // given
      int generations = 100;

      // when
      GeneticAlgorithmConfigurations actualGac = uut.defaultConfigurations(generations).create();

      // then
      Assert.assertEquals(GeneticSelectionMode.ROULETTE, actualGac.geneticSelectionMode);
      assertEquals(new Double(0.01), new Double(actualGac.mutationChance));
      assertEquals(generations, actualGac.generations);
      assertEquals(100, (int) actualGac.iterationsDisplaying);
      assertEquals(0, (int) actualGac.delay);
   }

   @Test
   public void shouldSetDelayToZeroWhenNotSet() {
      // when
      GeneticAlgorithmConfigurations config = uut.create();

      // then
      Assert.assertEquals(0, config.delay.intValue());
   }
}
