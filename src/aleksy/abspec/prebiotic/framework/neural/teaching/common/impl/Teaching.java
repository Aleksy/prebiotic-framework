package aleksy.abspec.prebiotic.framework.neural.teaching.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.neural.teaching.logic.DeepLearningLogic;
import aleksy.abspec.prebiotic.framework.neural.teaching.logic.HopfieldLearningLogic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.KohonenLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.api.TeachingApi;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teaching.logic.KohonenLearningLogic;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;

/**
 * Implementation of {@link TeachingApi}
 */
public class Teaching implements TeachingApi {
   private PrebioticApi prebiotic;

   public Teaching(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   @Override
   public NeuralNetwork deepLearning(TeachingData teachingData, NeuralNetwork network,
                                     DeepLearningConfigurations deepLearningConfigurations)
      throws NeuralNetworkException, DeepLearningException {
      return new DeepLearningLogic(prebiotic).start(teachingData, network, deepLearningConfigurations);
   }

   @Override
   public NeuralNetwork kohonenLearning(TeachingData teachingData, NeuralNetwork network, KohonenLearningConfigurations kohonenLearningConfigurations) throws KohonenLearningException, IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException {
      return new KohonenLearningLogic(prebiotic).start(teachingData, network, kohonenLearningConfigurations);
   }

   @Override
   public KohonenLearningConfigurationsBuilder getKohonenLearningConfigurationsBuilder() {
      return new KohonenLearningConfigurationsBuilder(prebiotic);
   }

   @Override
   public DeepLearningConfigurationsBuilder getDeepLearningConfigurationsBuilder() {
      return new DeepLearningConfigurationsBuilder(prebiotic);
   }

   @Override
   public NeuralNetwork hopfieldLearning(TeachingData teachingData, NeuralNetwork network) throws HopfieldLearningException {
      return new HopfieldLearningLogic(prebiotic).start(teachingData, network);
   }
}
