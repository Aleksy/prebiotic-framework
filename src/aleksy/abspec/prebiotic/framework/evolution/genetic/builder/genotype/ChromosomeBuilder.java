package aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Chromosome;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Gene;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

/**
 * Chromosome builder
 */
public class ChromosomeBuilder {
   private Chromosome chromosome;
   private Genotype genotype;
   private GenotypeBuilder genotypeBuilder;

   /**
    * Constructor
    *
    * @param gntb     genotype builder
    * @param genotype to build
    */
   public ChromosomeBuilder(GenotypeBuilder gntb, Genotype genotype) {
      chromosome = new Chromosome();
      this.genotype = genotype;
      this.genotypeBuilder = gntb;
   }

   /**
    * Constructor
    *
    * @param gntb        genotype builder
    * @param genotype    to build
    * @param description to set in chromosome
    */
   public ChromosomeBuilder(GenotypeBuilder gntb, Genotype genotype, String description) {
      chromosome = new Chromosome(description);
      this.genotype = genotype;
      this.genotypeBuilder = gntb;
   }

   /**
    * Adds new gene to the chromosome
    *
    * @param value       to set in gene
    * @param valueRange  possible values of gene (used by mutation function)
    * @param description of gene
    * @return builder
    */
   public ChromosomeBuilder addGene(Double value, Range valueRange, String description) {
      chromosome.getGenes().add(new Gene(value, valueRange, description));
      return this;
   }

   /**
    * Ends creating of new chromosome
    *
    * @return genotype builder
    */
   public GenotypeBuilder end() {
      genotype.getChromosomes().add(chromosome);
      return genotypeBuilder;
   }
}
