package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.WeightFunctionRecipe;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Abstract neuron
 */
public abstract class AbstractNeuron implements Neuron {

   protected List<WeightFunction> weights;
   protected CoreFunction core;
   protected AxonFunction axon;
   protected boolean biasPresence;
   protected WeightFunctionRecipe weightFunctionRecipe;
   protected Double defect;
   protected Double actualOutputFromCore;
   protected List<Double> actualInputs;
   protected Double actualAxonOutput;

   public AbstractNeuron(List<WeightFunction> weights, CoreFunction core, AxonFunction axon, boolean biasPresence) {
      this.weights = weights;
      this.core = core;
      this.axon = axon;
      this.weightFunctionRecipe = weights.get(0).getWeightFunctionRecipe();
      this.biasPresence = biasPresence;
   }

   @Override
   public Double f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException {
      if (xs.size() != weights.size())
         throw new IncorrectNumberOfInputsInNeuronException("expected: " + weights.size()
            + ", was: " + xs.size());
      List<Double> weightsOut = new ArrayList<>();
      for (int i = 0; i < xs.size(); i++) {
         weightsOut.add(weights.get(i).f(xs.get(i)));
      }
      actualInputs = new ArrayList<>(xs);
      actualOutputFromCore = core.f(weightsOut);
      actualAxonOutput = axon.f(actualOutputFromCore);
      return actualAxonOutput;
   }

   @Override
   public int size() {
      return weights.size();
   }

   @Override
   public String toString() {
      return "neuron: " + weights.toString();
   }

   @Override
   public void randomizeWeights(Double from, Double to) {
      Random r = new Random();
      for (WeightFunction w : weights) {
         w.randomizeFactors(from, to);
      }
   }

   @Override
   public List<WeightFunction> cloneWeights() {
      List<WeightFunction> newWeights = new ArrayList<>();
      for (WeightFunction w : weights) {
         WeightFunction newWeight = w.getWeightFunctionRecipe().newInstance(false);
         newWeights.add(newWeight);
         newWeight.setFactors(new ArrayList<>(w.getFactors()));
      }
      return newWeights;
   }

   @Override
   public CoreFunction getCoreFunction() {
      return core;
   }

   @Override
   public AxonFunction getAxonFunction() {
      return axon;
   }

   @Override
   public boolean isBiasPresence() {
      return biasPresence;
   }

   @Override
   public void setBiasPresence(boolean biasPresence) {
      this.biasPresence = biasPresence;
   }

   @Override
   public void addBiasWeight() {
      weights.add(weights.get(0).getWeightFunctionRecipe().newInstance(false));
   }

   @Override
   public void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe) {
      this.weightFunctionRecipe = weightFunctionRecipe;
   }

   @Override
   public WeightFunctionRecipe getWeightFunctionRecipe() {
      return weightFunctionRecipe;
   }

   @Override
   public void setDefect(Double defect) {
      this.defect = defect;
   }

   @Override
   public Double getDefect() {
      return defect;
   }

   @Override
   public List<WeightFunction> getWeights() {
      return weights;
   }

   @Override
   public Double getActualOutputFromCore() {
      return actualOutputFromCore;
   }

   @Override
   public void setActualInputs(List<Double> actualInputs) {
      this.actualInputs = actualInputs;
   }

   @Override
   public List<Double> getActualInputs() {
      return actualInputs;
   }

   @Override
   public void setActualAxonOutput(Double actualAxonOutput) {
      this.actualAxonOutput = actualAxonOutput;
   }

   @Override
   public Double getActualAxonOutput() {
      return actualAxonOutput;
   }

   @Override
   public List<Double> getVectorOfWeights() {
      List<Double> vector = new ArrayList<>();
      for (WeightFunction w : weights)
         vector.addAll(w.getFactors());
      return vector;
   }
}
