package aleksy.abspec.prebiotic.framework.common.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VectorNormalizatorTest {
   private List<Double> vector;

   @Before
   public void init() {
      vector = new ArrayList<>(Arrays.asList(1.0, 2.0, 3.0, 4.0));
   }

   @Test
   public void shouldScaleToAverage() {
      // given
      double avg = 0.0;
      for(Double v : vector) {
         avg += v;
      }
      avg /= vector.size();
      List<Double> expectedVector = new ArrayList<>(Arrays.asList(
         vector.get(0) / avg,
         vector.get(1) / avg,
         vector.get(2) / avg,
         vector.get(3) / avg));

      // when
      List<Double> actualVector = VectorNormalizator.scaleToAverage(vector);

      // then
      assertEquals(expectedVector.get(0), actualVector.get(0));
      assertEquals(expectedVector.get(1), actualVector.get(1));
      assertEquals(expectedVector.get(2), actualVector.get(2));
      assertEquals(expectedVector.get(3), actualVector.get(3));
   }

   @Test
   public void shouldScaleToDeviationFromAverage() {
      // given
      double avg = 0.0;
      for(Double v : vector) {
         avg += v;
      }
      avg /= vector.size();
      List<Double> expectedVector = new ArrayList<>(Arrays.asList(
         (vector.get(0) - avg) / avg,
         (vector.get(1) - avg) / avg,
         (vector.get(2) - avg) / avg,
         (vector.get(3) - avg) / avg));

      // when
      List<Double> actualVector = VectorNormalizator.scaleToDeviationFromAverage(vector);

      // then
      assertEquals(expectedVector.get(0), actualVector.get(0));
      assertEquals(expectedVector.get(1), actualVector.get(1));
      assertEquals(expectedVector.get(2), actualVector.get(2));
      assertEquals(expectedVector.get(3), actualVector.get(3));
   }

   @Test
   public void shouldScaleToDeviationFromMinValue() {
      // given
      double min = Double.MAX_VALUE;
      double max = Double.MIN_VALUE;

      for(Double x : vector) {
         if (x < min)
            min = x;
         if (x > max)
            max = x;
      }
      List<Double> expectedVector = new ArrayList<>(Arrays.asList(
         (vector.get(0) - min) / (max - min),
         (vector.get(1) - min) / (max - min),
         (vector.get(2) - min) / (max - min),
         (vector.get(3) - min) / (max - min)));

      // when
      List<Double> actualVector = VectorNormalizator.scaleToDeviationFromMinValue(vector);

      // then
      assertEquals(expectedVector.get(0), actualVector.get(0));
      assertEquals(expectedVector.get(1), actualVector.get(1));
      assertEquals(expectedVector.get(2), actualVector.get(2));
      assertEquals(expectedVector.get(3), actualVector.get(3));
   }

   @Test
   public void shouldScaleToMaxValue() {
      // given
      double max = Double.MIN_VALUE;

      for(Double x : vector) {
         if (x > max)
            max = x;
      }
      List<Double> expectedVector = new ArrayList<>(Arrays.asList(
         (vector.get(0)) / max,
         (vector.get(1)) / max,
         (vector.get(2)) / max,
         (vector.get(3)) / max));

      // when
      List<Double> actualVector = VectorNormalizator.scaleToMaxValue(vector);

      // then
      assertEquals(expectedVector.get(0), actualVector.get(0));
      assertEquals(expectedVector.get(1), actualVector.get(1));
      assertEquals(expectedVector.get(2), actualVector.get(2));
      assertEquals(expectedVector.get(3), actualVector.get(3));
   }

   @Test
   public void shouldNormalizeToUnitVector() {
      // given
      double sqrt = 0.0;
      for(Double x : vector) {
         sqrt += x * x;
      }
      sqrt = Math.sqrt(sqrt);
      List<Double> expectedVector = new ArrayList<>(Arrays.asList(
         (vector.get(0)) / sqrt,
         (vector.get(1)) / sqrt,
         (vector.get(2)) / sqrt,
         (vector.get(3)) / sqrt));

      // when
      List<Double> actualVector = VectorNormalizator.normalizeToUnitVector(vector);

      // then
      assertEquals(expectedVector.get(0), actualVector.get(0));
      assertEquals(expectedVector.get(1), actualVector.get(1));
      assertEquals(expectedVector.get(2), actualVector.get(2));
      assertEquals(expectedVector.get(3), actualVector.get(3));
   }

   @Test
   public void shouldSetValueBetween() {
      // given
      double from = 3.0;
      double to = 4.0;
      double value = 0.5;
      Double expected = 3.5;

      // when
      Double actual = VectorNormalizator.setValueBetween(value, from, to, 0.0, 1.0);

      // then
      assertEquals(expected, actual);
   }

   @Test
   public void shouldSetValueBetweenZeroAndOne() {
      // given
      double from = 3.0;
      double to = 5.0;
      double value = 4.0;
      Double expected = 0.5;

      // when
      Double actual = VectorNormalizator.setValueBetweenZeroAndOne(value, from, to);

      // then
      assertEquals(expected, actual);
   }

   @Test
   public void shouldFindMaxValue() {
      // when
      Double max = VectorNormalizator.findMaxValue(vector);

      // then
      assertEquals(new Double(4.0), max);
   }
}
