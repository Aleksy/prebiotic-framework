package aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix;

import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractWeightMatrixNeuralPanel;

import java.awt.*;
import java.util.Collections;
import java.util.List;

public class KohonenWeightMatrixNeuralPanel extends AbstractWeightMatrixNeuralPanel {
   private KohonenNeuralNetwork knn;

   public KohonenWeightMatrixNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
      knn = null;
      try {
         knn = Prebiotic.newInstance().neuralApi().convertToKohonenNeuralNetwork(nn);
      } catch (InvalidStructureException e) {
         e.printStackTrace();
      }
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(createColor(0.0));
      Neuron inputNeuron = nn.getInputLayer().getNeurons().get(0);
      List<Double> actualInputs = inputNeuron.getActualInputs();
      if (actualInputs != null) actualInputs = VectorNormalizator
         .setValuesBetweenZeroAndOne(actualInputs, inputRange.getFrom(), inputRange.getTo());
      drawInputLayer(actualInputs, g);
   }

   private void drawInputLayer(List<Double> inputs, Graphics g) {
      if (inputs != null) {
         int inputsSize = inputs.size();
         int yOffset = (int) (getSize().height * 0.85) / 8;
         int xStep = (int) ((double) getSize().width / (double) (inputsSize + 1));
         for (int i = 0; i < inputsSize; i++) {
            g.setColor(createDarkerColor(inputs.get(i)));
            drawKohonenSynapsesFromInput(knn, xStep, yOffset, i, g);
         }
         int actOutIndex = 0;
         for (int x = 0; x < knn.getXSize(); x++) {
            for (int y = 0; y < knn.getYSize(); y++) {
               int xNeuronStep = (int) ((double) getSize().width / (double) (knn.getXSize()));
               int yNeuronStep = (int) ((double) getSize().height / (double) (knn.getYSize() + 1));
               int xNeuron = (int) (xNeuronStep / 2.0) + x * xNeuronStep + 3;
               int yNeuron = (int) ((yNeuronStep + y * yNeuronStep) * 0.85) + yOffset;
               Double axon = VectorNormalizator.scaleToMaxValue(knn.getActualOutput()).get(actOutIndex++);
               drawWeightMatrixNeuron(
                  VectorNormalizator.scaleToDeviationFromMinValue(knn.getNeuron(x, y).getVectorOfWeights()),
                  axon, xNeuron, yNeuron, g);
            }
         }
         for (int i = 0; i < inputsSize; i++) {
            drawWeightMatrixNeuron(Collections.singletonList(inputs.get(i)), inputs.get(i),
               xStep + i * xStep, yOffset, g);
         }
      }
   }
}
