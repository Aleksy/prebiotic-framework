package aleksy.abspec.prebiotic.framework.vision.display.panel.abstr;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

import java.awt.*;

/**
 * Abstract panel to draw weight of matrix of each neuron
 */
public abstract class AbstractWeightMatrixNeuralPanel extends AbstractNeuralPanel {
   public AbstractWeightMatrixNeuralPanel(NeuralNetwork nn, Double hue, Range inpRange) {
      super(nn, hue, inpRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(Color.GRAY);
      g.setFont(font);
      g.drawString("weight matrix of each neuron:", 5, getSize().height / 20);
   }
}
