package aleksy.abspec.prebiotic.framework.vision.display.panel.weightmatrix;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.common.constants.VisionConstants;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractWeightMatrixNeuralPanel;

import java.awt.*;

public class FeedforwardWeightMatrixNeuralPanel extends AbstractWeightMatrixNeuralPanel {
   public FeedforwardWeightMatrixNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(createColor(0.0));
      Range axonRange;
      axonRange = nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues();
      int inputVectorSize = nn.getInputLayer().getNeurons().get(0).size();
      if (nn.isBiasPresence())
         inputVectorSize -= 1;
      Neuron inputNeuron = nn.getInputLayer().getNeurons().get(0);
      java.util.List<Double> actualInputs = inputNeuron.getActualInputs();
      if (actualInputs != null) actualInputs = VectorNormalizator
         .setValuesBetweenZeroAndOne(actualInputs, inputRange.getFrom(), inputRange.getTo());
      g.setColor(createColor(0.0));
      feedforwardSkeleton(inputVectorSize, actualInputs, axonRange, DisplayMode.WEIGHT_MATRIX, g);
      for (int l = 0; l < nn.numberOfLayers(); l++) {
         NeuralLayer layer = nn.getLayers().get(l);
         for (int n = 0; n < layer.size(); n++) {
            Neuron neuron = layer.getNeurons().get(n);
            double axonNormalized = 0.0;
            if (neuron.getActualAxonOutput() != null) {
               axonNormalized = VectorNormalizator.setValueBetweenZeroAndOne(neuron.getActualAxonOutput(),
                  axonRange.getFrom(), axonRange.getTo());
               g.setColor(createColor(axonNormalized));
            } else
               g.setColor(createColor(0.0));
            drawNeuron(neuron, l, n, nn.numberOfLayers(), layer.size(), axonNormalized, g);
         }
      }
      setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
   }

   private void drawNeuron(Neuron neuron, int layerNumber, int neuronNumber, int allLayersNumber, int allNeuronsNumber, double output, Graphics g) {
      int xStep = (int) ((this.getSize().width) / ((double) allNeuronsNumber + 1));
      int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
      int yOffset = generateOffset(yStep);
      int xNeuron = xStep + neuronNumber * xStep;
      int yNeuron = yStep + layerNumber * yStep;
      drawWeightMatrixNeuron(VectorNormalizator.scaleToDeviationFromMinValue(neuron.getVectorOfWeights()),
         output, xNeuron, yNeuron + yOffset, g);
   }

   private int generateOffset(int yStep) {
      return yStep / 3;
   }
}
