package aleksy.abspec.prebiotic.framework.neural.neuralstructure.common.model.network;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class KohonenNeuralNetworkTest {
   private KohonenNeuralNetwork uut;

   @Before
   public void init() throws EmptyFunctionDefinitionException, InvalidStructureException {
      PrebioticApi p = Prebiotic.newInstance();
      uut = (KohonenNeuralNetwork) p.neuralApi().getNeuralNetworkBuilder(
         p.neuralApi().getStructureConfigurationsBuilder()
            .defaultKohonenConfigurations()
            .create()
      ).buildKohonenMap(2, 2, 1)
         .create();
   }

   @Test
   public void shouldReturnCorrectVectorOfWeightsOfNeuron() {
      // given
      uut.getAllNeurons().get(2).getWeights().get(0).getFactors().set(0, 0.3012);

      // when
      List<Double> vector = uut.getVectorOfWeightsOfNeuron(1, 0);

      // then
      assertEquals(new Double(0.3012), vector.get(0));
   }
}
