package aleksy.abspec.prebiotic.framework.neural.teaching.common.exception;

import aleksy.abspec.prebiotic.framework.common.exception.PrebioticException;

/**
 * Deep learning exception
 */
public class DeepLearningException extends PrebioticException {
   public DeepLearningException(String message) {
      super("\nDeep learning exception: " + message);
   }
}
