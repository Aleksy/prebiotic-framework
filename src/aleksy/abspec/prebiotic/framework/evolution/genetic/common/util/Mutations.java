package aleksy.abspec.prebiotic.framework.evolution.genetic.common.util;

import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Gene;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

import java.util.List;
import java.util.Random;

/**
 * Mutations functions util class
 */
public class Mutations {

   /**
    * Default mutation method. This function selects random parts of chromosomes
    * and creates random changes of this parts.
    *
    * @param genotype       to mutate
    * @param mutationChance to check if mutation can appear
    * @return mutated (or not changed) genotype
    */
   public static Genotype def(Genotype genotype, double mutationChance) {
      Random r = new Random();
      if (r.nextDouble() < mutationChance) {
         List<Gene> genes = genotype.getAllGenes();
         int toChange = r.nextInt(genes.size());
         while (toChange > 0) {
            genes.get(r.nextInt(genes.size())).mutate();
            toChange--;
         }
      }
      return genotype;
   }
}
