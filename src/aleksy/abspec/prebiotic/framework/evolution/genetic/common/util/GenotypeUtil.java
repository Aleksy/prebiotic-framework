package aleksy.abspec.prebiotic.framework.evolution.genetic.common.util;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.NeuralNetworkType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.ChromosomeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.genotype.Genotype;

import java.util.ArrayList;
import java.util.List;

/**
 * Util class with useful method for {@link Genotype}
 */
public class GenotypeUtil {
   /**
    * Creates one genotype from two another genotypes
    *
    * @param g1 first genotype
    * @param g2 second genotype
    * @return combined one genotype
    */
   public static Genotype combineToOneGenotype(Genotype g1, Genotype g2) {
      Genotype g1Copy = new Genotype(g1);
      Genotype g2Copy = new Genotype(g2);
      g1Copy.getChromosomes().addAll(g2Copy.getChromosomes());
      return g1Copy;
   }

   /**
    * Creates one genotype from list of genotypes
    *
    * @param genotypes list
    * @return one genotype
    */
   public static Genotype combineToOneGenotype(List<Genotype> genotypes) {
      List<Genotype> copies = new ArrayList<>();
      for (Genotype genotype : genotypes) {
         copies.add(new Genotype(genotype));
      }
      Genotype one = new Genotype();
      for (Genotype genotype : copies) {
         one.getChromosomes().addAll(genotype.getChromosomes());
      }
      return one;
   }

   /**
    * Creates genotype of neural network. Each layer of network is different chromosome.
    * Chromosomes have description like this:
    * [additionalDescriptionOfChromosome]nnlayer[X] where X is a number of layer
    *
    * @param nn                                neural network
    * @param additionalDescriptionOfChromosome to set in chromosomes, before 'nn_layerX'
    * @return genotype with neurons weight values
    */
   public static Genotype createGenotypeOfNetwork(NeuralNetwork nn, String additionalDescriptionOfChromosome) {
      GenotypeBuilder gb = new GenotypeBuilder();
      int layerCounter = 0;
      for (NeuralLayer layer : nn.getLayers()) {
         ChromosomeBuilder chb = gb.addChromosome(additionalDescriptionOfChromosome + "nn_layer" + layerCounter++);
         int factorCounter = 0;
         for (Neuron neuron : layer.getNeurons()) {
            for (WeightFunction weight : neuron.getWeights()) {
               for (Double factor : weight.getFactors()) {
                  chb.addGene(factor, nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues(), "w" + factorCounter++);
               }
            }
         }
         chb.end();
      }
      return gb.create();
   }

   /**
    * Creates genotype of brain. Each network from brain has code of it's type:
    * - fed for FEEDFORWARD
    * - koh for KOHONEN
    * - hop for HOPFIELD
    * <p>
    * + _nmb[X] where X is the layer number
    *
    * @param brain to create the genotype
    * @return genotype from brain
    */
   public static Genotype createGenotypeOfBrain(Brain brain) {
      List<Genotype> parts = new ArrayList<>();
      Integer networkNumber = 0;
      for (NeuralNetwork nn : brain.getNeuralNetworks()) {
         String code = "";
         if (nn.getType() == NeuralNetworkType.KOHONEN)
            code = "koh";
         if (nn.getType() == NeuralNetworkType.FEEDFORWARD)
            code = "fed";
         if (nn.getType() == NeuralNetworkType.HOPFIELD)
            code = "hop";
         parts.add(createGenotypeOfNetwork(nn, code + "_nmb" + networkNumber.toString()));
         networkNumber++;
      }
      return combineToOneGenotype(parts);
   }
}
