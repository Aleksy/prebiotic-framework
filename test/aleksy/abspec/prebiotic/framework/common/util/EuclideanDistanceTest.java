package aleksy.abspec.prebiotic.framework.common.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class EuclideanDistanceTest {
   @Test
   public void shouldReturnCorrectDistance() {
      // given
      Double expected = 4.0;
      List<Double> from = new ArrayList<>();
      List<Double> to = new ArrayList<>();
      from.add(-2.0);
      from.add(0.0);
      to.add(2.0);
      to.add(0.0);

      // when
      Double actual = EuclideanDistance.between(from, to);

      // then
      assertTrue(expected.equals(actual));
   }
}
