package aleksy.abspec.prebiotic.framework.evolution.genetic.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.evolution.genetic.logic.GeneticLogic;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.config.GeneticAlgorithmConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.api.GeneticApi;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.exception.GeneticAlgorithmException;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.brain.Brain;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.environment.Environment;
import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;

import java.util.List;

/**
 * Genetic submodule api basic implementation
 */
public class Genetic implements GeneticApi {
   private PrebioticApi prebiotic;

   public Genetic(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   @Override
   public GenotypeBuilder getGenotypeBuilder() {
      return new GenotypeBuilder();
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, Environment environment, GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmException {
      return new GeneticLogic(prebiotic).start(population, environment, configurations);
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, Environment environment, GeneticAlgorithmConfigurations configurations, Brain templateBrain) throws GeneticAlgorithmException {
      return new GeneticLogic(prebiotic).start(population, environment, configurations, templateBrain);
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmException {
      return new GeneticLogic(prebiotic).start(population, configurations);
   }

   @Override
   public List<Geneticable> geneticAlgorithm(List<Geneticable> population, GeneticAlgorithmConfigurations configurations, Brain templateBrain) throws GeneticAlgorithmException {
      return new GeneticLogic(prebiotic).start(population, configurations, templateBrain);
   }

   @Override
   public GeneticAlgorithmConfigurationsBuilder getGeneticAlgorithmConfigurationsBuilder() {
      return new GeneticAlgorithmConfigurationsBuilder(prebiotic);
   }
}

