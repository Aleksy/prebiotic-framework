package aleksy.abspec.prebiotic.framework.neural.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.neural.common.api.NeuralApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.NeuralNetworkBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config.NeuralNetworkStructureConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.api.NeuralStructureApi;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.impl.NeuralStructure;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.KohonenNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.Converter;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.builder.KohonenLearningConfigurationsBuilder;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.api.TeachingApi;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.HopfieldLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.KohonenLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.impl.Teaching;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.KohonenLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.builder.TeachingDataBuilder;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.api.TeachingDataManagementApi;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.impl.TeachingDataManagement;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;

/**
 * Neural api basic implementation
 */
public class Neural implements NeuralApi {
   private TeachingDataManagementApi teachingData;
   private NeuralStructureApi neuralStructure;
   private TeachingApi teaching;

   public Neural(PrebioticApi prebiotic) {
      neuralStructure = new NeuralStructure(prebiotic);
      teaching = new Teaching(prebiotic);
      teachingData = new TeachingDataManagement(prebiotic);
   }

   @Override
   public NeuralNetworkStructureConfigurationsBuilder getStructureConfigurationsBuilder() {
      return neuralStructure.getStructureConfigurationsBuilder();
   }

   @Override
   public NeuralNetworkBuilder getNeuralNetworkBuilder(StructureConfigurations structureConfigurations) throws EmptyFunctionDefinitionException {
      return neuralStructure.getNeuralNetworkBuilder(structureConfigurations);
   }

   @Override
   public TeachingDataBuilder getTeachingDataBuilder() {
      return teachingData.getTeachingDataBuilder();
   }

   @Override
   public KohonenLearningConfigurationsBuilder getKohonenLearningConfigurationsBuilder() {
      return teaching.getKohonenLearningConfigurationsBuilder();
   }

   @Override
   public DeepLearningConfigurationsBuilder getDeepLearningConfigurationsBuilder() {
      return teaching.getDeepLearningConfigurationsBuilder();
   }

   @Override
   public NeuralNetwork deepLearning(TeachingData teachingData, NeuralNetwork network, DeepLearningConfigurations configurations) throws NeuralNetworkException, DeepLearningException {
      return teaching.deepLearning(teachingData, network, configurations);
   }

   @Override
   public NeuralNetwork kohonenLearning(TeachingData teachingData, NeuralNetwork network, KohonenLearningConfigurations configurations) throws KohonenLearningException, IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException {
      return teaching.kohonenLearning(teachingData, network, configurations);
   }

   @Override
   public NeuralNetwork hopfieldLearning(TeachingData teachingData, NeuralNetwork network) throws HopfieldLearningException {
      return teaching.hopfieldLearning(teachingData, network);
   }

   @Override
   public KohonenNeuralNetwork convertToKohonenNeuralNetwork(NeuralNetwork neuralNetworkInterface) throws InvalidStructureException {
      return Converter.convertToKohonenNeuralNetwork(neuralNetworkInterface);
   }
}
