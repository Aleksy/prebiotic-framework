package aleksy.abspec.prebiotic.framework.neural.neuralstructure.common.model.neuron;

import aleksy.abspec.prebiotic.framework.common.util.ListCreator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.impl.NeuronImpl;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.AxonFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.CoreFunctions;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.util.WeightFunctions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NeuronTest {
   private Neuron uut;

   @Before
   public void init() {
      List<WeightFunction> weights = new ArrayList<>();
      weights.add(new WeightFunctions.DEFAULT_LINEAR(false));
      weights.add(new WeightFunctions.DEFAULT_LINEAR(false));
      weights.add(new WeightFunctions.DEFAULT_LINEAR(false));

      for (WeightFunction w : weights)
         w.getFactors().set(0, 0.5);
      uut = new NeuronImpl(weights, new CoreFunctions.DEFAULT_ADDER(), new AxonFunctions.LINEAR(), false);
   }

   @Test
   public void shouldMakeCalculations() throws IncorrectNumberOfInputsInNeuronException {
      // when
      Double out = uut.f(ListCreator.createFrom(1.0, 2.0, 1.0));

      // then
      //      assertEquals(new Double(2.0), out);
   }

   @Test
   public void shouldCloneWeights() {
      // when
      List<WeightFunction> clonedWeights = uut.cloneWeights();

      // then
      assertEquals(new Double(0.5), clonedWeights.get(0).getFactors().get(0));
      assertEquals(new Double(0.5), clonedWeights.get(1).getFactors().get(0));
      assertEquals(new Double(0.5), clonedWeights.get(2).getFactors().get(0));
      assertFalse(clonedWeights.equals(uut.getWeights()));
   }

   @Test
   public void shouldAddBias() {
      // when
      uut.addBiasWeight();

      // then
      assertEquals(4, uut.size());
   }

   @Test
   public void shouldReturnVectorOfWeights() {
      // when
      List<Double> vectorOfWeights = uut.getVectorOfWeights();

      // then
      assertEquals(new Double(0.5), vectorOfWeights.get(0));
      assertEquals(new Double(0.5), vectorOfWeights.get(0));
      assertEquals(new Double(0.5), vectorOfWeights.get(0));
   }

}
