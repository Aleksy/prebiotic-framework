package aleksy.abspec.prebiotic.framework.vision.display.panel.structure;

import aleksy.abspec.prebiotic.framework.common.model.Range;
import aleksy.abspec.prebiotic.framework.common.util.VectorNormalizator;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.layer.NeuralLayer;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.neuron.Neuron;
import aleksy.abspec.prebiotic.framework.vision.common.enumerate.DisplayMode;
import aleksy.abspec.prebiotic.framework.vision.display.panel.abstr.AbstractStructureNeuralPanel;

import java.awt.*;

public class FeedforwardStructureSquareNeuralPanel extends AbstractStructureNeuralPanel {

   public FeedforwardStructureSquareNeuralPanel(NeuralNetwork nn, Double hue, Range inputRange) {
      super(nn, hue, inputRange);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setColor(createColor(0.0));
      Range axonRange;
      axonRange = nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues();
      int inputVectorSize = nn.getInputLayer().getNeurons().get(0).size();
      if (nn.isBiasPresence())
         inputVectorSize -= 1;
      Neuron inputNeuron = nn.getInputLayer().getNeurons().get(0);
      java.util.List<Double> inputs = inputNeuron.getActualInputs();
      if (inputs != null) inputs = VectorNormalizator
         .setValuesBetweenZeroAndOne(inputs, inputRange.getFrom(), inputRange.getTo());
      g.setColor(createColor(0.0));
      feedforwardSkeleton(inputVectorSize, inputs, axonRange, DisplayMode.STRUCTURE_SQUARE, g);
      if (nn.numberOfLayers() != 1)
         for (int l = nn.numberOfLayers() - 2; l < nn.numberOfLayers(); l++) {
            NeuralLayer layer = nn.getLayers().get(l);
            for (int n = 0; n < layer.size(); n++) {
               Neuron neuron = layer.getNeurons().get(n);
               if (neuron.getActualAxonOutput() != null) {
                  double axonNormalized = VectorNormalizator.setValueBetweenZeroAndOne(neuron.getActualAxonOutput(),
                     axonRange.getFrom(), axonRange.getTo());
                  g.setColor(createBitDarkerColor(axonNormalized));
               } else
                  g.setColor(createBitDarkerColor(0.0));
               if (l != nn.numberOfLayers() - 1) {
                  drawOutputLayer(l, n, nn.numberOfLayers(), layer.size(), g);
               }
            }
         }
      else
         for (int i = 0; i < nn.sizeOfInputVector(); i++) {
            if (nn.getActualInputVector().get(i) != null) {
               double axonNormalized = VectorNormalizator.setValueBetweenZeroAndOne(nn.getActualInputVector().get(i),
                  inputRange.getFrom(), inputRange.getTo());
               g.setColor(createBitDarkerColor(axonNormalized));
            } else
               g.setColor(createBitDarkerColor(0.0));
            drawOutputLayer(-1, i, nn.numberOfLayers(), nn.sizeOfInputVector(), g);
         }
      for (int l = 0; l < nn.numberOfLayers() - 1; l++) {
         NeuralLayer layer = nn.getLayers().get(l);
         for (int n = 0; n < layer.size(); n++) {
            Neuron neuron = layer.getNeurons().get(n);
            double axonNormalized;
            if (neuron.getActualAxonOutput() != null) {
               axonNormalized = VectorNormalizator.setValueBetweenZeroAndOne(neuron.getActualAxonOutput(),
                  axonRange.getFrom(), axonRange.getTo());
               g.setColor(createColor(axonNormalized));
            } else
               g.setColor(createColor(0.0));
            if (neuron.getActualAxonOutput() != null)
               drawNeuron(l, n, nn.numberOfLayers(), layer.size(), neuron.getActualAxonOutput(), g);
            else
               drawNeuron(l, n, nn.numberOfLayers(), layer.size(), 1.0, g);
         }
      }
   }

   private void drawNeuron(int layerNumber, int neuronNumber, int allLayersNumber, int allNeuronsNumber, double output, Graphics g) {
      int xStep = (int) ((this.getSize().width) / ((double) allNeuronsNumber + 1));
      int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
      int yOffset = generateOffset(yStep);
      int xNeuron = xStep + neuronNumber * xStep;
      int yNeuron = yStep + layerNumber * yStep;
      drawNeuron(xNeuron, yNeuron + yOffset, g);
   }

   private void drawOutputLayer(int layerNumber, int neuronNumber,
                                int allLayersNumber, int allNeuronsNumber, Graphics g) {
      int allNeuronsInOutLayer = nn.getOutputLayer().size();
      int square = (int) Math.ceil(Math.sqrt(allNeuronsInOutLayer));
      for (int i = 0; i < allNeuronsInOutLayer; i++) {
         for (int x = 0; x < square; x++) {
            for (int y = 0; y < square; y++) {
               int xStep = (int) ((this.getSize().width) / ((double) allNeuronsNumber + 1));
               int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
               int xFrom = xStep + neuronNumber * xStep;
               int yFrom = yStep + layerNumber * yStep;
               if (allLayersNumber == 1) {
                  yFrom = yStep / 3;
               }
               int yTo = yStep + (layerNumber + 1) * yStep + y * 6;
               int xOffset = (getSize().width / 2) - (square * 6 / 2);
               g.setColor(g.getColor().darker());
               drawSynapse(xFrom, yFrom + generateOffset(yStep),
                  x * 6 + xOffset, yTo + generateOffset(yStep), g);
               g.setColor(g.getColor().brighter());
            }
         }
      }
      int cnt = 0;
      java.util.List<Neuron> neurons = nn.getOutputLayer().getNeurons();
      Range axonRange = nn.getAxonFunction().getAxonFunctionRecipe().getRangeOfPossibleValues();
      for (int x = 0; x < square; x++) {
         for (int y = 0; y < square; y++) {
            int yStep = (int) (((double) this.getSize().height) / ((double) allLayersNumber + 1));
            int yTo = yStep + (layerNumber + 1) * yStep + y * 6;
            int xOffset = (getSize().width / 2) - (square * 6 / 2);
            if (cnt < allNeuronsInOutLayer)
               if (neurons.get(cnt).getActualAxonOutput() != null) {
                  drawLittleNeuron(x * 6 + xOffset, yTo + generateOffset(yStep),
                     VectorNormalizator.setValueBetweenZeroAndOne(neurons.get(cnt++).getActualAxonOutput(),
                        axonRange.getFrom(),
                        axonRange.getTo()), g);
               }
         }
      }
   }

   private int generateOffset(int yStep) {
      return yStep / 3;
   }

}
