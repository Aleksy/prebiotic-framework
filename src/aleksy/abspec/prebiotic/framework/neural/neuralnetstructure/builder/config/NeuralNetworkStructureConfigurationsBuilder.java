package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.builder.config;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.logic.Loggable;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.*;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.config.StructureConfigurations;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.AxonFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.CoreFunction;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.function.WeightFunction;

/**
 * Neural network structure configurations builder
 */
public class NeuralNetworkStructureConfigurationsBuilder extends Loggable {
   private StructureConfigurations sc;
   private PrebioticApi prebiotic;

   /**
    * Constructor
    *
    * @param prebiotic api
    */
   public NeuralNetworkStructureConfigurationsBuilder(PrebioticApi prebiotic) {
      sc = new StructureConfigurations();
      this.prebiotic = prebiotic;
   }

   /**
    * Setter for weight function for neural network structure configuration
    *
    * @param weightFunctionRecipe to define
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder setWeightFunction(WeightFunctionRecipe weightFunctionRecipe) {
      sc.weightFunctionRecipe = weightFunctionRecipe;
      return this;
   }

   /**
    * Setter for core function for neural network structure configuration
    *
    * @param coreFunctionRecipe to define
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder setCoreFunction(CoreFunctionRecipe coreFunctionRecipe) {
      sc.coreFunctionRecipe = coreFunctionRecipe;
      return this;
   }

   /**
    * Setter for axon function for neural network structure configuration
    *
    * @param axonFunctionRecipe to define
    * @return builder
    */
   @SuppressWarnings(value = "hallo")
   public NeuralNetworkStructureConfigurationsBuilder setAxonFunction(AxonFunctionRecipe axonFunctionRecipe) {
      sc.axonFunctionRecipe = axonFunctionRecipe;
      return this;
   }

   /**
    * Setter for neural network type
    *
    * @param neuralNetworkType to define
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder setNeuralNetworkType(NeuralNetworkType neuralNetworkType) {
      sc.neuralNetworkType = neuralNetworkType;
      return this;
   }

   /**
    * Setter for bias presence
    *
    * @param biasPresence to set
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder setBiasPresence(boolean biasPresence) {
      sc.biasPresence = biasPresence;
      return this;
   }

   /**
    * Default configurations for FEEDFORWARD neural network. Weight function is set abspec DEFAULT_LINEAR,
    * axon function abspec SIGMOID, bias is presented, which means that network will have additional weight
    * for each neuron, data normalization type is NORMALIZATION_TO_UNIT_VECTOR.
    *
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder defaultFeedforwardConfigurations() {
      sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      sc.axonFunctionRecipe = AxonFunctionRecipe.SIGMOID;
      sc.biasPresence = true;
      sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
      prebiotic.prebioticSystemApi().log("Default structure configurations for feedforward network has been built.",
         getSenderName());
      return this;
   }

   /**
    * Default configurations for KOHONEN neural network. Weight function is set abspec SQUARE_DIFFERENCE,
    * axon function abspec HYPERBOLIC, bias is not presented, data normalization type is NO_NORMALIZATION.
    *
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder defaultKohonenConfigurations() {
      sc.weightFunctionRecipe = WeightFunctionRecipe.SQUARE_DIFFERENCE;
      sc.axonFunctionRecipe = AxonFunctionRecipe.HYPERBOLIC;
      sc.biasPresence = false;
      sc.dataNormalization = DataNormalizationType.NO_NORMALIZATION;
      sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      sc.neuralNetworkType = NeuralNetworkType.KOHONEN;
      prebiotic.prebioticSystemApi().log("Default structure configurations for Kohonen's network has been built.",
         getSenderName());
      return this;
   }

   /**
    * Default configurations for HOPFIELD neural network. Weight function is set abspec DEFAULT_LINEAR,
    * axon function abspec HOPFIELD_THRESHOLD, bias is not presented, data normalization type is
    * NORMALIZATION_TO_UNIT_VECTOR.
    *
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder defaultHopfieldConfigurations() {
      sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
      sc.axonFunctionRecipe = AxonFunctionRecipe.HOPFIELD_THRESHOLD;
      sc.biasPresence = false;
      sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
      sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
      sc.neuralNetworkType = NeuralNetworkType.HOPFIELD;
      prebiotic.prebioticSystemApi().log("Default structure configurations for Hopfield's network has been built.",
         getSenderName());
      return this;
   }

   /**
    * Setter for data normalization. Use it when you would to scale neural network
    * input data to average of input data values.
    *
    * @param dataNormalization to set
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder setDataNormalization(DataNormalizationType dataNormalization) {
      sc.dataNormalization = dataNormalization;
      return this;
   }

   /**
    * Defines axon instance in configurations. Should be used when axon recipe is set to AXON_CUSTOM
    *
    * @param axon custom instance
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder defineAxonInstance(AxonFunction axon) {
      sc.axonInstance = axon;
      return this;
   }

   /**
    * Defines core instance in configurations. Should be used when core recipe is set to CORE_CUSTOM
    *
    * @param core custom instance
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder defineCoreInstance(CoreFunction core) {
      sc.coreInstance = core;
      return this;
   }

   /**
    * Defines weight instance in configurations. Should be used when weight recipe is set to WEIGHT_CUSTOM
    *
    * @param weight custom instance
    * @return builder
    */
   public NeuralNetworkStructureConfigurationsBuilder defineWeightInstance(WeightFunction weight) {
      sc.weightInstance = weight;
      return this;
   }

   /**
    * Builds a configurations object
    *
    * @return structure configurations with parameters set in builder
    */
   public StructureConfigurations create() {
      return sc;
   }

   @Override
   protected String getSenderName() {
      return "STRUCT-CONFIG-BLD";
   }
}
