package aleksy.abspec.prebiotic.framework.neural.teachingdata.common.impl;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.builder.TeachingDataBuilder;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.api.TeachingDataManagementApi;

/**
 * Teaching data management submodule api basic implementation
 */
public class TeachingDataManagement implements TeachingDataManagementApi {
   private PrebioticApi prebiotic;

   public TeachingDataManagement(PrebioticApi prebiotic) {
      this.prebiotic = prebiotic;
   }

   @Override
   public TeachingDataBuilder getTeachingDataBuilder() {
      return new TeachingDataBuilder(prebiotic);
   }
}
