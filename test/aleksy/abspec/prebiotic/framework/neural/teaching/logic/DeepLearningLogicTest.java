package aleksy.abspec.prebiotic.framework.neural.teaching.logic;

import aleksy.abspec.prebiotic.framework.common.api.PrebioticApi;
import aleksy.abspec.prebiotic.framework.common.impl.Prebiotic;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.exception.abstr.NeuralNetworkException;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningConfigurationsException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.exception.DeepLearningException;
import aleksy.abspec.prebiotic.framework.neural.teaching.common.model.config.DeepLearningConfigurations;
import aleksy.abspec.prebiotic.framework.neural.teachingdata.common.model.TeachingData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeepLearningLogicTest {
   private DeepLearningLogic uut;
   private PrebioticApi prebiotic;

   @Before
   public void init() {
      prebiotic = Prebiotic.newInstance();
      uut = new DeepLearningLogic(prebiotic);
   }

   @Test(expected = DeepLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenIterationsAreLessThanZero() throws DeepLearningException, NeuralNetworkException {
      // given
      DeepLearningConfigurations config = prepareTestConfig();
      config.iterations = -1L;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = DeepLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenLearningRateAreInvalid() throws DeepLearningException, NeuralNetworkException {
      // given
      DeepLearningConfigurations config = prepareTestConfig();
      config.learningRate = -0.2;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = DeepLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenReducedRateIsSetButSpeedOfRatingNo() throws DeepLearningException, NeuralNetworkException {
      // given
      DeepLearningConfigurations config = prepareTestConfig();
      config.reducedLearningRate = 0.0001;
      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   @Test(expected = DeepLearningConfigurationsException.class)
   public void shouldThrowExceptionWhenConfigsNotIncludeAnyEndingParams() throws DeepLearningException, NeuralNetworkException {
      // given
      DeepLearningConfigurations config = prepareTestConfig();
      config.iterations = null;
      config.minimalDefect = null;

      // when
      uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
      // then exception
   }

   private TeachingData prepareTestTeachingData() {
      List<Double> inputs = new ArrayList<>(Collections.singletonList(1.0));
      List<Double> outputs = new ArrayList<>(Collections.singletonList(0.0));
      return prebiotic.neuralApi().getTeachingDataBuilder()
         .addDataPackage(inputs, outputs)
         .create();
   }

   private DeepLearningConfigurations prepareTestConfig() {
      return prebiotic.neuralApi().getDeepLearningConfigurationsBuilder()
         .setLearningRate(1.0)
         .setIterations(1).create();
   }

   private NeuralNetwork prepareTestNeuralNetwork() {
      NeuralNetwork testNetwork = null;
      try {
         testNetwork = prebiotic.neuralApi().getNeuralNetworkBuilder(
            prebiotic.neuralApi().getStructureConfigurationsBuilder()
               .defaultFeedforwardConfigurations()
               .setBiasPresence(false)
               .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
               .create()
         ).addDefaultInputLayer(1, 1)
            .endBuildingNetwork()
            .create();
      } catch (InvalidStructureException | EmptyFunctionDefinitionException e) {
         e.printStackTrace();
      }
      return testNetwork;
   }
}
