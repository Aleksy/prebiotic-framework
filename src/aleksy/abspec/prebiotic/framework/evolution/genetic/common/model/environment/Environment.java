package aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.environment;

import aleksy.abspec.prebiotic.framework.evolution.genetic.common.model.individual.Geneticable;

import java.util.Collection;

/**
 * Abstract environment class
 */
public abstract class Environment {
   /**
    * Rates an individual by setting the fitness value
    *
    * @param geneticable to rate
    */
   public abstract void rate(Geneticable geneticable);

   /**
    * Rates collection of individuals by setting the fitness value for all of then
    *
    * @param geneticables to rate
    */
   public void rate(Collection<Geneticable> geneticables) {
      for (Geneticable individual : geneticables) {
         rate(individual);
      }
   }
}
