package aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.impl;

import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.AbstractNeuralNetwork;
import aleksy.abspec.prebiotic.framework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

/**
 * Basic implementation of neural network
 */
public class NeuralNetworkImpl extends AbstractNeuralNetwork implements NeuralNetwork {
}
